# muslim

## Discussion Module

##### What is Fixed

1. On Discussion Screen Show All Discussions of all categories order by created_at and (Top tab bar "All" button will active.)
2. On Discussion Screen scroll down to refresh the data (loading spinner will active for few second)
3. On Discussion screen scroll up to load more data
4. On Discussion screen top tab button active and show the discussions
5. OnPress Add button will come the action sheet dialog box from the bottom.
6. Action Sheet dialog box outside touch will dismiss the dialog box.
7. Action Sheet dialog box swipe down to dismiss the dialog box.
8. Action Sheet dialog box onPress category will open form dialog box. adn Action sheet dialog box will dismiss.
9. Form dialog box will ask native photo album permission.
10. Form dialog box header will show close icon, title (new [category name]) and post button.
11. OnPress close icon will dismiss the dialog box.
12. Discussion and comment report work.
13. Discussion and comment Delete button only show if your and login and you're owner of the item.

##### What is need to Fixed

1. delete function has some bug.

##### What is need to add.

1. after any exaction need notify (show some notification message to user)
