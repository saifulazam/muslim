import Reactotron from 'reactotron-react-native'
import AsyncStorage from '@react-native-community/async-storage'

Reactotron.setAsyncStorageHandler(AsyncStorage)
    .configure({
        name: 'Alnur debugger',
        host: '192.168.1.11',
    })
    .useReactNative({
        asyncStorage: false,
        networking: {
            ignoreUrls: /symbolicate|127.0.0.1/,
        },
        editor: false,
        errors: { veto: (stackFrame) => false },
        overlay: false,
    })
    .connect()
