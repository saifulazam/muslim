import React from 'react'
import RootNavigation from './src/navigation'
import { Provider } from 'react-redux'
import store from './src/store'
import { authCheck } from './src/modules/Auth/redux/AuthAction'
import { AppLoading } from 'expo'
import * as Font from 'expo-font'
import { getStorage } from './src/utils/Storage'
import { FontAwesome } from '@expo/vector-icons'
import { StackNavigationProp } from '@react-navigation/stack'
import { checkIsFirstTimeHere } from './src/modules/Welcome/redux/WelcomeAction'
import Root from './src/lib/Toast/Root'
import { Image } from 'react-native'
import { Asset } from 'expo-asset'

let customFonts = {
    'SpaceMono-Regular': require('./assets/fonts/SpaceMono-Regular.ttf'),
    'AvantGardeLT-Book': require('./assets/fonts/ITC-Avant-Garde-Gothic-LT-Book.ttf'),
    'AvantGardeLT-Demi': require('./assets/fonts/ITC-Avant-Garde-Gothic-LT-Bold.ttf'),
    'MaisonNeue-Book': require('./assets/fonts/MaisonNeue-Book.ttf'),
    'MaisonNeue-Medium': require('./assets/fonts/MaisonNeue-Medium.ttf'),
}

interface Props {
    navigation: StackNavigationProp<any>
}

function cacheFonts(fonts) {
    return fonts.map((font) => Font.loadAsync(font))
}

function cacheImages(images) {
    return images.map((image) => {
        if (typeof image === 'string') {
            return Image.prefetch(image)
        } else {
            return Asset.fromModule(image).downloadAsync()
        }
    })
}

export default class App extends React.Component<Props> {
    state = {
        isReady: false,
    }

    async _loadAssetsAsync() {
        const imagesAssets = cacheImages([
            require('./assets/images/slides/image1.png'),
            require('./assets/images/slides/image2.png'),
            require('./assets/images/slides/image3.png'),
        ])
        const fontAssets = cacheFonts([FontAwesome.font, customFonts])
        await Promise.all([...fontAssets, ...imagesAssets])
    }

    async _loadFontsAsync() {
        const fontAssets = cacheFonts([FontAwesome.font, customFonts])
        await Promise.all([...fontAssets])
    }

    async componentDidMount() {
        this._loadFontsAsync()

        const token = await getStorage('access_token')
        const isFirstTimeHere = await getStorage('isFirstTimeHere')

        if (isFirstTimeHere) {
            store.dispatch(checkIsFirstTimeHere())
        }
        if (token) {
            store.dispatch(authCheck(token))
        }
    }

    render() {
        if (!this.state.isReady) {
            return (
                <AppLoading
                    startAsync={this._loadAssetsAsync}
                    onFinish={() => this.setState({ isReady: true })}
                    onError={console.warn}
                />
            )
        }
        return (
            <Provider store={store}>
                <Root>
                    <RootNavigation />
                </Root>
            </Provider>
        )
    }
}
