import React, { useRef } from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator, StackNavigationOptions, TransitionPresets } from '@react-navigation/stack'
import RootTab from './RootTab'
import AuthStack from '../modules/Auth/navigation/AuthStack'
import QiblaDirectionScreen from '../modules/Qibla/Screens/QiblaDirectionScreen'
import SetLocationScreen from '../modules/Location/Screens/setLocationScreen'
import MosqueDonationScreen from '../modules/Donation/Screens/MosqueDonationScreen'
import WelcomeStack from '../modules/Welcome/navigation/WelcomeStack'
import { connect } from 'react-redux'
import { getAuthUser } from '../modules/Auth/services/AuthService'
import ThreadDetailsScreen from '../modules/Forum/Screens/ThreadDetailsScreen'
import PostFromScreen from '../modules/Forum/Screens/PostFromScreen'
import { getStorage } from '../utils/Storage'

export interface Props {
    isFirstTimeHere: boolean
    getAuthUser?: any
    user?: any
}

const RootStack = createStackNavigator()

const index = (props: Props) => {
    const routeNameRef: React.MutableRefObject<undefined> = useRef()
    const navigationRef: React.MutableRefObject<undefined> = useRef()

    const navigationOptions: StackNavigationOptions = {
        headerShown: false,
        gestureEnabled: false,
        cardStyle: {
            // backgroundColor: 'transparent',
            // opacity: 0.99,
        },
    }

    React.useEffect(() => {
        const getToken = async (): Promise<string | boolean> => {
            const token = await getStorage('access_token')
            return token
        }
        const checkToken = async () => {
            if (await getToken()) {
                if (!props.user) {
                    props.getAuthUser()
                }
            }
        }
        checkToken()
    }, [])

    return (
        <NavigationContainer
            //@ts-ignore
            onReady={() => (routeNameRef.current = navigationRef.current.getCurrentRoute().name)}
            ref={navigationRef}
            onStateChange={() => {
                const previousRouteName = routeNameRef.current
                // @ts-ignore
                const currentRouteName = navigationRef.current.getCurrentRoute().name

                if (previousRouteName !== currentRouteName) {
                    console.log(`Navigated from ${previousRouteName} to ${currentRouteName}`)
                }
                routeNameRef.current = currentRouteName
            }}
        >
            <RootStack.Navigator
                initialRouteName={props.isFirstTimeHere ? 'Welcome' : 'InitialRoute'}
                screenOptions={navigationOptions}
                mode="modal"
            >
                <RootStack.Screen name="InitialRoute" component={AuthStack} />

                <RootStack.Screen name="Welcome" component={WelcomeStack} />

                <RootStack.Screen name="Discover" component={RootTab} />
                <RootStack.Screen name="DiscussionRoute" component={RootTab} />
                <RootStack.Screen name="MosqueRoute" component={RootTab} />
                <RootStack.Screen name="UserRoute" component={RootTab} />

                <RootStack.Screen name="QiblaDirection" component={QiblaDirectionScreen} />
                <RootStack.Screen name="SetLocation" component={SetLocationScreen} />
                <RootStack.Screen name="MosqueDonation" component={MosqueDonationScreen} />
                <RootStack.Screen name="ThreadDetails" component={ThreadDetailsScreen} />
                <RootStack.Screen name="ThreadFormScreen" component={PostFromScreen} />
            </RootStack.Navigator>
        </NavigationContainer>
    )
}

const mapStateToProps = (state) => {
    return {
        isFirstTimeHere: state.welcome.isFirstTimeHere,
        user: state.auth.user,
    }
}

export default connect(mapStateToProps, { getAuthUser })(index)
