import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import React from 'react'
import Icon from '../common/Icon'
import DiscoverStack from '../modules/Discover/navigation/DiscoverStack'
import DiscoverScreen from '../modules/Discover/Screens/DiscoverScreen'
import DiscussionStack from '../modules/Discussion/navigation/DiscussionStack'
import ForumStack from '../modules/Forum/navigation/ForumStack'
import MosqueStack from '../modules/Mosque/navigation/MosqueStack'
import UserStack, { UserStackParamList } from '../modules/User/navigation/UserStack'
import { _ROUTE_LABEL_NAME } from '../utils/constants'

export type rootStackParamList = {
    Discover: undefined
    DiscussionRoute: undefined
    MosqueRoute: undefined
    UserRoute: undefined
    ForumStack: undefined
}

export type commonParamList = rootStackParamList & UserStackParamList

const RootTab = createBottomTabNavigator<rootStackParamList>()

const tabBarOptions = {
    activeTintColor: 'black',
    labelStyle: {
        margin: 0,
    },
}

const index = () => {
    return (
        <RootTab.Navigator tabBarOptions={tabBarOptions}>
            <RootTab.Screen
                name="Discover"
                component={DiscoverStack}
                options={{
                    title: _ROUTE_LABEL_NAME.discover,
                    tabBarIcon: ({ focused }) => (
                        <Icon name={focused ? 'discover_dark' : 'discover_light'} width="24" />
                    ),
                }}
            />
            {/* <RootTab.Screen
                name="DiscussionRoute"
                component={DiscussionStack}
                options={{
                    title: _ROUTE_LABEL_NAME.discussion,
                    tabBarIcon: ({ focused }) => <Icon name={focused ? 'message_dark' : 'message_light'} width="24" />,
                }}
            /> */}
            <RootTab.Screen
                name="ForumStack"
                component={ForumStack}
                options={{
                    title: _ROUTE_LABEL_NAME.community,
                    tabBarIcon: ({ focused }) => <Icon name={focused ? 'message_dark' : 'message_light'} width="24" />,
                }}
            />
            <RootTab.Screen
                name="MosqueRoute"
                component={MosqueStack}
                options={{
                    title: _ROUTE_LABEL_NAME.mosque,
                    tabBarIcon: ({ focused }) => <Icon name={focused ? 'mosque_dark' : 'mosque_light'} width="24" />,
                }}
            />
            <RootTab.Screen
                name="UserRoute"
                component={UserStack}
                options={{
                    title: _ROUTE_LABEL_NAME.account,
                    tabBarIcon: ({ focused }) => <Icon name={focused ? 'account_dark' : 'account_light'} width="24" />,
                }}
            />
        </RootTab.Navigator>
    )
}

export default index
