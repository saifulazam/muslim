import { Positions } from '../Modal/Constants'
import React from 'react'
import { View, Text, StyleSheet, Platform, PixelRatio } from 'react-native'

const isAndroid = Platform.OS === 'android'

interface Props {
    title: any
    style?: any
    textStyle?: any
    align?: string
    hasTitleBar?: boolean
}

const ModalTitle = ({ title, style, textStyle, hasTitleBar = true, align = 'center' }: Props) => {
    const titleBar = hasTitleBar ? styles.titleBar : null
    const titleAlign = { alignItems: Positions[align] }

    return (
        <View style={[styles.title, titleAlign, titleBar, style]}>
            <Text style={[styles.text, textStyle]}>{title}</Text>
        </View>
    )
}

export default ModalTitle

const styles = StyleSheet.create({
    title: {
        padding: 14,
        paddingHorizontal: 18,
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
    },
    titleBar: {
        borderBottomWidth: 1 / PixelRatio.get(),
        backgroundColor: '#F9F9FB',
        borderColor: '#DAD9DC',
    },
    text: {
        fontWeight: isAndroid ? '400' : '500',
        fontFamily: isAndroid ? 'sans-serif-medium' : 'System',
        fontSize: isAndroid ? 19 : 15,
        color: '#151822',
    },
})
