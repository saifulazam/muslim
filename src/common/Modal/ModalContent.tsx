import React from 'react'
import { View, StyleSheet } from 'react-native'
import ModalContext from './ModalContext'

interface Props {
    children: any
    style?: any
}

const ModalContent = ({ style, children }: Props) => (
    <ModalContext.Consumer>
        {({ hasTitle }) => <View style={[styles.content, hasTitle && styles.noPaddingTop, style]}>{children}</View>}
    </ModalContext.Consumer>
)

export default ModalContent

const styles = StyleSheet.create({
    content: {
        paddingVertical: 24,
        paddingHorizontal: 18,
    },
    noPaddingTop: {
        paddingTop: 0,
    },
})
