import React, { Component } from 'react'
import { Text, View, TouchableOpacity } from 'react-native'

interface Props {
    onPress: noop
    name: string
    color?: string
}

export default class ActionButton extends Component<Props> {
    render() {
        return (
            <TouchableOpacity onPress={this.props.onPress}>
                <View style={{ padding: 15 }}>
                    <Text
                        style={{
                            color: this.props.color ?? '#000',
                            alignSelf: 'center',
                            fontSize: 18,
                            fontWeight: 'bold',
                            fontFamily: 'AvantGardeLT-Demi',
                        }}
                    >
                        {this.props.name}
                    </Text>
                </View>

                <View style={{ borderColor: '#eee', borderBottomWidth: 1 }} />
            </TouchableOpacity>
        )
    }
}
