import React from 'react'
import { CheckBox as NativeCheckBox, CheckBoxProps } from 'react-native-elements'

interface Props extends CheckBoxProps {
    checked: boolean
    onPress: noop
}

interface State {}

export default class CheckBox extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {
            isChecked: false,
        }
    }

    public render() {
        return (
            <NativeCheckBox
                checked={this.props.checked}
                onPress={this.props.onPress}
                size={28}
                textStyle={{ color: '#222222', fontSize: 15, height: 20, fontWeight: '400' }}
                title={'Remember me'}
                containerStyle={{ backgroundColor: 'transparent' }}
                checkedColor={'#6500d3'}
                uncheckedColor={'#a7a7a7'}
                {...this.props}
            />
        )
    }
}
