import React, { useState, useEffect } from 'react'
import { View, Text, TouchableOpacity, Modal, Dimensions } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { reports } from '../data/reports'
import COLORS from '../utils/Color'
import ModalButton from './Modal/ModalButton'
import ModalFooter from './Modal/ModalFooter'
import ModalTitle from './Modal/ModalTitle'
import Radio from './Radio'

const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height

interface Props {
    visible: boolean
    onPress: any
    onCancel: any
}

const ReportModal = (props: Props) => {
    const [value, setValue] = useState(null)

    const renderReportItem = () => {
        return reports.map((item, index) => {
            return (
                <View key={index}>
                    <View style={{ flexDirection: 'row', marginVertical: 15 }}>
                        <TouchableOpacity
                            style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                // height: 50,
                                paddingHorizontal: 15,
                                width: '100%',
                            }}
                            activeOpacity={1}
                            onPress={() => setValue(item)}
                        >
                            <View
                                style={{
                                    overflow: 'hidden',
                                    height: 24,
                                    width: 24,
                                    borderRadius: 24,
                                    borderColor: '#ACACAC',
                                    borderWidth: 1,
                                }}
                            >
                                {value === item && (
                                    <View style={{ width: '100%', height: '100%' }}>
                                        <View
                                            style={{
                                                position: 'absolute',
                                                left: 0,
                                                zIndex: -1,
                                                top: 0,
                                                borderRadius: 999,
                                                height: '100%',
                                                width: '100%',
                                                backgroundColor: '#6500d3',
                                            }}
                                        />
                                        <View
                                            style={{
                                                position: 'absolute',
                                                left: (24 - 2 - 10) / 2,
                                                zIndex: 1,
                                                top: (24 - 2 - 10) / 2,
                                                borderRadius: 999,
                                                height: 10,
                                                width: 10,
                                                backgroundColor: '#fff',
                                            }}
                                        />
                                    </View>
                                )}
                            </View>
                            <Text style={{ marginLeft: 10, fontSize: 15, color: '#222222' }}>{item}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ borderBottomColor: '#eee', borderBottomWidth: 1 }} />
                </View>
            )
        })
    }

    return (
        <Modal visible={props.visible} animationType="slide" presentationStyle="pageSheet">
            <View style={{ height: windowHeight - 200 }}>
                <ModalTitle title="Report" />
                <View style={{ padding: 20 }}>
                    <ScrollView>{renderReportItem()}</ScrollView>
                    <ModalFooter>
                        <ModalButton
                            text="CANCEL"
                            textStyle={{ color: COLORS.gray }}
                            bordered
                            onPress={() => props.onCancel()}
                            key="button-1"
                        />
                        <ModalButton
                            text="SUBMIT"
                            textStyle={{ color: COLORS.primary }}
                            bordered
                            onPress={() => props.onPress(value)}
                            key="button-2"
                        />
                    </ModalFooter>
                </View>
            </View>
        </Modal>
    )
}

export default ReportModal
