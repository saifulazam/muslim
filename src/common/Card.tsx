import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const Card = (props) => {
    const { width, height, round } = props

    return (
        <View style={[styles.card, { width: width, height: height ? height : 'auto', borderRadius: round }]}>
            {props.children}
        </View>
    )
}

export default Card

Card.defaultProps = {
    width: '100%',
    height: 150,
    round: 5,
}

const styles = StyleSheet.create({
    card: {
        backgroundColor: 'white',
        shadowColor: '#000000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowRadius: 1,
        shadowOpacity: 0.1,
        elevation: 5,
        borderRadius: 15,
        borderColor: 'rgba(0,0,0, 0.1)',
        borderWidth: 0.3,
        // padding: 10,
        margin: 10,
    },
})
