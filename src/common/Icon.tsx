import React from 'react'
import { Text, ViewStyle, View } from 'react-native'
import { SvgXml } from 'react-native-svg'
import SvgIcon from '../utils/SvgIcon'

const Icon = ({
    name,
    height = '32',
    width = '32',
    style,
}: {
    name: string
    height?: string
    width?: any
    style?: ViewStyle
}) => {
    const iconName = SvgIcon[name]

    if (!iconName) {
        return <Text style={style}>X</Text>
    }
    return <SvgXml xml={iconName} width={width} height={height} />
}

export default Icon
