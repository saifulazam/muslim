import { TouchableOpacity, Text, ButtonProps, ViewStyle } from 'react-native'
import React from 'react'
import _COLORS from '../utils/Color'

interface Props extends Partial<ButtonProps> {
    isActive: boolean
    title: string
    onPress: noop
    style?: ViewStyle
    textColor?: any
}

export default function DefaultButton({ title, onPress, isActive, style, textColor }: Props) {
    return (
        <>
            <TouchableOpacity
                onPress={isActive ? onPress : () => {}}
                style={
                    style
                        ? {
                              ...style,
                              height: 55,
                              // backgroundColor: isActive ? '#3c00b5' : "grey", // #eeeeee is too light
                          }
                        : { height: 55, backgroundColor: isActive ? _COLORS.primary : '#eeeeee' }
                }
            >
                <Text
                    style={{
                        color: isActive ? textColor : 'black',
                        fontSize: 16,
                        marginVertical: 16,
                        fontWeight: 'bold',
                        alignSelf: 'center',
                    }}
                >
                    {title}
                </Text>
            </TouchableOpacity>
        </>
    )
}
