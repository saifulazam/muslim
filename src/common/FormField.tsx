import React, { useState } from 'react'
import { TextInput, View, Text, TextInputProps, TextStyle, TouchableOpacity } from 'react-native'
import Icon from '../common/Icon'

export default function FormField({
    title,
    placeholder,
    value,
    onChangeText,
    style,
    icon,
    iconOnPress,
    iconOnChange,
    secureTextEntry,
    autoCorrect,
    autoCapitalize,
    error,
    errorMessage,
    autoCompleteType,
    keyboardType,
    returnKeyType,
    editable,
}: {
    style?: TextStyle
    title?: string
    placeholder?: string
    value: string
    onChangeText: (value: string) => void
    icon?: boolean
    iconOnPress?: any
    iconOnChange?: any
    secureTextEntry?: boolean
    autoCorrect?: boolean
    autoCapitalize?: 'none' | 'words' | 'sentences' | 'characters'
    error?: any
    errorMessage?: string
    autoCompleteType?:
        | 'email'
        | 'password'
        | 'off'
        | 'cc-csc'
        | 'cc-exp'
        | 'cc-exp-month'
        | 'cc-exp-year'
        | 'cc-number'
        | 'name'
        | 'postal-code'
        | 'street-address'
        | 'tel'
        | 'username'
    keyboardType?: any
    returnKeyType?: any
    editable?: boolean
}) {
    const [focus, setFocus] = useState(false)
    return (
        <>
            {title && (
                <View>
                    <Text
                        style={
                            style
                                ? style
                                : {
                                      fontSize: 15,
                                      marginVertical: 20,
                                      fontWeight: '700',
                                      color: '#a7a7a7',
                                  }
                        }
                    >
                        {title}
                    </Text>
                </View>
            )}
            <View
                style={{
                    height: 60,
                    borderWidth: 1,
                    marginVertical: -10,
                    bottom: 5,
                    borderColor: getBorderColor(),
                }}
            >
                <TextInput
                    placeholder={placeholder}
                    textAlignVertical="center"
                    style={[
                        {
                            padding: 20,
                            // marginHorizontal: 0,
                            fontSize: 16,
                            // marginVertical: 0,
                            // marginStart: 0,
                            fontWeight: '800',
                        },
                    ]}
                    value={value}
                    onChangeText={onChangeText}
                    onBlur={() => setFocus(false)}
                    onFocus={() => setFocus(true)}
                    secureTextEntry={secureTextEntry ?? false}
                    autoCorrect={autoCorrect ?? false}
                    autoCapitalize={autoCapitalize ?? 'none'}
                    autoCompleteType={autoCompleteType ? autoCompleteType : 'off'}
                    keyboardType={keyboardType ? keyboardType : 'default'}
                    returnKeyType={returnKeyType ? returnKeyType : 'default'}
                    editable={editable}
                />
                {/* {error && <Text style={{ color: 'red', marginVertical: 5 }}>{errorMessage}</Text>} */}
                {icon && (
                    <TouchableOpacity
                        onPress={iconOnPress}
                        style={{ position: 'absolute', right: 20, marginVertical: 13 }}
                    >
                        <Icon name={iconOnChange ? 'eye_close' : 'eye_open'} width="24" />
                    </TouchableOpacity>
                )}

                {error && (
                    <>
                        <Text style={{ marginTop: 5, color: '#e50a00' }}>{error}</Text>
                    </>
                )}
            </View>
        </>
    )
    function getBorderColor(): string {
        let borderColor = ''
        if (error) {
            borderColor = '#e50a00'
        } else if (focus) {
            borderColor = '#6500d3'
        } else {
            borderColor = '#808080'
        }
        return borderColor
    }
}
