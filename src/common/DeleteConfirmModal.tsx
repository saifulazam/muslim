import React from 'react'
import { View, Text, Modal, Dimensions, TouchableOpacity, StyleSheet } from 'react-native'
import ModalTitle from './Modal/ModalTitle'
import ModalButton from './Modal/ModalButton'
import ModalFooter from './Modal/ModalFooter'
// import Modal, { SlideAnimation, ModalContent, ModalTitle, ModalFooter, ModalButton } from 'react-native-modals'

interface Props {
    visibility: boolean
    onCancel: any
    onPress: any
    message?: string
}

const { width, height } = Dimensions.get('screen')

const DeleteConfirmModal = (props: Props) => {
    return (
        <Modal
            visible={props.visibility}
            transparent={true}
            onRequestClose={() => props.onCancel()}
            animationType="fade"
        >
            <View style={style.modal}>
                <View style={style.modalContainer}>
                    <ModalTitle title="Confirmation" />
                    <View style={style.modalBody}>
                        <Text>{props.message ?? 'Are you sure you want to delete this item?'}</Text>
                    </View>

                    <ModalFooter>
                        <ModalButton text="CANCEL" bordered onPress={() => props.onCancel()} key="button-1" />
                        <ModalButton
                            textStyle={{ color: 'red' }}
                            text="DELETE"
                            bordered
                            onPress={() => props.onPress()}
                            key="button-2"
                        />
                    </ModalFooter>
                </View>
            </View>
            {/* <TouchableOpacity onPress={() => props.onCancel()} style={style.backdrop} /> */}
        </Modal>
    )
}

export default DeleteConfirmModal

const style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    modal: {
        backgroundColor: '#00000099',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    modalContainer: {
        backgroundColor: '#f9fafb',
        width: '80%',
        borderRadius: 5,
    },
    modalHeader: {},
    title: {
        fontWeight: 'bold',
        fontSize: 20,
        padding: 15,
        color: '#000',
    },
    divider: {
        width: '100%',
        height: 1,
        backgroundColor: 'lightgray',
    },
    modalBody: {
        backgroundColor: '#fff',
        paddingVertical: 20,
        paddingHorizontal: 10,
    },
    modalFooter: {},
    actions: {
        borderRadius: 5,
        marginHorizontal: 10,
        paddingVertical: 10,
        paddingHorizontal: 20,
    },
    actionText: {
        color: '#fff',
    },
})
