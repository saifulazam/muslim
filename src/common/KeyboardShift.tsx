import React, { Component } from 'react'
import { Animated, Dimensions, Keyboard, StyleSheet, TextInput, UIManager } from 'react-native'

const { State: TextInputState } = TextInput

interface Props {
    children: any
}
interface State {
    shift: any
}

export default class KeyboardShift extends Component<Props, State> {
    state = {
        shift: new Animated.Value(0),
    }

    UNSAFE_componentWillMount() {
        //@ts-ignore
        this.keyboardDidShowSub = Keyboard.addListener('keyboardDidShow', this.handleKeyboardDidShow)
        //@ts-ignore
        this.keyboardDidHideSub = Keyboard.addListener('keyboardDidHide', this.handleKeyboardDidHide)
    }

    UNSAFE_componentWillUnmount() {
        //@ts-ignore
        this.keyboardDidShowSub.remove()
        //@ts-ignore
        this.keyboardDidHideSub.remove()
    }

    handleKeyboardDidShow = (event) => {
        const { height: windowHeight } = Dimensions.get('window')
        const keyboardHeight = event.endCoordinates.height
        const currentlyFocusedField = TextInputState.currentlyFocusedField()
        UIManager.measure(currentlyFocusedField, (originX, originY, width, height, pageX, pageY) => {
            const fieldHeight = height
            const fieldTop = pageY
            const gap = windowHeight - keyboardHeight - (fieldTop + fieldHeight)
            if (gap >= 0) {
                return
            }
            Animated.timing(this.state.shift, {
                toValue: gap,
                duration: 1000,
                useNativeDriver: true,
            }).start()
        })
    }

    handleKeyboardDidHide = () => {
        Animated.timing(this.state.shift, {
            toValue: 0,
            duration: 1000,
            useNativeDriver: true,
        }).start()
    }

    render() {
        const { children } = this.props
        const { shift } = this.state
        return (
            <Animated.View style={[styles.container, { transform: [{ translateY: shift }] }]}>{children}</Animated.View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        left: 0,
        position: 'absolute',
        top: 0,
        width: '100%',
    },
})
