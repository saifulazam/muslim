import React from 'react'
import { Button, Text, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'

interface Props {
    errorMessage: string
}

export default class ScreenLevelAlert extends React.Component<Props> {
    public render() {
        return (
            this.props.errorMessage && (
                <View
                    style={{
                        marginTop: 30,
                        backgroundColor: '#ffe8e7',
                        padding: 25,
                        borderColor: '#e50a00',
                        borderWidth: 1,
                        borderRadius: 8,
                        shadowOffset: { width: 2, height: 4 },
                        shadowColor: 'rgba(0, 0, 0, 0.2)',
                    }}
                >
                    <Text style={{ alignSelf: 'center', fontSize: 15 }}>{this.props.errorMessage}</Text>
                </View>
            )
        )
    }
}
