import React from 'react'
import { View, Text } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Icon from './Icon'

interface Props {
    onPress?: any
    likeCount?: number
    isLiked?: boolean
}

function intlFormat(num) {
    return new Intl.NumberFormat().format(Math.round(num * 10) / 10)
}
function makeFriendly(num) {
    if (num >= 1000000) return intlFormat(num / 1000000) + 'M'
    if (num >= 1000) return intlFormat(num / 1000) + 'k'
    return intlFormat(num)
}

const LikeButton = (props: Props) => {
    if (props.isLiked) {
        return (
            <TouchableOpacity
                onPress={() => props.onPress()}
                style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: 5,
                    backgroundColor: 'rgba(224,204,246, 0.2)',
                    borderRadius: 100,
                    borderColor: '#E0CCF6',
                    borderWidth: 1,
                    width: 96,
                    height: 34,
                }}
            >
                <Icon name="thumbs_up_primary" width="14" />
                <Text style={{ paddingHorizontal: 3, color: '#6500D3' }}>{makeFriendly(props.likeCount)}</Text>
            </TouchableOpacity>
        )
    } else {
        return (
            <TouchableOpacity
                onPress={() => props.onPress()}
                style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: 5,
                    backgroundColor: '#FFFFFF',
                    borderRadius: 100,
                    borderColor: '#EBECF0',
                    borderWidth: 1,
                    width: 96,
                    height: 34,
                }}
            >
                <Icon name="thumbs_up" width="14" />
                <Text style={{ paddingHorizontal: 3, color: '#000' }}>{makeFriendly(props.likeCount)}</Text>
            </TouchableOpacity>
        )
    }
}

export default LikeButton
