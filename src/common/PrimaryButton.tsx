import { TouchableOpacity, Text, ButtonProps, ViewStyle } from 'react-native'
import React from 'react'
import _COLORS from '../utils/Color'
import SpinnerEllips from '../components/SpinnerEllips'

interface Props extends Partial<ButtonProps> {
    isActive: boolean
    title: string
    onPress: noop
    style?: ViewStyle
    isLoading?: boolean
    round?: boolean
}

export default function PrimaryButton({ title, onPress, isActive, style, isLoading }: Props) {
    return (
        <>
            <TouchableOpacity
                onPress={isActive ? onPress : () => {}}
                activeOpacity={!isActive ? 1 : 0.5}
                style={
                    style
                        ? {
                              ...style,
                              height: 50,
                              backgroundColor: isActive
                                  ? _COLORS.primary
                                  : isLoading
                                  ? _COLORS.primary_01
                                  : _COLORS.silver,
                          }
                        : { height: 50, backgroundColor: isActive ? _COLORS.primary : _COLORS.silver }
                }
            >
                {isLoading ? (
                    <SpinnerEllips numberOfDots={3} minOpacity={0.4} animationDelay={200} color="white" />
                ) : (
                    <Text
                        style={{
                            color: isActive ? 'white' : '#969696',
                            fontSize: 15,
                            textAlign: 'center',
                            marginVertical: 15,
                            fontWeight: 'bold',
                        }}
                    >
                        {title}
                    </Text>
                )}
            </TouchableOpacity>
        </>
    )
}
