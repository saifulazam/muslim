import React, { Component } from 'react'
import ProgressiveImage from './ProgressiveImage'

const placeHolder =
    'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkqAcAAIUAgUW0RjgAAAAASUVORK5CYII='

interface Props {
    source?: any
    style: any
}

export default class LazyImage extends Component<Props> {
    render() {
        return (
            <ProgressiveImage
                thumbnailSource={placeHolder}
                source={this.props.source}
                style={this.props.style}
                resizeMode="cover"
                {...this.props}
            />
        )
    }
}
