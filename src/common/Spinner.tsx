import React from 'react'
import { View, Text, ActivityIndicator } from 'react-native'
import _Color from '../utils/Color'

interface Props {
    color?: string
    size?: number | 'small' | 'large'
    animating?: boolean
}

const Spinner = (props: Props) => {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <ActivityIndicator color={props.color ?? _Color.darkcyan} size={props.size} animating />
        </View>
    )
}

export default Spinner
