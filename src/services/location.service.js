import Geocoder from 'react-native-geocoding'
import { GOOGLE_API, MAPBOX_TOKEN, MAPBOX_API } from 'react-native-dotenv'

// Geocoder.init(GOOGLE_API)

export const getLocation = () => {
    return new Promise((resolve, reject) => {
        navigator.geolocation.getCurrentPosition(
            (data) => resolve(data.coords),
            (err) => reject(err)
        )
    })
}

export const geocodeLocationByName = (locationName) => {
    return new Promise((resolve, reject) => {
        Geocoder.from(locationName)
            .then((json) => {
                const addressComponent = json.results[0].address_components[0]
                resolve(addressComponent)
            })
            .catch((error) => reject(error))
    })
}

export const geocodeCoordsByAddress = (address) => {
    return new Promise((resulve, reject) => {
        Geocoder.from(address)
            .then((json) => {
                const { location } = json.results[0].geometry
                resulve(location)
            })
            .catch((error) => reject(error))
    })
}

export const geocodeLocationByCoords = (lat, long) => {
    return new Promise((resolve, reject) => {
        Geocoder.from(lat, long)
            .then((json) => {
                const addressComponent = json.results[0].formatted_address
                resolve(addressComponent)
            })
            .catch((error) => reject(error))
    })
}

export const googleSearchAddress = (input) => {
    return new Promise((resolve, reject) => {
        try {
            fetch(
                `https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${input}&key=${GOOGLE_API}&sessiontoken=${generateToken(
                    10
                )}`
            )
                .then((res) => res.json())
                .then((result) => {
                    return resolve(result)
                })
        } catch (err) {
            reject(err)
        }
    })
}
/**
 *  Final method for mapbox search
 * @param {string} input
 */
export const mapBoxSearch = (input) => {
    return new Promise((resolve, reject) => {
        try {
            fetch(`${MAPBOX_API}/mapbox.places/${input}.json?country=us&limit=20&access_token=${MAPBOX_TOKEN}`)
                .then((res) => res.json())
                .then((result) => {
                    return resolve(result.features)
                })
        } catch (err) {
            reject(err)
        }
    })
}

export const generateToken = (length) => {
    let result = ''
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    const charactersLength = characters.length
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength))
    }
    return result
}
