import { BASE_URL, API_VERSION } from 'react-native-dotenv'
import Http from '../utils/Http'
import { getStorage } from '../utils/Storage'
import Transformer from '../utils/Transformer'

const API_URL = `${BASE_URL}/api/${API_VERSION}/client`

export const AxiosServiceExecutorGet = async (
    url: string,
    callback?: CallableFunction,
    isPrivate?: boolean
): Promise<any> => {
    let response
    try {
        // console.log(`Making a GET service call to ${url}`)
        if (isPrivate) {
            const token = await getStorage('access_token')
            Http.defaults.headers.common['Authorization'] = `Bearer ${token}`
        }
        response = await Http.get(url).then((response) => {
            const data = Transformer.fetch(response.data)
            // console.log(`Successfull response for ${url} with result`, data)
            callback(data)
        })
    } catch (error) {
        console.warn(`something went wrong ${error}`)
    }
    return Promise.resolve(response)
}

export const AxiosServiceExecutorPost = async (
    url: string,
    bodyParameters,
    callback?: CallableFunction,
    isPrivate?: boolean,
    hasFile?: boolean
): Promise<any> => {
    let response
    try {
        console.log(`Making a POST service call to ${url} with headers ${JSON.stringify(bodyParameters)}`)
        if (isPrivate) {
            const token = await getStorage('access_token')
            Http.defaults.headers.common['Authorization'] = `Bearer ${token}`
        }
        if (hasFile) {
            Http.defaults.headers.common['Content-Type'] = `multipart/form-data`
        }
        response = await Http.post(url, bodyParameters).then((response) => {
            const data = Transformer.fetch(response.data)
            console.log(`Successful response for ${url} with result`, data)
            callback(data)
        })
    } catch (error) {
        console.warn(`something went wrong ${error}`)
    }
    return Promise.resolve(response)
}

export const AxiosServiceExecutorPrivateDelete = async (url: string, callback?: CallableFunction): Promise<any> => {
    let response
    try {
        // console.log(`Making a PRIVATE Delete service call to ${url}`)
        const token = await getStorage('access_token')
        Http.defaults.headers.common['Authorization'] = `Bearer ${token}`
        response = await Http.delete(url).then((response) => {
            // const data = Transformer.fetch(response.data)
            // console.log(`Successful response for ${url} with result: ${response}`)
            callback(response)
        })
    } catch (error) {
        console.warn(`something went wrong ${error}`)
    }
    return Promise.resolve(response)
}

export const ServiceExecutorPost = async (url: string, bodyParameters, callback?: CallableFunction): Promise<any> => {
    let response
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
        },
        body: JSON.stringify(bodyParameters),
    }
    try {
        console.log(`Making a POST service call to ${url} with headers ${JSON.stringify(requestOptions)}`)
        response = await fetch(url, requestOptions)
            .then((resp) => resp.json())
            .then((response) => {
                console.log(`Successfull response for ${url} with result`, response)
                callback(response)
            })
    } catch (error) {
        console.warn(`something went wrong ${error}`)
    }
    return Promise.resolve(response)
}

export const ServicePrivateExecutorPost = async (
    url: string,
    bodyParameters,
    callback?: CallableFunction
): Promise<any> => {
    let response
    const token = await getStorage('access_token')
    const requestOptions = {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/json',
            Accept: 'application/json',
        },
        body: JSON.stringify(bodyParameters),
    }
    try {
        // console.log(`Making a private POST service call to ${url} with headers ${JSON.stringify(requestOptions)}`)
        response = await fetch(url, requestOptions)
            .then((resp) => resp.json())
            .then((response) => {
                // console.log(`Successfull response for ${url} with result`, response)
                callback(response)
            })
    } catch (error) {
        console.warn(`something went wrong ${error}`)
    }
    return Promise.resolve(response)
}

export const ServiceExecutorGet = async (url: string, callback?: CallableFunction): Promise<any> => {
    let response
    const requestOptions = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
        },
    }
    try {
        // console.log(`Making a GET service call to ${url} with headers ${JSON.stringify(requestOptions)}`)
        response = await fetch(url, requestOptions)
            .then((resp) => resp.json())
            .then((response) => {
                // console.log(`Successfull response for ${url} with result`, response)
                callback(response)
            })
    } catch (error) {
        console.warn(`something went wrong ${error}`)
    }
    return Promise.resolve(response)
}

export const ServiceExecutorPrivateGet = async (url: string, callback?: CallableFunction): Promise<any> => {
    let response
    const token = await getStorage('access_token')
    const requestOptions = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: `Bearer ${token}`,
        },
    }
    try {
        // console.log(`Making a PRIVATE GET service call to ${url} with headers ${JSON.stringify(requestOptions)}`)
        response = await fetch(url, requestOptions)
            .then((resp) => resp.json())
            .then((response) => {
                // console.log(`Successful response for ${url} with result`, response)
                callback(response)
            })
    } catch (error) {
        console.warn(`something went wrong ${error}`)
    }
    return Promise.resolve(response)
}

export const ServiceExecutorPrivateDelete = async (url: string, callback?: CallableFunction): Promise<any> => {
    let response
    const token = await getStorage('access_token')
    const requestOptions = {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: `Bearer ${token}`,
        },
    }
    try {
        // console.log(`Making a PRIVATE Delete service call to ${API_URL + url}`)
        const token = await getStorage('access_token')
        Http.defaults.headers.common['Authorization'] = `Bearer ${token}`
        response = await Http.delete(url).then((response) => {
            // console.log(
            //     `Successful response for ${API_URL + url} with result: ${response} and headers:${requestOptions}`
            // )
            callback(response)
        })
    } catch (error) {
        console.warn(`something went wrong ${error}`)
    }
    return Promise.resolve(response)
}
