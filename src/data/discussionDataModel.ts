export interface Discussion {
    added: string
    category: Category
    comments: Comments[]
    content: string
    creator: User
    id: number
    images: any[]
}

interface Category {
    created_at: string
    description: null
    id: number
    image: null
    name: string
    slug: string
    updated_at: string
}

interface Comments {
    added: string
    id: number
    message: string
    user: User
}

interface User {
    avatar: string
    id: number
    name: string
}
