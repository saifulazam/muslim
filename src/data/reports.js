export const reports = [
    "Misleading information",
    "Dangerous organizations and individuals",
    "Illegal activities and regulated goods",
    "Violent and graphic content",
    "Animal cruelty",
    "Suicide, self-harm, and dangerous acts",
    "Hate speech",
    "Harassment or bullying",
    "Pornography and nudity",
    "Minor safety",
    "Spam",
    "Intellectual property infringement",
    "Other"
]
