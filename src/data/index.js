export default {
    mosques_1: [
        {
            id: 1,
            name: 'Parkchester Jame Masjid',
            location: '1203 Virginia Ave, Bronx, NY 10472',
            photo:
                'https://natgeo.imgix.net/factsheets/thumbnails/sultan-ahmed-mosque-istanbul-turkey.adapt.1900.1.jpg?auto=compress,format&w=1600&h=900&fit=crop',
            verified: true,
        },
        {
            id: 2,
            name: 'Unionport Jame Masjid',
            location: '702 Rhinelander Ave, Bronx, NY 10462',
            photo:
                'https://assets.atlasobscura.com/media/W1siZiIsInVwbG9hZHMvcGxhY2VfaW1hZ2VzLzJmYmFlNTM2LWI5NDMtNDY2ZS1iNjM0LWIyMmRjMDU4MWFlN2E2YWI5NjgxZWRkMTkzOGQ2YV9COTRDMUEuanBnIl0sWyJwIiwidGh1bWIiLCJ4MzkwPiJdLFsicCIsImNvbnZlcnQiLCItcXVhbGl0eSA4MSAtYXV0by1vcmllbnQiXV0/B94C1A.jpg',
            verified: false,
        },
    ],

    discussions: [
        {
            id: 1,
            userName: 'Saiful Azam',
            userAvatar: 'https://pbs.twimg.com/profile_images/491607415688036352/_Vo_ogcR_400x400.jpeg',
            content: `Bismillah, praise be to Allah the lord of the worlds, He is the creator of all creations, He does not need anything and is not like anything. Allah exists without a place and cannot be imagined in the minds. We ask Allah to raise the rank of Prophet Muhammad and his kind Aal and companions. Know that It is not permissible to ask those who do not have knowledge an Islamic Judgement, nor is it permissible for the unqualified person to answer.  The best answers are those who are supported by proof from Qur'an, Alsunna, and Ijma^ scholarly consensus, and based on that, our questions are answered.`,
            createTime: '1 hour ago',
            type: 'Question',
            replies: 5,
        },
        {
            id: 2,
            userName: 'Ali',
            userAvatar: '',
            content: '',
            createTime: '',
            type: '',
            replies: '',
        },
    ],
    mosques: [
        {
            business_status: 'OPERATIONAL',
            geometry: {
                location: {
                    lat: 40.833792,
                    lng: -73.846408,
                },
                viewport: {
                    northeast: {
                        lat: 40.83526097989272,
                        lng: -73.84508622010728,
                    },
                    southwest: {
                        lat: 40.83256132010728,
                        lng: -73.84778587989273,
                    },
                },
            },
            icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/worship_islam-71.png',
            id: 'de9ab637dbae30b91eda2323cffe122156c1cf30',
            name: 'Baitul Aman Islamic Center Inc.',
            photos: [
                {
                    height: 3024,
                    html_attributions: [
                        '\u003ca href="https://maps.google.com/maps/contrib/102825189542682154765"\u003eMaksudur Rahman\u003c/a\u003e',
                    ],
                    photo_reference:
                        'CmRaAAAAlQNsbgte3mDs0GosQqfbZOWdkLQG04zMchvECRBroRmuo56VvQiTnO3tlWtYXzwX0UnWl10YwlCD5bvgjrui-w3FgDe8ErmWKYfK40dMIGxifgH3e81j3mggQm5gQeIiEhAA6X4cDCS5ERU7KJvJOdmPGhRok9X__dSshH2m5JKdNjalQU7iUQ',
                    width: 4032,
                },
            ],
            place_id: 'ChIJJUpOdcr0wokRFrzGf1Jr7OY',
            plus_code: {
                compound_code: 'R5M3+GC The Bronx, New York',
                global_code: '87G8R5M3+GC',
            },
            rating: 4.7,
            reference: 'ChIJJUpOdcr0wokRFrzGf1Jr7OY',
            scope: 'GOOGLE',
            types: ['mosque', 'place_of_worship', 'point_of_interest', 'establishment'],
            user_ratings_total: 43,
            vicinity: '2348 Newbold Ave, The Bronx',
        },
        {
            business_status: 'OPERATIONAL',
            geometry: {
                location: {
                    lat: 40.830744,
                    lng: -73.860899,
                },
                viewport: {
                    northeast: {
                        lat: 40.83212652989272,
                        lng: -73.85942162010728,
                    },
                    southwest: {
                        lat: 40.82942687010728,
                        lng: -73.86212127989273,
                    },
                },
            },
            icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/worship_islam-71.png',
            id: '0cdc5675271d2ba2e682ee30ec91796fa66f751c',
            name: 'Parkchester Jame Masjid',
            photos: [
                {
                    height: 1080,
                    html_attributions: [
                        '\u003ca href="https://maps.google.com/maps/contrib/101058301515496302880"\u003eBehzod Erjigitov\u003c/a\u003e',
                    ],
                    photo_reference:
                        'CmRaAAAAr1-tt5YQDuNn47RJpvSVB7VaGL6YUdUqHALsN8-minDiOpNt5onEwss5fIf-yIeoomTbM_5UNbQWD662m7C_iszRrcVImoML06JeMow_AxGQarZ7rnRSleet81xhUBeUEhBPzLfyL6eWIXAen1RQB8QuGhRbvyacHgD0JV9DBpBvp7acSZgT3Q',
                    width: 1920,
                },
            ],
            place_id: 'ChIJPcdF6MP0wokRNWH2mUgAXlI',
            plus_code: {
                compound_code: 'R4JQ+7J The Bronx, New York',
                global_code: '87G8R4JQ+7J',
            },
            rating: 4.8,
            reference: 'ChIJPcdF6MP0wokRNWH2mUgAXlI',
            scope: 'GOOGLE',
            types: ['mosque', 'place_of_worship', 'point_of_interest', 'establishment'],
            user_ratings_total: 127,
            vicinity: '1203 Virginia Ave, The Bronx',
        },
        {
            business_status: 'OPERATIONAL',
            geometry: {
                location: {
                    lat: 40.8703961,
                    lng: -73.84810639999999,
                },
                viewport: {
                    northeast: {
                        lat: 40.87174457989272,
                        lng: -73.84668077010727,
                    },
                    southwest: {
                        lat: 40.86904492010728,
                        lng: -73.84938042989272,
                    },
                },
            },
            icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/worship_islam-71.png',
            id: '8a4861be5f9cb8389af0b7031896a0b9f09b6632',
            name: 'Masjid Noor-ul-Huda Islamic Center',
            photos: [
                {
                    height: 3024,
                    html_attributions: [
                        '\u003ca href="https://maps.google.com/maps/contrib/117537820351101572569"\u003eMohammad Sadequl Islam\u003c/a\u003e',
                    ],
                    photo_reference:
                        'CmRaAAAAa2HGncj1Jr6ViOQsR1hD-fyroH0Pk4rs5WXeLQ7kN-s6U2mIwqfEQmgemtdMezvW1J8wlnkd6R25249eEl4XGcR_lvVWdENHCMXLZpvyN-_KItqZKvEXTZqn21k2E6kQEhA2zdAQ08Qm0L_tFk7n6V-PGhQ44X8vc5YCxwXfkN3D2-pPMUDVIg',
                    width: 4032,
                },
            ],
            place_id: 'ChIJSWONYEnzwokRF7XAY5xWbe8',
            plus_code: {
                compound_code: 'V5C2+5Q The Bronx, New York',
                global_code: '87G8V5C2+5Q',
            },
            rating: 4.6,
            reference: 'ChIJSWONYEnzwokRF7XAY5xWbe8',
            scope: 'GOOGLE',
            types: ['mosque', 'place_of_worship', 'point_of_interest', 'establishment'],
            user_ratings_total: 97,
            vicinity: '3033 Young Ave, The Bronx',
        },
        {
            business_status: 'OPERATIONAL',
            geometry: {
                location: {
                    lat: 40.8391559,
                    lng: -73.84582139999999,
                },
                viewport: {
                    northeast: {
                        lat: 40.84045757989272,
                        lng: -73.84439902010726,
                    },
                    southwest: {
                        lat: 40.83775792010728,
                        lng: -73.84709867989271,
                    },
                },
            },
            icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/worship_islam-71.png',
            id: 'b8aac01ac717e8ea37ad605b633ed4b8c832c42e',
            name: 'Masjid Mumineen',
            photos: [
                {
                    height: 4032,
                    html_attributions: [
                        '\u003ca href="https://maps.google.com/maps/contrib/101671571929848133815"\u003eMahtab Chowdhury\u003c/a\u003e',
                    ],
                    photo_reference:
                        'CmRaAAAAvgirNqjyzYnWsov9qQfv76Tcl76Ng2GeCKbitEXVLjkONapsU0PFjb13kHBS0MW85IPNGxSEKUV4FPPabhKAzDxUiKtnXVCe8OSlRKbjuBeQvBfaiss293UKOBZnWIOcEhAtFQqBwP14SATZANVmjH6tGhT7JtiJirW_K2ODG4f3kSc7c3BCVg',
                    width: 3024,
                },
            ],
            place_id: 'ChIJ82QmdrT0wokRYJjdu0YKm8Q',
            plus_code: {
                compound_code: 'R5Q3+MM The Bronx, New York',
                global_code: '87G8R5Q3+MM',
            },
            rating: 4.3,
            reference: 'ChIJ82QmdrT0wokRYJjdu0YKm8Q',
            scope: 'GOOGLE',
            types: ['mosque', 'place_of_worship', 'point_of_interest', 'establishment'],
            user_ratings_total: 14,
            vicinity: '2481 Tratman Ave, The Bronx',
        },
        {
            business_status: 'OPERATIONAL',
            geometry: {
                location: {
                    lat: 40.8555639,
                    lng: -73.86170969999999,
                },
                viewport: {
                    northeast: {
                        lat: 40.85691607989273,
                        lng: -73.86048352010727,
                    },
                    southwest: {
                        lat: 40.85421642010728,
                        lng: -73.86318317989272,
                    },
                },
            },
            icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/worship_islam-71.png',
            id: '230224b70d95dde2d258666186f8b96604d35fd5',
            name: 'Gulzar e Madinah Masjid',
            photos: [
                {
                    height: 3024,
                    html_attributions: [
                        '\u003ca href="https://maps.google.com/maps/contrib/115093679693909384788"\u003eMichael Scottland\u003c/a\u003e',
                    ],
                    photo_reference:
                        'CmRaAAAAECNDslNf03puqDzQ5CgJQDyI7yv9a2Vu99FrubQw4HvvqCHDm_eL5w6hLmYHoyOYIFqe-b07WJLZTjDAffkpkUFYn465_lW6hGbdWZ9FrLom1t46xoKcWp6GdQspavRyEhDSsJXXkBkygkmNuAcSkP6iGhQD62TVxfqQfWwa1wDj_mdO0uuV5g',
                    width: 4032,
                },
            ],
            place_id: 'ChIJNf8ZkKD0wokRKy_qUruii4c',
            plus_code: {
                compound_code: 'V44Q+68 The Bronx, New York',
                global_code: '87G8V44Q+68',
            },
            rating: 4.8,
            reference: 'ChIJNf8ZkKD0wokRKy_qUruii4c',
            scope: 'GOOGLE',
            types: ['mosque', 'place_of_worship', 'point_of_interest', 'establishment'],
            user_ratings_total: 12,
            vicinity: '2162 Muliner Ave, The Bronx',
        },
        {
            business_status: 'OPERATIONAL',
            geometry: {
                location: {
                    lat: 40.8399439,
                    lng: -73.8469719,
                },
                viewport: {
                    northeast: {
                        lat: 40.84134917989272,
                        lng: -73.84555522010727,
                    },
                    southwest: {
                        lat: 40.83864952010727,
                        lng: -73.84825487989272,
                    },
                },
            },
            icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/worship_islam-71.png',
            id: '20198f0a67a55e7692333c9d7be9c587c290dc38',
            name: 'Baitul Maamur Jame Masjid',
            photos: [
                {
                    height: 4032,
                    html_attributions: [
                        '\u003ca href="https://maps.google.com/maps/contrib/109291327478643627478"\u003e109291327478643627478\u003c/a\u003e',
                    ],
                    photo_reference:
                        'CmRaAAAAcyP0EuZ3QqCIEp-ayEoWE0pCJRjNbimTb5ruDWrN7rD3N65XZbgyDjuyAOUtvpTtZuyo_dtJOh_cAu_jcfxzC48YzeLFjk8TovKrv8cHerABdJ8fHBpCLiSzZKzbTa53EhCRrs7Tqpo_kXao23swAHb3GhSJUDfCr_XnV1Y5BqYF_aOpL-H4lQ',
                    width: 3024,
                },
            ],
            place_id: 'ChIJMcqo-rP0wokRmJlXqAiEu5U',
            plus_code: {
                compound_code: 'R5Q3+X6 The Bronx, New York',
                global_code: '87G8R5Q3+X6',
            },
            rating: 4.8,
            reference: 'ChIJMcqo-rP0wokRmJlXqAiEu5U',
            scope: 'GOOGLE',
            types: ['mosque', 'place_of_worship', 'point_of_interest', 'establishment'],
            user_ratings_total: 29,
            vicinity: '1511 St Peters Ave, The Bronx',
        },
        {
            business_status: 'CLOSED_TEMPORARILY',
            geometry: {
                location: {
                    lat: 40.83222,
                    lng: -73.8698828,
                },
                viewport: {
                    northeast: {
                        lat: 40.83359967989272,
                        lng: -73.86858767010727,
                    },
                    southwest: {
                        lat: 40.83090002010728,
                        lng: -73.87128732989271,
                    },
                },
            },
            icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/worship_islam-71.png',
            id: 'e7ed789a4ba3758b227bc179d826371530c21a85',
            name: 'The Bronx Islamic Society',
            permanently_closed: true,
            photos: [
                {
                    height: 3024,
                    html_attributions: [
                        '\u003ca href="https://maps.google.com/maps/contrib/108076745698960695885"\u003eTanvir Azahar\u003c/a\u003e',
                    ],
                    photo_reference:
                        'CmRaAAAAZniKIuVMgwprbzoLibQwmoF9Zeo3yFGgPY0A1ukdyR9XAxqDD9qRuYFsp2M-q62WxlNRtjfEtbD0QwxQdGU6UXTXTHBXwY8KZ6CnUfe3z1zvP-Oo00FOtbecw6NkhpMgEhCo_xvKAZ7nsvjQuUnxsWYtGhRTJLafPZoaoyKrqC5d7wwK-VhrGw',
                    width: 4032,
                },
            ],
            place_id: 'ChIJa7PbXOn0wokRYsVSwMbk66M',
            plus_code: {
                compound_code: 'R4JJ+V2 The Bronx, New York',
                global_code: '87G8R4JJ+V2',
            },
            rating: 4.4,
            reference: 'ChIJa7PbXOn0wokRYsVSwMbk66M',
            scope: 'GOOGLE',
            types: ['mosque', 'place_of_worship', 'point_of_interest', 'establishment'],
            user_ratings_total: 42,
            vicinity: '1726 E 172nd St, The Bronx',
        },
        {
            business_status: 'OPERATIONAL',
            geometry: {
                location: {
                    lat: 40.7683639,
                    lng: -73.8250889,
                },
                viewport: {
                    northeast: {
                        lat: 40.76972067989272,
                        lng: -73.82368187010728,
                    },
                    southwest: {
                        lat: 40.76702102010728,
                        lng: -73.82638152989273,
                    },
                },
            },
            icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/worship_islam-71.png',
            id: 'b0659e56745296417e5407966cf22f099587edbc',
            name: 'Masjid Hazrati Abu Bakr Siddique',
            photos: [
                {
                    height: 3024,
                    html_attributions: [
                        '\u003ca href="https://maps.google.com/maps/contrib/103191476859211360097"\u003eAbdullah Nazari\u003c/a\u003e',
                    ],
                    photo_reference:
                        'CmRaAAAAP08-Ucb-Hiz74p8aWmY5JP8U0GaFe5e-C9m4DNlwMaK63hVdpazpfJPuG5UGbpMABPiYrgPj-4OAioxus0Hpj9dhO7470Gs5kBNFvEf7VEisbQdmbKeEwiGQoyHoJTpaEhDHvauBcg2Gi5O2_cLkLWZUGhSt-nlT4KcKCVdzB6z0Pr_xIgYpJg',
                    width: 4032,
                },
            ],
            place_id: 'ChIJL7xwPRxgwokR9iI9uNcwT-g',
            plus_code: {
                compound_code: 'Q59F+8X Flushing, Queens, NY',
                global_code: '87G8Q59F+8X',
            },
            rating: 4.9,
            reference: 'ChIJL7xwPRxgwokR9iI9uNcwT-g',
            scope: 'GOOGLE',
            types: ['mosque', 'place_of_worship', 'point_of_interest', 'establishment'],
            user_ratings_total: 187,
            vicinity: '14147 33rd Ave, Flushing',
        },
        {
            business_status: 'OPERATIONAL',
            geometry: {
                location: {
                    lat: 40.8521215,
                    lng: -73.8624901,
                },
                viewport: {
                    northeast: {
                        lat: 40.85356702989273,
                        lng: -73.86113542010727,
                    },
                    southwest: {
                        lat: 40.85086737010728,
                        lng: -73.86383507989272,
                    },
                },
            },
            icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/worship_islam-71.png',
            id: '1dbe0ab1bb37e09b675ddba3132335f36c43e299',
            name: 'Madinah Masjid',
            photos: [
                {
                    height: 4032,
                    html_attributions: [
                        '\u003ca href="https://maps.google.com/maps/contrib/101534153138081822800"\u003eWAQAR ALAM KHAN\u003c/a\u003e',
                    ],
                    photo_reference:
                        'CmRaAAAAxCWUruuNuBXCs0HkJpxXA1KbfFJR2OJwEOdc7H756pFavHEvCJtPMucOmG4Nri3S_oqT6w13WOea7fHLEycYlfjF3yar9ZSRvwOa09Nx1ZyvrFQP-9tmu-cQ5EF6W8jkEhD4-h2nvAdcJAIofXB8zXyTGhQI-o3knFu1_LnxdgwviJA9N03RHA',
                    width: 3024,
                },
            ],
            place_id: 'ChIJBSlgnaH0wokRfJRe6sCU7UU',
            plus_code: {
                compound_code: 'V42Q+R2 The Bronx, New York',
                global_code: '87G8V42Q+R2',
            },
            rating: 4.9,
            reference: 'ChIJBSlgnaH0wokRfJRe6sCU7UU',
            scope: 'GOOGLE',
            types: ['mosque', 'place_of_worship', 'point_of_interest', 'establishment'],
            user_ratings_total: 14,
            vicinity: '834 Brady Ave, The Bronx',
        },
        {
            business_status: 'OPERATIONAL',
            geometry: {
                location: {
                    lat: 40.8376688,
                    lng: -73.84901099999999,
                },
                viewport: {
                    northeast: {
                        lat: 40.83905067989271,
                        lng: -73.84770952010726,
                    },
                    southwest: {
                        lat: 40.83635102010727,
                        lng: -73.85040917989271,
                    },
                },
            },
            icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/worship_islam-71.png',
            id: '8464f82308b08d3048ec8ab814ab17c5d9448c3b',
            name: 'Masjid Taqwa Islamic Center',
            photos: [
                {
                    height: 3264,
                    html_attributions: [
                        '\u003ca href="https://maps.google.com/maps/contrib/102786372892264947559"\u003eFadhle Nagi\u003c/a\u003e',
                    ],
                    photo_reference:
                        'CmRaAAAAn0U3n49y8mrLkW-8D9A942tgYKEgIGgHCEGRgT_Cfze402Of7t8NI1WjSXzX54fIm2J5Moes0PxwC-x8caPmhWTcwoI0KoJyDzTHypERIFPxB6ly_6lQZ5Du4cdvJP4QEhA_ZgYU7AsCo_Mfu81_Zq6kGhQLh_VIVg3qgacej724TuIvX9HFFw',
                    width: 2448,
                },
            ],
            place_id: 'ChIJ0-Zt2bX0wokRQ6NL3ZbVn8M',
            plus_code: {
                compound_code: 'R5Q2+39 The Bronx, New York',
                global_code: '87G8R5Q2+39',
            },
            rating: 4.9,
            reference: 'ChIJ0-Zt2bX0wokRQ6NL3ZbVn8M',
            scope: 'GOOGLE',
            types: ['mosque', 'place_of_worship', 'point_of_interest', 'establishment'],
            user_ratings_total: 18,
            vicinity: '1510 Parker St, The Bronx',
        },
        {
            business_status: 'OPERATIONAL',
            geometry: {
                location: {
                    lat: 40.7854415,
                    lng: -73.9485316,
                },
                viewport: {
                    northeast: {
                        lat: 40.78657467989272,
                        lng: -73.94751132010728,
                    },
                    southwest: {
                        lat: 40.78387502010728,
                        lng: -73.95021097989273,
                    },
                },
            },
            icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/worship_islam-71.png',
            id: 'd7535ae3a154f6ab877db5a912ee5ddc423942f9',
            name: 'Islamic Cultural Center of New York',
            opening_hours: {
                open_now: false,
            },
            photos: [
                {
                    height: 4032,
                    html_attributions: [
                        '\u003ca href="https://maps.google.com/maps/contrib/111003933428668593271"\u003eTalal Jawaid\u003c/a\u003e',
                    ],
                    photo_reference:
                        'CmRaAAAAnjQH9qAewUSrQB6jjdSMQplll6LulOmVk4V3Efa7aNqcRCh_XLyl5hFwhBj2d8JeGrE-HUK4N3OyBAgHnXS6lTtnPtWBqKjfX-5e2LDw4e_3-MNWGXlulsANHCEID0JLEhCVia_pOmQakA6xq4TIvjB_GhRB-00oKVWI0dhQ8sVBNM4E379reg',
                    width: 3024,
                },
            ],
            place_id: 'ChIJ4TXQD6ZYwokRTp5VAlJdYj8',
            plus_code: {
                compound_code: 'Q3P2+5H New York',
                global_code: '87G8Q3P2+5H',
            },
            rating: 4.8,
            reference: 'ChIJ4TXQD6ZYwokRTp5VAlJdYj8',
            scope: 'GOOGLE',
            types: ['mosque', 'tourist_attraction', 'place_of_worship', 'point_of_interest', 'establishment'],
            user_ratings_total: 943,
            vicinity: '1711 3rd Ave, New York',
        },
        {
            business_status: 'OPERATIONAL',
            geometry: {
                location: {
                    lat: 40.8342763,
                    lng: -73.85614850000002,
                },
                viewport: {
                    northeast: {
                        lat: 40.83561947989273,
                        lng: -73.85484322010728,
                    },
                    southwest: {
                        lat: 40.83291982010729,
                        lng: -73.85754287989273,
                    },
                },
            },
            icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/worship_islam-71.png',
            id: '498d24818ff384466a98a9f534f9c3969b777588',
            name: 'the african islamic center',
            photos: [
                {
                    height: 3024,
                    html_attributions: [
                        '\u003ca href="https://maps.google.com/maps/contrib/101671571929848133815"\u003eMahtab Chowdhury\u003c/a\u003e',
                    ],
                    photo_reference:
                        'CmRaAAAAzkt4zuuDmOwZna8UVXV3btLTBc1pMD1FwYarhYmfcpFAl0BmbZYOCUivWYWpI36kUvzw1Pc5IqE4r_58Pl_BVnQn2Ter72Zr9XMFlAfRHYqXSJUvafxSABPeVjjSZI7HEhC3iP1PeulAY04P7tMEXIjCGhSRm_MQMgsFgauWfSuqZVp7NVEKkA',
                    width: 4032,
                },
            ],
            place_id: 'ChIJ3xayo8f0wokRhEpxniL8VqU',
            plus_code: {
                compound_code: 'R4MV+PG The Bronx, New York',
                global_code: '87G8R4MV+PG',
            },
            rating: 4.9,
            reference: 'ChIJ3xayo8f0wokRhEpxniL8VqU',
            scope: 'GOOGLE',
            types: ['mosque', 'place_of_worship', 'point_of_interest', 'establishment'],
            user_ratings_total: 12,
            vicinity: '2044 Benedict Ave, The Bronx',
        },
        {
            business_status: 'OPERATIONAL',
            geometry: {
                location: {
                    lat: 40.8020169,
                    lng: -73.95023929999999,
                },
                viewport: {
                    northeast: {
                        lat: 40.80346157989273,
                        lng: -73.94881932010728,
                    },
                    southwest: {
                        lat: 40.80076192010728,
                        lng: -73.95151897989273,
                    },
                },
            },
            icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/worship_islam-71.png',
            id: '577ceaedf3956e32ba45dffb15251a01a4c0621e',
            name: 'Masjid Malcolm Shabazz',
            photos: [
                {
                    height: 3000,
                    html_attributions: [
                        '\u003ca href="https://maps.google.com/maps/contrib/107533869649641980878"\u003eHüseyin Fatih YAZ\u003c/a\u003e',
                    ],
                    photo_reference:
                        'CmRaAAAA3bpNGSl1l33HPjhOqD8yq2W-EYmnDwdVf39TTUmlDQZENGR0oTB5rimvw2cXx6KUJG2mdOWa6deZqTXhMORx9jT0FBGR0aT3EUXtiVCgHsdgh1RZwlw0-95MPf-uYZkREhAUUzpDOg8vu9eeAceKLL7TGhSDLhHIwkBjVum16mh6w_w5t65VpA',
                    width: 4000,
                },
            ],
            place_id: 'ChIJw6noUxD2wokRnj5_OciSlA8',
            plus_code: {
                compound_code: 'R22X+RW New York',
                global_code: '87G8R22X+RW',
            },
            rating: 4.3,
            reference: 'ChIJw6noUxD2wokRnj5_OciSlA8',
            scope: 'GOOGLE',
            types: ['mosque', 'tourist_attraction', 'place_of_worship', 'point_of_interest', 'establishment'],
            user_ratings_total: 177,
            vicinity: '102 W 116th St, New York',
        },
        {
            business_status: 'OPERATIONAL',
            geometry: {
                location: {
                    lat: 40.9273528,
                    lng: -73.81768889999999,
                },
                viewport: {
                    northeast: {
                        lat: 40.92875857989272,
                        lng: -73.81606777010728,
                    },
                    southwest: {
                        lat: 40.92605892010728,
                        lng: -73.81876742989273,
                    },
                },
            },
            icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/worship_islam-71.png',
            id: '6ced8333faa88e6c84d8554cef6aef4e92bf1a67',
            name: 'Westchester Muslim Center',
            photos: [
                {
                    height: 1122,
                    html_attributions: [
                        '\u003ca href="https://maps.google.com/maps/contrib/107348792796521320021"\u003eTarek A. Sabhah\u003c/a\u003e',
                    ],
                    photo_reference:
                        'CmRaAAAASGkwy4v3QJOma4jG8WiVs3yWCxgqf84X9UFxa4r1xcrvIdzG3ENwtLOZnypH9U9Ngpf7TakbLhzu1aTGrmsYBERBzRqBcgPEyH6OjwCf3ZPd1_lPLFGSFQhPG52XrD4CEhBsymN1510UfeOczMH1-kA_GhTfA3r5tJ-_Bz023Ccf_6dSSxwTUg',
                    width: 2208,
                },
            ],
            place_id: 'ChIJ0zU3nGCNwokRwQ1L9WLFhFc',
            plus_code: {
                compound_code: 'W5GJ+WW Mt Vernon, Pelham, NY',
                global_code: '87G8W5GJ+WW',
            },
            rating: 4.7,
            reference: 'ChIJ0zU3nGCNwokRwQ1L9WLFhFc',
            scope: 'GOOGLE',
            types: ['mosque', 'place_of_worship', 'point_of_interest', 'establishment'],
            user_ratings_total: 169,
            vicinity: '22 Brookfield Rd, Mt Vernon',
        },
        {
            business_status: 'OPERATIONAL',
            geometry: {
                location: {
                    lat: 40.81216269999999,
                    lng: -73.923575,
                },
                viewport: {
                    northeast: {
                        lat: 40.81349822989272,
                        lng: -73.92218807010727,
                    },
                    southwest: {
                        lat: 40.81079857010727,
                        lng: -73.92488772989272,
                    },
                },
            },
            icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/worship_islam-71.png',
            id: '155c73566222d3cb47d4737624eaa3e18797b282',
            name: 'Masjid Ebun Abass',
            opening_hours: {
                open_now: true,
            },
            photos: [
                {
                    height: 3024,
                    html_attributions: [
                        '\u003ca href="https://maps.google.com/maps/contrib/105042035445578450001"\u003eShamim\u003c/a\u003e',
                    ],
                    photo_reference:
                        'CmRaAAAAg-GeBvGuaWG77PZrgkg_2r2f59lbMtNrQL-dFDgvIy4cut0XICKdCDDqtjE_EhbML2Zp-_xZ4uJJC5Umh0wUpsOIk9wCIM_kaeaVqEN4SEI4SQCK-sFcL2TvxLG0qlSVEhDJFS-pANQXF4fSAzObZDlsGhQ2dAZx8NUpQfNOmDmhoEhyxUCo5g',
                    width: 4032,
                },
            ],
            place_id: 'ChIJr4AsysT1wokRZxNz3b7Nkig',
            plus_code: {
                compound_code: 'R36G+VH The Bronx, New York',
                global_code: '87G8R36G+VH',
            },
            rating: 4.7,
            reference: 'ChIJr4AsysT1wokRZxNz3b7Nkig',
            scope: 'GOOGLE',
            types: ['mosque', 'place_of_worship', 'point_of_interest', 'establishment'],
            user_ratings_total: 54,
            vicinity: '333 Alexander Ave, The Bronx',
        },
        {
            business_status: 'OPERATIONAL',
            geometry: {
                location: {
                    lat: 40.7590786,
                    lng: -73.9692446,
                },
                viewport: {
                    northeast: {
                        lat: 40.76051062989272,
                        lng: -73.96783357010727,
                    },
                    southwest: {
                        lat: 40.75781097010727,
                        lng: -73.97053322989272,
                    },
                },
            },
            icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/worship_islam-71.png',
            id: 'c71623ebb0031261efe5e3cd9bdba494498dde62',
            name: 'Islamic Society of Mid Manhattan',
            photos: [
                {
                    height: 4032,
                    html_attributions: [
                        '\u003ca href="https://maps.google.com/maps/contrib/111189408183585523812"\u003eA Google User\u003c/a\u003e',
                    ],
                    photo_reference:
                        'CmRaAAAAbFKEWRJTTbDeJnf-8H88vkT6QdXCIcc4fwRINKFomAq2KzDgL4G4TvPbbxzYZwIDKqo38sBwgqrdNjt9QXGwtokAbX9Jctm-YKi_M2HhYjJHQD_rADJAlkYSvWUOldqhEhDTqmZfLMLQTXwH_0HyCRw_GhSX9xSnpmBFye1miVEEZSUqWKi6Pw',
                    width: 3024,
                },
            ],
            place_id: 'ChIJI0nKk-RYwokRJYHko9AjvH4',
            plus_code: {
                compound_code: 'Q25J+J8 New York',
                global_code: '87G8Q25J+J8',
            },
            rating: 5,
            reference: 'ChIJI0nKk-RYwokRJYHko9AjvH4',
            scope: 'GOOGLE',
            types: ['mosque', 'place_of_worship', 'point_of_interest', 'establishment'],
            user_ratings_total: 189,
            vicinity: '154 E 55th St, New York',
        },
        {
            business_status: 'OPERATIONAL',
            geometry: {
                location: {
                    lat: 40.800718,
                    lng: -73.95282259999999,
                },
                viewport: {
                    northeast: {
                        lat: 40.80210897989272,
                        lng: -73.95144147010728,
                    },
                    southwest: {
                        lat: 40.79940932010727,
                        lng: -73.95414112989273,
                    },
                },
            },
            icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/worship_islam-71.png',
            id: '78b5f397fffb31dc920089b63acfc971f92913f0',
            name: 'The Mosque of Islamic Brotherhood',
            photos: [
                {
                    height: 2976,
                    html_attributions: [
                        '\u003ca href="https://maps.google.com/maps/contrib/113814750039264932510"\u003eTALIB ABDUR-RASHID\u003c/a\u003e',
                    ],
                    photo_reference:
                        'CmRaAAAAlqQ2e-wuDUWtekfc8vdPmOWXKOcig6Tr64IiqcVWEZ39NHKYu_ZzMwKnQorS64G-2g1JlP0ZvEJumt6hO7kvRDvuluNK6Tcl6zuk0ivnNwiOCXrGdBO4pDXCDUsMn7KKEhCdBysldpOya2zUbdvJAo-JGhRm-UZqAWXEK5-5lXOGKcPvgNDL7g',
                    width: 3968,
                },
            ],
            place_id: 'ChIJrYKdpRD2wokRvObFDYGXqy0',
            plus_code: {
                compound_code: 'R22W+7V New York',
                global_code: '87G8R22W+7V',
            },
            rating: 4.7,
            reference: 'ChIJrYKdpRD2wokRvObFDYGXqy0',
            scope: 'GOOGLE',
            types: ['mosque', 'place_of_worship', 'point_of_interest', 'establishment'],
            user_ratings_total: 53,
            vicinity: '130 W 113th St, New York',
        },
        {
            business_status: 'OPERATIONAL',
            geometry: {
                location: {
                    lat: 40.7559828,
                    lng: -73.9358081,
                },
                viewport: {
                    northeast: {
                        lat: 40.75729647989272,
                        lng: -73.93439617010728,
                    },
                    southwest: {
                        lat: 40.75459682010728,
                        lng: -73.93709582989273,
                    },
                },
            },
            icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/worship_islam-71.png',
            id: 'a3758ffadebca29df2ce00bdf6da462bb77d8c74',
            name: 'Bosnian-Herzegovinian Islamic Center of New York',
            photos: [
                {
                    height: 2988,
                    html_attributions: [
                        '\u003ca href="https://maps.google.com/maps/contrib/103034332044000258419"\u003eemir mulahmetovic\u003c/a\u003e',
                    ],
                    photo_reference:
                        'CmRaAAAAALVLM0zlmBw1NBjvlAbe4pHGaYurG5009dVsH_IuBcJYFxJNPiU0jGP6in-9ytU3QE1cliCgh8K7sY7tqPQPrxrXyiag2ulnyFF330EF9ar0WZYt1qsQ91oD7MaaMUNPEhDLAOJUt4PKBQfe3w1Jd8sUGhRmdPnx-vC_ewahc8yiqk9Ywo7Qyw',
                    width: 5312,
                },
            ],
            place_id: 'ChIJE5c5Ky1fwokRvexeB1_4tZk',
            plus_code: {
                compound_code: 'Q347+9M Long Island City, Queens, NY',
                global_code: '87G8Q347+9M',
            },
            rating: 5,
            reference: 'ChIJE5c5Ky1fwokRvexeB1_4tZk',
            scope: 'GOOGLE',
            types: ['mosque', 'place_of_worship', 'point_of_interest', 'establishment'],
            user_ratings_total: 93,
            vicinity: '37-46 Crescent St, Long Island City',
        },
        {
            business_status: 'OPERATIONAL',
            geometry: {
                location: {
                    lat: 40.758375,
                    lng: -73.91031599999999,
                },
                viewport: {
                    northeast: {
                        lat: 40.75963042989272,
                        lng: -73.90900227010728,
                    },
                    southwest: {
                        lat: 40.75693077010728,
                        lng: -73.91170192989271,
                    },
                },
            },
            icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/worship_islam-71.png',
            id: '1f0ad551959e4a074ebac2341cf348ee151c9d90',
            name: 'Masjid Al-Hikmah',
            photos: [
                {
                    height: 3024,
                    html_attributions: [
                        '\u003ca href="https://maps.google.com/maps/contrib/109291327478643627478"\u003e109291327478643627478\u003c/a\u003e',
                    ],
                    photo_reference:
                        'CmRaAAAAiKLHaVrbvygx3a5odgFAxMeoaLyZB7b0_SOogt9KuueyDEWbur3xigClVTt_vPFk9bsBQih_ibZNce42Xu_MKEsVwW5MWYIwh7F5K5SEZaB7OBxbPVhU-16CeuaAejH7EhD8t3ZvNNevrBgKnR8C77fqGhT7ML907gYi6lmSTP02vmP4u-YwXA',
                    width: 4032,
                },
            ],
            place_id: 'ChIJu78KaBZfwokR8jVdepzUW1k',
            plus_code: {
                compound_code: 'Q35Q+9V Astoria, Queens, NY',
                global_code: '87G8Q35Q+9V',
            },
            rating: 4.5,
            reference: 'ChIJu78KaBZfwokR8jVdepzUW1k',
            scope: 'GOOGLE',
            types: ['mosque', 'place_of_worship', 'school', 'point_of_interest', 'establishment'],
            user_ratings_total: 71,
            vicinity: '48-01 31st Ave, Astoria',
        },
        {
            business_status: 'OPERATIONAL',
            geometry: {
                location: {
                    lat: 40.7293685,
                    lng: -73.9837228,
                },
                viewport: {
                    northeast: {
                        lat: 40.73067812989272,
                        lng: -73.98240277010729,
                    },
                    southwest: {
                        lat: 40.72797847010727,
                        lng: -73.98510242989272,
                    },
                },
            },
            icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/worship_islam-71.png',
            id: '7b35f0a6cf48587e9ddf6dd4a9563a378584a6f6',
            name: 'Madina Masjid Islamic Council of America',
            photos: [
                {
                    height: 3480,
                    html_attributions: [
                        '\u003ca href="https://maps.google.com/maps/contrib/106441095174298626107"\u003eMohd H\u003c/a\u003e',
                    ],
                    photo_reference:
                        'CmRaAAAAkU6he0JcYWWXjuuYvu9NA_jdnEBc3zgEaEPrS1M3smN3v2jkfAn8IRMavRrQ7g9p4IWyIcU0xNPc-fkBr2wJE_hfpkBquYkd1fSjiZ7VEtni7d03SwrnswfQhlrsReQLEhC346SNgVng6jPD0FUR_glDGhTjzLK5DGtwXBMnAHZTZOPD--D-dA',
                    width: 4640,
                },
            ],
            place_id: 'ChIJ1Uxi7Z1ZwokRObI0kHr-2Uk',
            plus_code: {
                compound_code: 'P2H8+PG New York',
                global_code: '87G8P2H8+PG',
            },
            rating: 4.7,
            reference: 'ChIJ1Uxi7Z1ZwokRObI0kHr-2Uk',
            scope: 'GOOGLE',
            types: ['mosque', 'place_of_worship', 'point_of_interest', 'establishment'],
            user_ratings_total: 248,
            vicinity: '401 E 11th St, New York',
        },
    ],
}
