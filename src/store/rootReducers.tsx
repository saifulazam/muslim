import { combineReducers } from 'redux'
import auth from '../modules/Auth/redux/AuthReducer'
import user from '../modules/User/redux/UserReducer'
import location from '../modules/Location/redux/LocationReducer'
import mosques from '../modules/Mosque/redux/MosqueReducer'
import prayers from '../modules/Prayer/redux/PrayerReducer'
import welcome from '../modules/Welcome/redux/WelcomeReducer'
import forums from '../modules/Forum/redux/ForumReducer'

export default combineReducers({
    auth,
    user,
    location,
    mosques,
    prayers,
    welcome,
    forums,
})
