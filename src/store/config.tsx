import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import rootReducer from './rootReducers'
import { createLogger } from 'redux-logger'

export default function (initialState = {}) {
    // Middleware and store enhancers
    const enhancers = [applyMiddleware(thunk)]

    // Will uncomment these later. DONT DELETE

    //@ts-ignore
    if (process.env.NODE_ENV !== 'production') {
        enhancers.push(applyMiddleware(createLogger()))
        //@ts-ignore
        window.__REDUX_DEVTOOLS_EXTENSION__ && enhancers.push(window.__REDUX_DEVTOOLS_EXTENSION__())
    }

    const store = createStore(rootReducer, initialState, compose(...enhancers))

    // For hot reloading reducers
    //@ts-ignore
    if (module.hot) {
        //@ts-ignore
        module.hot.accept('./rootReducers', () => {
            const nextReducer = require('./rootReducers').default // eslint-disable-line global-require
            store.replaceReducer(nextReducer)
        })
    }

    return store
}
