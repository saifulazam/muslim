export default {
    black: '#000000',
    dimgray: '#696969',
    gray: '#808080',
    darkgray: '#A9A9A9',
    silver: '#C0C0C0',
    white: '#ffffff',
    mediumaquamarine: '#66CDAA',
    darkcyan: '#008B8B',
    lightseagreen: '#20B2AA',
    teal: '##008080',
    primary: '#6500D3',
    primary_01: '#4D00A0',
}
