import * as RootNavigation from '../navigation/NavigationRef'
import { getStorage } from './Storage'

/**
 * Its checks if the user loggedin.
 * if not, its will redriect to loginScreen
 */
export const authentication = async () => {
    const token = await getStorage('access_token')
    if (token) {
        return true
    } else {
        return RootNavigation.navigate('LoginScreen')
    }
}

export const getUserFullName = (user) => {
    return user.first_name + ' ' + user.last_name
}
