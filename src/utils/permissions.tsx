import * as Permissions from 'expo-permissions'
import { Alert, Linking } from 'react-native'
import { navigation } from '../navigation/rootNavigation'
import * as ImagePicker from 'expo-image-picker'
import Constants from 'expo-constants'

export const checkUserPermission = async (callback?: CallableFunction): Promise<boolean> => {
    let permissionStatus = false
    let { status } = await Permissions.getAsync(Permissions.LOCATION)
    if (status === 'denied') {
        Alert.alert(
            'Permission to access location was denied, Please enable location services from your phone settings',
            '',
            [
                { text: 'open settings', onPress: () => Linking.openURL('app-settings:') },
                { text: 'Cancel', onPress: () => navigation.navigate('AlmostThere') },
            ]
        )
        permissionStatus = false
    } else if (status === 'undetermined') {
        const { granted } = await Permissions.askAsync(Permissions.LOCATION)
        if (granted) {
            permissionStatus = true
        }
    } else {
        permissionStatus = true
    }
    return Promise.resolve(permissionStatus)
}

export const checkLocationPermissionStatus = async () => {
    let { status } = await Permissions.getAsync(Permissions.LOCATION)

    if (status !== 'granted') {
        return false
    } else {
        return true
    }
}

export const checkCameraGetPermissionAsync = async () => {
    if (Constants.platform.ios) {
        const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
        if (status !== 'granted') {
            Alert.alert('Sorry, we need camera roll permissions to make this work!')
        }
    }
}

//Note working
export const pickImageFromCameraRoll = async (callback?: CallableFunction): Promise<any> => {
    checkCameraGetPermissionAsync()
    try {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        })
        if (!result.cancelled) {
            //@ts-ignore
            return Promise.resolve(result.uri)
        }
    } catch (E) {
        console.warn(E)
    }
}
