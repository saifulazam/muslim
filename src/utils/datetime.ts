export const timestampToString = (create_at: number, suffix?: boolean): string => {
    let diffTime: string | number = (new Date().getTime() - (create_at || 0)) / 1000
    if (diffTime < 60) {
        diffTime = 'Just now'
    } else if (diffTime > 60 && diffTime < 3600) {
        diffTime =
            Math.floor(diffTime / 60) +
            (Math.floor(diffTime / 60) > 1 ? (suffix ? ' minutes' : 'm') : suffix ? ' minute' : 'm') +
            (suffix ? ' ago' : '')
    } else if (diffTime > 3600 && diffTime / 3600 < 24) {
        diffTime =
            Math.floor(diffTime / 3600) +
            (Math.floor(diffTime / 3600) > 1 ? (suffix ? ' hours' : 'h') : suffix ? ' hour' : 'h') +
            (suffix ? ' ago' : '')
    } else if (diffTime > 86400 && diffTime / 86400 < 30) {
        diffTime =
            Math.floor(diffTime / 86400) +
            (Math.floor(diffTime / 86400) > 1 ? (suffix ? ' days' : 'd') : suffix ? ' day' : 'd') +
            (suffix ? ' ago' : '')
    } else {
        diffTime = new Date(create_at || 0).toDateString()
    }
    return diffTime
}

function formatDate(date) {
    var d = new Date(date)
    var hh = d.getHours()
    var m = d.getMinutes()
    var s = d.getSeconds()
    var dd = 'AM'
    var h = hh
    if (h >= 12) {
        h = hh - 12
        dd = 'PM'
    }
    if (h == 0) {
        h = 12
    }
    m = m < 10 ? '0' + m : m

    s = s < 10 ? '0' + s : s

    /* if you want 2 digit hours:
    h = h<10?"0"+h:h; */

    var pattern = new RegExp('0?' + hh + ':' + m + ':' + s)

    var replacement = h + ':' + m
    /* if you want to add seconds
    replacement += ":"+s;  */
    replacement += ' ' + dd

    return date.replace(pattern, replacement)
}

function tConvert(time) {
    // Check correct time format and split into components
    time = time?.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time]

    if (time.length > 1) {
        // If time format correct
        time = time.slice(1) // Remove full string match value
        time[5] = +time[0] < 12 ? ' AM' : ' PM' // Set AM/PM
        time[0] = +time[0] % 12 || 12 // Adjust hours
    }
    return time.join('') // return adjusted time or original string
}

function time12hWithoutAmPm(time) {
    // Check correct time format and split into components
    time = time?.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time]

    if (time.length > 1) {
        // If time format correct
        time = time.slice(1) // Remove full string match value
        time[5] = +time[0] < 12 ? '' : '' // Set AM/PM
        time[0] = +time[0] % 12 || 12 // Adjust hours
    }
    return time.join('') // return adjusted time or original string
}

function convertTime12to24(value) {
    const [time, modifier] = value.split(' ')
    let [hours, minutes] = time.split(':')
    if (hours === '12') {
        hours = '00'
    }

    if (modifier === 'PM') {
        hours = parseInt(hours, 10) + 12
    }

    return `${hours}:${minutes}:00`
}

function timeSplit(time, split) {
    const cTime = convertTime12to24(time)
    let [hours, minutes, seconds] = cTime.split(':')
    if (split === 'h') {
        return hours
    } else if (split === 'm') {
        return minutes
    } else if (split === 's') {
        return seconds
    }
}

const wait = (timeout) => {
    return new Promise((resolve) => {
        setTimeout(resolve, timeout)
    })
}

export { formatDate, tConvert, convertTime12to24, timeSplit, time12hWithoutAmPm, wait }
