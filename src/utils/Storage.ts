import AsyncStorage  from '@react-native-community/async-storage'

export const setStorage = async (key:string, value: boolean | string): Promise<void> => {
    if(typeof value === 'boolean'){
        value = value ? 'true' || 'yes' : 'false'
    }
    try {
        return AsyncStorage.setItem(key, value)
    } catch (e) {
        console.warn(`Unable to save ${key} to local storage, ${e}`)
    }
}

export const multiSetStorage = async (key:string[][], callback?: (errors?: Error[]) => void): Promise<void> => {
    try {
        return AsyncStorage.multiSet(key, callback)
    } catch (e) {
        console.warn(`Unable to save ${key} to multiset local storage, ${e}`)
    }
}

export const multiGetStorage = async (keys:string[], callback?: (errors?: Error[], result?: [string, string | null][]) => void): Promise<[string, string | null][]> => {
    try {
        return AsyncStorage.multiGet(keys, callback)
    } catch (e) {
        console.warn(`Unable to get ${keys} to multiGet local storage, ${e}`)
    }
}

export const getStorage = async (key: string): Promise<boolean|string> => {
    try {
        const value = await AsyncStorage.getItem(key)
        if (value  ==='true' || value === 'false') {
            return value === 'true'
        }
        return value
    } catch (e) {
        console.warn(`Unable to retrieve ${key} from local storage, ${e}`)
    }
}

export const removeFromStorage = async(key: string): Promise<void> =>{
    try{
        await AsyncStorage.removeItem(key)
    } catch(e){
        console.warn(`Unable to remove ${key} from local storage, ${e}`)
    }
}
