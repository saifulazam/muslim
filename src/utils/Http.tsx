import axios from 'axios'
import store from '../store'
import { authLogout } from '../modules/Auth/redux/AuthAction'
import { BASE_URL, API_VERSION } from 'react-native-dotenv'

const API_URL = `${BASE_URL}/api/${API_VERSION}/client`

axios.defaults.baseURL = API_URL
axios.defaults.headers.common.Accept = 'application/json'
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
axios.defaults.headers.common['Content-Type'] = 'application/json'

axios.interceptors.response.use(
    (response) => response,
    (error) => {
        if (error.response.status === 401) {
            // store.dispatch(authLogout())
        }
        return Promise.reject(error)
    }
)

export default axios
