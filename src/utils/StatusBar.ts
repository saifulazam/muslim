import { StatusBar } from 'react-native'

export const CustomStatusBar = ({ addListener }, style: 'default' | 'light-content' | 'dark-content' = 'default') => {
    addListener('focus', () => {
        StatusBar.setBarStyle(style, true)
    })
}
