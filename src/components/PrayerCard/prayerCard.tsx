import React, { useState, useEffect, useRef } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Alert } from 'react-native'
import { convertTime12to24, timeSplit } from '../../utils/datetime'
import Icon from '../../common/Icon'
import _Color from '../../utils/Color'
import TimeCountdown from '../TimeCountdown'

const PrayerCard = (props) => {
    const { time, name, now } = props
    const [showPrimaryTime, setShowPrimaryTime] = useState(false)
    const monthNames = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
    ]
    if (now) {
        const currentDate = new Date()
        const date = currentDate.getDate()
        const month = currentDate.getMonth()
        const year = currentDate.getFullYear()
        const monthDateYear = monthNames[month] + ' ' + date + ', ' + year

        const countEnd = new Date(`${monthDateYear} ${convertTime12to24(time)}`).getTime()
        return (
            <View
                style={{
                    backgroundColor: '#1ee592',
                    width: 198,
                    height: 50,
                    borderRadius: 25,
                    display: 'flex',
                    flexDirection: 'row',
                    marginStart: 10,
                }}
            >
                <TouchableOpacity
                    onPress={() => setShowPrimaryTime(showPrimaryTime ? false : true)}
                    style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
                >
                    <Text
                        style={{
                            fontSize: 16,
                            fontWeight: 'bold',
                            letterSpacing: -0.4,
                            color: 'black',
                            fontFamily: 'AvantGardeLT-Demi',
                        }}
                    >
                        {showPrimaryTime ? (
                            time
                        ) : (
                            <TimeCountdown finishTime={Math.round(countEnd / 1000)} format="{h}:{m}:{s}" />
                        )}
                    </Text>
                    <Text style={{ fontWeight: '500', fontSize: 11 }}>
                        {showPrimaryTime ? `Upcoming: ${name}` : `Remaining for ${name}`}
                    </Text>
                </TouchableOpacity>
                <View
                    style={{
                        width: 49,
                        height: 50,
                        borderTopEndRadius: 25,
                        borderBottomEndRadius: 25,
                        backgroundColor: '#15b975',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                >
                    <TouchableOpacity
                        onPress={() =>
                            props.navigation.navigate('QiblaDirection', {
                                location: props.location,
                                geocode: props.geocode,
                            })
                        }
                    >
                        <Icon name="qibla" width="27" />
                    </TouchableOpacity>
                </View>
            </View>
        )
    } else {
        return (
            <View
                style={{
                    backgroundColor: 'rgba(118, 118, 128, 0.24)',
                    width: 121,
                    height: 50,
                    borderRadius: 25,
                    display: 'flex',
                    flexDirection: 'row',
                    marginStart: 10,
                }}
            >
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text
                        style={{
                            fontSize: 16,
                            fontWeight: 'bold',
                            letterSpacing: -0.4,
                            color: 'white',
                            fontFamily: 'AvantGardeLT-Demi',
                        }}
                    >
                        {time}
                    </Text>
                    <Text style={{ fontWeight: '500', fontSize: 11, color: '#ffffff' }}>
                        {capitalizeFirstLetter(name)}
                    </Text>
                </View>
            </View>
        )
    }
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1)
}

export default PrayerCard

const styles = StyleSheet.create({})
