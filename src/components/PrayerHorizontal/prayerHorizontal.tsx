import React from 'react'
import { ScrollView, ActivityIndicator, Alert } from 'react-native'
import PrayerCard from '../PrayerCard/prayerCard'
import { connect } from 'react-redux'
import { StackNavigationProp } from '@react-navigation/stack'

interface Props {
    navigation?: StackNavigationProp<any>
    prayers?: any
    nextPrayers?: any
}

interface State {}

export class PrayerHorizontal extends React.Component<Props, State> {
    constructor(props) {
        super(props)
        this.state = {}
    }

    render() {
        return (
            <ScrollView
                horizontal={true}
                decelerationRate={1}
                snapToInterval={200}
                snapToAlignment={'center'}
                showsHorizontalScrollIndicator={false}
            >
                <this.CurrentTime />
                <this.AllTimes />
            </ScrollView>
        )
    }

    public handleUnavailableTime = (prayer): { defaultName: string; defaultTime: string } => {
        let defaultName = ''
        let defaultTime = ''
        if (prayer?.length > 0) {
            defaultTime = prayer[0][1].time
            defaultName = prayer[0][0]
        }
        return { defaultName, defaultTime }
    }

    public CurrentTime = (): JSX.Element => {
        if (!this.props.prayers) {
            return <ActivityIndicator />
        }
        const value = this._omit(this.props.nextPrayers) ?? []
        const now = !!value
        const { defaultName, defaultTime } = this.handleUnavailableTime(this.props.prayers)
        return (
            <PrayerCard
                key={Math.random()}
                name={value.length > 0 ? `${value[0][0]}` : `${defaultName}`}
                time={value.length > 0 ? value[0][1]['time'] : defaultTime}
                now={now}
                {...this.props}
            />
        )
    }

    public AllTimes = (): JSX.Element => {
        const formatted: any[] = this._omit(this.props.prayers)
        // @ts-ignore
        return formatted?.map((item, index) => {
            return <PrayerCard key={Math.random()} name={item[0] ?? ''} time={item[1]['time'] ?? ''} />
        })
    }

    public _omit = (item: any[] = []): any[] => {
        const excludedItems = ['sunset', 'sunrise', 'imsak']
        const filter = item?.filter((i) => {
            return !excludedItems.includes(i[0])
        })

        return filter
    }
}

const mapStateToProps = (state: any) => {
    return {
        prayers: state.prayers.prayers,
        nextPrayers: state.prayers.nextPrayer,
    }
}

export default connect(mapStateToProps)(PrayerHorizontal)
