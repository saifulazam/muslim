import React, { Component } from 'react'
import { Text, View, StyleSheet, Modal } from 'react-native'
interface Props {
    modalVisible: boolean
}

interface State {}

export default class CustomModal extends Component<Props> {
    render() {
        return (
            <View style={styles.container}>
                <Modal transparent={false} visible={this.props.modalVisible}>
                    <View style={styles.modal}>
                        <View style={styles.modalContainer}>
                            <View style={styles.modalBody}>{this.props.children}</View>
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    modal: {
        backgroundColor: '#00000099',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    modalContainer: {
        backgroundColor: '#f9fafb',
        width: '80%',
        borderRadius: 14,
    },
    modalHeader: {},
    title: {
        fontWeight: 'bold',
        fontSize: 20,
        padding: 15,
        color: '#000',
    },
    divider: {
        width: '100%',
        height: 1,
        backgroundColor: 'lightgray',
    },
    modalBody: {
        backgroundColor: '#fff',
        paddingVertical: 20,
        paddingHorizontal: 10,
        borderRadius: 14,
    },
    modalFooter: {},
    actions: {
        borderRadius: 5,
        marginHorizontal: 10,
        paddingVertical: 10,
        paddingHorizontal: 20,
    },
    actionText: {
        color: '#fff',
    },
})
