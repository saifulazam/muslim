import React, { Component } from 'react'
import { Modal, StyleSheet, TouchableOpacity, View, Text } from 'react-native'
import ActionButton from '../../common/ActionButton'

interface Props {
    modalVisitble: boolean
    onCancel?: any
}

interface State {}

export default class ActionModal extends Component<Props, State> {
    render() {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.props.modalVisitble}
                onRequestClose={this.props.onCancel}
            >
                <View style={styles.modalContainer}>
                    <TouchableOpacity style={styles.container} onPress={this.props.onCancel}></TouchableOpacity>

                    <View style={[styles.modalView]}>
                        {this.props.children}

                        <ActionButton onPress={this.props.onCancel} name="Cancel" color="gray" />
                    </View>
                </View>
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    modalContainer: {
        flex: 1,
        padding: 0,
        paddingBottom: 0,
        justifyContent: 'flex-end',
        backgroundColor: 'rgba(0,0,0,0.3)',
    },

    modalView: {
        margin: 0,
        backgroundColor: 'rgba(255,255,255, 1)',
        // borderRadius: 20,
        // padding: 35,
        paddingBottom: 35,
        // alignItems: 'center',
        shadowColor: '#000',
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
})
