import React from 'react'
import { View, Text, Image } from 'react-native'
import moment from 'moment'

const CommentCard = ({ comments }) => {
    if (comments.length < 1) {
        return <Text style={{ alignSelf: 'center', padding: 10 }}>Be the first answer</Text>
    }
    return comments.map((comment) => {
        return (
            <View key={comment.id} style={{ paddingBottom: 20 }}>
                <View style={{ flexDirection: 'row' }}>
                    <Image style={{ width: 50, height: 50, borderRadius: 25 }} source={{ uri: comment.user.avatar }} />
                    <View style={{ paddingLeft: 10 }}>
                        <Text style={{ fontSize: 20 }}>{comment.user.name}</Text>
                        <Text style={{ color: 'gray' }}>{moment(comment.added).fromNow()}</Text>
                    </View>
                </View>
                <Text style={{ padding: 10 }}>{comment.message}</Text>
                <View style={{ borderBottomColor: 'rgba(0,0,0, 0.2)', borderBottomWidth: 0.5, paddingVertical: 10 }} />
            </View>
        )
    })
}

export default CommentCard
