import * as React from 'react'
import { Text } from 'react-native'
import { time12hWithoutAmPm } from '../utils/datetime'

export interface Props {
    finishTime: any
    format?: any
}

export interface State {
    millisecondsLeft: any
    show: any
}

export default class TomeCountdown extends React.Component<Props, State, any> {
    constructor(props: Props) {
        super(props)
        this.left = null
        this.state = {
            millisecondsLeft: this.props.finishTime * 1000 - new Date().getTime(),
            show: '',
        }
    }

    public componentDidMount() {
        this.left = setInterval(() => {
            this.setState({ millisecondsLeft: this.props.finishTime * 1000 - new Date().getTime() }) // change the number of the milliseconds
            this.millisecondsToString() // calls the method that converts the milliseconds into a readable string
        }, 1000) // this methods is being invocated every second (1000 milliseconds)
    }

    public componentWillUnmount() {
        clearInterval(this.left)
    }

    public checkNegative = (milliseconds: number): number => {
        const convertT0String = String(milliseconds).includes('-')
        if (convertT0String) {
            const trim = String(milliseconds).replace('-', '').trim()
            return Number(trim) / 1000
        }
        return milliseconds
    }

    public millisecondsToString = () => {
        // the methods that converts the milliseconds remaining to a readable string
        const seconds = this.state.millisecondsLeft / 1000 - ((this.state.millisecondsLeft / 1000) % 1) // seconds
        const minutes = seconds / 60 - ((seconds / 60) % 1) // minutes
        const hours = minutes / 60 - ((minutes / 60) % 1) // hours
        const days = hours / 24 - ((hours / 24) % 1) // days

        const removeNegative = {
            seconds: Number(String(seconds).replace('-', '')),
            hours: Number(String(hours).replace('-', '')),
            minutes: Number(String(minutes).replace('-', '')),
            days: Number(String(days).replace('-', '')),
        }
        if (this.checkNegative(this.state.millisecondsLeft) < 0) {
            // if the countdown is finished
            this.setState({
                show: this.props.format
                    .replace('{d}', 0)
                    .replace('{h}', 0)
                    .replace('{m}', this.addZeroIfNeeded(0))
                    .replace('{s}', this.addZeroIfNeeded(0)),
            })
        } else {
            this.setState({
                show: this.props.format
                    .replace('{d}', removeNegative.days)
                    .replace('{h}', removeNegative.hours % 24)
                    .replace('{m}', this.addZeroIfNeeded(removeNegative.minutes % 60))
                    .replace('{s}', this.addZeroIfNeeded(removeNegative.seconds % 60)),
            })
        }
    }

    addZeroIfNeeded = (num) => {
        // if the number of hours, minutes or seconds (in the string) is less than 10
        if (num < 10) {
            return `0${num}`
        } else {
            return num
        }
    }

    public render() {
        return <Text>{time12hWithoutAmPm(this.state.show)}</Text>
    }
}
