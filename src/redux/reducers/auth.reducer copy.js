import { authType } from '../types'
import { fromJS } from 'immutable'

const INITIAL_STATE = {
    loggedIn: false,
    loading: false,
    error: '',
    token: null,
    logout: false,
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case authType.LOGIN_REQUEST:
            return { ...state, loading: true }
        case authType.LOGIN_SUCCESS:
            return { ...state, loggedIn: true, loading: false, token: action.payload }
        case authType.LOGIN_FAILED:
            return { ...state, loading: false, loggedIn: false, error: action.payload }
        case authType.LOGIN_OUT:
            return { ...state, loggedIn: false, token: null, logout: true }
        default:
            return state
    }
}
