import { getStorage, removeFromStorage, setStorage } from '../../utils/Storage'
import { authType } from '../types'

const INITIAL_STATE = {
    isAuthenticated: false,
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case authType.AUTH_LOGIN:
            setStorage('accessToekn', JSON.stringify(action.payload))
            return { ...state, isAuthenticated: true }
        default:
            return state
    }
}

export const login = (state, payload) => {
    setStorage('accessToekn', JSON.stringify(payload))
    return { ...state, isAuthenticated: true }
}

export const checkAuth = (state) => {
    state = Object.assign({}, state, {
        isAuthenticated: !!getStorage('accessToekn'),
    })

    if (state.isAuthenticated) {
        return true
    }

    return state
}

export const logout = (state) => {
    removeFromStorage(accessToekn)

    return { ...state, isAuthenticated: false }
}

export const authError = (state, payload) => {
    return { ...state, errorMessage: payload, isLoading: false }
}

export const getAuth = (state) => state.auth.isAuthenticated
