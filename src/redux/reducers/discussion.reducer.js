import { discussionType } from '../types'

const INITIAL_STATE = {
    loading: false,
    discussions: [],
    error: '',
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case discussionType.DISCUSSION_ALL_REQUEST:
            return { ...state, loading: true }
        case discussionType.DISCUSSION_ALL_SUCCESS:
            return { ...state, loading: false, discussions: action.payload }
        default:
            return state
    }
}
