import { combineReducers } from 'redux'
import mosques from './mosque.reducer'
import auth from './auth.reducer'
import discussion from './discussion.reducer'
import prayerTimes from './prayerTimes.reducer'
import locationData from './location.reducer'
import user from './user.reducer'

const rootReducer = combineReducers({
    mosques,
    auth,
    discussion,
    prayerTimes,
    locationData,
    user
})

export default rootReducer
