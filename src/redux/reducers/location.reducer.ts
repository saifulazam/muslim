import { LOCATION_DATA, UNAUTHORIZED_LOCATION_DATA } from '../actions/location.action'

const INITIAL_STATE = {
    locationData: {},
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case LOCATION_DATA:
            return { ...state, locationData: action.payload }
        case UNAUTHORIZED_LOCATION_DATA:
            return { ...state, locationData: action.payload }
        default:
            return state
    }
}
