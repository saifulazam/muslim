import { mosqueType } from '../types'

const INITIAL_STATE = {
    mosques: [],
    homeMosques: [],
    loading: false,
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case mosqueType.NEARBY_VRIFIED_MOSQUE_REQUEST:
            return { ...state, loading: true }
        case mosqueType.NEARBY_VRIFIED_MOSQUE_SUCCESS:
            return { ...state, homeMosques: action.payload, loading: false }
        case mosqueType.NEARBY_MOSQUE_REQUEST:
            return { ...state, loading: true }
        case mosqueType.NEARBY_MOSQUE_SUCCESS:
            return { ...state, mosques: action.payload, loading: false }
        default:
            return state
    }
}
