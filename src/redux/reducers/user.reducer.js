import { userType } from '../types'

const INITIAL_STATE = {
    user: {},
    isLoading: false,
    errors: '',
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case userType.USER_REQUEST:
            return { ...state, isLoading: true }
        case userType.USER_SUCCESS:
            return { ...state, user: action.payload, isLoading: false }
        case userType.USER_FAILED:
            return { ...state, errors: action.payload, isLoading: false }
        default:
            return state
    }
}
