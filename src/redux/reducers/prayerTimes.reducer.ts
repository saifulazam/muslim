import { PRAYER_TIMES, ALL_PRAYER_TIMES } from '../actions/prayerTimes.action'
import moment from 'moment'

const INITIAL_STATE = {
    prayerTimes: [],
    allPrayerTimes: [],
    loading: false,
}

const prayerTimes = (times: any) => {
    const objToArr = Object.entries(times)
    let prayers = []
    let nextprayerTime = []
    var currentTime = moment()
    objToArr?.map((item: any, index) => {
        const serv = item && item[1].time
        prayers.push(item[1].time)
        if (moment(serv, 'hh:mm A') > moment(currentTime, 'hh:mm A')) {
            nextprayerTime.push(item)
            return nextprayerTime
        }
        return nextprayerTime
    })
    return nextprayerTime
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case PRAYER_TIMES:
            return {
                ...state,
                prayerTimes: prayerTimes(action.payload),
                loading: true,
            }
        case ALL_PRAYER_TIMES:
            return {
                ...state,
                allPrayerTimes: Object.entries(action.payload),
                loading: true,
            }
        default:
            return state
    }
}
