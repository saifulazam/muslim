import { userType } from '../types'
import { API } from 'react-native-dotenv'
import { ServiceExecutorPrivateGet } from '../../services/ServiceExecutor'
import { getStorage } from '../../utils/Storage'

export const getAuthUser = () => {
    return async (dispatch) => {
        dispatch({ type: userType.USER_REQUEST })

        try {
            const url = `${API}/auth/user`
            const token = await getStorage('accessToekn')
            const requestOptions = {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            }
            return fetch(url, {
                method: 'GET',
                headers: headers,
            })
                .then((response) => response.json())
                .then((result) => {
                    if (result.success) {
                        dispatch({
                            type: userType.USER_SUCCESS,
                            payload: result.data,
                        })
                    } else {
                        dispatch({
                            type: userType.USER_FAILED,
                            payload: result,
                        })
                    }
                })
        } catch (err) {
            console.warn(err)
        }
    }
}
