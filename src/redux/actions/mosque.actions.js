import { mosqueType } from '../types'
import { API } from 'react-native-dotenv'

export const getNearbyVirefiedMossque = (latitude, longitude) => {
    return async (dispatch) => {
        dispatch({ type: mosqueType.NEARBY_VRIFIED_MOSQUE_REQUEST })

        try {
            const url = `${API}/mosques/find/nearby/verified?limit=10&latitude=${latitude}&longitude=${longitude}&radius=10`
            let response = await fetch(`${url}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                },
            })
            const results = await response.json()
            dispatch({
                type: mosqueType.NEARBY_VRIFIED_MOSQUE_SUCCESS,
                payload: results.data,
            })
        } catch (err) {
            console.warn(err)
        }
    }
}

export const getNearByAllMosques = (latitude, longitude) => {
    return async (dispatch) => {
        dispatch({ type: mosqueType.NEARBY_MOSQUE_REQUEST })

        try {
            const url = `${API}/mosques/find/nearby?limit=20&latitude=${latitude}&longitude=${longitude}&radius=10`
            let response = await fetch(`${url}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                },
            })
            const results = await response.json()
            dispatch({
                type: mosqueType.NEARBY_MOSQUE_SUCCESS,
                payload: results.data,
            })
        } catch (err) {
            console.warn(err)
        }
    }
}
