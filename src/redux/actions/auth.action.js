import { authType } from '../types'
import { API } from 'react-native-dotenv'
import { getStorage, removeFromStorage, setStorage } from '../../utils/Storage'

export const authCheck = () => {
    return { type: authType.AUTH_CHECK }
}

export const authLogin = (payload) => {
    return (dispatch) => {
        dispatch({
            ttype: authType.AUTH_LOGIN,
            payload,
        })
    }
}

export const authLogout = () => {
    return { type: authType.AUTH_LOGOUT }
}

export const authRefreshToken = (payload) => {
    return { type: authType.AUTH_REFRESH_TOKEN }
}

export const authResetPassword = () => {
    return { type: authType.AUTH_RESET_PASSWOOD }
}

export const authUser = (payload) => {
    return { type: authType.AUTH_USER, payload }
}

export const authError = (payload) => {
    return { type: authType.AUTH_ERROR, payload }
}

/**
 * Not useing
 * @param {*} email
 * @param {*} password
 * @param {*} navigation
 */
export const loginUser = (email, password, navigation) => {
    return async (dispatch) => {
        try {
            dispatch({ type: authType.LOGIN_REQUEST })

            let headers = {
                'Content-Type': 'application/json',
                Accept: 'application/json',
            }

            let body = {
                username: email,
                password: password,
            }

            const url = `${API}/auth/login`

            fetch(url, {
                method: 'POST',
                headers: headers,
                body: JSON.stringify(body),
            })
                .then((response) => response.json())
                .then((json) => {
                    if (json.hasOwnProperty('message')) {
                        dispatch({
                            type: authType.LOGIN_FAILED,
                            payload: json.message,
                        })
                    }
                    if (json.hasOwnProperty('error')) {
                        dispatch({
                            type: authType.LOGIN_FAILED,
                            payload: json.error,
                        })
                    } else {
                        dispatch({
                            type: authType.LOGIN_SUCCESS,
                            payload: json.access_token,
                        })
                        setStorage('token', JSON.stringify(json.access_token))
                        navigation.navigate('Discover')
                    }
                })
        } catch (err) {
            console.warn(err)
        }
    }
}

export const logoutCurrent = (navigation) => {
    return async (dispatch) => {
        try {
            const url = `${API}/auth/logout`

            let headers = {
                Authorization: `Bearer ${_retrieveToken}`,
                'Content-Type': 'application/json',
                Accept: 'application/json',
            }

            return fetch(url, {
                method: 'GET',
                headers: headers,
            })
                .then((response) => response.json())
                .then((json) => {
                    removeFromStorage('token')
                    if (json.hasOwnProperty('message') == 'Successfully logged out') {
                        dispatch({ type: authType.LOGIN_OUT })

                        navigation.navigate('Discover')
                    }
                })
        } catch (err) {
            console.warn(err)
        }
    }
}

const _retrieveToken = async () => {
    try {
        const value = await getStorage('token')
        if (value !== null) {
            return value
        }
    } catch (error) {
        // Error retrieving data
    }
}
