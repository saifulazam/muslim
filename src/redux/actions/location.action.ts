import { Dispatch } from 'redux'
import * as Location from 'expo-location'
import { ServiceExecutorGet } from '../../services/ServiceExecutor'
import * as LocationAction from '../../modules/Location/redux/LocationAction'

export const LOCATION_DATA = 'LOCATION_DATA'
export const UNAUTHORIZED_LOCATION_DATA = 'UNAUTHORIZED_LOCATION_DATA'

export const onPermissionGrantedAction = () => {
    return async (dispatch: Dispatch) => {
        await getLocation()
            .then((res: any) => {
                dispatch(locationData(res))
                // dispatch(LocationAction.insertLocationCoords({ latitude: res.coords.lat, longitude: res.coords.lng }))
                //dispatch(LocationAction.insertLocationAddress(res.completeAddress))
            })
            .catch((err) => {
                console.warn(err)
            })
    }
}

export const onPermissionDeniedAction = (item) => {
    return async (dispatch: Dispatch) => {
        const coords = {
            lat: item.geometry.coordinates[1],
            lng: item.geometry.coordinates[0],
        }
        const address = item.context[0].text + ', ' + item.context[2].text

        dispatch(unauthorizedLocationData(coords, address))
    }
}

const getUnauthorisedCoords = async (): Promise<any> => {
    let closestLocation
    await ServiceExecutorGet('https://ipinfo.io/geo', (resp) => {
        const splitingCoords = resp.loc.split(',')
        closestLocation = {
            latitude: splitingCoords[0],
            longitude: splitingCoords[1],
            city: resp.city,
            region: resp.region,
        }
    })
    return Promise.resolve(closestLocation)
}

const getLocation = async () => {
    let location = await Location.getCurrentPositionAsync({ accuracy: Location.Accuracy.Highest })
    const { latitude, longitude } = location.coords
    const address = await Location.reverseGeocodeAsync({ latitude, longitude })
    const addressItem = address[0]
    const formatAddress = `${addressItem.city}, ${addressItem.region} ${addressItem.postalCode}`
    return {
        completeAddress: addressItem,
        address: formatAddress,
        addressItem: [formatAddress],
        coords: { lat: latitude, lng: longitude },
    }
}

export const locationData = (data: any) => ({
    type: LOCATION_DATA,
    payload: data,
})

export const unauthorizedLocationData = (coords: any, address: any) => ({
    type: UNAUTHORIZED_LOCATION_DATA,
    payload: {
        coords,
        address,
    },
})
