import { discussionType } from '../types'
import { API } from 'react-native-dotenv'

export const geAlltDiscussion = (page) => {
    return async (dispatch) => {
        try {
            dispatch({ type: discussionType.DISCUSSION_ALL_REQUEST })
            const url = `${API}/discussions/all?page=${page}`
            let headers = {
                'Content-Type': 'application/json',
                Accept: 'application/json',
            }

            fetch(url, {
                method: 'GET',
                headers: headers,
            })
                .then((response) => response.json())
                .then((json) => {
                    dispatch({
                        type: discussionType.DISCUSSION_ALL_SUCCESS,
                        payload: json.data,
                    })
                })
        } catch (err) {
            console.warn(err)
        }
    }
}
