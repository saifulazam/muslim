import { getPrayerTime } from '../services'
import { Dispatch } from 'redux'

export const PRAYER_TIMES = 'PRAYER_TIMES'
export const ALL_PRAYER_TIMES = 'ALL_PRAYER_TIMES'
export const DATA_LOADING = 'DATA_LOADING'

export const fetchPrayerTimes = (latitude, longitude, timezone?) => {
    return (dispatch: Dispatch) => {
        dispatch(loading(true))
        getPrayerTime(latitude, longitude, timezone)
            .then((res: any) => {
                dispatch(prayerTimes(res))
                dispatch(allPrayerTimes(res))
                dispatch(loading(false))
            })
            .catch((err) => {
                dispatch(loading(false))
            })
    }
}

export const loading = (loader: boolean) => ({
    type: DATA_LOADING,
    payload: loader,
})

const prayerTimes = (data: any[]) => ({
    type: PRAYER_TIMES,
    payload: data,
})

const allPrayerTimes = (data: any[]) => ({
    type: ALL_PRAYER_TIMES,
    payload: data,
})
