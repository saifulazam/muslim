import { ServiceExecutorPost, ServiceExecutorPrivateGet } from './serviceExecutor'
import { API } from 'react-native-dotenv'
import * as authAction from '../actions/auth.action'

export const fetchUser = async () => {
    ServiceExecutorPrivateGet(`${API}/auth/user`, fetchUserResponseCallback)
}

export const fetchUserResponseCallback = (response) => {
    return (dispatch) => {
        if (response.success) {
            dispatch(authAction.authUser(response.data))
        }
    }
}

export const login = (credentials) => {
    const url = `${API}/auth/login`
    ServiceExecutorPost(url, credentials, LoginResponseCallback)
}

export const LoginResponseCallback = (response) => {
    return (dispatch) => {
        if (response.access_token) {
            dispatch(authAction.authLogin(response.access_token))
        } else if (response.message) {
            dispatch(authAction.authError('The email or password you entered do not match. Please try again.'))
            console.warn(response.message)
        } else {
            console.warn(response)
        }
    }
}
