import { API } from 'react-native-dotenv'

export const mosquesSerives = {
    getMosqueNearBy,
    getVerifiedMosqueNearBy,
}

function getMosqueNearBy(latitude, longitude) {
    const url = `${API}/mosques/find/nearby?limit=20&latitude=${latitude}&longitude=${longitude}&radius=10`
    const requestOptions = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
        },
    }

    return fetch(`${url}`, requestOptions)
        .then((res) => res.json())
        .then((results) => {
            return results
        })
}

function getVerifiedMosqueNearBy(latitude, longitude) {
    const url = `${API}/mosques/find/nearby?limit=10&latitude=${latitude}&longitude=${longitude}&radius=5`
    const requestOptions = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
        },
    }

    return fetch(`${url}`, requestOptions)
        .then((res) => res.json())
        .then((results) => {
            return results
        })
}
