import { GOOGLE_API } from 'react-native-dotenv'

export const gooleMosqueService = {
    getMosqueNearBy,
    getMosquDetailsByPlaceId,
}

function getMosqueNearBy(latitude, longitude, radius = 20000) {
    // const url = `https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${latitude},${longitude}&radius=${radius}&type=mosques&keyword=mosques&key=${GOOGLE_API}`;

    const requestOptions = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        },
    }

    return fetch(`${url}`, requestOptions)
        .then(handleReponse)
        .then((result) => {
            return result
        })
}

function getMosquDetailsByPlaceId(place_id) {
    // const url = `https://maps.googleapis.com/maps/api/place/details/json?place_id=${place_id}&fields=name,rating,formatted_phone_number&key=${GOOGLE_API}`;
    const requestOptions = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        },
    }

    return fetch(`${url}`, requestOptions)
        .then(handleReponse)
        .then((result) => {
            return result
        })
}

function handleReponse(response) {
    return response.text().then((text) => {
        const data = text && JSON.parse(text)
        if (!response.ok) {
            if (response.status === 401) {
                console.warn('ServerSide error')
            }
            const error = (data && data.error) || response.statusText
            return Promise.reject(error)
        }
        return data
    })
}
