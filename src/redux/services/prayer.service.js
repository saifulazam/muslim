import { API } from 'react-native-dotenv'

export const prayerSerives = {
    getPrayerTime,
}

export function getPrayerTime(latitude, longitude, timezone) {

    const url = `${API}/prayer/time?latitude=${latitude}&longitude=${longitude}&timezone=${timezone}`
    const requestOptions = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
        },
    }

    return fetch(`${url}`, requestOptions)
        .then((res) => res.json())
        .then((results) => {
            return results
        })
}
