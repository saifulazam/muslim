import { PUSHER_API } from 'react-native-dotenv'
import Pusher from 'pusher-js/react-native'

export const pusherServiceExecutorGet = (channel: string, event: string) => {
    var pusher = new Pusher(PUSHER_API, {
        cluster: 'us2',
    })

    var pusherChannel = pusher.subscribe(`${channel}`)
    pusherChannel.bind(`${event}`, function (response) {
        return response
    })
}
