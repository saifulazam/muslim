import { compose, applyMiddleware, createStore } from 'redux'
import thunk from 'redux-thunk'
// import { persistStore, persistReducer } from "redux-persist";
// import storage from "redux-persist/lib/storage";
import combineReducers from './reducers'

// const persistConfig = {
//   key: "app",
//   storage
// };

const middlewares = [thunk]

// Will uncomment these later. DONT DELETE

//@ts-ignore
if (process.env.NODE_ENV === `development`) {
    // const { logger } = require(`redux-logger`)
    // middlewares.push(logger)
}

// export default function configureStore() {
const enhancer = compose(applyMiddleware(...middlewares))
//   const persistedReducer = persistReducer(persistConfig, combineReducers);
//   const store = createStore(persistedReducer, enhancer);
//   const persistor = persistStore(store);
//   return { store, persistor };
// }

export const store = createStore(combineReducers, enhancer)
