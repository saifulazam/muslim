import { createStackNavigator, StackNavigationOptions, TransitionPresets } from '@react-navigation/stack'
import React from 'react'
import DiscussionDetailScreen from '../../Discussion/Screens/DiscussionDetailScreen'
import MosqueDetailsScreen from '../../Mosque/Screens/MosqueDetailsScreen'
import DiscoverScreen from '../Screens/DiscoverScreen'

export type MainStackParamList = {
    InitialAuth: undefined
    Discover: undefined
    QiblaDirection: undefined
    SetLocation: undefined
    MosqueDetails: undefined
    MosqueDonation: undefined
    PodcastDetails: undefined
    DiscussionDetail: undefined
}

const Stack = createStackNavigator<MainStackParamList>()

const DiscoverStack = () => {
    const navigationOptions: StackNavigationOptions = {
        headerShown: false,
        gestureEnabled: false,
    }

    return (
        <Stack.Navigator screenOptions={navigationOptions}>
            <Stack.Screen
                name="Discover"
                component={DiscoverScreen}
                options={{
                    ...TransitionPresets.DefaultTransition,
                }}
            />

            <Stack.Screen
                name="MosqueDetails"
                component={MosqueDetailsScreen}
                options={{
                    ...TransitionPresets.DefaultTransition,
                }}
            />
            <Stack.Screen
                name="DiscussionDetail"
                component={DiscussionDetailScreen}
                options={{
                    ...TransitionPresets.DefaultTransition,
                }}
            />
        </Stack.Navigator>
    )
}
export default DiscoverStack
