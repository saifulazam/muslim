import React from 'react'
import { View, Text, ScrollView, TouchableOpacity, Animated } from 'react-native'
import MosqueCard from '.././../Mosque/components/MosqueCard'
import _COLORS from '../../../utils/Color'
import { FlatList } from 'react-native-gesture-handler'

interface Props {
    mosques: any
    navigation?: any
}

const VIABILITY_CONFIG = {
    minimumViewTime: 3000,
    viewAreaCoveragePercentThreshold: 95,
    waitForInteraction: true,
}

export default class NearbyMosque extends React.Component<Props> {
    public render() {
        return <this.NearbyMosque mosques={this.props.mosques} />
    }

    public RenderMosqueCard = ({ mosques }): JSX.Element => {
        return mosques?.map((mosque) => {
            return (
                <MosqueCard
                    key={mosque?.id}
                    onPress={() => this.props.navigation.navigate('MosqueDetails', { mosqueId: mosque.id })}
                    data={mosque}
                    size={300}
                />
            )
        })
    }

    public NearbyMosque = ({ mosques }) => {
        return (
            <>
                <View style={{ backgroundColor: 'white' }}>
                    <View
                        style={{
                            marginHorizontal: 10,
                            marginTop: 20,
                            flex: 1,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignContent: 'space-between',
                            paddingBottom: 10,
                        }}
                    >
                        <Text
                            style={{
                                color: 'black',
                                fontWeight: 'bold',
                                fontSize: 24,
                                fontFamily: 'AvantGardeLT-Demi',
                            }}
                        >
                            Nearby Mosques
                        </Text>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('MosqueRoute')}>
                            <Text style={{ color: _COLORS.primary, fontWeight: 'bold', fontSize: 14 }}>SEE ALL</Text>
                        </TouchableOpacity>
                    </View>
                    <Animated.ScrollView
                        scrollEventThrottle={1}
                        horizontal={true}
                        decelerationRate={1}
                        snapToInterval={200}
                        snapToAlignment={'center'}
                        showsHorizontalScrollIndicator={false}
                    >
                        {Array.isArray(mosques) ? (
                            this.RenderMosqueCard({ mosques })
                        ) : (
                            <Text style={{ marginStart: 20 }}>Loading ...</Text>
                        )}
                    </Animated.ScrollView>
                </View>
            </>
        )
    }
}
