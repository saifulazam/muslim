import React, { Component } from 'react'
import { Text, View, ScrollView } from 'react-native'
import __COLORS from '../../../utils/Color'
import { NavigationProp } from '@react-navigation/native'
import Card from '../../../common/Card'
import LazyImage from '../../../common/LazyImage/LazyImage'
import moment from 'moment'
import { TouchableOpacity } from 'react-native-gesture-handler'

interface Props {
    navigation?: NavigationProp<any>
    auth?: any
    discussions: any
}

interface State {}

export default class NewDiscuss extends Component<Props, State> {
    constructor(props) {
        super(props)
        this.state = {}
    }

    public renderDiscuss = () => {
        return this.props.discussions.map((discuss) => {
            return (
                <TouchableOpacity
                    key={discuss.id}
                    onPress={() => this.props.navigation.navigate('DiscussionDetail', { discussion_id: discuss.id })}
                >
                    <Card width={300} height={170} round={8}>
                        <View
                            style={{
                                padding: 10,
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                            }}
                        >
                            <View style={{ flexDirection: 'row' }}>
                                {this.__renderCreator(discuss)}
                                <View style={{ paddingStart: 10 }}>
                                    <Text style={{ fontSize: 20 }}>
                                        {discuss.creator ? discuss.creator.name : 'Alnur'}
                                    </Text>
                                    <Text style={{ color: 'gray' }}>{moment(discuss.added).fromNow()}</Text>
                                </View>
                            </View>
                            <View
                                style={{
                                    backgroundColor: '#ededed',
                                    padding: 5,
                                    borderRadius: 15,
                                    borderColor: '#e4e4e4',
                                    borderWidth: 1,
                                }}
                            >
                                <Text style={{ color: '#c9c9c9' }}>{discuss.category.name}</Text>
                            </View>
                        </View>
                        <View style={{ padding: 10 }}>
                            <Text numberOfLines={3}>{discuss.content}</Text>
                        </View>
                        <Text style={{ color: 'gray', padding: 10 }}>{discuss.comments.length} replies</Text>
                    </Card>
                </TouchableOpacity>
            )
        })
    }

    __renderCreator = (discussion) => {
        if (discussion.creator) {
            return (
                <LazyImage
                    source={{ url: discussion.creator.avatar }}
                    style={{ width: 40, height: 40, borderRadius: 20 }}
                />
            )
        } else {
            return (
                <LazyImage
                    source={require('../../../../assets/images/appIconSmall.png')}
                    style={{ width: 40, height: 40, borderRadius: 20 }}
                />
            )
        }
    }

    render() {
        return (
            <View>
                <View
                    style={{
                        marginHorizontal: 10,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingBottom: 15,
                    }}
                >
                    <Text style={{ color: 'black', fontWeight: 'bold', fontSize: 24, fontFamily: 'AvantGardeLT-Demi' }}>
                        Discussion
                    </Text>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('ForumStack')}>
                        <Text style={{ color: __COLORS.primary, fontWeight: 'bold', fontSize: 14 }}>SEE ALL</Text>
                    </TouchableOpacity>
                </View>
                <ScrollView
                    horizontal={true}
                    decelerationRate={1}
                    snapToInterval={200}
                    snapToAlignment={'center'}
                    showsHorizontalScrollIndicator={false}
                    scrollEventThrottle={16}
                >
                    {this.renderDiscuss()}
                </ScrollView>
            </View>
        )
    }
}
