import React, { useState, useEffect } from 'react'
import { View, Text, SafeAreaView, TouchableOpacity, Alert } from 'react-native'
import PrayerHorizontal from '../../../components/PrayerHorizontal/prayerHorizontal'
import _Color from '../../../utils/Color'
import Spinner from '../../../common/Spinner'
import Icon from '../../../common/Icon'

interface Props {
    location: any
    geocode: any
    navigation?: any
}

const PrayerSchedule = (props: Props) => {
    const getCity = props.geocode
        ? `${props.geocode.city}, ${props.geocode.region}, ${props.geocode.postalCode ?? ''}`
        : 'Finding your location'
    return (
        <View style={{ backgroundColor: _Color.black, paddingBottom: 20 }}>
            <SafeAreaView>
                <View style={{ marginHorizontal: 10, flexDirection: 'row' }}>
                    <Text
                        style={{
                            color: 'white',
                            fontSize: 32,
                            fontWeight: 'bold',
                            marginTop: 20,
                            fontFamily: 'AvantGardeLT-Demi',
                        }}
                    >
                        Assalamu alaykum
                    </Text>
                    <Text style={{ color: '#1ee592', fontSize: 32, fontWeight: 'bold', marginTop: 20 }}>.</Text>
                </View>
                <View>
                    <TouchableOpacity
                        style={{
                            marginTop: 20,
                            marginHorizontal: 10,
                            flexDirection: 'row',
                            alignItems: 'center',
                        }}
                    >
                        <Icon name="map_pin_primary" width="24" />
                        <Text
                            style={{ color: 'white', textDecorationLine: 'underline', fontSize: 13, fontWeight: '500' }}
                            onPress={() => props.navigation.navigate('SetLocation')}
                        >
                            {getCity}
                        </Text>
                        <Icon name="arrow_right_light" width="24" />
                    </TouchableOpacity>
                </View>
                <View style={{ marginTop: 20 }}>
                    {!props.geocode ? <Spinner /> : <PrayerHorizontal navigation={props.navigation} {...props} />}
                </View>
            </SafeAreaView>
        </View>
    )
}

export default PrayerSchedule
