import React, { Component } from 'react'
import { Text, View, TouchableOpacity, ScrollView } from 'react-native'
import _COLORS from '../../../utils/Color'
import { ServiceExecutorGet } from '../../../services/ServiceExecutor'
import { API } from 'react-native-dotenv'
import PodCastItemCard from '../../Podcast/components/PodCastItemCard'
import { NavigationProp } from '@react-navigation/native'

interface Props {
    navigation?: NavigationProp<any>
}

interface State {
    categories: any
    isLoading: boolean
}

export default class NewPodcast extends Component<Props, State> {
    constructor(props) {
        super(props)
        this.state = {
            categories: [],
            isLoading: true,
        }
    }
    public componentDidMount() {
        ServiceExecutorGet(`${API}/podcasts/categories`, this.responseCallback)
    }

    public responseCallback = (response) => {
        if (response.success) {
            this.setState({ categories: response.data, isLoading: false })
        } else {
            console.warn(response)
        }
    }

    public renderItemCard = (items) => {
        return items.map((item) => {
            if (item.category == 'Live') {
                return (
                    <PodCastItemCard
                        key={item.id}
                        data={item}
                        onPress={() => this.props.navigation.navigate('PodcastDetails', { data: item })}
                        width={157}
                        imageHeight={240}
                    />
                )
            } else {
                return null
            }
        })
    }

    public renderCategories = () => {
        return this.state.categories.map((category) => {
            return (
                <ScrollView
                    horizontal={true}
                    decelerationRate={1}
                    snapToInterval={200}
                    snapToAlignment={'center'}
                    showsHorizontalScrollIndicator={false}
                    scrollEventThrottle={16}
                    key={category.id}
                >
                    {this.renderItemCard(category.podcasts)}
                </ScrollView>
            )
        })
    }

    render() {
        return (
            <View>
                <View
                    style={{
                        marginHorizontal: 10,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingBottom: 15,
                    }}
                >
                    <Text style={{ color: 'black', fontWeight: 'bold', fontSize: 24, fontFamily: 'AvantGardeLT-Demi' }}>
                        Live
                    </Text>
                    <TouchableOpacity
                        onPress={() => {
                            this.props.navigation.navigate('Podcast')
                        }}
                    >
                        <Text style={{ color: _COLORS.primary, fontWeight: 'bold', fontSize: 14 }}>SEE ALL</Text>
                    </TouchableOpacity>
                </View>
                {this.renderCategories()}
            </View>
        )
    }
}
