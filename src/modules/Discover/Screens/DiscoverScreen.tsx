import React, { PureComponent } from 'react'
import { View, ScrollView } from 'react-native'
import PrayerSchedule from '../components/PrayerSchedule'
import NearbyMosque from '../components/NearbyMosques'
import _Color from '../../../utils/Color'
import Spinner from '../../../common/Spinner'
import { connect } from 'react-redux'
import { getNearByMosques } from '../../Mosque/Services/MosqueService'
import { CustomStatusBar } from '../../../utils/StatusBar'
import { ForumExploreItems } from '../../Forum/components/ForumExploreItems'
import { getThreadsService } from '../../Forum/Services/ForumServices'
import { StackNavigationProp } from '@react-navigation/stack'
import MosqueAds from '../../Company/components/MosqueAds'
import AppFooter from '../../Company/components/AppFooter'

interface Props {
    navigation: StackNavigationProp<any>
    mosques: any
    isAuthenticated: boolean
    route?: any
    exploreThreads?: any
    coords: any
    getNearByMosques: any
    prayers?: any
    address?: any
    getThreadsService?: any
    threads?: any
    user?: any
}

interface State {
    location: any // determine type for these
    locationError: string
}

class DiscoverScreen extends PureComponent<Props, State> {
    constructor(props) {
        super(props)
        this.state = {
            location: null,
            locationError: '',
        }
    }

    componentDidMount() {
        CustomStatusBar(this.props.navigation, 'light-content')
        this.props.getNearByMosques(this.props.coords.latitude, this.props.coords.longitude)
        this.props.getThreadsService('all', 1)
    }

    render() {
        if (!this.props.coords) {
            return <Spinner />
        }
        return (
            <>
                <View style={{ flex: 1, backgroundColor: 'rgba(245,245,245,1)' }}>
                    <ScrollView showsVerticalScrollIndicator={false} alwaysBounceVertical={false}>
                        <PrayerSchedule
                            location={this.props.coords}
                            geocode={this.props.address}
                            navigation={this.props.navigation}
                        />
                        {Array.isArray(this.props.mosques) ? (
                            <NearbyMosque mosques={this.props.mosques} {...this.props} />
                        ) : (
                            <Spinner />
                        )}
                        <ForumExploreItems navigation={this.props.navigation} threads={this.props.threads} />
                        <View style={{ backgroundColor: 'white', paddingVertical: 20, paddingHorizontal: 10 }}>
                            <MosqueAds />
                        </View>
                        <View>
                            <AppFooter user={this.props.user} />
                        </View>
                    </ScrollView>
                </View>
            </>
        )
    }
}

const mapStateToProps = (state) => {
    const { mosques } = state.mosques
    const { isAuthenticated, user } = state.auth
    const { coords, address } = state.location
    const { prayers } = state.prayers
    const { threads } = state.forums
    return { mosques, isAuthenticated, coords, address, prayers, threads, user }
}

export default connect(mapStateToProps, {
    getNearByMosques,
    getThreadsService,
})(DiscoverScreen)
