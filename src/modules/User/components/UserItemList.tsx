import React from 'react'
import { ListItem, Avatar } from 'react-native-elements'
import Icon from '../../../common/Icon'
import TouchableScale from 'react-native-touchable-scale'

interface Props {
    icon?: string
    title: string
    onPress?: any
    avatar?: string
}

const UserItemList = (props: Props) => {
    return (
        <ListItem Component={TouchableScale} bottomDivider onPress={props.onPress}>
            {props.avatar ? <Avatar size={24} source={{ uri: props.avatar }} /> : <Icon name={props.icon} />}
            <ListItem.Content>
                <ListItem.Title>{props.title}</ListItem.Title>
            </ListItem.Content>
            <Icon name="arrow_right_light" />
        </ListItem>
    )
}

export default UserItemList
