import React from 'react'
import { View, Text } from 'react-native'
import { Avatar } from 'react-native-elements'

interface Props {
    navigation?: any
    user: any
}

const UserMainCard = (props: Props) => {
    return (
        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingTop: 20 }}>
            <Avatar source={{ uri: props.user?.avatar }} size={80} rounded />
            <View style={{ paddingLeft: 10 }}>
                <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{props.user?.name}</Text>
                <Text style={{ fontSize: 15, fontWeight: '500' }}>{props.user?.email}</Text>
                <Text style={{ fontSize: 12 }}>account since: {props.user?.accountCreated}</Text>
            </View>
        </View>
    )
}

export default UserMainCard
