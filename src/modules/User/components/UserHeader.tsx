import React from 'react'
import { View, Text } from 'react-native'
import { Header } from 'react-native-elements'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Icon from '../../../common/Icon'
import COLOR from '../../../utils/Color'
import { CustomStatusBar } from '../../../utils/StatusBar'

interface Props {
    navigation: any
    title: string
    route?: any
}

const UserHeader = (props: Props) => {
    React.useEffect(() => {
        CustomStatusBar(props.navigation, 'light-content')
    }, [])
    const headerLef = () => {
        //@ts-ignore
        if (props.route?.name === 'UserProfile') {
        } else {
            return (
                <TouchableOpacity onPress={() => props.navigation.goBack()}>
                    <Icon name="back_light" width="16" />
                </TouchableOpacity>
            )
        }
    }
    return (
        <Header
            placement="left"
            leftComponent={headerLef()}
            centerComponent={{ text: props.title, style: { color: '#fff', fontSize: 16, fontWeight: '700' } }}
            containerStyle={{
                backgroundColor: COLOR.primary,
                justifyContent: 'space-around',
            }}
        />
    )
}

export default UserHeader
