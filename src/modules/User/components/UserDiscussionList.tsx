import React from 'react'
import { View, Text } from 'react-native'
import { ListItem } from 'react-native-elements'

interface Props {
    discussion: any
}

const UserDiscussionList = (props: Props) => {
    return (
        <ListItem bottomDivider>
            <ListItem.Content>
                <ListItem.Title>{props.discussion?.content}</ListItem.Title>
                <ListItem.Subtitle>{props.discussion?.category?.name}</ListItem.Subtitle>
            </ListItem.Content>
        </ListItem>
    )
}

export default UserDiscussionList
