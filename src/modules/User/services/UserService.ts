import { func } from 'prop-types'
import Http from '../../../utils/Http'
import Transformer from '../../../utils/Transformer'
import * as UserAction from '../redux/UserAction'
import * as AuthService from '../../Auth/services/AuthService'
import { dispatch } from '../../../navigation/rootNavigation'

export function userDiscussions(token) {
    return (dispatch) => {
        new Promise<void>((resolve, reject) => {
            Http.defaults.headers.common['Authorization'] = `Bearer ${token}`
            Http.get('user/discussions')
                .then((response) => {
                    const data = Transformer.fetch(response.data)
                    dispatch(UserAction.getUserDiscussions(data))
                    return resolve()
                })
                .catch((error) => {
                    console.warn(error)
                    return reject(error)
                })
        })
    }
}

export function userComments(token) {
    return (dispatch) => {
        new Promise<void>((resolve, reject) => {
            Http.defaults.headers.common['Authorization'] = `Bearer ${token}`
            Http.get('user/comments')
                .then((response) => {
                    const data = Transformer.fetch(response.data)
                    dispatch(UserAction.getUserComments(data))
                    return resolve()
                })
                .catch((error) => {
                    console.warn(error)
                    return reject(error)
                })
        })
    }
}

export function postUserGeneralInformation(token, paramBody) {
    return (dispatch) => {
        new Promise<void>((resolve, reject) => {
            Http.defaults.headers.common['Authorization'] = `Bearer ${token}`
            Http.post('user/change/basic', paramBody)
                .then((response) => {
                    if (response.status === 200) {
                        dispatch(UserAction.updateUserGeneral())
                        dispatch(AuthService.getAuthUser())
                    }
                    return resolve()
                })
                .catch((error) => {
                    console.warn(error)
                    dispatch(UserAction.updateUserGeneralError('Something went wrong!'))
                    return reject(error)
                })
        })
    }
}

export function uploadAvatar(token, paramBody) {
    return (dispatch) => {
        new Promise<void>((resolve, reject) => {
            Http.defaults.headers.common['Authorization'] = `Bearer ${token}`
            Http.defaults.headers.common['Content-Type'] = `multipart/form-data`
            Http.post('user/upload/avatar', paramBody)
                .then((response) => {
                    if (response.status === 200) {
                        dispatch(UserAction.updateUserAvatar())
                        dispatch(AuthService.getAuthUser())
                    }
                    return resolve()
                })
                .catch((error) => {
                    console.warn(error)
                    return reject(error)
                })
        })
    }
}

export function getUserThreads() {
    return (dispatch) => {
        new Promise<void>((resolve, reject) => {
            Http.get('user/threads')
                .then((response) => {
                    const data = Transformer.fetch(response.data.data)
                    dispatch(UserAction.setUserThreads(data))
                    return resolve()
                })
                .catch((error) => {
                    console.warn(error)
                    return reject(error)
                })
        })
    }
}
