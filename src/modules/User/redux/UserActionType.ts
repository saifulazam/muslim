import { ThreadModel } from '../../Forum/redux/ForumActionType'
import { DiscussionModel, DiscussionCommentModel } from './../../Discussion/redux/DiscussionActionType'

export enum UserActionType {
    USER_GENERAL_UPDATED = '@USER_GENERAL_UPDATED',
    USER_GENERAL_UPDATED_ERROR = '@USER_GENERAL_UPDATED_ERROR',
    USER_UPLOAD_AVATAR = '@USER_UPLOAD_AVATAR',
    USER_DISCUSSIONS = '@USER_DISCUSSIONS',
    USER_COMMENTS = '@USER_COMMENTS',
    USER_THREADS = '@USER_THREADS',
}

export type UserState = {
    discussions: DiscussionModel[]
    comments: DiscussionCommentModel[]
    generalUpdated: boolean
    generalUpdatedError: string
    threads?: ThreadModel[]
}

export type UserAction = {
    type: string
    payload: any
}
