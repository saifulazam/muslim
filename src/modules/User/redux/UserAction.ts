import { UserActionType } from './UserActionType'

export function getUserDiscussions(payload) {
    return {
        type: UserActionType.USER_DISCUSSIONS,
        payload,
    }
}

export function getUserComments(payload) {
    return {
        type: UserActionType.USER_COMMENTS,
        payload,
    }
}

export function updateUserGeneral() {
    return {
        type: UserActionType.USER_GENERAL_UPDATED,
    }
}

export function updateUserGeneralError(payload) {
    return {
        type: UserActionType.USER_GENERAL_UPDATED_ERROR,
        payload,
    }
}

export function updateUserAvatar() {
    return {
        type: UserActionType.USER_UPLOAD_AVATAR,
    }
}

export function setUserThreads(payload) {
    return {
        type: UserActionType.USER_THREADS,
        payload,
    }
}
