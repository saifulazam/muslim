import { UserAction, UserActionType, UserState } from './UserActionType'
import Http from '../../../utils/Http'

const INITIAL_STATE: UserState = {
    discussions: [],
    comments: [],
    generalUpdated: false,
    generalUpdatedError: '',
    threads: [],
}

const reducer = (state = INITIAL_STATE, actionType: UserAction) => {
    switch (actionType.type) {
        case UserActionType.USER_DISCUSSIONS:
            return discussions(state, actionType.payload)
        case UserActionType.USER_GENERAL_UPDATED:
            return userGeneralUpdated(state)
        case UserActionType.USER_GENERAL_UPDATED_ERROR:
            return userGeneralUpdatedError(state, actionType.payload)
        case UserActionType.USER_UPLOAD_AVATAR:
            return userUploadAvatar(state)
        case UserActionType.USER_COMMENTS:
            return userComments(state, actionType.payload)
        case UserActionType.USER_THREADS:
            return getUserThreads(state, actionType.payload)
        default:
            return state
    }
}

const userGeneralUpdated = (state) => {
    return { ...state, generalUpdated: true, generalUpdatedError: '' }
}

const userGeneralUpdatedError = (state, payload) => {
    return { ...state, generalUpdated: false, generalUpdatedError: payload }
}

const userUploadAvatar = (state) => {
    return { ...state }
}

const discussions = (state, payload) => {
    return {
        ...state,
        discussions: payload.data,
    }
}

const userComments = (state, payload) => {
    return { ...state, comments: payload.data }
}

const getUserThreads = (state, payload) => {
    return { ...state, threads: payload }
}

export default reducer
