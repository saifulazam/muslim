import { StackNavigationProp } from '@react-navigation/stack'
import * as ImagePicker from 'expo-image-picker'
import * as React from 'react'
import { View, StyleSheet, Text, Alert, Platform } from 'react-native'
import { Avatar, Button } from 'react-native-elements'
import { connect } from 'react-redux'
import UserHeader from '../components/UserHeader'
import Constants from 'expo-constants'
import * as Permissions from 'expo-permissions'
import Toast from 'react-native-tiny-toast'
import { uploadAvatar } from '../services/UserService'

export interface Props {
    isAuthenticated?: any
    navigation: StackNavigationProp<any>
    user?: any
    token?: string
    uploadAvatar?: any
}

export interface State {
    image: string
}

export class UserAvatarUploadScreen extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {
            image: '',
        }
    }

    getPermissionAsync = async () => {
        if (Constants.platform.ios) {
            const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
            if (status !== 'granted') {
                Alert.alert('Sorry, we need camera roll permissions to make this work!')
            }
        }
    }

    _pickImage = async () => {
        this.getPermissionAsync()
        try {
            let result = await ImagePicker.launchImageLibraryAsync({
                mediaTypes: ImagePicker.MediaTypeOptions.All,
                allowsEditing: true,
                aspect: [4, 3],
                quality: 1,
            })
            if (!result.cancelled) {
                //@ts-ignore
                this.setState({ image: result.uri })
                this.onPressUpload()
            }

            // console.log(result)
        } catch (E) {
            console.warn(E)
        }
    }

    onPressUpload = () => {
        const formData = new FormData()
        if (this.state.image) {
            let imageUrl = this.state.image
            let filename = imageUrl.split('/').pop()
            let match = /\.(\w+)$/.exec(filename)
            let type = match ? `image/${match[1]}` : `image`
            formData.append('avatar', { uri: imageUrl, name: filename, type })
        }
        this.props.uploadAvatar(this.props.token, formData)
        Toast.showSuccess('success')
    }

    public render() {
        return (
            <View>
                <UserHeader title="Avatar" navigation={this.props.navigation} />
                <View style={{ justifyContent: 'center', alignItems: 'center', paddingTop: 30 }}>
                    <Avatar
                        size={200}
                        rounded
                        source={{ uri: this.state.image ? this.state.image : this.props.user.avatar }}
                        activeOpacity={0.7}
                        // containerStyle={{ flex: 2, marginLeft: 20, marginTop: 115 }}
                    />
                    <View style={{ paddingTop: 20 }}>
                        <Button title="Choose Photo" onPress={this._pickImage} />
                    </View>
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        isAuthenticated: state.auth.isAuthenticated,
        user: state.auth.user,
        token: state.auth.token,
    }
}

export default connect(mapStateToProps, { uploadAvatar })(UserAvatarUploadScreen)
