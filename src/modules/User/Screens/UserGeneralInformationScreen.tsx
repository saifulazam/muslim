import { StackNavigationProp } from '@react-navigation/stack'
import * as React from 'react'
import { View, StyleSheet, Text, KeyboardAvoidingView, Platform } from 'react-native'
import { connect } from 'react-redux'
import FormField from '../../../common/FormField'
import PrimaryButton from '../../../common/PrimaryButton'
import ScreenLevelAlert from '../../../common/ScreenLevelAlert'
import UserHeader from '../components/UserHeader'
import { postUserGeneralInformation } from '../services/UserService'
import Toast from 'react-native-tiny-toast'

interface Props {
    isAuthenticated?: any
    navigation?: StackNavigationProp<any>
    user?: any
    token?: string
    postUserGeneralInformation?: any
    generalUpdated?: boolean
    generalUpdatedError?: string
}

interface State {
    first_name: string
    last_name: string
    email: string
    phone_number: string
}

export class UserGeneralInformationScreen extends React.Component<Props, State> {
    state = {
        first_name: this.props.user?.firstName,
        last_name: this.props.user?.lastName,
        email: this.props.user?.email,
        phone_number: this.props.user?.phoneNumber,
    }

    onPressSubmitForm = () => {
        const { first_name, last_name, phone_number } = this.state
        const paramBody = {
            first_name,
            last_name,
            phone_number,
        }
        this.props.postUserGeneralInformation(this.props.token, paramBody)
        Toast.showSuccess('success')
    }

    componentDidMount() {}

    _onChangeEmailHandler = (email) => {
        this.setState({ email })
    }

    _onChangeFirstNameHandler = (first_name) => {
        this.setState({ first_name })
    }

    _onChangeLastNameHandler = (last_name) => {
        this.setState({ last_name })
    }

    _onChangePhoneNumberHandler = (phone_number) => {
        this.setState({ phone_number })
    }

    renderUpdatedError = () => {
        if (this.props.generalUpdatedError) {
            return <ScreenLevelAlert errorMessage={this.props.generalUpdatedError} />
        }
    }

    render() {
        return (
            <View>
                <UserHeader navigation={this.props.navigation} title="General Information" />
                <View style={{ paddingHorizontal: 20 }}>
                    <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : null}>
                        {this.renderUpdatedError()}
                        <FormField
                            title="First Name"
                            value={this.state.first_name}
                            onChangeText={this._onChangeFirstNameHandler}
                        />
                        <FormField
                            title="Last Name"
                            value={this.state.last_name}
                            onChangeText={this._onChangeLastNameHandler}
                        />
                        {/* <FormField
                            title="Email (not editable)"
                            value={this.state.email}
                            onChangeText={this._onChangeEmailHandler}
                            editable={false}
                        /> */}
                        <FormField
                            title="Phone Number"
                            value={this.state.phone_number}
                            onChangeText={this._onChangePhoneNumberHandler}
                        />
                        <View style={{ paddingTop: 30 }}>
                            <PrimaryButton isActive={true} onPress={this.onPressSubmitForm} title="Save Change" />
                        </View>
                    </KeyboardAvoidingView>
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        isAuthenticated: state.auth.isAuthenticated,
        user: state.auth.user,
        token: state.auth.token,
        generalUpdated: state.user.generalUpdated,
        generalUpdatedError: state.user.generalUpdatedError,
    }
}

export default connect(mapStateToProps, { postUserGeneralInformation })(UserGeneralInformationScreen)
