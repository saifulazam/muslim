import { StackNavigationProp } from '@react-navigation/stack'
import * as React from 'react'
import { View, StyleSheet, Text, KeyboardAvoidingView, Platform } from 'react-native'
import { connect } from 'react-redux'
import FormField from '../../../common/FormField'
import PrimaryButton from '../../../common/PrimaryButton'
import UserHeader from '../components/UserHeader'

export interface Props {
    isAuthenticated?: any
    navigation: StackNavigationProp<any>
    user?: any
}

export interface State {
    currentPassword: string
    newPassword: string
    newPasswordConfirmation: string
    hideCPassword: boolean
    hideNPassword: boolean
    hideNPasswordC: boolean
}

export class UserPasswordScreen extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {
            currentPassword: '',
            newPassword: '',
            newPasswordConfirmation: '',
            hideCPassword: true,
            hideNPassword: true,
            hideNPasswordC: true,
        }
    }

    onPressChangeCPW = (currentPassword) => {
        this.setState({ currentPassword })
    }
    onPressChangeNPW = (newPassword) => {
        this.setState({ newPassword })
    }
    onPressChangeNPWC = (newPasswordConfirmation) => {
        this.setState({ newPasswordConfirmation })
    }

    _onPressHideCPassword = () => {
        this.setState({ hideCPassword: !this.state.hideCPassword })
    }
    _onPressHideNPassword = () => {
        this.setState({ hideNPassword: !this.state.hideNPassword })
    }
    _onPressHideNPasswordC = () => {
        this.setState({ hideNPasswordC: !this.state.hideNPasswordC })
    }

    public render() {
        return (
            <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : null}>
                <UserHeader navigation={this.props.navigation} title="Password" />

                <View style={{ paddingHorizontal: 20 }}>
                    <FormField
                        title={'Current Password'}
                        placeholder="enter current Password"
                        value={this.state.currentPassword}
                        onChangeText={this.onPressChangeCPW}
                        icon
                        iconOnPress={this._onPressHideCPassword}
                        iconOnChange={this.state.hideCPassword}
                        secureTextEntry={this.state.hideCPassword}
                    />
                    <FormField
                        title={'New Password'}
                        placeholder="enter new Password"
                        value={this.state.newPassword}
                        onChangeText={this.onPressChangeNPW}
                        icon
                        iconOnPress={this._onPressHideNPassword}
                        iconOnChange={this.state.hideNPassword}
                        secureTextEntry={this.state.hideNPassword}
                    />
                    <FormField
                        title={'New Password Confirmation'}
                        placeholder="enter new Password Confirmation"
                        value={this.state.newPassword}
                        onChangeText={this.onPressChangeNPWC}
                        icon
                        iconOnPress={this._onPressHideNPasswordC}
                        iconOnChange={this.state.hideNPasswordC}
                        secureTextEntry={this.state.hideNPasswordC}
                    />
                    <View style={{ paddingTop: 30 }}>
                        <PrimaryButton title="Save New Password" onPress={() => {}} isActive={true} />
                    </View>
                </View>
            </KeyboardAvoidingView>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        isAuthenticated: state.auth.isAuthenticated,
        user: state.auth.user,
    }
}

export default connect(mapStateToProps)(UserPasswordScreen)
