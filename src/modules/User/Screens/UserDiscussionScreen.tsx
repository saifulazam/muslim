import { StackNavigationProp } from '@react-navigation/stack'
import * as React from 'react'
import { View, StyleSheet, Text, FlatList } from 'react-native'
import { Badge } from 'react-native-elements'
import { connect } from 'react-redux'
import DiscussionCard from '../../Discussion/components/DiscussionCard'
import UserDiscussionList from '../components/UserDiscussionList'
import UserHeader from '../components/UserHeader'
import { userDiscussions } from '../services/UserService'

export interface Props {
    isAuthenticated?: any
    navigation: StackNavigationProp<any>
    token?: string
    discussions?: any
    userDiscussions?: any
}

export interface State {}

export class UserDiscussionScreen extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {}
    }

    componentDidMount() {
        this.props.userDiscussions(this.props.token)
    }

    renderDiscussions = () => {
        if (Array.isArray(this.props.discussions)) {
            return (
                <FlatList
                    data={this.props.discussions}
                    renderItem={({ item }) => <DiscussionCard discussion={item} navigation={this.props.navigation} />}
                    keyExtractor={(item) => Math.random() + item.id.toString()}
                    contentContainerStyle={{ paddingBottom: 150 }}
                />
            )
        } else {
            return <Text>No data found!</Text>
        }
    }

    public render() {
        return (
            <View>
                <UserHeader title="My Discussions" navigation={this.props.navigation} />
                <View style={{ padding: 10 }}>
                    <Text>
                        We found <Text style={{ color: 'red' }}>{this.props.discussions.length}</Text> discussions.
                    </Text>
                </View>
                {this.renderDiscussions()}
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        isAuthenticated: state.auth.isAuthenticated,
        token: state.auth.token,
        discussions: state.user.discussions,
    }
}

export default connect(mapStateToProps, { userDiscussions })(UserDiscussionScreen)
