import * as React from 'react'
import { View, StyleSheet, Text, SafeAreaView, Alert, ScrollView, StatusBar } from 'react-native'
import { connect } from 'react-redux'
import { StackNavigationProp } from '@react-navigation/stack'
import UserHeader from '../components/UserHeader'
import UserItemList from '../components/UserItemList'
import { logout } from '../../Auth/services/AuthService'
import UserMainCard from '../components/UserMainCard'
import { CustomStatusBar } from '../../../utils/StatusBar'

export interface Props {
    navigation: StackNavigationProp<any>
    route?: any
    user?: any
    logout?: any
    isAuthenticated?: any
}
export interface State {}

class UserAccountScreen extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {}
        this.__onPressLogoutHandler = this.__onPressLogoutHandler.bind(this)
    }

    componentDidMount() {
        CustomStatusBar(this.props.navigation, 'dark-content')
        if (!this.props.isAuthenticated) {
            this.props.navigation.navigate('InitialRoute', { screen: 'InitialAuth' })
        }
    }

    __onPressLogoutHandler = () => {
        this.props.logout()
        this.props.navigation.navigate('InitialRoute', { screen: 'InitialAuth' })
    }

    onPressHandler = () => {
        Alert.alert('Come back later.')
    }
    render() {
        return (
            <SafeAreaView>
                {this.props.isAuthenticated ? <UserMainCard user={this.props?.user} /> : null}
                <ScrollView>
                    <View style={{ paddingVertical: 10, marginTop: 10 }}>
                        <UserItemList
                            title="Name/Email/Phone"
                            icon="person"
                            onPress={() => this.props.navigation.navigate('UserGeneralInformationScreen')}
                        />
                        {/* <UserItemList
                            title="Password"
                            icon="fingerprint"
                            onPress={() => this.props.navigation.navigate('UserPasswordScreen')}
                        /> */}
                        <UserItemList
                            title="Upload Avatar"
                            avatar={this.props.user?.avatar}
                            onPress={() => this.props.navigation.navigate('UserAvatarUploadScreen')}
                        />
                    </View>
                    <View style={{ paddingVertical: 10 }}>
                        <UserItemList
                            title="Discussions"
                            icon="description"
                            onPress={() => this.props.navigation.navigate('UserDiscussionScreen')}
                        />
                        <UserItemList
                            title="Comments"
                            icon="chat"
                            onPress={() => this.props.navigation.navigate('UserCommentScreen')}
                        />
                    </View>
                    <View style={{ paddingVertical: 10 }}>
                        <UserItemList title="Favorite Mosques" icon="favorite" onPress={this.onPressHandler} />
                        <UserItemList title="Donations" icon="date_range_calender" onPress={this.onPressHandler} />
                        {/* <UserItemList title="My Wallet" icon="wallet" onPress={this.onPressHandler} /> */}
                    </View>
                    <View style={{ paddingVertical: 10 }}>
                        <UserItemList title="Settings" icon="settings" onPress={this.onPressHandler} />
                    </View>
                    <View style={{ paddingVertical: 10 }}>
                        <UserItemList title="Logout" icon="logout" onPress={this.__onPressLogoutHandler} />
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        isAuthenticated: state.auth.isAuthenticated,
        user: state.auth.user,
    }
}

export default connect(mapStateToProps, { logout })(UserAccountScreen)
