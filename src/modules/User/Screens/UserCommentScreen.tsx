import { StackNavigationProp } from '@react-navigation/stack'
import * as React from 'react'
import { View, StyleSheet, Text } from 'react-native'
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler'
import { connect } from 'react-redux'
import ReadMore from '../../../common/ReadMore'
import UserHeader from '../components/UserHeader'
import { userComments } from '../services/UserService'
import { Card, ListItem, Button, Icon } from 'react-native-elements'
import { timestampToString } from '../../../utils/datetime'
import moment from 'moment'

export interface Props {
    isAuthenticated?: any
    navigation: StackNavigationProp<any>
    token?: any
    comments: any
    userComments?: any
}

export interface State {}

export class UserCommentScreen extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {}
    }

    componentDidMount() {
        this.props.userComments(this.props.token)
    }

    _renderCommentItem = (item) => {
        return (
            <Card>
                <Card.Title>{moment(item.added).fromNow()}</Card.Title>
                <Card.Divider />
                <View style={{ paddingBottom: 20 }}>
                    <ReadMore>{item.message}</ReadMore>
                </View>
                <TouchableOpacity
                    onPress={() =>
                        this.props.navigation.navigate('DiscussionDetail', { discussion_id: item.discussId })
                    }
                    style={{ flexDirection: 'row', backgroundColor: '#eee', padding: 10, justifyContent: 'center' }}
                >
                    <Text style={{ color: 'gray', fontWeight: '500' }}>View</Text>
                </TouchableOpacity>
            </Card>
        )
    }

    public render() {
        return (
            <View>
                <UserHeader title="My Comments" navigation={this.props.navigation} />
                <View style={{ padding: 10 }}>
                    <Text>
                        We found <Text style={{ color: 'red' }}>{this.props.comments.length}</Text> comments.
                    </Text>
                </View>
                <FlatList
                    data={this.props.comments}
                    keyExtractor={(item) => Math.random() + item.id.toString()}
                    renderItem={({ item }: { item: any }) => this._renderCommentItem(item)}
                    contentContainerStyle={{ paddingBottom: 150 }}
                />
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        isAuthenticated: state.auth.isAuthenticated,
        token: state.auth.token,
        comments: state.user.comments,
    }
}

export default connect(mapStateToProps, { userComments })(UserCommentScreen)
