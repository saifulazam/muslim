import { createStackNavigator, StackNavigationOptions, TransitionPresets } from '@react-navigation/stack'
import React from 'react'
import DiscussionDetailScreen from '../../Discussion/Screens/DiscussionDetailScreen'
import UserAvatarUploadScreen from '../Screens/UserAvatarUploadScreen'
import UserCommentScreen from '../Screens/UserCommentScreen'
import UserDiscussionScreen from '../Screens/UserDiscussionScreen'
import UserGeneralInformationScreen from '../Screens/UserGeneralInformationScreen'
import UserPasswordScreen from '../Screens/UserPasswordScreen'
import UserAccountScreen from '../Screens/UserAccountScreen'

export type UserStackParamList = {
    UserAccount: undefined
    UserGeneralInformationScreen: undefined
    UserPasswordScreen: undefined
    UserAvatarUploadScreen: undefined
    UserDiscussionScreen: undefined
    UserCommentScreen: undefined
    DiscussionDetail: undefined
}

const Stack = createStackNavigator<UserStackParamList>()

const UserStack = () => {
    const navigationOptions: StackNavigationOptions = {
        headerShown: false,
        gestureEnabled: false,
    }
    return (
        <Stack.Navigator screenOptions={navigationOptions}>
            <Stack.Screen
                name="UserAccount"
                component={UserAccountScreen}
                options={{
                    ...TransitionPresets.DefaultTransition,
                }}
            />
            <Stack.Screen
                name="UserGeneralInformationScreen"
                component={UserGeneralInformationScreen}
                options={{
                    ...TransitionPresets.DefaultTransition,
                }}
            />
            <Stack.Screen
                name="UserPasswordScreen"
                component={UserPasswordScreen}
                options={{
                    ...TransitionPresets.DefaultTransition,
                }}
            />
            <Stack.Screen
                name="UserAvatarUploadScreen"
                component={UserAvatarUploadScreen}
                options={{
                    ...TransitionPresets.DefaultTransition,
                }}
            />
            <Stack.Screen
                name="UserDiscussionScreen"
                component={UserDiscussionScreen}
                options={{
                    ...TransitionPresets.DefaultTransition,
                }}
            />
            <Stack.Screen
                name="UserCommentScreen"
                component={UserCommentScreen}
                options={{
                    ...TransitionPresets.DefaultTransition,
                }}
            />
            <Stack.Screen
                name="DiscussionDetail"
                component={DiscussionDetailScreen}
                options={{
                    ...TransitionPresets.DefaultTransition,
                }}
            />
        </Stack.Navigator>
    )
}

export default UserStack
