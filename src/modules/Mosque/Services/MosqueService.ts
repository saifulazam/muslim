import * as MosqueAction from '../redux/MosqueAction'
import Http from '../../../utils/Http'
import Transformer from '../../../utils/Transformer'

export function getNearByMosques(latitude, longitude, radius = 10) {
    return (dispatch) => {
        new Promise((resolve, reject) => {
            Http.get(`mosques/find/nearby?limit=20&latitude=${latitude}&longitude=${longitude}&${radius}`)
                .then((response) => {
                    const data = Transformer.fetch(response.data)
                    dispatch(MosqueAction.setMosqueRadius(radius))
                    dispatch(MosqueAction.nearByMosques(data))
                    return resolve()
                })
                .catch((error) => {
                    console.warn(error)
                    return reject()
                })
        })
    }
}
