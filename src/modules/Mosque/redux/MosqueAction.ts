import { MosqueActionType } from './MosqueActionType'

export function nearByMosques(payload) {
    return {
        type: MosqueActionType.ALL_MASQUES_FETCH,
        payload,
    }
}

export function setMosqueRadius(payload) {
    return {
        type: MosqueActionType.ALL_MOSQUE_FETCH_RADIUS,
        payload,
    }
}
