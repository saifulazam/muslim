export enum MosqueActionType {
    ALL_MASQUES_FETCH = '@ALL_MASQUES_FETCH',
    ALL_MOSQUE_FETCH_RADIUS = '@ALL_MOSQUE_FETCH_RADIUS',
}

export type MosqueLocationModel = {
    id: number
    address: string
    address2?: string
    city: string
    state: string
    zip_code: string
    country: string
    latitude: string
    longitude: string
    formatted_address: string
    vicinity: string
}

export type MosqueModel = {
    id: number
    name: string
    phone_number: string
    photo: string
    verified: boolean
    website: string
    email: string
    description: string
    location: MosqueLocationModel
    services?: any
    staffs?: any
}
export type MosqueState = {
    mosques: MosqueModel[]
    radius: Number
}
export type MosqueAction = {
    type: string
    payload: any
}
