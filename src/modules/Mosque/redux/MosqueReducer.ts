import { MosqueState, MosqueActionType, MosqueAction } from './MosqueActionType'

const INITIAL_STATE: MosqueState = {
    mosques: [],
    radius: null,
}

const reducer = (state = INITIAL_STATE, actionType: MosqueAction) => {
    switch (actionType.type) {
        case MosqueActionType.ALL_MASQUES_FETCH:
            return getMosques(state, actionType.payload)
        case MosqueActionType.ALL_MOSQUE_FETCH_RADIUS:
            return setMosqueRadius(state, actionType.payload)
        default:
            return state
    }
}

const getMosques = (state, payload) => {
    return { ...state, mosques: state.radius === 10 ? Array.from(payload.data) : [...state.mosques, ...payload.data] }
}
const setMosqueRadius = (state, payload) => {
    return { ...state, radius: payload }
}

export default reducer
