import { createStackNavigator, StackNavigationOptions, TransitionPresets } from '@react-navigation/stack'
import React from 'react'
import MosqueDonationScreen from '../../Donation/Screens/MosqueDonationScreen'
import MosqueDetailsScreen from '../Screens/MosqueDetailsScreen'
import MosqueLandingScreen from '../Screens/MosqueLandingScreen'

export type MosqueStackParamList = {
    Mosque: undefined
    MosqueDetails: undefined
    MosqueDonation: undefined
}

const Stack = createStackNavigator<MosqueStackParamList>()

const MosqueStack = () => {
    const navigationOptions: StackNavigationOptions = {
        headerShown: false,
        gestureEnabled: false,
    }

    return (
        <Stack.Navigator screenOptions={navigationOptions} mode="modal">
            <Stack.Screen
                name="Mosque"
                component={MosqueLandingScreen}
                options={{
                    ...TransitionPresets.DefaultTransition,
                }}
            />
            <Stack.Screen
                name="MosqueDetails"
                component={MosqueDetailsScreen}
                options={{
                    ...TransitionPresets.DefaultTransition,
                }}
            />
            {/* <Stack.Screen name="MosqueDonation" component={MosqueDonationScreen} /> */}
        </Stack.Navigator>
    )
}

export default MosqueStack
