import React, { Component } from 'react'
import { Text, View, ScrollView, TouchableOpacity, Linking, Image, Alert, Share, Platform } from 'react-native'
import Icon from '../../../common/Icon'
import MosqueStaffCard from '../components/MosqueStaffCard'
import { StackNavigationProp } from '@react-navigation/stack'
import RenderMosqueHeader from '../components/mosqueHeader'
import MosqueDetailInfo from '../components/MosqueDetailInfo'
import { connect } from 'react-redux'
import { AxiosServiceExecutorGet } from '../../../services/ServiceExecutor'
import Spinner from '../../../common/Spinner'
import { CustomStatusBar } from '../../../utils/StatusBar'
import Toast from '../../../lib/Toast/Toast'

interface Props {
    navigation?: StackNavigationProp<any>
    route: any
    isAuthenticated?: boolean
}

interface State {
    mosque: any
    isLoading: boolean
}

export class MosqueDetailsScreen extends Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {
            mosque: null,
            isLoading: true,
        }
    }
    componentDidMount() {
        CustomStatusBar(this.props.navigation, 'light-content')
        this._fetchMosque()
    }
    checkVerifiedMosque = (mosque) => {
        if (mosque.verified) {
            return <Icon name="verified" width={16} />
        }
        return null
    }

    _fetchMosque = () => {
        const { mosqueId } = this.props.route.params
        if (this.props.isAuthenticated) {
            AxiosServiceExecutorGet(`mosques/authorized/${mosqueId}`, this.responseMosqueCallback, true)
        } else {
            AxiosServiceExecutorGet(`mosques/${mosqueId}`, this.responseMosqueCallback)
        }
    }

    responseMosqueCallback = (response) => {
        if (response.success) {
            this.setState({ mosque: response.data, isLoading: false })
        } else {
            this.props.navigation.goBack()
            Toast.show({
                title: 'Something Went wrong.',
                text: 'Please come back later.',
                color: 'red',
                timing: 3000,
            })
        }
    }

    mosqueShare = async () => {
        const { name, location } = this.state.mosque
        try {
            const result = await Share.share({
                message: `I love to go this mosque to pray at ${name}-${location}`,
                title: name,
                url: '#',
            })

            if (result.action === Share.sharedAction) {
                Alert.alert('Post Shared')
            } else if (result.action === Share.dismissedAction) {
                Alert.alert('Post cancelled')
            }
        } catch (error) {
            Alert.alert(error.massage)
        }
    }

    renderStaffs = (mosque) => {
        if (mosque.staffs.length < 1) {
            return null
        }
        return (
            <View style={{ paddingTop: 20 }}>
                <Text style={{ fontSize: 20, fontWeight: 'bold', fontFamily: 'AvantGardeLT-Demi', marginLeft: 10 }}>
                    Administration
                </Text>
                <ScrollView
                    horizontal={true}
                    decelerationRate={0}
                    snapToInterval={200}
                    // snapToAlignment={'center'}
                    showsHorizontalScrollIndicator={false}
                >
                    <MosqueStaffCard users={mosque.staffs} />
                </ScrollView>
            </View>
        )
    }
    render() {
        // @ts-ignore
        const { navigation } = this.props
        const { mosque, isLoading } = this.state
        if (isLoading) {
            return <Spinner />
        }
        return (
            <ScrollView
                showsVerticalScrollIndicator={false}
                alwaysBounceVertical={false}
                style={{ backgroundColor: '#fff' }}
            >
                <RenderMosqueHeader
                    onPressBack={() => navigation.goBack()}
                    mosque={mosque}
                    navigation={navigation}
                    onPressShare={() => this.mosqueShare()}
                    reRender={this._fetchMosque}
                />
                <MosqueDetailInfo
                    mosque={mosque}
                    navigation={this.props.navigation}
                    isAuthenticated={this.props.isAuthenticated}
                />
                {this.renderStaffs(mosque)}
            </ScrollView>
        )
    }
}

const mapStateToProps = function (state) {
    return {
        isAuthenticated: state.auth.isAuthenticated,
    }
}

export default connect(mapStateToProps)(MosqueDetailsScreen)
