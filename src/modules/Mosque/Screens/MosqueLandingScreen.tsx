import React, { Component } from 'react'
import { Text, View, SafeAreaView, TextInput, StyleSheet, ScrollView, Animated } from 'react-native'
import { connect } from 'react-redux'
import { StackNavigationProp } from '@react-navigation/stack'
import { getNearByMosques } from '../Services/MosqueService'
import MosqueListItems from '../components/MosqueListItems'
import { CustomStatusBar } from '../../../utils/StatusBar'
import MosqueSearchBar from '../components/MosqueSearchBar'
import Icon from '../../../common/Icon'
import { AxiosServiceExecutorPost } from '../../../services/ServiceExecutor'

interface Props {
    navigation: StackNavigationProp<any>
    mosques: any // type this
    getNearByMosques: any
    coords: any
    address: any
}

interface State {
    search: string
    radius: number
    isLoadMore: boolean
    isRefresh: boolean
    scrollYValue: any
    searchResult: any
}

class MosqueLandingScreen extends Component<Props, State> {
    constructor(props) {
        super(props)
        this.state = {
            search: '',
            radius: 10,
            isLoadMore: false,
            isRefresh: false,
            scrollYValue: new Animated.Value(0),
            searchResult: [],
        }
    }

    componentDidMount() {
        CustomStatusBar(this.props.navigation, 'dark-content')
        // this._fetchAllNearMosques()
    }

    onChangeSearchHandler = (value) => {
        this.setState({ search: value })
        this._searchFetch()
    }

    _fetchAllNearMosques = () => {
        const { latitude, longitude } = this.props.coords
        this.props.getNearByMosques(latitude, longitude, this.state.radius)
    }

    _handleLoadMore = () => {
        this.setState(
            (prevState, nextProps) => ({
                radius: prevState.radius + 5,
                isLoadMore: true,
            }),
            () => {
                this._fetchAllNearMosques()
            }
        )
    }

    _onRefresh = () => {
        this.setState({ isRefresh: true })
        this._fetchAllNearMosques()
        setTimeout(() => {
            this.setState({ isRefresh: false })
        }, 2000)
    }

    _searchFetch = () => {
        const bodyParameters = {
            name: this.state.search,
        }
        AxiosServiceExecutorPost('/mosques/search', bodyParameters, this.searchResponseCallback)
    }

    searchResponseCallback = (response) => {
        if (response.success) {
            this.setState({ searchResult: response.data })
        }
    }

    render() {
        const clampedScroll = Animated.diffClamp(
            Animated.add(
                this.state.scrollYValue.interpolate({
                    inputRange: [0, 1],
                    outputRange: [0, 1],
                    extrapolateLeft: 'clamp',
                }),
                new Animated.Value(0)
            ),
            0,
            50
        )
        return (
            <Animated.View style={{ backgroundColor: 'white', flex: 1 }}>
                <SafeAreaView>
                    <View style={{ paddingHorizontal: 10, flexDirection: 'row', paddingVertical: 10 }}>
                        <Text
                            style={{
                                fontSize: 24,
                                fontWeight: 'bold',
                                letterSpacing: -0.6,
                                color: '#000',
                                fontFamily: 'AvantGardeLT-Demi',
                            }}
                        >
                            Near {this.props.address.street}
                        </Text>
                        <Icon name="arrow_down_light" />
                    </View>
                    <MosqueSearchBar
                        clampedScroll={clampedScroll}
                        value={this.state.search}
                        onChangeText={this.onChangeSearchHandler}
                    />
                    <View>
                        {this.state.search ? (
                            <MosqueListItems
                                navigation={this.props.navigation}
                                mosques={this.state.searchResult}
                                onLoadMore={this._handleLoadMore}
                                onRefresh={this._onRefresh}
                                isRefresh={this.state.isRefresh}
                                isLoadMore={this.state.isLoadMore}
                                scrollYValue={this.state.scrollYValue}
                            />
                        ) : (
                            <MosqueListItems
                                navigation={this.props.navigation}
                                mosques={this.props.mosques}
                                onLoadMore={this._handleLoadMore}
                                onRefresh={this._onRefresh}
                                isRefresh={this.state.isRefresh}
                                isLoadMore={this.state.isLoadMore}
                                scrollYValue={this.state.scrollYValue}
                            />
                        )}
                    </View>
                </SafeAreaView>
            </Animated.View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        mosques: state.mosques.mosques,
        coords: state.location.coords,
        address: state.location.address,
    }
}

export default connect(mapStateToProps, { getNearByMosques })(MosqueLandingScreen)

const styles = StyleSheet.create({
    searchSection: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderWidth: 0.5,
        borderColor: 'gray',
        height: 50,
        borderRadius: 25,
        marginVertical: 20,
        marginHorizontal: 10,
    },
    searchIcon: {
        // padding: 10,
        margin: 10,
        alignItems: 'center',
        color: 'gray',
    },
})
