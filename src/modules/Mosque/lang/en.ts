const toastLang = {
    FAVORITE_SUCCESS: {
        title: 'Thanks you',
        text: 'Now this mosque added to you favorite list',
        color: '#2ecc71',
        timing: 2000,
    },
    FAVORITE_DELETED_SUCCESS: {
        title: '',
        text: 'We successfully remove this mosque from your favorite list',
        color: '#2ecc71',
        timing: 2000,
    },
    REPORT_SUCCESS: {
        title: 'Thank you',
        text: `We'll look at this matter.`,
        color: '#2ecc71',
        timing: 2000,
    },
}

export { toastLang }
