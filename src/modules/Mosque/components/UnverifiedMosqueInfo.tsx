import React from 'react'
import { View, Text, TouchableOpacity, Linking } from 'react-native'

interface Props {}

const UnverifiedMosqueInfo = (props: Props) => {
    return (
        <View
            style={{
                marginHorizontal: 50,
                justifyContent: 'center',
                alignItems: 'center',
                padding: 30,
                backgroundColor: '#fafafa',
                marginTop: 20,
                borderRadius: 10,
                borderColor: '#dddddd',
                borderWidth: 1,
            }}
        >
            <Text
                style={{
                    fontSize: 18,
                    fontWeight: 'bold',
                    color: '#308788',
                    paddingBottom: 10,
                    fontFamily: 'AvantGardeLT-Demi',
                }}
            >
                Claiming Mosque
            </Text>
            <Text style={{ textAlign: 'center', paddingBottom: 10 }}>
                Want to unlock donation feature and update important information about this mosque?
            </Text>
            <TouchableOpacity
                onPress={() => Linking.openURL('mailto: teamalnur@gmail.com')}
                style={{
                    width: '100%',
                    height: 44,
                    backgroundColor: '#651DD3',
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderRadius: 31,
                }}
            >
                <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 16 }}>Submit Request</Text>
            </TouchableOpacity>
        </View>
    )
}

export default UnverifiedMosqueInfo
