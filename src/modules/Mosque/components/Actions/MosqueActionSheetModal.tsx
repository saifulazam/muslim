import ActionButton from '../../../../common/ActionButton'
import React from 'react'
import { View, Text } from 'react-native'
import Modal, { SlideAnimation, ModalContent, ModalTitle } from 'react-native-modals'

interface Props {
    visitable: boolean
    onCancel: any
    mosque: any
    onPress?: any
}

const MosqueActionSheetModal = (props: Props) => {
    return (
        <Modal.BottomModal
            visible={props.visitable}
            modalTitle={<ModalTitle title="Choose" hasTitleBar />}
            width={1}
            onTouchOutside={() => props.onCancel()}
            modalAnimation={
                new SlideAnimation({
                    slideFrom: 'bottom',
                })
            }
            onSwipeOut={props.onCancel}
            swipeDirection="down"
        >
            <ModalContent>
                <ActionButton onPress={() => {}} name="Follow" />
                <ActionButton onPress={() => {}} name="Call" />
                <ActionButton onPress={() => {}} name="Direction" />
                <ActionButton onPress={() => {}} name="Website" />
                <ActionButton onPress={() => {}} name="Report" />
                <ActionButton onPress={() => props.onCancel()} name="Cancel" color="gray" />
                <View style={{ paddingVertical: 20 }} />
            </ModalContent>
        </Modal.BottomModal>
    )
}

export default MosqueActionSheetModal
