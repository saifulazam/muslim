import React from 'react'
import { View, Text, Animated, StyleSheet, TextInput, Dimensions } from 'react-native'
import FormField from '../../../common/FormField'

const { width, height } = Dimensions.get('screen')

interface Props {
    clampedScroll?: any
    value?: string
    onChangeText: (value: string) => void
}

const MosqueSearchBar = (props: Props) => {
    const searchBarTranslate = props.clampedScroll.interpolate({
        inputRange: [0, 50],
        outputRange: [0, -250],
        extrapolate: 'clamp',
    })
    const searchBarOpacity = props.clampedScroll.interpolate({
        inputRange: [0, 10],
        outputRange: [1, 0],
        extrapolate: 'clamp',
    })
    return (
        <Animated.View
            style={[
                styles.container,
                {
                    transform: [
                        {
                            translateY: searchBarTranslate,
                        },
                    ],
                    opacity: searchBarOpacity,
                },
            ]}
        >
            <TextInput
                value={props.value}
                onChangeText={props.onChangeText}
                placeholder="Find mosque by name"
                style={styles.formField}
                placeholderTextColor={'#888888'}
            />
        </Animated.View>
    )
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 95,
        width: width,
        paddingHorizontal: 10,
        zIndex: 99,
        backgroundColor: 'white',
        paddingBottom: 20,
    },
    formField: {
        padding: 12,
        paddingLeft: 20,
        paddingRight: 20,
        borderRadius: 25,
        fontSize: 18,
        height: 50,
        backgroundColor: '#e8e8e8',
    },
})

export default MosqueSearchBar
