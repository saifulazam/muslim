import React from 'react'
import { View, Text, Image } from 'react-native'
import Card from '../../../common/Card'

interface Props {
    users: any
}

const MosqueStaffCard = ({ users }: Props) => {
    return users.map((user) => {
        return (
            <Card key={user.id} width={150}>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={{ uri: user.avatar }} style={{ width: 80, height: 80, borderRadius: 40 }} />
                    <Text style={{ fontWeight: 'bold', fontSize: 16, marginTop: 10, paddingHorizontal: 2 }}>
                        {user.name}
                    </Text>
                    <Text style={{ color: 'gray' }}>{user.job_title}</Text>
                </View>
            </Card>
        )
    })
}

export default MosqueStaffCard
