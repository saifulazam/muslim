import React, { useState } from 'react'
import { View, Text } from 'react-native'
import HTML from 'react-native-render-html'
import ReadMore from '../../../common/ReadMore'
import COLORS from '../../../utils/Color'

interface Props {
    mosque?: any
}

const MosqueAbout = (props: Props) => {
    const [showMore, setShowMore] = useState(false)

    return (
        <View style={{ marginHorizontal: 10 }}>
            <Text style={{ fontSize: 20, fontWeight: 'bold', fontFamily: 'AvantGardeLT-Demi', paddingVertical: 10 }}>
                About
            </Text>
            <ReadMore>{props.mosque.description}</ReadMore>
        </View>
    )
}

export default MosqueAbout
