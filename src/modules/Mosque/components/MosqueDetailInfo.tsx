import React from 'react'
import { View, Text, TouchableOpacity, Platform, Linking } from 'react-native'
import { authentication } from '../../../utils/Auth'
import Icon from '../../../common/Icon'
import MosqueAbout from './MosqueAbout'
import MosqueInfo from '../components/MosqueInfo'
import MosqueServices from '../components/MosqueServices'
import UnverifiedMosqueInfo from '../components/UnverifiedMosqueInfo'

interface Props {
    mosque: any
    navigation?: any
    isAuthenticated?: any
}

const onPressDonation = (props: Props) => {
    if (props.isAuthenticated) {
        return props.navigation.navigate('MosqueDonation', { mosque: props.mosque })
    } else {
        return props.navigation.navigate('LoginScreen')
    }
}

const checkVerifiedMosque = (mosque) => {
    if (mosque.verified) {
        return <Icon name="verified" width={16} />
    }
    return null
}

const mosqueName = (mosque) => {
    return (
        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ fontSize: 20, fontWeight: 'bold', fontFamily: 'AvantGardeLT-Demi', textAlign: 'center' }}>
                {mosque.name}
            </Text>
            {checkVerifiedMosque(mosque)}
        </View>
    )
}

const verifiedMosque = (props: Props) => {
    return (
        <View>
            <View style={{ marginHorizontal: 20 }}>
                <View style={{ justifyContent: 'center', alignItems: 'center', paddingVertical: 10 }}>
                    {mosqueName(props.mosque)}
                    <View
                        style={{
                            borderBottomColor: '#ebe8e8',
                            borderBottomWidth: 1,
                            padding: 10,
                            marginHorizontal: 20,
                        }}
                    />
                    <TouchableOpacity
                        onPress={() => onPressDonation(props)}
                        style={{
                            width: '100%',
                            height: 44,
                            backgroundColor: '#651DD3',
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderRadius: 31,
                        }}
                    >
                        <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 16 }}>Make a donation</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <MosqueInfo mosque={props.mosque} />
            {props.mosque.description ? <MosqueAbout mosque={props.mosque} /> : null}
            <MosqueServices mosque={props.mosque} />
        </View>
    )
}

const unverifiedMosque = (mosque) => {
    return (
        <View>
            <View style={{ justifyContent: 'center', alignItems: 'center', paddingVertical: 10 }}>
                {mosqueName(mosque)}
            </View>
            <MosqueInfo mosque={mosque} />
            <UnverifiedMosqueInfo />
        </View>
    )
}

const MosqueDetailInfo = (props: Props) => {
    if (props.mosque.verified) {
        return verifiedMosque(props)
    } else {
        return unverifiedMosque(props.mosque)
    }
}

export default MosqueDetailInfo
