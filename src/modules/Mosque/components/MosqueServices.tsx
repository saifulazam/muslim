import React from 'react'
import { View, Text } from 'react-native'
import Icon from '../../../common/Icon'

interface Props {
    mosque?: any
}

const MosqueServices = (props: Props) => {
    const renderMosqueServices = () => {
        return props.mosque.services.map((service) => {
            return (
                <View key={service.id} style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Icon name="check" width="24" />
                    <Text style={{ paddingLeft: 10 }}>{service.name}</Text>
                </View>
            )
        })
    }
    if (!props.mosque.services.length) {
        return null
    }
    return (
        <View style={{ marginHorizontal: 10 }}>
            <Text style={{ fontSize: 20, fontWeight: 'bold', fontFamily: 'AvantGardeLT-Demi', paddingVertical: 10 }}>
                Facility & Services
            </Text>
            {renderMosqueServices()}
        </View>
    )
}

export default MosqueServices
