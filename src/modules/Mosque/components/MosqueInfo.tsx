import React from 'react'
import { View, Text, Platform, Linking, TouchableOpacity } from 'react-native'
import { formatPhoneNumber } from '../../../utils/phone_number_format'
import * as WebBrowser from 'expo-web-browser'
import Icon from '../../../common/Icon'

interface Props {
    mosque: any
}

const mosqueInfoItem = (name, icon = null, onPress = null) => {
    return (
        <TouchableOpacity
            activeOpacity={0.5}
            onPress={onPress}
            style={{
                paddingVertical: 10,
                borderBottomColor: '#dcdcdc',
                borderBottomWidth: 1,
            }}
        >
            <View style={{ marginHorizontal: 20, flexDirection: 'row', alignItems: 'center' }}>
                <Icon name={icon} width={24} />
                <Text style={{ paddingLeft: 10, color: '#222222', fontSize: 15, fontFamily: 'MaisonNeue-Medium' }}>
                    {name}
                </Text>
            </View>
        </TouchableOpacity>
    )
}

const MosqueInfo = (props: Props) => {
    const { mosque } = props
    const scheme = Platform.select({ ios: 'maps:0,0?daddr=', android: 'google.navigation:?q=' })
    const encodedAddress = encodeURI(`${mosque.location.formatted_address}`)
    const url = Platform.select({
        android: `${scheme}${encodedAddress}`, // need to test it on android
        ios: `${scheme}${encodedAddress}`,
    })
    const phoneNumber = `${
        Platform.OS === 'ios'
            ? `telprompt:${formatPhoneNumber(mosque.phone_number)}`
            : `tel:${formatPhoneNumber(mosque.phone_number)}`
    }`

    return (
        <View style={{ flexDirection: 'column' }}>
            {mosque.location.vicinity
                ? mosqueInfoItem(mosque.location.vicinity, 'map_pin', () => {
                      Linking.openURL(url)
                  })
                : null}
            {mosque.phone_number
                ? mosqueInfoItem(formatPhoneNumber(mosque.phone_number), 'call', () => {
                      Linking.openURL(phoneNumber).catch((error) => console.warn(error))
                  })
                : null}
            {mosque.website
                ? mosqueInfoItem(mosque.website, 'web', () => {
                      WebBrowser.openBrowserAsync(mosque.website)
                  })
                : null}
            {mosque.email
                ? mosqueInfoItem(mosque.email, 'email', () => {
                      Linking.openURL(`mailto:${mosque.email}`)
                  })
                : null}
        </View>
    )
}

export default MosqueInfo
