import React from 'react'
import { View, Text, FlatList, RefreshControl, Animated } from 'react-native'
import { StackNavigationProp } from '@react-navigation/stack'
import Spinner from '../../../common/Spinner'
import MosqueCard from './MosqueCard'

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList)

interface Props {
    navigation?: StackNavigationProp<any>
    onLoadMore?: any
    onRefresh?: any
    isRefresh?: boolean
    isLoadMore?: boolean
    mosques: any
    scrollYValue?: any
}

const MosqueListItems = (props: Props) => {
    return (
        <AnimatedFlatList
            showsHorizontalScrollIndicator={false}
            onScroll={Animated.event(
                [{ nativeEvent: { contentOffset: { y: props.scrollYValue } } }],
                { useNativeDriver: true },
                //@ts-ignore
                () => {} // Optional async listener
            )}
            contentInsetAdjustmentBehavior="automatic"
            style={{
                paddingTop: 60,
                backgroundColor: 'white',
            }}
            data={props.mosques}
            renderItem={({ item }: { item: any }) => (
                <MosqueCard
                    data={item}
                    onPress={() => props.navigation.navigate('MosqueDetails', { mosqueId: item.id })}
                />
            )}
            keyExtractor={(item) => Math.random() + item.id.toString()}
            showsVerticalScrollIndicator={false}
            refreshControl={<RefreshControl refreshing={props.isRefresh} onRefresh={props.onRefresh.bind(this)} />}
            initialNumToRender={5}
            maxToRenderPerBatch={10}
            // onEndReached={props.onLoadMore}
            // onEndReachedThreshold={0}
            // // initialNumToRender={10}
            // ListFooterComponent={() => {
            //     if (!props.isLoadMore) return null
            //     return <Spinner />
            // }}
        />
    )
}

export default MosqueListItems
