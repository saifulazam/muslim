import React from 'react'
import { View, ImageBackground, Text, SafeAreaView, TouchableOpacity } from 'react-native'
import { StackNavigationProp } from '@react-navigation/stack'
import Icon from '../../../common/Icon'
import ActionButton from '../../../common/ActionButton'

//Need To change new Modal package
import ActionModal from '../../../components/ActionModal/ActionModal'
import { reports } from '../../../data/reports'
import CustomModal from '../../../components/CustomModal'

import { API } from 'react-native-dotenv'
import {
    ServicePrivateExecutorPost,
    ServiceExecutorPrivateGet,
    AxiosServiceExecutorPost,
    AxiosServiceExecutorPrivateDelete,
} from '../../../services/ServiceExecutor'
import { authentication } from '../../../utils/Auth'
import MosqueActionSheetModal from './Actions/MosqueActionSheetModal'
import ReportModal from '../../../common/ReportModal'
import Toast from '../../../lib/Toast/Toast'
import { toastLang } from '../lang/en'

interface Props {
    navigation: StackNavigationProp<any>
    mosque: any
    onPressShare: noop
    onPressBack: noop
    reRender?: any
}

interface State {
    actionModalVisitble: boolean
    reportModalVisitble: boolean
    reportValue: string
    isFavorited: boolean
}

export default class MosqueHeader extends React.Component<Props, State> {
    constructor(props) {
        super(props)
        this.state = {
            actionModalVisitble: false,
            reportModalVisitble: false,
            reportValue: '',
            isFavorited: this.props.mosque.isFavorited,
        }
    }

    onPressFavorite = () => {
        if (this.props.mosque.isFavorited) {
            AxiosServiceExecutorPrivateDelete(
                `mosques/${this.props.mosque.id}/favorites`,
                this.favoriteResponseCallback
            )
        } else {
            AxiosServiceExecutorPost(
                `mosques/${this.props.mosque.id}/favorites`,
                {},
                this.favoriteResponseCallback,
                true
            )
        }
    }

    favoriteResponseCallback = (response) => {
        if (response.status === 201) {
            Toast.show(toastLang.FAVORITE_SUCCESS)
            this.props.reRender()
        } else {
            Toast.show(toastLang.FAVORITE_DELETED_SUCCESS)
            this.props.reRender()
        }
    }

    renderActionSheet = () => {
        return (
            <ActionModal modalVisitble={this.state.actionModalVisitble} onCancel={() => this.onCancelActionModal()}>
                {this.props.mosque.isFavorited ? (
                    <ActionButton onPress={() => {}} name="unFavorite" />
                ) : (
                    <ActionButton onPress={() => this.onPressFavorite()} name="Favorite" />
                )}
                <ActionButton onPress={() => {}} name="Call" />
                <ActionButton onPress={() => {}} name="Direction" />
                <ActionButton onPress={() => {}} name="Website" />
                <ActionButton onPress={() => this.onPressReport()} name="Report" />
            </ActionModal>
        )
    }

    renderReportItem = () => {
        return reports.map((item, index) => {
            return (
                <View key={index}>
                    <View style={{ flexDirection: 'row', marginVertical: 15 }}>
                        <TouchableOpacity
                            style={{
                                height: 24,
                                width: 24,
                                borderRadius: 12,
                                borderWidth: 1,
                                borderColor: '#ACACAC',
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}
                            onPress={() => this.setState({ reportValue: item })}
                        >
                            {this.state.reportValue === item && (
                                <View
                                    style={{
                                        width: 14,
                                        height: 14,
                                        borderRadius: 7,
                                        backgroundColor: '#6500d3',
                                    }}
                                />
                            )}
                        </TouchableOpacity>
                        <Text
                            onPress={() => this.setState({ reportValue: item })}
                            style={{ marginLeft: 10, fontSize: 15, color: '#222222' }}
                        >
                            {item}
                        </Text>
                    </View>
                    <View style={{ width: '100%', height: 1, backgroundColor: 'lightgray' }} />
                </View>
            )
        })
    }

    renderReportModal = () => {
        return (
            <CustomModal modalVisible={this.state.reportModalVisitble}>
                <View style={{ justifyContent: 'center', alignItems: 'center', paddingBottom: 20 }}>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', fontFamily: 'AvantGardeLT-Demi' }}>
                        What's wrong with this?
                    </Text>
                </View>
                <View style={{ width: '100%', height: 1, backgroundColor: 'lightgray' }} />
                <View>{this.renderReportItem()}</View>
                <View
                    style={{
                        justifyContent: 'space-between',
                        flexDirection: 'row',
                        marginHorizontal: 30,
                        paddingTop: 20,
                    }}
                >
                    <TouchableOpacity
                        onPress={() => {
                            this.setState({ reportModalVisitble: false })
                        }}
                    >
                        <Text
                            style={{
                                fontSize: 18,
                                fontWeight: 'bold',
                                fontFamily: 'AvantGardeLT-Demi',
                                color: '#6500d3',
                            }}
                        >
                            Cancel
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.onPressReportExecution()}>
                        <Text
                            style={{
                                fontSize: 18,
                                fontWeight: 'bold',
                                fontFamily: 'AvantGardeLT-Demi',
                                color: '#6500d3',
                            }}
                        >
                            Save
                        </Text>
                    </TouchableOpacity>
                </View>
            </CustomModal>
        )
    }

    onPressReportExecution = () => {
        const bodyParams = {
            type: this.state.reportValue,
        }
        AxiosServiceExecutorPost(
            `reports/mosque/${this.props.mosque.id}`,
            bodyParams,
            this.responseReportCallback,
            true
        )
    }

    responseReportCallback = (response) => {
        if (response.status === 201) {
            this.setState({ reportModalVisitble: false })
            Toast.show(toastLang.REPORT_SUCCESS)
        } else {
            console.warn('something went wrong')
        }
    }

    onPressReport = async () => {
        const auth = await authentication()
        if (auth) {
            this.setState({ reportModalVisitble: true, actionModalVisitble: false })
        }
    }

    onPressActionModel = () => {
        this.setState({ actionModalVisitble: true })
    }
    onCancelActionModal = () => {
        this.setState({ actionModalVisitble: false })
    }

    onCancelReportModal = () => {
        this.setState((prevState) => ({ reportModalVisitble: !prevState.reportModalVisitble }))
    }

    render() {
        const { navigation, mosque, onPressShare, onPressBack } = this.props
        return (
            <ImageBackground source={{ uri: mosque.photo }} style={{ width: '100%', height: 250 }}>
                <View style={{ backgroundColor: 'rgba(0,0,0,0.3)', height: 250 }}>
                    <SafeAreaView
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            marginTop: 40,
                            marginHorizontal: 20,
                        }}
                    >
                        <TouchableOpacity onPress={() => onPressBack()}>
                            <Icon name="go_back_light" width={24} />
                        </TouchableOpacity>
                        <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity onPress={() => this.onPressFavorite()} style={{ marginRight: 10 }}>
                                <Icon
                                    name={this.props.mosque.isFavorited ? 'heart_active' : 'heart_inactive'}
                                    width={24}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.onPressActionModel()}>
                                <Icon name="more_vertical_light" width={24} />
                            </TouchableOpacity>
                            {this.renderActionSheet()}
                            {/* <MosqueActionSheetModal
                                visitable={this.state.actionModalVisitble}
                                onCancel={this.onCancelActionModal}
                                mosque={mosque}
                            /> */}

                            <ReportModal
                                visible={this.state.reportModalVisitble}
                                onCancel={this.onCancelReportModal}
                                onPress={this.onPressReportExecution}
                            />
                            {/* {this.state.reportModalVisitble ? this.renderReportModal() : null} */}
                        </View>
                    </SafeAreaView>
                </View>
            </ImageBackground>
        )
    }
}
