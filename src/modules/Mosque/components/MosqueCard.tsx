import Icon from '../../../common/Icon'
import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import LazyImage from '../../../common/LazyImage/LazyImage'
interface Props {
    data: any
    size?: any
    onPress: any
}

const MosqueCard = ({ data, size, onPress }: Props) => {
    const CheckVerifiedMosque = () => {
        if (data.verified) {
            return <Icon name="verified" width="16" />
        }
        return null
    }
    return (
        <TouchableOpacity
            activeOpacity={1}
            onPress={onPress}
            style={{ marginHorizontal: 10, width: size, paddingBottom: 20 }}
        >
            <LazyImage style={{ width: '100%', height: 170 }} source={{ uri: data.photo }} />
            <View style={{ flexDirection: 'row' }}>
                <Text
                    numberOfLines={2}
                    style={{ color: 'black', fontSize: 18, fontWeight: 'bold', paddingTop: 10, paddingBottom: 5 }}
                >
                    {data.name}
                </Text>
                <CheckVerifiedMosque />
            </View>
            <Text numberOfLines={1} style={{ color: 'gray' }}>
                {data['location'].vicinity}
            </Text>
        </TouchableOpacity>
    )
}

export default MosqueCard
