import { AuthModel } from '../redux/AuthActionType'
import Http from '../../../utils/Http'
import Transformer from '../../../utils/Transformer'
import * as AuthAction from '../redux/AuthAction'

export function login(credentials: AuthModel) {
    return (dispatch) =>
        new Promise((resolve, reject) => {
            Http.post('auth/login', credentials)
                .then((res) => {
                    const data = Transformer.fetch(res.data)
                    dispatch(AuthAction.authLogin(data.accessToken))
                    return resolve()
                })
                .catch((err) => {
                    const statusCode = err.response.status
                    const data = {
                        error: null,
                        statusCode,
                    }

                    if (statusCode === 422) {
                        const resetErrors = {
                            errors: err.response.data.errors,
                            replace: false,
                            searchStr: '',
                            replaceStr: '',
                        }
                        return {
                            ...data,
                            error: Transformer.resetValidationFields(resetErrors),
                        }
                    } else if (statusCode === 401) {
                        dispatch(
                            AuthAction.loginError('The email or password you entered do not match. Please try again.')
                        )
                        return {
                            ...data,
                            error: err.response.data.message,
                        }
                    }
                    return reject(data)
                })
        })
}

/**
 * logout user
 *
 * @returns {function(*)}
 */
export function logout() {
    return (dispatch) => {
        new Promise((resolve, reject) => {
            Http.delete('auth/logout')
                .then(() => {
                    dispatch(AuthAction.authLogout())
                    return resolve()
                })
                .catch((err) => {
                    console.warn(err)
                    return reject()
                })
        })
    }
}

export function getAuthUser() {
    return (dispatch) => {
        new Promise((resolve, reject) => {
            Http.get('auth/user')
                .then((response) => {
                    const data = Transformer.fetch(response.data)
                    dispatch(AuthAction.authUser(data))
                    return resolve()
                })
                .catch((error) => {
                    console.warn(error)
                    return reject()
                })
        })
    }
}
