import { AuthActionType, AuthState, AuthAction } from './AuthActionType'
import Http from '../../../utils/Http'
import { removeFromStorage, setStorage } from '../../../utils/Storage'

const initialState: AuthState = {
    isAuthenticated: false,
    user: null,
    token: null,
    loginError: '',
}

const reducer = (state = initialState, actionType: AuthAction) => {
    switch (actionType.type) {
        case AuthActionType.AUTH_REFRESH_TOKEN:
        case AuthActionType.AUTH_LOGIN:
            return login(state, actionType.payload)
        case AuthActionType.AUTH_CHECK:
            return checkAuth(state, actionType.payload)
        case AuthActionType.AUTH_LOGOUT:
            return logout(state)
        case AuthActionType.AUTH_RESET_PASSWORD:
            return resetPassword(state)
        case AuthActionType.AUTH_USER:
            return authUser(state, actionType.payload)
        case AuthActionType.AUTH_LOGIN_ERROR:
            return loginError(state, actionType.payload)
        default:
            return state
    }
}

const authUser = (state, payload) => {
    const token = state.token

    if (token) {
        Http.defaults.headers.common['Authorization'] = `Bearer ${token}`
        return { ...state, user: payload.data }
    } else {
        return { ...state, isAuthenticated: false, user: null, token: null }
    }
}

const login = (state, payload) => {
    setStorage('access_token', JSON.stringify(payload))
    Http.defaults.headers.common['Authorization'] = `Bearer ${payload}`
    return {
        ...state,
        isAuthenticated: true,
        token: payload,
        loginError: '',
    }
}

const loginError = (state, payload) => {
    return { ...state, loginError: payload, isAuthenticated: false }
}

//Has some bugs
const checkAuth = (state, payload) => {
    if (payload) {
        Http.defaults.headers.common['Authorization'] = `Bearer ${payload}`
    }
    return { ...state, isAuthenticated: true, token: payload, loginError: '' }
}

const logout = (state) => {
    removeFromStorage('access_token')
    return {
        ...state,
        isAuthenticated: false,
        token: null,
        user: null,
    }
}

function resetPassword(state) {
    return {
        ...state,
        resetPassword: true,
    }
}

export const getAuth = (state) => state.auth.isAuthenticated

export default reducer
