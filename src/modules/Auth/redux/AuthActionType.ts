export enum AuthActionType {
    AUTH_CHECK = '@AUTH_CHECK',
    AUTH_LOGIN = '@AUTH_LOGIN',
    AUTH_LOGIN_ERROR = '@AUTH_LOGIN_ERROR',
    AUTH_LOGOUT = '@AUTH_LOGOUT',
    AUTH_REFRESH_TOKEN = '@AUTH_REFRESH_TOKEN',
    AUTH_RESET_PASSWORD = '@AUTH_RESET_PASSWORD',
    AUTH_USER = '@AUTH_USER',
}

export type AuthModel = {
    email: string
    password: string
    remember: boolean
}

export type AuthState = {
    isAuthenticated: boolean
    user: any
    token?: string
    loginError?: string
}

export type AuthAction = {
    type: string
    payload: any
}
