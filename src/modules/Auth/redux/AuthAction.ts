import { AuthActionType } from './AuthActionType'

export function authCheck(payload) {
    return {
        type: AuthActionType.AUTH_CHECK,
        payload,
    }
}

export function authLogin(payload) {
    return {
        type: AuthActionType.AUTH_LOGIN,
        payload,
    }
}

export function authLogout() {
    return {
        type: AuthActionType.AUTH_LOGOUT,
    }
}

export function authRefreshToken(payload) {
    return {
        type: AuthActionType.AUTH_REFRESH_TOKEN,
        payload,
    }
}

export function authResetPassword() {
    return {
        type: AuthActionType.AUTH_RESET_PASSWORD,
    }
}

export function authUser(payload) {
    return {
        type: AuthActionType.AUTH_USER,
        payload,
    }
}

export function loginError(payload) {
    return {
        type: AuthActionType.AUTH_LOGIN_ERROR,
        payload,
    }
}

export function resetLoginError() {
    return {
        type: AuthActionType.AUTH_LOGIN_ERROR,
        payload: '',
    }
}
