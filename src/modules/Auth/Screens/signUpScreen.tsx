import React from 'react'
import {
    Text,
    View,
    KeyboardAvoidingView,
    Platform,
    StatusBar,
    ScrollView,
    TouchableWithoutFeedback,
    Keyboard,
    StyleSheet,
} from 'react-native'
import { NavigationProp } from '@react-navigation/native'
import { SafeAreaView } from 'react-native-safe-area-context'
import FormField from '../../../common/FormField'
import PrimaryButton from '../../../common/PrimaryButton'
import { API } from 'react-native-dotenv'
import ScreenLevelAlert from '../../../common/ScreenLevelAlert'
import { ServiceExecutorPost } from '../../../services/ServiceExecutor'
import _COLORS from '../../../utils/Color'
import Icon from '../../../common/Icon'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { CustomStatusBar } from '../../../utils/StatusBar'
import SuccessCreateAccount from '../components/SuccessCreateAccount'

interface Props {
    navigation?: NavigationProp<any>
}

interface State {
    firstname: string
    lastname: string
    email: string
    password: string
    isActive: boolean
    firstnameError: string
    lastnameError: string
    emailError: string
    passwordError: string
    errorMessage: string
    hidePassword: boolean
    isLoading: boolean
    isValidationContainerVisible: boolean
    successCreateAccount: boolean
}

export default class signUpScreen extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            firstname: '',
            lastname: '',
            isActive: false,
            firstnameError: null,
            lastnameError: null,
            emailError: null,
            passwordError: null,
            errorMessage: null,
            hidePassword: true,
            isLoading: false,
            isValidationContainerVisible: false,
            successCreateAccount: false,
        }
    }

    public componentDidMount() {
        CustomStatusBar(this.props.navigation, 'dark-content')
    }

    public render() {
        const { isLoading } = this.state
        if (this.state.successCreateAccount) {
            return <SuccessCreateAccount navigation={this.props.navigation} />
        }
        return (
            <KeyboardAvoidingView
                behavior={Platform.OS === 'ios' ? 'padding' : null}
                style={{ flex: 1, backgroundColor: 'white' }}
            >
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <SafeAreaView style={{ flex: 1, marginHorizontal: 20 }}>
                        <View
                            style={{
                                flexDirection: 'row',
                                marginTop: 20,
                                alignItems: 'center',
                                justifyContent: 'space-between',
                            }}
                        >
                            <Text style={{ fontSize: 24, fontWeight: 'bold' }}>Join now – it’s free!</Text>
                            <TouchableOpacity style={{ padding: 10 }} onPress={() => this.props.navigation.goBack()}>
                                <Icon name="close_dark" width={17} />
                            </TouchableOpacity>
                        </View>
                        <ScrollView showsVerticalScrollIndicator={false}>
                            {this.state.errorMessage && <ScreenLevelAlert errorMessage={this.state.errorMessage} />}
                            <View style={{ marginTop: 25 }}>
                                <FormField
                                    title={'First name'}
                                    value={this.state.firstname}
                                    onChangeText={this.onFirstnameTextInputChange}
                                    autoCapitalize="words"
                                    autoCorrect
                                    error={this.state.firstnameError}
                                    returnKeyType="next"
                                />
                            </View>
                            <View style={{ marginTop: 20 }}>
                                <FormField
                                    title={'Last name'}
                                    value={this.state.lastname}
                                    onChangeText={this.onLastnameTextInputChange}
                                    autoCapitalize="words"
                                    autoCorrect
                                    error={this.state.lastnameError}
                                    returnKeyType="next"
                                />
                            </View>
                            <View style={{ marginTop: 20 }}>
                                <FormField
                                    // error={this.state.email.length <= 0 && !this.state.email.includes('@')}
                                    error={this.state.emailError}
                                    title={'Email'}
                                    value={this.state.email}
                                    onChangeText={this.onEmailTextInputChange}
                                    autoCorrect
                                    // errorMessage={this.state.emailError}
                                    keyboardType="email-address"
                                    returnKeyType="next"
                                />
                            </View>
                            <View style={{ marginTop: 20 }}>
                                <FormField
                                    title={'Password'}
                                    value={this.state.password}
                                    onChangeText={this.onPasswordTextInputChange}
                                    secureTextEntry={this.state.hidePassword ? true : false}
                                    error={this.state.passwordError}
                                    icon={true}
                                    iconOnPress={this.setPasswordVisibility}
                                    iconOnChange={this.state.hidePassword}
                                    returnKeyType="done"
                                />
                            </View>
                            {this.state.isValidationContainerVisible && (
                                <this.PasswordValidationContainer password={this.state.password} />
                            )}
                            <View style={{ marginTop: 30 }}>
                                <PrimaryButton
                                    isActive={
                                        this.state.email &&
                                        this.state.password &&
                                        this.state.firstname &&
                                        this.state.lastname
                                            ? true
                                            : false
                                    }
                                    title={'Sign up'}
                                    onPress={() => this.onPressNext()}
                                    style={{ borderRadius: 31 }}
                                    isLoading={isLoading}
                                />
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        justifyContent: 'center',
                                        marginTop: 20,
                                    }}
                                >
                                    <Text>Already have an account?</Text>
                                    <Text
                                        onPress={() => this.props.navigation.navigate('LoginScreen')}
                                        style={{ color: _COLORS.primary_01, textDecorationLine: 'underline' }}
                                    >
                                        Log In
                                    </Text>
                                </View>
                                <View style={{ flex: 1 }} />
                            </View>
                        </ScrollView>
                    </SafeAreaView>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        )
    }

    public PasswordValidationContainer = ({ password }: { password: string }) => {
        const {
            validPasswordLength,
            isUpperCaseIncluded,
            isLowerCaseIncluded,
            isNumberIncluded,
            isSymbolIncluded,
        } = this.fieldValidation(password)
        return (
            <>
                <KeyboardAvoidingView behavior={Platform.OS == 'ios' ? 'padding' : 'height'} style={{ flex: 1 }}>
                    <View style={{ backgroundColor: '#e6e3e3', marginTop: 20 }}>
                        <View style={styles.innerContainer}>
                            <Icon name={validPasswordLength ? 'check_green' : 'uncheck_green'} />
                            <Text style={styles.textContainer}>{'8 characters'}</Text>
                        </View>
                        <View style={styles.innerContainer}>
                            <Icon name={!isUpperCaseIncluded ? 'check_green' : 'uncheck_green'} />
                            <Text style={styles.textContainer}>{'1 uppercase letter'}</Text>
                        </View>
                        <View style={styles.innerContainer}>
                            <Icon name={!isLowerCaseIncluded ? 'check_green' : 'uncheck_green'} />
                            <Text style={styles.textContainer}>{'1 lowercase letter'}</Text>
                        </View>
                        <View style={styles.innerContainer}>
                            <Icon name={!isNumberIncluded ? 'check_green' : 'uncheck_green'} />
                            <Text style={styles.textContainer}>{'1 number'}</Text>
                        </View>
                        <View style={styles.innerContainer}>
                            <Icon name={!isSymbolIncluded ? 'check_green' : 'uncheck_green'} />
                            <Text style={styles.textContainer}>{'1 symbol'}</Text>
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </>
        )
    }

    protected fieldValidation = (
        password: string
    ): {
        validPasswordLength: boolean
        isUpperCaseIncluded: boolean
        isLowerCaseIncluded: boolean
        isNumberIncluded: boolean
        isSymbolIncluded: number
    } => {
        const validPasswordLength = password.length >= 8
        const isUpperCaseIncluded = password.search(/[A-Z]/) == -1
        const isLowerCaseIncluded = password.search(/[a-z]/) == -1
        const isNumberIncluded = password.search(/[0-9]/) == -1
        const isSymbolIncluded = password.search(/(?=.*[!@#$%^&*])/)

        return { validPasswordLength, isUpperCaseIncluded, isLowerCaseIncluded, isNumberIncluded, isSymbolIncluded }
    }

    public isPrimaryButtonActive = (value: boolean) => {
        this.setState({ isActive: value })
    }

    public setPasswordVisibility = () => {
        this.setState({ hidePassword: !this.state.hidePassword })
    }

    public onFirstnameTextInputChange = (firstname: string) => {
        this.setState({
            firstname,
            // isActive: firstname.length > 0,
            firstnameError: null,
            lastnameError: null,
            emailError: null,
            passwordError: null,
            errorMessage: null,
        })
    }

    public onLastnameTextInputChange = (lastname: string) => {
        this.setState({
            lastname,
            // isActive: lastname.length > 0,
            firstnameError: null,
            lastnameError: null,
            emailError: null,
            passwordError: null,
            errorMessage: null,
        })
    }

    public onEmailTextInputChange = (email: string) => {
        this.setState({
            email,
            // isActive: email.length > 0,
            firstnameError: null,
            lastnameError: null,
            emailError: null,
            passwordError: null,
            errorMessage: null,
        })
    }

    public onPasswordTextInputChange = (password: string) => {
        const {
            validPasswordLength,
            isUpperCaseIncluded,
            isLowerCaseIncluded,
            isNumberIncluded,
            isSymbolIncluded,
        } = this.fieldValidation(password)
        const areAllFieldsValid =
            validPasswordLength &&
            !isUpperCaseIncluded &&
            !isLowerCaseIncluded &&
            !isNumberIncluded &&
            !isSymbolIncluded

        this.setState({
            password,
            isActive: areAllFieldsValid,
            firstnameError: null,
            lastnameError: null,
            emailError: null,
            passwordError: null,
            errorMessage: null,
            isValidationContainerVisible: password.length > 0,
        })
    }

    public submitButtonDisabled = () => {
        const { email, password, firstname, lastname } = this.state
        if (email && password && firstname && lastname) {
            this.setState({ isActive: true })
        }
    }

    public onPressNext = () => {
        const { email, password, firstname, lastname } = this.state
        this.setState({ isActive: false, isLoading: true })
        this.register(firstname, lastname, email, password)
    }

    protected register = (firstname, lastname, email, password) => {
        const url = `${API}/user/register`

        let bodyParams = {
            first_name: firstname,
            last_name: lastname,
            email: email,
            password: password,
        }
        ServiceExecutorPost(url, bodyParams, this.responseCallback)
    }

    public responseCallback = (response) => {
        if (response.data) {
            this.setState({ isLoading: false, successCreateAccount: true })
            // this.props.navigation?.navigate('AlmostThere')
        } else if (response.hasOwnProperty('errors')) {
            this.setState({ errorMessage: response.message, isLoading: false })
            this.fieldErrorHandeler(response.errors)
        } else {
            if (response.hasOwnProperty('message')) {
                this.setState({ errorMessage: response.message, isLoading: false })
            }
            console.warn(response)
        }
    }

    protected fieldErrorHandeler = (errors) => {
        if (errors.email) {
            this.setState({ emailError: errors.email[0] })
        }
        if (errors.first_name) {
            this.setState({ firstnameError: errors.first_name[0] })
        }
        if (errors.last_name) {
            this.setState({ lastnameError: errors.last_name[0] })
        }
        if (errors.password) {
            this.setState({ passwordError: errors.password[0] })
        }
    }
}

const styles = StyleSheet.create({
    innerContainer: {
        flexDirection: 'row',
        margin: 5,
    },
    textContainer: {
        marginStart: 10,
        marginTop: 7,
        fontSize: 16,
    },
})
