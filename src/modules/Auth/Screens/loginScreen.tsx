import React, { Dispatch } from 'react'
import { Text, View, KeyboardAvoidingView } from 'react-native'
import { NavigationProp } from '@react-navigation/native'
import { SafeAreaView } from 'react-native-safe-area-context'
import FormField from '../../../common/FormField'
import PrimaryButton from '../../../common/PrimaryButton'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import CheckBox from '../../../common/CheckBox'
import ScreenLevelAlert from '../../../common/ScreenLevelAlert'
import _COLORS from '../../../utils/Color'
import Icon from '../../../common/Icon'
import { checkLocationPermissionStatus } from '../../../utils/permissions'
import { connect } from 'react-redux'
import { onPermissionGrantedAction } from '../../../redux/actions/location.action'
import * as Localization from 'expo-localization'
import { getAuthUser } from '../services/AuthService'
import { CustomStatusBar } from '../../../utils/StatusBar'
import { setAllPrayerTimes } from '../../Prayer/services/PrayerService'
import { resetLoginError } from '../redux/AuthAction'
import { ServiceExecutorPost } from '../../../services/ServiceExecutor'
import { authLogin } from '../redux/AuthAction'
import { BASE_URL, API_VERSION } from 'react-native-dotenv'
import { multiGetStorage, multiSetStorage, removeFromStorage } from '../../../utils/Storage'
const API_URL = `${BASE_URL}/api/${API_VERSION}/client`

interface Props {
    navigation?: NavigationProp<any>
    onPermissionGrantedAction: any
    auth: any
    isAuthenticated?: boolean
    authLogin?: any
    dispatch?: any
    getAuthUser?: any
    setAllPrayerTimes?: any
    coords?: any
    loginError?: any
    resetLoginError?: any
}

interface State {
    email: string
    password: string
    isActive: boolean
    hidePassword: boolean
    rememberUsernameAndPassword: boolean
    errorMessage: string
    isLoading: boolean
    token: string
}

export class LoginScreen extends React.PureComponent<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            isActive: false,
            hidePassword: true,
            rememberUsernameAndPassword: false,
            errorMessage: null,
            isLoading: false,
            token: null,
        }

        this.onPressNext = this.onPressNext.bind(this)
    }

    public componentDidMount() {
        CustomStatusBar(this.props.navigation, 'dark-content')

        multiGetStorage(['username', 'password']).then((resp) => {
            this.setState({
                email: resp[0][1],
                password: resp[1][1],
                rememberUsernameAndPassword: resp[0][1] ? true : false,
                isActive: resp[0][1] ? true : false,
            })
        })
    }

    renderError = () => {
        if (this.state.errorMessage) {
            if (this.state.errorMessage === 'You need to active your account. please check your email.') {
                return (
                    <View
                        style={{
                            marginTop: 30,
                            backgroundColor: '#ffe8e7',
                            padding: 25,
                            borderColor: '#e50a00',
                            borderWidth: 1,
                            borderRadius: 8,
                            shadowOffset: { width: 2, height: 4 },
                            shadowColor: 'rgba(0, 0, 0, 0.2)',
                        }}
                    >
                        <Text style={{ alignSelf: 'center', fontSize: 15 }}>{this.state.errorMessage}</Text>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('RequestVerifyToken')}>
                            <Text style={{ fontSize: 15, color: 'red', paddingTop: 10 }}>
                                Request another verification email.
                            </Text>
                        </TouchableOpacity>
                    </View>
                )
            } else {
                return <ScreenLevelAlert errorMessage={this.state.errorMessage} />
            }
        }
    }

    public render() {
        return (
            <SafeAreaView style={{ flex: 1, marginHorizontal: 20 }}>
                <ScrollView style={{}} showsVerticalScrollIndicator={false}>
                    <View
                        style={{
                            flexDirection: 'row',
                            marginTop: 20,
                            justifyContent: 'space-between',
                            alignItems: 'center',
                        }}
                    >
                        <Text style={{ marginStart: 0, fontSize: 24, fontWeight: 'bold' }}>Log in</Text>
                        <TouchableOpacity style={{ padding: 10 }} onPress={() => this.props.navigation.goBack()}>
                            <Icon name="close_dark" width={17} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ marginTop: 10 }}>
                        {this.renderError()}
                        <FormField
                            placeholder={'example@company.com'}
                            title={'Email'}
                            value={this.state.email}
                            onChangeText={this.onEmailTextInputChange}
                        />
                    </View>
                    <View style={{ marginTop: 20 }}>
                        <FormField
                            title={'Password'}
                            value={this.state.password}
                            onChangeText={this.onPasswordTextInputChange}
                            icon
                            iconOnPress={this.setPasswordVisibility}
                            iconOnChange={this.state.hidePassword}
                            secureTextEntry={this.state.hidePassword ? true : false}
                        />
                    </View>
                    <Text
                        style={{
                            marginTop: 30,
                            alignSelf: 'center',
                            color: '#3c00b5',
                            textDecorationLine: 'underline',
                        }}
                        onPress={() => this.props.navigation.navigate('ForgotPassword')}
                    >
                        Forgot Password?
                    </Text>
                    <CheckBox checked={this.state.rememberUsernameAndPassword} onPress={() => this.onPressCheckbox()} />
                    <KeyboardAvoidingView style={{ flex: 1, marginHorizontal: 10, top: 30 }}>
                        <PrimaryButton
                            isActive={this.state.email && this.state.password ? true : false}
                            title={'Log in'}
                            onPress={() => (!this.state.isLoading ? this.onPressNext() : null)}
                            style={{ borderRadius: 31 }}
                            isLoading={this.state.isLoading}
                        />
                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'center',
                                marginTop: 20,
                            }}
                        >
                            <Text>Dont’ have an account?</Text>
                            <Text
                                onPress={() => this.props.navigation.navigate('SignUp')}
                                style={{ color: '#3c00b5', textDecorationLine: 'underline' }}
                            >
                                Sign up
                            </Text>
                        </View>
                    </KeyboardAvoidingView>
                </ScrollView>
            </SafeAreaView>
        )
    }

    protected onPressCheckbox = () => {
        this.setState((prevState) => ({ rememberUsernameAndPassword: !prevState.rememberUsernameAndPassword }))
    }

    public setPasswordVisibility = () => {
        this.setState({ hidePassword: !this.state.hidePassword })
    }

    public onEmailTextInputChange = (email: string) => {
        //Email validation
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
        // if (reg.test(email)) {
        //     this.setState({ email, isActive: email.length > 0, isLoading: false, errorMessage: null })
        // }
        this.setState({ email, isActive: email.length > 0, isLoading: false, errorMessage: null })
    }

    public onPasswordTextInputChange = (password: string) => {
        this.setState({ password, isActive: password.length > 0, isLoading: false, errorMessage: null })
    }

    public onPressNext = async () => {
        const { email, password } = this.state
        this.setState({ isLoading: true })
        await this.authenticate(email, password)
        if (this.props.loginError) {
            this.setState({ isLoading: false })
        }
    }

    protected authenticate = async (email, password) => {
        if (this.state.rememberUsernameAndPassword) {
            await multiSetStorage([
                ['username', email],
                ['password', password],
            ])
        } else {
            await removeFromStorage('username')
            await removeFromStorage('password')
        }
        const bodyParams = {
            email,
            password,
            remember: this.state.rememberUsernameAndPassword,
        }

        ServiceExecutorPost(`${API_URL}/auth/login`, bodyParams, this.loginResponseCallback)
    }

    loginResponseCallback = (response) => {
        if (response.hasOwnProperty('access_token')) {
            this.props.authLogin(response.access_token)
            this.props.getAuthUser()
            this.updateState()
        }
        if (response.hasOwnProperty('message')) {
            this.setState({
                errorMessage: response.message,
                isLoading: false,
            })
        }
    }

    public updateState = async (): Promise<void> => {
        const status = await checkLocationPermissionStatus()
        if (status) {
            this.setState({ isLoading: true })
            await this.granted()
        } else {
            await this.props.navigation.navigate('AlmostThere')
        }
    }

    public granted = () => {
        const { latitude, longitude } = this.props.coords
        if (latitude && longitude) {
            this.props.setAllPrayerTimes(latitude, longitude, Localization.timezone)
        } else {
            setTimeout(this.granted(), 2000)
        }
        this.navigate()
    }

    public navigate = async () => {
        this.setState({ isLoading: false })
        await this.props.navigation.navigate('Discover')
    }
}

const mapStateToProps = function (state) {
    return {
        prayerTimes: state.prayerTimes,
        mosques: state.mosques.mosques,
        isAuthenticated: state.auth.isAuthenticated,
        coords: state.location.coords,
        loginError: state.auth.loginError,
    }
}

export default connect(mapStateToProps, {
    authLogin,
    onPermissionGrantedAction,
    getAuthUser,
    setAllPrayerTimes,
    resetLoginError,
})(LoginScreen)
