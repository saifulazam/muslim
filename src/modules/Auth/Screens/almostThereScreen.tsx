import React from 'react'
import { Text, View, Image, Switch, Alert, Linking, ActivityIndicator, StatusBar } from 'react-native'
import { NavigationProp } from '@react-navigation/native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { checkUserPermission } from '../../../utils/permissions'
import { connect } from 'react-redux'
import { CustomStatusBar } from '../../../utils/StatusBar'
import { locationCoordsStore } from '../../Location/Services/LocationService'
import * as Location from 'expo-location'
import PrimaryButton from '../../../common/PrimaryButton'
import Icon from '../../../common/Icon'
import { setAllPrayerTimes } from '../../Prayer/services/PrayerService'
import { getAuthUser } from '../services/AuthService'
import * as Localization from 'expo-localization'

interface Props {
    navigation?: NavigationProp<any>
    locationCoordsStore?: any
    setAllPrayerTimes?: any
    getAuthUser?: any
    coords?: any
}

interface State {
    isToggleEnabled: boolean
    coords: any
    isLoading: boolean
}

export class almostThereScreen extends React.Component<Props, State> {
    constructor(props) {
        super(props)
        this.state = {
            isToggleEnabled: false,
            coords: null,
            isLoading: false,
        }
    }

    componentDidMount() {
        CustomStatusBar(this.props.navigation, 'dark-content')
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
                <View style={{ marginTop: 50, marginHorizontal: 20 }}>
                    <Text
                        style={{
                            fontSize: 14,
                            lineHeight: 16,
                            letterSpacing: 0.5,
                            fontWeight: '500',
                            color: 'rgba(0, 0, 0, 0.54)',
                        }}
                    >
                        ALMOST THERE
                    </Text>
                </View>
                <View style={{ marginHorizontal: 20 }}>
                    <Text
                        style={{
                            paddingTop: 16,
                            fontSize: 25,
                            fontWeight: '700',
                            lineHeight: 28,
                            letterSpacing: -0.5,
                        }}
                    >
                        Alnur needs your location to continue
                    </Text>
                    <Text style={{ paddingTop: 16, fontWeight: 'normal', fontSize: 16, color: 'rgba(0, 0, 0, 0.82)' }}>
                        We use this to help you find nearby mosques, get prayer times, locate qibla direction and
                        improve services.
                    </Text>
                    <View style={{ paddingTop: 30 }}>
                        <Image
                            source={require('../../../../assets/images/location_enable_icon.png')}
                            style={{ width: '100%' }}
                        />
                    </View>
                </View>
                <View
                    style={{
                        position: 'absolute',
                        left: 0,
                        right: 0,
                        bottom: 0,
                        backgroundColor: '#F0E6FB',
                        // paddingBottom: 50,
                        borderTopColor: '#6500D3',
                        borderTopWidth: 1,
                    }}
                >
                    <View style={{ marginHorizontal: 20, paddingVertical: 30 }}>
                        <View style={{ paddingBottom: 16, flexDirection: 'row' }}>
                            <Icon name="thumbs_up" width="24" />
                            <View style={{ paddingStart: 5 }}>
                                <Icon name="primary_heart" width="16" />
                            </View>
                        </View>
                        <Text
                            style={{
                                fontWeight: '400',
                                fontSize: 15,
                                lineHeight: 21,
                                letterSpacing: -0.41,
                                paddingBottom: 16,
                            }}
                        >
                            It is very important that you choose the Always Allow option in the next dialog. It makes
                            the app work better. Thank you.
                        </Text>
                        <PrimaryButton
                            isActive={true}
                            title="Grant Access to Location"
                            onPress={this.onSwitch}
                            style={{ borderRadius: 50 }}
                        />
                        <Text
                            style={{
                                color: '#333',
                                fontSize: 15,
                                fontWeight: '400',
                                lineHeight: 22,
                                textAlign: 'center',
                                paddingTop: 21,
                                paddingBottom: 30,
                            }}
                            onPress={() => this.props.navigation.navigate('AuthSetLocation')}
                        >
                            Enter location manually
                        </Text>
                    </View>
                </View>
            </SafeAreaView>
        )
    }

    public onSwitch = async () => {
        let check
        let status = await checkUserPermission(check)
        if (status) {
            this.setState({ isLoading: true })
            let location = await Location.getCurrentPositionAsync({ accuracy: Location.Accuracy.Highest })
            const { latitude, longitude } = location.coords
            let geocode = await Location.reverseGeocodeAsync(location.coords)
            if (location.coords && geocode[0]) {
                this.props.locationCoordsStore(latitude, longitude, geocode[0])
            }

            await this.navigate()
        }
    }

    public navigate = async () => {
        this.setState((prevState) => ({ isLoading: false, isToggleEnabled: !prevState.isToggleEnabled }))
        const { latitude, longitude } = this.props.coords
        Promise.all([
            this.props.setAllPrayerTimes(latitude, longitude, Localization.timezone),
            this.props.getAuthUser(),
        ])
        await this.props.navigation.navigate('Discover')
    }
}

const mapStateToProps = function (state) {
    return {
        coords: state.location.coords,
    }
}

export default connect(mapStateToProps, { locationCoordsStore, setAllPrayerTimes, getAuthUser })(almostThereScreen)
