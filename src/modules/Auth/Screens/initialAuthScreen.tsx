import React from 'react'
import { Video } from 'expo-av'
import {
    Text,
    View,
    StatusBar,
    Alert,
    Dimensions,
    StyleSheet,
    ActivityIndicator,
    TouchableOpacity,
    Animated,
    Image,
} from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import DefaultButton from '../../../common/DefaultButton'
import { StackNavigationProp } from '@react-navigation/stack'
import Icon from '../../../common/Icon'
import { connect } from 'react-redux'
import _COLORS from '../../../utils/Color'
import { locationCoordsStore } from '../../Location/Services/LocationService'
import { CustomStatusBar } from '../../../utils/StatusBar'
import * as Localization from 'expo-localization'
import { setAllPrayerTimes } from '../../Prayer/services/PrayerService'
import { getNearByMosques } from '../../Mosque/Services/MosqueService'
import { checkLocationPermissionStatus, checkUserPermission } from '../../../utils/permissions'
import * as Location from 'expo-location'
import { TermsOfUse } from '../../Company/components/TermsOfUse'
import { PrivacyPolicy } from '../../Company/components/PrivacyPolicy'
import Swiper from '../../../lib/Swiper'
import PrimaryButton from '../../../common/PrimaryButton'
import * as WebBrowser from 'expo-web-browser'
import { wait } from '../../../utils/datetime'

const screenSize = Dimensions.get('screen')

interface Props {
    navigation: StackNavigationProp<any>
    isAuthenticated?: boolean
    locationCoordsStore?: any
    coords?: any
    setAllPrayerTimes?: any
    getNearByMosques?: any
    firstTimeHere?: boolean
}

interface State {
    isLoading: boolean
    location: any
    geocode: any
    errorMessage: string
    termsOfUseModalVisible: boolean
    privacyPolicyModalVisible: boolean
}

const { width, height } = Dimensions.get('window')

export class initialAuthScreen extends React.PureComponent<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {
            isLoading: false,
            location: null,
            geocode: null,
            errorMessage: '',
            termsOfUseModalVisible: false,
            privacyPolicyModalVisible: false,
        }

        this._conditionChecks = this._conditionChecks.bind(this)
    }

    public async componentDidMount() {
        CustomStatusBar(this.props.navigation, 'light-content')

        if (this.props.isAuthenticated) {
            await this._conditionChecks()
        }
    }

    _conditionChecks = async () => {
        let status = await checkLocationPermissionStatus()
        //Check location permission status
        if (!status) {
            this.props.navigation.navigate('AlmostThere')
        } else {
            //check redux has coords (lat, long)
            if (this.props.coords) {
                // check if is user already logged in
                if (this.props.isAuthenticated) {
                    await this._onPressSkep()
                }
            } else {
                let location = await Location.getCurrentPositionAsync({ accuracy: Location.Accuracy.Highest })
                const { latitude, longitude } = location.coords
                let geocode = await Location.reverseGeocodeAsync(location.coords)
                if (location.coords && geocode[0]) {
                    this.props.locationCoordsStore(latitude, longitude, geocode[0])
                    if (this.props.isAuthenticated) {
                        await this._onPressSkep()
                    }
                } else {
                    this.props.navigation.navigate('AlmostThere')
                }
            }
        }
    }

    _onPressSkep = async () => {
        let status = await checkLocationPermissionStatus()
        if (!status) {
            this.props.navigation.navigate('AlmostThere')
        }
        this.setState({ isLoading: true })
        const { latitude, longitude } = this.props.coords
        this.props.setAllPrayerTimes(latitude, longitude, Localization.timezone),
            this.props.getNearByMosques(latitude, longitude)

        wait(3000).then(() => this.setState({ isLoading: !this.state.isLoading }))
        this.props.navigation.navigate('Discover')

        //  Promise.all([
        //      this.props.setAllPrayerTimes(latitude, longitude, Localization.timezone),
        //      this.props.getNearByMosques(latitude, longitude),
        //      this.setState({ isLoading: false }),
        //  ]).finally(() => this.props.navigation.navigate('Discover'))
    }

    _onPressTermOfUseCloseHandler = () => {
        this.setState({ termsOfUseModalVisible: false })
    }

    _onPressPrivacyPolicyCloseHandler = () => {
        this.setState({ privacyPolicyModalVisible: false })
    }

    _renderFooterButtons() {
        if (!this.props.isAuthenticated) {
            return (
                <View>
                    <DefaultButton
                        isActive
                        title={'Create an Account'}
                        onPress={() => this.props.navigation.navigate('SignUp')}
                        textColor="white"
                        style={{
                            width: '90%',
                            alignSelf: 'center',
                            borderRadius: 31,
                            shadowColor: '#000',
                            shadowOffset: {
                                width: 0,
                                height: 2,
                            },
                            shadowOpacity: 0.23,
                            shadowRadius: 2.62,
                            elevation: 4,
                            backgroundColor: _COLORS.primary,
                        }}
                    />
                    <View style={{ marginTop: 10 }}>
                        <TouchableOpacity
                            activeOpacity={0.5}
                            onPress={() => this.props.navigation.navigate('LoginScreen')}
                            style={{ padding: 10 }}
                        >
                            <Text
                                style={{
                                    color: '#fff',
                                    fontSize: 15,
                                    lineHeight: 22,
                                    textAlign: 'center',
                                    fontWeight: '700',
                                }}
                            >
                                Sign In
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        } else {
            return (
                <View>
                    <PrimaryButton
                        isActive
                        title={'Go Explore'}
                        onPress={() => this._conditionChecks()}
                        style={{
                            width: '90%',
                            alignSelf: 'center',
                            borderRadius: 31,
                            shadowColor: '#000',
                            shadowOffset: {
                                width: 0,
                                height: 2,
                            },
                            shadowOpacity: 0.23,
                            shadowRadius: 2.62,
                            elevation: 4,
                            backgroundColor: _COLORS.primary,
                        }}
                        isLoading={this.state.isLoading}
                    />
                </View>
            )
        }
    }

    _renderFooter() {
        return (
            <View>
                <View
                    style={{
                        flexDirection: 'column',
                        marginTop: 20,
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                >
                    <Text style={{ color: '#cacaca' }}>{`By proceeding, you agree to Alnur's `}</Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Text
                            onPress={() =>
                                WebBrowser.openBrowserAsync('http://alnurapp.com/corporate/terms-of-service')
                            }
                            style={{ textDecorationLine: 'underline', color: 'white' }}
                        >
                            Terms of use
                        </Text>
                        <Text style={{ color: '#cacaca' }}> and </Text>
                        <Text
                            onPress={() => WebBrowser.openBrowserAsync('http://alnurapp.com/corporate/privacy-policy')}
                            style={{ textDecorationLine: 'underline', color: 'white' }}
                        >
                            Privacy Policy.
                        </Text>
                    </View>
                </View>
            </View>
        )
    }
    public render() {
        return (
            <View style={styles.container}>
                <Swiper
                    autoplay={true}
                    showsPagination={false}
                    automaticallyAdjustContentInsets={true}
                    autoplayTimeout={6}
                >
                    <View style={styles.slide}>
                        <Image
                            source={require('../../../../assets/images/slides/image1.png')}
                            style={{ width: '100%', height: '100%' }}
                        />
                    </View>
                    <View style={styles.slide}>
                        <Image
                            source={require('../../../../assets/images/slides/image2.png')}
                            style={{ width: '100%', height: '100%' }}
                        />
                    </View>
                    <View style={styles.slide}>
                        <Image
                            source={require('../../../../assets/images/slides/image3.png')}
                            style={{ width: '100%', height: '100%' }}
                        />
                    </View>
                </Swiper>
                <View
                    style={{
                        position: 'absolute',
                        // alignItems: 'center',
                        // justifyContent: 'center',
                        display: 'flex',
                        width: '100%',
                        height: '100%',
                        // bottom: 0,
                    }}
                >
                    <View style={{ top: 0.11 * screenSize.height, alignItems: 'center' }}>
                        <Icon
                            name="logo_with_icon"
                            width="96"
                            height="102"
                            style={{
                                shadowColor: '#000',
                                shadowOffset: {
                                    width: 0,
                                    height: 2,
                                },
                                shadowOpacity: 0.23,
                                shadowRadius: 2.62,
                            }}
                        />
                    </View>
                    <View style={{ top: 0.6 * screenSize.height }}>
                        {this._renderFooterButtons()}
                        {this._renderFooter()}
                    </View>
                </View>
            </View>
        )
    }
}

const mapStateToProps = function (state) {
    return {
        isAuthenticated: state.auth.isAuthenticated,
        coords: state.location.coords,
        firstTimeHere: state.welcome.isFirstTimeHere,
    }
}

export default connect(mapStateToProps, {
    locationCoordsStore,
    setAllPrayerTimes,
    getNearByMosques,
})(initialAuthScreen)

const styles = StyleSheet.create({
    container: {
        // backgroundColor: 'transparent',
        flex: 1,
    },
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        height: height,
    },
    slide: {
        backgroundColor:
            'linear-gradient(180deg, rgba(0, 0, 0, 0.5) 0%, rgba(0, 0, 0, 0) 45.83%, rgba(0, 0, 0, 0.5) 100%), rgba(0, 0, 0, 0.25);',
    },
})
