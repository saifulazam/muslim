import { NavigationProp } from '@react-navigation/native'
import * as React from 'react'
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native'
import { Header } from 'react-native-elements'
import FormField from '../../../common/FormField'
import Icon from '../../../common/Icon'
import PrimaryButton from '../../../common/PrimaryButton'
import { ServiceExecutorPost } from '../../../services/ServiceExecutor'
import { API } from 'react-native-dotenv'
import ScreenLevelAlert from '../../../common/ScreenLevelAlert'
export interface Props {
    navigation?: NavigationProp<any>
}

export interface State {
    email: string
    error: string
}

export default class RequestVerifyTokenScreen extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {
            email: '',
            error: null,
        }
    }

    headerLeft = () => {
        return (
            <TouchableOpacity style={{ padding: 10 }} onPress={() => this.props.navigation.goBack()}>
                <Icon name="back_dark" width={17} />
            </TouchableOpacity>
        )
    }

    emailTextHandle = (email) => {
        this.setState({ email, error: null })
    }

    onPress = () => {
        const bodyParameters = {
            email: this.state.email,
        }
        const url = `${API}/user/activate`
        ServiceExecutorPost(url, bodyParameters, this.responseCallback)
    }

    responseCallback = (response) => {
        if (response.hasOwnProperty('message')) {
            this.setState({ error: response.message })
        }
    }

    render() {
        return (
            <View style={{ backgroundColor: '#fff', flex: 1 }}>
                <Header
                    statusBarProps={{ barStyle: 'dark-content' }}
                    barStyle="light-content" // or directly
                    leftComponent={this.headerLeft()}
                    // centerComponent={{ text: 'Request Email verification token', style: { color: '#000' } }}
                    containerStyle={{
                        backgroundColor: '#fff',
                        justifyContent: 'flex-start',
                    }}
                />
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Request Email verification token.</Text>
                </View>
                <View style={{ paddingTop: 50, paddingHorizontal: 20 }}>
                    {this.state.error && <ScreenLevelAlert errorMessage={this.state.error} />}
                    <FormField
                        value={this.state.email}
                        onChangeText={this.emailTextHandle}
                        placeholder={'example@company.com'}
                        title={'Enter your Email Address'}
                    />
                </View>
                <View style={{ paddingTop: 50, paddingHorizontal: 20 }}>
                    <PrimaryButton
                        title="Send Request"
                        onPress={() => this.onPress()}
                        isActive={this.state.email ? true : false}
                        style={{ borderRadius: 100 }}
                    />
                </View>
            </View>
        )
    }
}
