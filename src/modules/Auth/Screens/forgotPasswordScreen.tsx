import React from 'react'
import { Text, View, StatusBar, Dimensions, KeyboardAvoidingView } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import PrimaryButton from '../../../common/PrimaryButton'
import { StackNavigationProp } from '@react-navigation/stack'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import FormField from '../../../common/FormField'
import { ServiceExecutorPost } from '../../../services/ServiceExecutor'
import { API } from 'react-native-dotenv'
import { Overlay } from 'react-native-elements'
import Icon from '../../../common/Icon'
import { CustomStatusBar } from '../../../utils/StatusBar'

interface Props {
    navigation: StackNavigationProp<any>
}

interface State {
    email: string
    isActive: boolean
    isModalVisible: boolean
}

const { height, width } = Dimensions.get('window')

export default class forgotPasswordScreen extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {
            email: '',
            isActive: false,
            isModalVisible: false,
        }
    }
    public componentDidMount() {
        CustomStatusBar(this.props.navigation, 'dark-content')
    }
    public render() {
        return (
            <>
                <SafeAreaView style={{ flex: 1, marginHorizontal: 20, marginTop: 20 }}>
                    <ScrollView>
                        <View
                            style={{
                                flexDirection: 'row',
                                marginTop: 20,
                                justifyContent: 'space-between',
                                alignItems: 'center',
                            }}
                        >
                            <Text style={{ fontSize: 24, fontWeight: 'bold' }}>Forgot password</Text>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                                <Icon name="close_dark" width={17} />
                            </TouchableOpacity>
                        </View>
                        <Text style={{ fontSize: 15, fontWeight: '400', color: '#222222', marginVertical: 10 }}>
                            Enter the email address associated with your account to receive password reset instruction.
                        </Text>
                        <FormField
                            placeholder={'example@company.com'}
                            title={'Email'}
                            value={this.state.email}
                            onChangeText={this.onPasswordTextInputChange}
                        />
                        <KeyboardAvoidingView style={{ flex: 1, marginHorizontal: 10, top: 30 }}>
                            <PrimaryButton
                                isActive={this.state.isActive}
                                title={'Send'}
                                onPress={() => this.onPressSend()}
                                style={{
                                    width: '90%',
                                    alignSelf: 'center',
                                    borderRadius: 31,
                                    shadowColor: '#000',
                                    shadowOffset: {
                                        width: 0,
                                        height: 2,
                                    },
                                    shadowOpacity: 0.23,
                                    shadowRadius: 2.62,
                                    elevation: 4,
                                    // backgroundColor: '#3c00b5',
                                }}
                            />
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    marginTop: 20,
                                }}
                            >
                                <Text>Dont’ have an account? </Text>
                                <Text
                                    onPress={() => this.props.navigation.navigate('SignUp')}
                                    style={{ color: '#3c00b5', textDecorationLine: 'underline' }}
                                >
                                    Sign up
                                </Text>
                            </View>
                        </KeyboardAvoidingView>
                        {this.state.isModalVisible && <this.RenderModal />}
                    </ScrollView>
                </SafeAreaView>
            </>
        )
    }

    public onPasswordTextInputChange = (email) => {
        this.setState({ email, isActive: email.length > 0 })
    }

    public onPressSend = () => {
        const { email } = this.state
        const url = `${API}/auth/password/reset`
        ServiceExecutorPost(url, { email }, this.onCallback)
    }

    public onCallback = (response) => {
        if (response.errors) {
            console.warn(`something went wrong - ${response.message}`)
        } else {
            this.isModalVisible()
        }
    }

    public isModalVisible = () => {
        this.setState((prevState) => ({ isModalVisible: !prevState.isModalVisible }))
    }

    public RenderModal = () => {
        return (
            <Overlay
                isVisible={this.state.isModalVisible}
                // onBackdropPress={() => this.isModalVisible()}
                // onDismiss={() => this.isModalVisible()}
            >
                <View style={{ height, width }}>
                    <View
                        style={{ justifyContent: 'center', marginTop: 200, alignItems: 'center', marginHorizontal: 30 }}
                    >
                        <Text style={{ fontSize: 24, fontWeight: 'bold' }}>Check your email</Text>
                        <Text
                            style={{
                                fontSize: 15,
                                fontWeight: '400',
                                color: '#222222',
                                textAlign: 'center',
                                marginTop: 15,
                            }}
                        >
                            If account exist in our system, we’ll send you password recovery instruction.
                        </Text>
                    </View>
                    <PrimaryButton
                        title={'Done'}
                        onPress={() => {
                            this.setState({ isModalVisible: false })
                            this.props.navigation.navigate('LoginScreen')
                        }}
                        isActive
                        style={{
                            width: '30%',
                            alignSelf: 'center',
                            backgroundColor: '#3c00b5',
                            borderRadius: 31,
                            marginTop: 30,
                        }}
                    />
                </View>
            </Overlay>
        )
    }
}
