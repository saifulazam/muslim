import { createStackNavigator, StackNavigationOptions, TransitionPresets } from '@react-navigation/stack'
import React from 'react'
import setLocationScreen from '../../Location/Screens/setLocationScreen'
import almostThereScreen from '../Screens/almostThereScreen'
import forgotPasswordScreen from '../Screens/forgotPasswordScreen'
import initialAuthScreen from '../Screens/initialAuthScreen'
import LoginScreen from '../Screens/loginScreen'
import RequestVerifyTokenScreen from '../Screens/RequestVerifyTokenScreen'
import signUpScreen from '../Screens/signUpScreen'

export type AuthStackParamList = {
    InitialAuth: undefined
    LoginScreen: undefined
    SignUp: undefined
    ForgotPassword: undefined
    AlmostThere: undefined
    AuthSetLocation: undefined
    RequestVerifyToken: undefined
}

const Stack = createStackNavigator<AuthStackParamList>()

const AuthStack = () => {
    const navigationOptions: StackNavigationOptions = {
        headerShown: false,
        gestureEnabled: false,
    }

    return (
        <Stack.Navigator screenOptions={navigationOptions}>
            <Stack.Screen
                name="InitialAuth"
                component={initialAuthScreen}
                options={{
                    ...TransitionPresets.DefaultTransition,
                }}
            />
            <Stack.Screen
                name="LoginScreen"
                component={LoginScreen}
                options={{
                    ...TransitionPresets.DefaultTransition,
                }}
            />
            <Stack.Screen
                name="SignUp"
                component={signUpScreen}
                options={{
                    ...TransitionPresets.DefaultTransition,
                }}
            />
            <Stack.Screen
                name="ForgotPassword"
                component={forgotPasswordScreen}
                options={{
                    ...TransitionPresets.DefaultTransition,
                }}
            />
            <Stack.Screen
                name="AlmostThere"
                component={almostThereScreen}
                options={{
                    ...TransitionPresets.DefaultTransition,
                }}
            />
            <Stack.Screen
                name="AuthSetLocation"
                component={setLocationScreen}
                options={{
                    ...TransitionPresets.DefaultTransition,
                }}
            />
            <Stack.Screen
                name="RequestVerifyToken"
                component={RequestVerifyTokenScreen}
                options={{
                    ...TransitionPresets.DefaultTransition,
                }}
            />
        </Stack.Navigator>
    )
}

export default AuthStack
