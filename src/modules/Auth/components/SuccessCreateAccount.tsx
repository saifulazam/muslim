import { NavigationProp } from '@react-navigation/native'
import React from 'react'
import { View, Text, SafeAreaView, TouchableOpacity } from 'react-native'
import DefaultButton from '../../../common/DefaultButton'
import Icon from '../../../common/Icon'
import PrimaryButton from '../../../common/PrimaryButton'

interface Props {
    navigation?: NavigationProp<any>
}

const SuccessCreateAccount = (props: Props) => {
    return (
        <SafeAreaView style={{ flex: 1, marginHorizontal: 20 }}>
            <View
                style={{
                    flexDirection: 'row',
                    marginTop: 20,
                    alignItems: 'center',
                    justifyContent: 'space-between',
                }}
            >
                <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Verify your email Address</Text>
                <TouchableOpacity style={{ padding: 10 }} onPress={() => props.navigation.goBack()}>
                    <Icon name="close_dark" width={17} />
                </TouchableOpacity>
            </View>
            <View style={{ paddingTop: 30 }}>
                <View>
                    <Text>Before proceeding, please check your email for a verification link.</Text>
                </View>
                <View style={{ paddingTop: 100 }}>
                    <PrimaryButton
                        title="Go to login"
                        onPress={() => props.navigation.navigate('LoginScreen')}
                        isActive={true}
                        style={{ borderRadius: 100 }}
                    />
                </View>
                <View style={{ paddingTop: 20 }}>
                    <DefaultButton
                        title="Request another"
                        onPress={() => props.navigation.navigate('RequestVerifyToken')}
                        style={{ borderRadius: 100 }}
                        isActive={true}
                    />
                </View>
            </View>
        </SafeAreaView>
    )
}

export default SuccessCreateAccount
