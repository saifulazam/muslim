import React from 'react'
import { Alert, Modal, StyleSheet, Text, TouchableHighlight, View } from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import Icon from '../../../common/Icon'

interface Props {
    visible: boolean
    onClose: any
}

export const TermsOfUse = (props: Props) => {
    return (
        <Modal
            animationType="slide"
            transparent={false}
            visible={props.visible}
            onDismiss={() => props.onClose()}
            presentationStyle="pageSheet"
            onRequestClose={() => props.onClose()}
        >
            <View>
                <View
                    style={{
                        padding: 10,
                        borderBottomWidth: 1,
                        borderBottomColor: 'rgba(0, 0, 0, 0.3)',
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                    }}
                >
                    <TouchableOpacity onPress={() => props.onClose()}>
                        <Icon name="back_dark" width="18" />
                    </TouchableOpacity>
                    <Text style={{ fontSize: 18, color: '#333', fontWeight: '700' }}>Terms of Use</Text>
                    <Text />
                </View>
                <ScrollView style={{ marginHorizontal: 20, paddingTop: 30 }}>
                    <Text>Last Update: September 15, 2020</Text>
                    <Text
                        style={{
                            fontWeight: 'bold',
                            fontSize: 22,
                            lineHeight: 28,
                            letterSpacing: -0.25,
                            paddingTop: 16,
                        }}
                    >
                        TERMS AND CONDITIONS FOR THE ACQUISITION, MAINTENANCE AND USE OF ALNUR
                    </Text>
                </ScrollView>
            </View>
        </Modal>
    )
}
