import React from 'react'
import { View, Text, Share, Alert } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import LinkWithIcon from './LinkWithIcon'
import * as StoreReview from 'expo-store-review'

interface Props {
    user?: any
}

const OnPressInvite = async (user) => {
    try {
        const result = await Share.share({
            message: `${user?.name}: Hey, I found a cool muslim app where you can find prayer time, near mosque, donation to mosque etc...`,
            url: 'https://testflight.apple.com/join/fVnB7a3c',
        })
        if (result.action === Share.sharedAction) {
            if (result.activityType) {
                // shared with activity type of result.activityType
            } else {
                // shared
            }
        } else if (result.action === Share.dismissedAction) {
            // dismissed
        }
    } catch (error) {
        Alert.alert(error.message)
    }
}
const OnPressMail = () => {}

const AppFooter = (props: Props) => {
    return (
        <View style={{ padding: 20 }}>
            <Text style={{ fontWeight: '600', fontSize: 17, color: '#1C232D' }}>
                We are constantly working to improve Alnur app and we want to hear from you. Send us your feedback or a
                simple hello.
            </Text>
            <View style={{ paddingVertical: 20 }}>
                <LinkWithIcon title="salam@alnurapp.com" icon="mail" />
                <LinkWithIcon title="@teamalnur" icon="ig" />
                <LinkWithIcon title="@teamalnur" icon="tw" />
                <LinkWithIcon title="teamalnur" icon="fb" />
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                <TouchableOpacity
                    onPress={() => StoreReview.requestReview()}
                    style={{
                        backgroundColor: 'rgba(224,204,246,0.5)',
                        paddingHorizontal: 20,
                        paddingVertical: 10,
                        borderRadius: 100,
                    }}
                >
                    <Text style={{ color: '#6500D3', fontSize: 15, fontWeight: '700' }}>Rate Alnur App</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => OnPressInvite(props.user)}
                    style={{
                        backgroundColor: 'rgba(224,204,246,0.5)',
                        paddingHorizontal: 20,
                        paddingVertical: 10,
                        borderRadius: 100,
                    }}
                >
                    <Text style={{ color: '#6500D3', fontSize: 15, fontWeight: '700' }}>Invite a Friend</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default AppFooter
