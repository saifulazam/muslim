import React from 'react'
import { Alert, Modal, StyleSheet, Text, TouchableHighlight, TouchableOpacity, View } from 'react-native'
import Icon from '../../../common/Icon'

interface Props {
    visible: boolean
    onClose: any
}

export const PrivacyPolicy = (props: Props) => {
    return (
        <Modal
            animationType="slide"
            transparent={false}
            onDismiss={() => props.onClose()}
            visible={props.visible}
            presentationStyle="pageSheet"
            onRequestClose={() => props.onClose()}
        >
            <View>
                <View
                    style={{
                        padding: 10,
                        borderBottomWidth: 1,
                        borderBottomColor: 'rgba(0, 0, 0, 0.3)',
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                    }}
                >
                    <TouchableOpacity onPress={() => props.onClose()}>
                        <Icon name="back_dark" width="18" />
                    </TouchableOpacity>
                    <Text style={{ fontSize: 18, color: '#333', fontWeight: '700' }}>Privacy Policy</Text>
                    <Text />
                </View>
                <View style={{ marginHorizontal: 20, paddingTop: 30 }}>
                    <Text
                        style={{
                            fontWeight: 'bold',
                            fontSize: 22,
                            lineHeight: 28,
                            letterSpacing: -0.25,
                            paddingTop: 16,
                        }}
                    >
                        Privacy Policy
                    </Text>
                    <Text>Effective: As of September 15, 2020</Text>
                    <View>
                        <Text style={{ paddingVertical: 10 }}>
                            SAFT, LLC. and its subsidiary and affiliate companies (collectively, “Alnur,” “we,” “us” or
                            “our”) recognize the importance of protecting the personal information collected from users
                            in the operation of its services and taking reasonable steps to maintain the security,
                            integrity and privacy of any information in accordance with this Privacy Policy. By
                            submitting your information to Alnur you consent to the practices described in this policy.
                            If you are less than 18 years of age, then you must first seek the consent of your parent or
                            guardian prior to submitting any personal information.
                        </Text>
                        <Text style={{ paddingVertical: 10 }}>
                            This Privacy Policy describes how Alnur collects and uses the personal information you
                            provide to Alnur. It also describes the choices available to you regarding our use of your
                            personal information and how you can access and update this information.
                        </Text>
                    </View>
                </View>
            </View>
        </Modal>
    )
}
