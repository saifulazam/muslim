import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import Icon from '../../../common/Icon'

interface Props {
    onPress?: any
    title: string
    icon?: string
}

const LinkWithIcon = (props: Props) => {
    return (
        <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
            <Text style={{ fontSize: 16, fontWeight: '600' }}>{props.title}</Text>
            <Icon name={props.icon} width="24" />
        </TouchableOpacity>
    )
}

export default LinkWithIcon
