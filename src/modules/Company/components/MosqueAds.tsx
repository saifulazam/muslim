import React from 'react'
import { View, Text } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import * as WebBrowser from 'expo-web-browser'

interface Props {}

const MosqueAds = (props: Props) => {
    const _handleOnPressLink = () => {
        WebBrowser.openBrowserAsync('http://alnurapp.com/')
    }
    return (
        <View
            style={{
                backgroundColor: '#3D007F',
                borderRadius: 13,
                justifyContent: 'center',
                alignItems: 'center',
                padding: 30,
            }}
        >
            <Text style={{ fontSize: 17, fontWeight: 'bold', color: 'white' }}>Are you a mosque?</Text>
            <Text style={{ color: 'white', fontSize: 13, padding: 20, textAlign: 'center' }}>
                Unlock donation feature and update imporant information about your mosque to your followers.
            </Text>
            <TouchableOpacity
                onPress={() => _handleOnPressLink()}
                style={{ backgroundColor: '#6500D3', borderRadius: 100, paddingVertical: 10, paddingHorizontal: 30 }}
            >
                <Text style={{ color: 'white', fontSize: 15, fontWeight: '700' }}>Get Listed for free</Text>
            </TouchableOpacity>
        </View>
    )
}

export default MosqueAds
