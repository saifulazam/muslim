import { createStackNavigator, StackNavigationOptions, TransitionPresets } from '@react-navigation/stack'
import React from 'react'
import WelcomeScreen from '../Screens/WelcomeScreen'

export type WelcomeStackParamList = {
    Welcome: undefined
}

const Stack = createStackNavigator<WelcomeStackParamList>()

const WelcomeStack = () => {
    const navigationOptions: StackNavigationOptions = {
        headerShown: false,
        gestureEnabled: false,
    }

    return (
        <Stack.Navigator screenOptions={navigationOptions}>
            <Stack.Screen
                name="Welcome"
                component={WelcomeScreen}
                options={{
                    ...TransitionPresets.DefaultTransition,
                }}
            />
        </Stack.Navigator>
    )
}

export default WelcomeStack
