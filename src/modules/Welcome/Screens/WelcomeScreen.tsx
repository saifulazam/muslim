import { StackNavigationProp } from '@react-navigation/stack'
import * as React from 'react'
import { View, StyleSheet, Text, TouchableOpacity, StatusBar, Dimensions, Image } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { CustomStatusBar } from '../../../utils/StatusBar'
import { connect } from 'react-redux'
import { setFirstTimeHere } from '../redux/WelcomeAction'
import Swiper from '../../../lib/Swiper'

export interface Props {
    navigation: StackNavigationProp<any>
    setFirstTimeHere?: any
}

export interface State {
    slide: number
}

export class WelcomeScreen extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {
            slide: 1,
        }
    }
    _onPressToDone = () => {
        this.props.setFirstTimeHere()
        this.props.navigation.navigate('InitialRoute')
        CustomStatusBar(this.props.navigation, 'dark-content')
    }

    public SlideItem(title, image, description, width = 156, height: 156) {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Image source={image} style={{ width, height }} />
                <Text style={{ paddingTop: 40, fontSize: 20, fontWeight: '700' }}>{title}</Text>
                <Text
                    style={{
                        paddingHorizontal: 44,
                        fontSize: 16,
                        fontWeight: '400',
                        textAlign: 'center',
                        paddingTop: 8,
                    }}
                >
                    {description}
                </Text>
            </View>
        )
    }

    public render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <Swiper activeDotColor="#1C232D" showsButtons={true}>
                    {this.SlideItem(
                        'Assalamualaikum',
                        require('../../../../assets/images/on_logo.png'),
                        'Alnur helps you connect with nearby mosques and your local community, from anywhere.',
                        95,
                        101
                    )}

                    {this.SlideItem(
                        'Make a Donation',
                        require('../../../../assets/images/donation.png'),
                        ' With a single tap send your donation to your go to mosque, anytime.',
                        156,
                        156
                    )}
                    {this.SlideItem(
                        'Discussion',
                        require('../../../../assets/images/discussion.png'),
                        'Ask a question and get helpful answers or share updates about your community with the world.',
                        156,
                        156
                    )}
                    {this.SlideItem(
                        'Prayer Times',
                        require('../../../../assets/images/prayer.png'),
                        'Get prayer times and find qibla direction right from your finger tip.',
                        156,
                        156
                    )}
                </Swiper>
                <View style={{ height: 300, justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity
                        onPress={() => this._onPressToDone()}
                        style={{
                            width: 145,
                            height: 46,
                            backgroundColor: 'rgba(224,204,246,0.5)',
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderRadius: 100,
                        }}
                    >
                        <Text style={{ color: '#6500D3', fontSize: 17, fontWeight: '700' }}>Get Started</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {}
}

export default connect(mapStateToProps, { setFirstTimeHere })(WelcomeScreen)
