export enum WelcomeActionType {
    FIRST_TIME_HERE = '@FIRST_TIME_HERE',
    CHECK_FIRST_TIME_HERE = '@CHECK_FIRST_TIME_HERE',
}

export type WelcomeState = {
    isFirstTimeHere: boolean
}

export type WelcomeAction = {
    type: string
    payload: any
}
