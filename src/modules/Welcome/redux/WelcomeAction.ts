import { WelcomeActionType } from './WelcomeActionType'

export function setFirstTimeHere() {
    return {
        type: WelcomeActionType.FIRST_TIME_HERE,
    }
}

export function checkIsFirstTimeHere() {
    return {
        type: WelcomeActionType.CHECK_FIRST_TIME_HERE,
    }
}
