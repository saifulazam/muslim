import { setStorage } from '../../../utils/Storage'
import { WelcomeAction, WelcomeActionType, WelcomeState } from './WelcomeActionType'

const INITIAL_STATE: WelcomeState = {
    isFirstTimeHere: true,
}

const reducer = (state = INITIAL_STATE, actionType: WelcomeAction) => {
    switch (actionType.type) {
        case WelcomeActionType.FIRST_TIME_HERE:
            setStorage('isFirstTimeHere', 'yes')
            return { ...state, isFirstTimeHere: false }
        case WelcomeActionType.CHECK_FIRST_TIME_HERE:
            return { ...state, isFirstTimeHere: false }
        default:
            return state
    }
}

export default reducer
