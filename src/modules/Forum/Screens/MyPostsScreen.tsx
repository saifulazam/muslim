import { StackNavigationProp } from '@react-navigation/stack'
import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { CustomStatusBar } from '../../../utils/StatusBar'
import { connect } from 'react-redux'
import { getUserThreads } from '../../User/services/UserService'
import Spinner from '../../../common/Spinner'
import { FlatList } from 'react-native-gesture-handler'
import ThreadCard from '../components/CardItem/ThreadCard'
import Echo from 'laravel-echo'
import Pusher from 'pusher-js/react-native'

interface Props {
    navigation: StackNavigationProp<any>
    isAuthenticated?: boolean
    threads?: any
    getUserThreads?: any
    token?: string
}
interface State {}

export class MyPostsScreen extends Component<Props, State> {
    state = {}

    componentDidMount() {
        CustomStatusBar(this.props.navigation, 'dark-content')
        this.props.getUserThreads()
    }

    render() {
        if (!Array.isArray(this.props.threads)) {
            return <Spinner size="large" />
        }
        return (
            <View>
                <FlatList
                    data={this.props.threads}
                    renderItem={({ item }: { item: any }) => (
                        <ThreadCard thread={item} navigation={this.props.navigation} />
                    )}
                    keyExtractor={(item) => Math.random() + item.id.toString()}
                    onEndReachedThreshold={0}
                    initialNumToRender={5}
                    maxToRenderPerBatch={10}
                    contentContainerStyle={{ paddingBottom: 80 }}
                    disableVirtualization
                />
            </View>
        )
    }
}

const mapStateToProps = function (state) {
    return {
        isAuthenticated: state.auth.isAuthenticated,
        token: state.auth.token,
        threads: state.user.threads,
    }
}

export default connect(mapStateToProps, { getUserThreads })(MyPostsScreen)
