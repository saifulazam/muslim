import { StackNavigationProp } from '@react-navigation/stack'
import React, { Component } from 'react'
import { View, Text, SafeAreaView } from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import Icon from '../../../common/Icon'
import { AxiosServiceExecutorGet } from '../../../services/ServiceExecutor'
import ThreadMainBody from '../components/Details/ThreadMainBody'
import ThreadReplyBox from '../components/Details/ThreadReplyBox'
import { connect } from 'react-redux'
import ShowReplies from '../components/Details/ShowReplies'
import ThreadCardFooter from '../components/CardItem/ThreadCardFooter'
import ThreadCommentBox from '../components/ThreadCommentBox'
import { getThreadsService } from '../Services/ForumServices'
import { CustomStatusBar } from '../../../utils/StatusBar'

interface Props {
    navigation: StackNavigationProp<any>
    route?: any
    isAuthenticated?: boolean
    token?: any
    getThreadsService?: any
}
interface State {
    thread: any
    replies: any
}

export class ThreadDetailsScreen extends Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {
            thread: null,
            replies: [],
        }
    }

    componentDidMount() {
        CustomStatusBar(this.props.navigation, 'dark-content')
        this.threadServices()
    }

    threadServices = () => {
        this._fetchThreadService()
        this._fetchThreadRepliesService()
        this.props.getThreadsService('all', 1)
    }

    _fetchThreadService = () => {
        const { threadId } = this.props.route.params
        AxiosServiceExecutorGet(`forums/threads/${threadId}`, this.threadResponseCallback)
    }

    threadResponseCallback = (response) => {
        if (response.success) {
            this.setState({ thread: response.data })
        } else {
            this.props.navigation.goBack()
        }
    }

    _fetchThreadRepliesService = () => {
        const { threadId } = this.props.route.params
        AxiosServiceExecutorGet(`forums/threads/${threadId}/replies`, this.threadRepliesResponseCallback)
    }
    threadRepliesResponseCallback = (response) => {
        this.setState({ replies: response.data })
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <SafeAreaView style={{ backgroundColor: '#fff' }}>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.goBack()}
                        style={{ flexDirection: 'row', justifyContent: 'flex-end', padding: 10 }}
                    >
                        <Icon name="remove_light" />
                    </TouchableOpacity>
                </SafeAreaView>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{ paddingBottom: 100 }}
                    overScrollMode="auto"
                >
                    <View style={{ backgroundColor: '#fff' }}>
                        <ThreadMainBody thread={this.state.thread} />
                    </View>

                    <ShowReplies replies={this.state.replies} thread={this.state.thread} />
                </ScrollView>
                <ThreadCommentBox
                    thread={this.state.thread}
                    onRender={this.threadServices}
                    navigation={this.props.navigation}
                    isAuthenticated={this.props.isAuthenticated}
                />
                <View style={{ backgroundColor: '#fff', paddingVertical: 20 }}>
                    <View style={{ paddingHorizontal: 20 }}>
                        <ThreadCardFooter
                            thread={this.state.thread}
                            navigation={this.props.navigation}
                            reRender={this.threadServices}
                            showReply={false}
                            showFavorite={true}
                            showShare={false}
                            showReport={true}
                        />
                    </View>
                </View>
            </View>
        )
    }
}

const mapStateToProps = function (state) {
    return {
        isAuthenticated: state.auth.isAuthenticated,
        token: state.auth.token,
    }
}

export default connect(mapStateToProps, { getThreadsService })(ThreadDetailsScreen)
