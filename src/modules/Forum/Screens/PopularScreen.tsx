import { StackNavigationProp } from '@react-navigation/stack'
import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { CustomStatusBar } from '../../../utils/StatusBar'
import { connect } from 'react-redux'
import { getPopularThreads } from '../Services/ForumServices'
import Spinner from '../../../common/Spinner'
import { FlatList } from 'react-native-gesture-handler'
import ThreadCard from '../components/CardItem/ThreadCard'

interface Props {
    navigation: StackNavigationProp<any>
    PopularThreads?: any
    getPopularThreads?: any
}
interface State {}

export class PopularScreen extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {}
    }

    componentDidMount() {
        CustomStatusBar(this.props.navigation, 'dark-content')
        this.fetchThreads()
    }

    fetchThreads = () => {
        this.props.getPopularThreads()
    }

    render() {
        if (!Array.isArray(this.props.PopularThreads)) {
            return <Spinner size="large" />
        }
        return (
            <FlatList
                data={this.props.PopularThreads}
                renderItem={({ item }: { item: any }) => (
                    <ThreadCard thread={item} navigation={this.props.navigation} />
                )}
                keyExtractor={(item) => Math.random() + item.id.toString()}
                onEndReachedThreshold={0}
                initialNumToRender={5}
                maxToRenderPerBatch={10}
                contentContainerStyle={{ paddingBottom: 80 }}
                disableVirtualization
            />
        )
    }
}

const mapStateToProps = (state) => {
    return {
        PopularThreads: state.forums.popular,
    }
}

export default connect(mapStateToProps, { getPopularThreads })(PopularScreen)
