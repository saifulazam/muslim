import { StackNavigationProp } from '@react-navigation/stack'
import React, { Component } from 'react'
import { View, Text, SafeAreaView } from 'react-native'
import { CustomStatusBar } from '../../../utils/StatusBar'
import { connect } from 'react-redux'
import Icon from '../../../common/Icon'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import { QuestionForm } from '../components/Form/QuestionForm'
import { LocalUpdateForm } from '../components/Form/LocalUpdateForm'
import { OfferForm } from '../components/Form/OfferForm'
import { DuaForm } from '../components/Form/DuaForm'
import {
    getThreadsService,
    getLocalUpdateChannelThreads,
    getQuestionChannelThreads,
    getOffersChannelThreads,
} from '../Services/ForumServices'

const tabs = [
    {
        id: 1,
        title: 'Ask a question',
        description: 'Ask questions only bla bla bla',
        active_icon: 'question_active',
        inactive_icon: 'question_inactive',
        headerBackgroundColor: '#D1E0D3',
    },
    {
        id: 2,
        title: 'Local Update',
        description: 'Inform community about new places or anything...',
        active_icon: 'local_update_active',
        inactive_icon: 'local_update_inactive',
        headerBackgroundColor: '#EBF2FF',
    },
    {
        id: 3,
        title: 'Post an offer',
        description: 'Post about an offer for sale or free.',
        active_icon: 'offer_active',
        inactive_icon: 'offer_inactive',
        headerBackgroundColor: '#FFEFDE',
    },
    {
        id: 4,
        title: 'Dua request',
        description: 'Ask to make a dua. Keep it short.',
        active_icon: 'dua_active',
        inactive_icon: 'dua_inactive',
        headerBackgroundColor: '#F5F5F5',
    },
]
interface Props {
    navigation: StackNavigationProp<any>
    token?: string
    getThreadsService?: any
    threads?: any
    getLocalUpdateChannelThreads?: any
    getQuestionChannelThreads?: any
    getOffersChannelThreads?: any
}
interface State {
    tab: any
}

export class PostFromScreen extends Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {
            tab: {
                id: 1,
                title: 'Ask a question',
                description: 'Ask questions only bla bla bla',
                active_icon: 'question_active',
                inactive_icon: 'question_inactive',
                headerBackgroundColor: '#D1E0D3',
            },
        }
    }

    componentDidMount() {
        CustomStatusBar(this.props.navigation, 'dark-content')
    }

    _reRenderServices = () => {
        this.props.getThreadsService('all', 1)
        this.props.getQuestionChannelThreads()
        this.props.getOffersChannelThreads()
        this.props.getLocalUpdateChannelThreads()
    }

    renderTabIcons = () => {
        return tabs.map((tab) => {
            return (
                <TouchableOpacity
                    onPress={() => this.setState({ tab })}
                    key={Math.random() + tab.id}
                    style={{ paddingHorizontal: 10 }}
                >
                    <Icon name={this.state.tab.id === tab.id ? this.state.tab.active_icon : tab.inactive_icon} />
                </TouchableOpacity>
            )
        })
    }

    showSelectedTabScreen = () => {
        switch (this.state.tab.id) {
            case 1:
                return (
                    <QuestionForm
                        navigation={this.props.navigation}
                        token={this.props.token}
                        reRender={this._reRenderServices}
                    />
                )
            case 2:
                return (
                    <LocalUpdateForm
                        navigation={this.props.navigation}
                        token={this.props.token}
                        reRender={this._reRenderServices}
                    />
                )
                break
            case 3:
                return (
                    <OfferForm
                        navigation={this.props.navigation}
                        token={this.props.token}
                        reRender={this._reRenderServices}
                    />
                )
            case 4:
                return (
                    <DuaForm
                        navigation={this.props.navigation}
                        token={this.props.token}
                        reRender={this._reRenderServices}
                    />
                )
                break
        }
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#fff' }}>
                <SafeAreaView
                    style={{
                        backgroundColor: this.state.tab.headerBackgroundColor,
                        height: 188,
                        shadowColor: '#000',
                        shadowOffset: {
                            width: 0,
                            height: 1,
                        },
                        shadowOpacity: 0.18,
                        shadowRadius: 1.0,

                        elevation: 1,
                    }}
                >
                    <View style={{ width: 50, height: 30 }}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.goBack()}
                            style={{ paddingHorizontal: 20 }}
                        >
                            <Icon name="close_dark" width="16" />
                        </TouchableOpacity>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                        {this.renderTabIcons()}
                    </View>
                    <View
                        style={{
                            flexDirection: 'column',
                            justifyContent: 'center',
                            alignItems: 'center',
                            paddingTop: 10,
                        }}
                    >
                        <Text style={{ fontSize: 18, fontWeight: '600' }}>{this.state.tab.title}</Text>
                        <Text style={{ paddingTop: 5 }}>{this.state.tab.description}</Text>
                    </View>
                </SafeAreaView>
                <ScrollView>{this.showSelectedTabScreen()}</ScrollView>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        token: state.auth.token,
        threads: state.forums.threads,
    }
}

export default connect(mapStateToProps, {
    getThreadsService,
    getLocalUpdateChannelThreads,
    getQuestionChannelThreads,
    getOffersChannelThreads,
})(PostFromScreen)
