import { StackNavigationProp } from '@react-navigation/stack'
import React, { PureComponent } from 'react'
import { View, Text, SafeAreaView, TouchableOpacity } from 'react-native'
import { CustomStatusBar } from '../../../utils/StatusBar'
import ThreadItemList from '../components/ThreadItemList'
import { getChannelService, getThreadsService } from '../Services/ForumServices'
import { connect } from 'react-redux'
import Spinner from '../../../common/Spinner'
import ChannelTabs from '../components/ChannelTabs'
import Icon from '../../../common/Icon'

interface Props {
    navigation: StackNavigationProp<any>
    isAuthenticated?: boolean
    getChannelService?: any
    getThreadsService?: any
    channels: any
    threads: any
    token: any
}

interface State {
    channel: any
    tabSelected: string
    page: any
    isLoadMore: boolean
    isRefresh: boolean
}

export class ThreadLendingScreen extends PureComponent<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {
            channel: null,
            tabSelected: 'all',
            page: 1,
            isLoadMore: false,
            isRefresh: false,
        }
    }

    componentDidMount() {
        CustomStatusBar(this.props.navigation, 'dark-content')
        this.props.getChannelService()
        this._fetchThreadsService()
    }

    _fetchThreadsService = () => {
        const { tabSelected, page } = this.state

        this.props.getThreadsService(tabSelected, page)
    }

    _handleLoadMore = () => {
        this.setState(
            (prevState, nextProps) => ({
                page: prevState.page + 1,
                isLoadMore: true,
            }),
            () => {
                this._fetchThreadsService()
            }
        )
    }

    _onRefresh = () => {
        this.setState({ isRefresh: true })
        this._fetchThreadsService()
        setTimeout(() => {
            this.setState({ isRefresh: false })
        }, 2000)
    }

    __onPressTabChannelsHandler = (item) => {
        requestAnimationFrame(() => {
            this.setState(
                {
                    tabSelected: item.slug ? item.slug : item,
                    page: 1,
                },
                () => this._fetchThreadsService()
            )
        })
    }

    onPressFormButton = () => {
        if (this.props.isAuthenticated) {
            this.props.navigation.navigate('ThreadFormScreen')
        } else {
            this.props.navigation.navigate('InitialRoute')
        }
    }

    render() {
        if (!Array.isArray(this.props.threads)) {
            return <Spinner />
        }
        return (
            <View style={{ flexDirection: 'column', flex: 1 }}>
                <ChannelTabs
                    channels={this.props.channels}
                    tabSelected={this.state.tabSelected}
                    onPress={this.__onPressTabChannelsHandler}
                />
                <ThreadItemList
                    threads={this.props.threads}
                    navigation={this.props.navigation}
                    isRefresh={this.state.isRefresh}
                    isLoadMore={this.state.isLoadMore}
                    onRefresh={this._onRefresh}
                    onLoadMore={this._handleLoadMore}
                    reRender={this._fetchThreadsService}
                />
                <TouchableOpacity
                    onPress={() => this.onPressFormButton()}
                    style={{
                        position: 'absolute',
                        alignSelf: 'flex-end',
                        paddingBottom: 20,
                        paddingRight: 5,
                        shadowColor: '#000000',
                        shadowOffset: {
                            width: 0,
                            height: 3,
                        },
                        shadowRadius: 5,
                        shadowOpacity: 0.5,
                        bottom: 0,
                        right: 0,
                    }}
                >
                    <Icon name="add" width="50" height="50" />
                </TouchableOpacity>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        isAuthenticated: state.auth.isAuthenticated,
        token: state.auth.token,
        channels: state.forums.channels,
        threads: state.forums.threads,
    }
}

export default connect(mapStateToProps, { getChannelService, getThreadsService })(ThreadLendingScreen)
