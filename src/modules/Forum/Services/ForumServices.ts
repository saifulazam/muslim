import * as ForumAction from '../redux/ForumAction'
import Http from '../../../utils/Http'
import Transformer from '../../../utils/Transformer'

export function getChannelService() {
    return (dispatch) => {
        new Promise<void>((resolve, reject) => {
            Http.get('forums')
                .then((response) => {
                    const data = Transformer.fetch(response.data.data.channels)
                    dispatch(ForumAction.getChannels(data))
                    return resolve()
                })
                .catch((error) => {
                    console.warn(error)
                    return reject(error)
                })
        })
    }
}

export function getThreadsService(channel, page) {
    return (dispatch) => {
        new Promise<void>((resolve, reject) => {
            Http.get(`forums${channel === 'all' ? '' : '/' + channel}?page=${page}`)
                .then((response) => {
                    const data = Transformer.fetch(response.data.data.threads)
                    dispatch(ForumAction.setPageNumber(page))
                    dispatch(ForumAction.getThreads(data))
                    return resolve()
                })
                .catch((error) => {
                    console.warn(error)
                    return reject(error)
                })
        })
    }
}

export function getLocalUpdateChannelThreads() {
    return (dispatch) => {
        new Promise<void>((resolve, reject) => {
            Http.get('forums/local-updates')
                .then((response) => {
                    const data = Transformer.fetch(response.data.data.threads)
                    dispatch(ForumAction.setLocalUpdatesChannel(data))
                    return resolve()
                })
                .catch((error) => {
                    console.warn(error)
                    return reject(error)
                })
        })
    }
}

export function getOffersChannelThreads() {
    return (dispatch) => {
        new Promise<void>((resolve, reject) => {
            Http.get('forums/offers')
                .then((response) => {
                    const data = Transformer.fetch(response.data.data.threads)
                    dispatch(ForumAction.setOffersChannel(data))
                    return resolve()
                })
                .catch((error) => {
                    console.warn(error)
                    return reject(error)
                })
        })
    }
}

export function getQuestionChannelThreads() {
    return (dispatch) => {
        new Promise<void>((resolve, reject) => {
            Http.get('forums/questions')
                .then((response) => {
                    const data = Transformer.fetch(response.data.data.threads)
                    dispatch(ForumAction.setQuestionChannel(data))
                    return resolve()
                })
                .catch((error) => {
                    console.warn(error)
                    return reject(error)
                })
        })
    }
}

export function getPopularThreads() {
    return (dispatch) => {
        new Promise<void>((resolve, reject) => {
            Http.get('forums/popular?filter=visits')
                .then((response) => {
                    const data = Transformer.fetch(response.data.data.threads)
                    dispatch(ForumAction.setPopular(data))
                    return resolve()
                })
                .catch((error) => {
                    console.warn(error)
                    return reject(error)
                })
        })
    }
}
