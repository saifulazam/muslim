import {
    createStackNavigator,
    StackNavigationOptions,
    StackNavigationProp,
    TransitionPresets,
} from '@react-navigation/stack'
import React from 'react'
import ThreadLendingScreen from '../Screens/ThreadLendingScreen'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'
import PopularScreen from '../Screens/PopularScreen'
import MyPostsScreen from '../Screens/MyPostsScreen'
import PostFromScreen from '../Screens/PostFromScreen'
import { connect } from 'react-redux'

export type ForumStackParamList = {
    Newest: undefined
    Popular: undefined
    'My Posts': undefined
    'Add Post': undefined
}

const TopTab = createMaterialTopTabNavigator<ForumStackParamList>()

export interface Props {
    isAuthenticated?: boolean
    navigation: StackNavigationProp<any>
}
const ForumStack = (props: Props) => {
    const navigationOptions: StackNavigationOptions = {
        headerShown: false,
        gestureEnabled: false,
    }
    const tabBarOptions = {
        style: { paddingTop: 40 },
        indicatorStyle: {
            backgroundColor: '#6500D3',
        },
    }

    return (
        <TopTab.Navigator tabBarOptions={tabBarOptions} screenOptions={navigationOptions} initialRouteName={'Newest'}>
            <TopTab.Screen name="Newest" component={ThreadLendingScreen} />
            <TopTab.Screen name="Popular" component={PopularScreen} />
            {props.isAuthenticated ? <TopTab.Screen name="My Posts" component={MyPostsScreen} /> : null}
            {/* <TopTab.Screen name="Add Post" component={PostFromScreen} /> */}
        </TopTab.Navigator>
    )
}

const mapStateToProps = function (state) {
    return {
        isAuthenticated: state.auth.isAuthenticated,
    }
}

export default connect(mapStateToProps)(ForumStack)
