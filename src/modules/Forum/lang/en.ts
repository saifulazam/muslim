const toast = {
    REPLY_SUCCESS: {
        title: 'Thank You',
        text: 'Record has been created successfully.',
        color: '#2ecc71',
        timing: 2000,
    },

    THREAD_POST_SUCCESS: {
        title: '',
        text: 'Your post has been published!',
        color: '#2ecc71',
        timing: 2000,
    },
    REPORT_SUCCESS: {
        title: 'Successful',
        text: '',
        color: '#2ecc71',
        timing: 2000,
    },
}

export { toast }
