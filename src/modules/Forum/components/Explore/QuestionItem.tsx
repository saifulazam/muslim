import { NavigationProp } from '@react-navigation/native'
import React from 'react'
import { View, Text, Dimensions } from 'react-native'
import { Avatar } from 'react-native-elements'
import { TouchableOpacity } from 'react-native-gesture-handler'

const { width } = Dimensions.get('window')

interface Props {
    thread: any
    navigation?: NavigationProp<any>
}

const QuestionItem = (props: Props) => {
    return (
        <TouchableOpacity
            onPress={() => props.navigation.navigate('ThreadDetails', { threadId: props.thread.id })}
            style={{ paddingVertical: 10, width: width, paddingStart: 10 }}
        >
            <View style={{ flexDirection: 'row' }}>
                {props.thread?.isAnonymous ? (
                    <Avatar source={require('../../../../../assets/images/anonymous.png')} rounded />
                ) : (
                    <Avatar source={{ uri: props.thread.creator.avatar }} rounded />
                )}

                <View style={{ paddingStart: 5 }}>
                    {props.thread?.isAnonymous ? <Text>Anonymous</Text> : <Text>{props.thread.creator.firstName}</Text>}

                    <Text numberOfLines={2} style={{ fontSize: 13, color: 'rgba(0,0,0,1)', width: width - 100 }}>
                        {props.thread.title}
                    </Text>
                    <Text numberOfLines={1} style={{ fontSize: 12, color: 'rgba(46,58,89,0.7)', width: width - 100 }}>
                        {props.thread.body}
                    </Text>
                </View>
            </View>
        </TouchableOpacity>
    )
}

export default QuestionItem
