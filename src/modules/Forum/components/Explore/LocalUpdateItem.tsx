import React from 'react'
import { View, Text, Image } from 'react-native'
import moment from 'moment'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { NavigationProp } from '@react-navigation/native'

interface Props {
    thread: any
    navigation?: NavigationProp<any>
}

const LocalUpdateItem = (props: Props) => {
    const randomColor = Math.floor(Math.random() * 16777215).toString(16)
    return (
        <TouchableOpacity
            onPress={() => props.navigation.navigate('ThreadDetails', { threadId: props.thread.id })}
            activeOpacity={0.9}
            style={{ width: 217, height: 267, paddingTop: 10, paddingHorizontal: 10 }}
        >
            {props.thread.image ? (
                <Image source={{ uri: props.thread.image }} style={{ width: 217, height: 139 }} />
            ) : (
                <View style={{ width: 217, height: 139, backgroundColor: `#${randomColor}` }} />
            )}

            <Text style={{ paddingTop: 8, fontSize: 12, fontWeight: '500', color: 'rgba(60,60,67,0.6)' }}>
                {moment(props.thread.createdAt).format('lll')}
            </Text>
            <Text numberOfLines={2} style={{ fontSize: 13, color: 'rgba(0,0,0,1)' }}>
                {props.thread.title}
            </Text>
            <Text numberOfLines={2} style={{ fontSize: 12, color: 'rgba(46,58,89,0.7)' }}>
                {props.thread.body}
            </Text>
        </TouchableOpacity>
    )
}

export default LocalUpdateItem
