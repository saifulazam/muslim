import React from 'react'
import { View, Text } from 'react-native'
import { Avatar } from 'react-native-elements'
import moment from 'moment'
import ReadMore from '../../../../common/ReadMore'
import { reports } from '../../../../data/reports'

interface Props {
    replies: any
    thread?: any
}

const ShowReplies = (props: Props) => {
    const renderReplies = () => {
        return props.replies.map((reply) => {
            return (
                <View key={Math.random() + reply.id} style={{ paddingBottom: 10 }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Avatar source={{ uri: reply.owner.avatar }} rounded />
                        <Text style={{ paddingHorizontal: 10, fontSize: 13, fontWeight: '600', lineHeight: 18 }}>
                            {reply.owner.firstName}
                        </Text>
                        <Text style={{ fontSize: 12, fontWeight: '400', color: '#707989' }}>
                            {moment(reply.createdAt).fromNow()}
                        </Text>
                    </View>
                    <View style={{ paddingLeft: 30 }}>
                        <ReadMore>{reply.body}</ReadMore>
                    </View>
                </View>
            )
        })
    }
    return (
        <View style={{ padding: 10 }}>
            <View style={{ paddingBottom: 10 }}>
                <Text
                    style={{ color: '#424A54', fontSize: 17, fontWeight: 'bold', lineHeight: 28, letterSpacing: -0.4 }}
                >
                    Comments ({props.thread?.repliesCount})
                </Text>
            </View>
            {renderReplies()}
        </View>
    )
}

export default ShowReplies
