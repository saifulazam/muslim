import { StackNavigationProp } from '@react-navigation/stack'
import React, { useRef, useEffect, useState } from 'react'
import { View, Text, Dimensions, TextInput, Platform, Button } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { Modalize } from 'react-native-modalize'
import PrimaryButton from '../../../../common/PrimaryButton'
import { AxiosServiceExecutorPost } from '../../../../services/ServiceExecutor'

const { width, height } = Dimensions.get('window')
interface Props {
    isAuthenticated?: boolean
    navigation?: any
    thread: any
    reRender?: any
}

const ThreadReplyBox = (props: Props) => {
    const modalizeRef = useRef<Modalize>(null)
    const [body, setBody] = React.useState('')

    const onPressHandle = () => {
        AxiosServiceExecutorPost(`forums/threads/${props.thread.id}/replies`, { body }, postResponseCallback, true)
    }
    const postResponseCallback = (response) => {
        setBody('')
        props.reRender()
        handleClose(() => {})
    }
    const handleOpen = () => {
        if (modalizeRef.current) {
            modalizeRef.current.open()
        }
    }

    const handleClose = (dest) => {
        if (modalizeRef.current) {
            modalizeRef.current.close(dest)
        }
    }

    return (
        <View style={{ padding: 10 }}>
            <Text style={{ color: '#424A54', fontSize: 17, fontWeight: 'bold', lineHeight: 28, letterSpacing: -0.4 }}>
                Comments
            </Text>
            <TouchableOpacity
                onPress={() => handleOpen()}
                style={{
                    backgroundColor: '#FFF',
                    borderRadius: 20,
                    height: 40,
                    paddingVertical: 0,
                    paddingHorizontal: 20,
                    borderColor: 'rgba(235, 236, 240, 0.5)',
                    borderWidth: 1,
                    justifyContent: 'center',
                }}
            >
                <Text style={{ color: 'gray' }}>Write a comment...</Text>
            </TouchableOpacity>
            <Modalize
                ref={modalizeRef}
                snapPoint={300}
                // keyboardAvoidingBehavior={Platform.OS === 'ios' ? 'padding' : 'height'}
            >
                <View style={{ padding: 20 }}>
                    <TextInput
                        placeholder="Write a comment..."
                        style={{
                            backgroundColor: '#f2f2f2',
                            borderRadius: 5,
                            height: 100,
                            paddingVertical: 0,
                            paddingHorizontal: 10,
                        }}
                        numberOfLines={20}
                        multiline={true}
                        autoFocus={true}
                        returnKeyType="done"
                        value={body}
                        onChangeText={(text) => setBody(text)}
                    />
                    <View style={{ paddingVertical: 15 }}>
                        <PrimaryButton
                            title="Post"
                            onPress={onPressHandle}
                            isActive={true}
                            style={{ borderRadius: 25 }}
                        />
                        <View>
                            <Button title="Close" onPress={handleClose} />
                        </View>
                    </View>
                </View>
            </Modalize>
        </View>
    )
}

export default ThreadReplyBox
