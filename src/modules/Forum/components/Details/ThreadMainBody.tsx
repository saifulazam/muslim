import React from 'react'
import { View, Text } from 'react-native'
import LazyImage from '../../../../common/LazyImage/LazyImage'
import ReadMore from '../../../../common/ReadMore'
import ThreadCardFooter from '../CardItem/ThreadCardFooter'
import ThreadCardHeader from '../CardItem/ThreadCardHeader'

interface Props {
    thread: any
}

const ThreadMainBody = (props: Props) => {
    return (
        <View style={{ paddingHorizontal: 20, paddingBottom: 20 }}>
            <ThreadCardHeader thread={props.thread} />
            <View style={{ paddingVertical: 10 }}>
                {props?.thread?.image ? (
                    <View style={{ paddingBottom: 10 }}>
                        <LazyImage source={{ uri: props?.thread?.image }} style={{ width: '100%', height: 187 }} />
                    </View>
                ) : null}
                {props.thread?.title ? (
                    <Text style={{ fontWeight: 'bold', fontSize: 20, color: '#333333', paddingBottom: 8 }}>
                        {props?.thread?.title}
                    </Text>
                ) : null}
                <ReadMore>{props?.thread?.body}</ReadMore>
            </View>
            {/* <ThreadCardFooter thread={props.thread} /> */}
        </View>
    )
}

export default ThreadMainBody
