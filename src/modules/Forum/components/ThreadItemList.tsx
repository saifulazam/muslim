import { StackNavigationProp } from '@react-navigation/stack'
import React from 'react'
import { View, Text, FlatList, RefreshControl, Animated, Platform } from 'react-native'
import Spinner from '../../../common/Spinner'
import ThreadCard from './CardItem/ThreadCard'

interface Props {
    threads: any
    navigation: StackNavigationProp<any>
    onLoadMore?: any
    onRefresh?: any
    isRefresh?: boolean
    isLoadMore?: boolean
    reRender?: any
}

const HEADER_MAX_HEIGHT = 300
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 60 : 73
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList)

const ThreadItemList = (props: Props) => {
    return (
        <AnimatedFlatList
            data={props.threads}
            renderItem={({ item }: { item: any }) => (
                <ThreadCard thread={item} navigation={props.navigation} reRender={props.reRender} />
            )}
            keyExtractor={(item) => Math.random() + item.id.toString()}
            showsVerticalScrollIndicator={false}
            onEndReachedThreshold={0}
            initialNumToRender={5}
            maxToRenderPerBatch={10}
            refreshControl={<RefreshControl refreshing={props.isRefresh} onRefresh={props.onRefresh.bind(this)} />}
            onEndReached={props.onLoadMore}
            ListFooterComponent={() => {
                if (props.isLoadMore) return null
                return <Spinner />
            }}
            contentContainerStyle={{ paddingBottom: 80 }}
            disableVirtualization
            ItemSeparatorComponent={FilterSeparator}
        />
    )
}

export default ThreadItemList

class FilterSeparator extends React.PureComponent {
    render() {
        return <View />
    }
}
