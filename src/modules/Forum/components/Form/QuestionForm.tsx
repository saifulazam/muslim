import { StackNavigationProp } from '@react-navigation/stack'
import * as React from 'react'
import { View, StyleSheet, Text, KeyboardAvoidingView, Platform, Alert, Image, Switch } from 'react-native'
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler'
import Icon from '../../../../common/Icon'
import PrimaryButton from '../../../../common/PrimaryButton'
import ScreenLevelAlert from '../../../../common/ScreenLevelAlert'
import Toast from '../../../../lib/Toast/Toast'
import { toast } from '../../lang/en'
import { AxiosServiceExecutorPost } from '../../../../services/ServiceExecutor'
import { checkCameraGetPermissionAsync } from '../../../../utils/permissions'
import * as ImagePicker from 'expo-image-picker'
export interface Props {
    navigation: StackNavigationProp<any>
    token: string
    reRender?: any
}
export interface State {
    title: string
    body: string
    is_anonymous: boolean
    image: string
    channel_id: number
    error?: string
    isLoading?: boolean
}

export class QuestionForm extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {
            title: '',
            body: '',
            is_anonymous: false,
            image: null,
            channel_id: 1,
            error: null,
            isLoading: false,
        }
    }

    _pickImage = async () => {
        checkCameraGetPermissionAsync()
        try {
            let result = await ImagePicker.launchImageLibraryAsync({
                mediaTypes: ImagePicker.MediaTypeOptions.All,
                allowsEditing: true,
                aspect: [4, 3],
                quality: 1,
            })
            if (!result.cancelled) {
                //@ts-ignore
                this.setState({ image: result.uri })
            }

            // console.log(result)
        } catch (E) {
            console.warn(E)
        }
    }

    changeTitleHandle = (text) => {
        this.setState({ title: text, error: null })
    }
    changeBodyHandle = (text) => {
        this.setState({ body: text, error: null })
    }

    onSwitchIsAnonymousToggle = () => {
        this.setState({ is_anonymous: !this.state.is_anonymous })
    }

    onPress = () => {
        this.setState({ isLoading: true })
        const { title, body, is_anonymous, image, channel_id } = this.state
        var data = new FormData()
        data.append('title', title)
        data.append('body', body)
        data.append('channel_id', channel_id)
        if (image) {
            let imageUrl = image
            let filename = imageUrl.split('/').pop()
            let match = /\.(\w+)$/.exec(filename)
            let type = match ? `image/${match[1]}` : `image`
            data.append('image', { uri: imageUrl, name: filename, type })
        }
        if (is_anonymous) {
            data.append('is_anonymous', 1)
        }
        AxiosServiceExecutorPost('forums/threads/store', data, this.onPressResponseCallback, true)
    }
    onPressResponseCallback = (response) => {
        if (response.status === 201) {
            this.setState({ title: '', body: '', is_anonymous: false, image: null })
            this.props.reRender()
            Toast.show(toast.THREAD_POST_SUCCESS)
            this.props.navigation.goBack()
        } else {
            this.setState({ isLoading: false })
            this.setState({ error: response.message })
            console.warn('something went wrong')
        }
    }

    render() {
        return (
            <KeyboardAvoidingView
                behavior={Platform.OS === 'ios' ? 'padding' : null}
                style={{ paddingHorizontal: 20, position: 'absolute', width: '100%' }}
            >
                {this.state.error && <ScreenLevelAlert errorMessage={this.state.error} />}
                <View style={{ borderBottomColor: '#EBECF0', borderBottomWidth: 1, paddingVertical: 16 }}>
                    <TextInput
                        style={[{ fontSize: 20, fontWeight: '700' }]}
                        placeholder="What’s your question?"
                        placeholderTextColor="#858E9D"
                        value={this.state.title}
                        onChangeText={this.changeTitleHandle}
                        multiline={true}
                        returnKeyType="done"
                    />
                </View>
                <View style={{ borderBottomColor: '#EBECF0', borderBottomWidth: 1, paddingVertical: 16 }}>
                    <TextInput
                        style={[{ fontSize: 16, fontWeight: '400' }]}
                        placeholder="Add description (optional)"
                        placeholderTextColor="#858E9D"
                        value={this.state.body}
                        onChangeText={this.changeBodyHandle}
                        multiline={true}
                        returnKeyType="done"
                    />
                </View>
                <View style={{ borderBottomColor: '#EBECF0', borderBottomWidth: 1, paddingVertical: 16 }}>
                    <View
                        style={{
                            backgroundColor: '#fafafa',
                            width: 65,
                            height: 65,
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderWidth: 1,
                            borderColor: '#ccc',
                            borderRadius: 100,
                        }}
                    >
                        <TouchableOpacity onPress={() => this._pickImage()} style={{ padding: 20 }}>
                            {this.state.image ? (
                                <Image
                                    source={{ uri: this.state.image }}
                                    style={{ width: 64, height: 64, borderRadius: 100 }}
                                />
                            ) : (
                                <Icon name="photo" />
                            )}
                        </TouchableOpacity>
                    </View>
                </View>
                <View
                    style={{
                        borderBottomColor: '#EBECF0',
                        borderBottomWidth: 1,
                        paddingVertical: 22,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                    }}
                >
                    <View>
                        <Text style={{ color: '#1C232D', fontSize: 16, fontWeight: '400' }}>Post anonymously</Text>
                        {!this.state.is_anonymous ? (
                            <Text style={{ fontSize: 10, color: '#1C232D' }}>
                                Enable on to hide your name and avatar from this post.
                            </Text>
                        ) : null}
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Switch
                            trackColor={{ false: '#858E9D', true: '#6500D3' }}
                            thumbColor="white"
                            ios_backgroundColor="#858E9D"
                            onValueChange={() => this.onSwitchIsAnonymousToggle()}
                            value={this.state.is_anonymous}
                        />
                        <Text style={{ color: '#1C232D', fontSize: 16, fontWeight: '400', paddingStart: 10 }}>
                            {this.state.is_anonymous ? 'On' : 'Off'}
                        </Text>
                    </View>
                </View>
                <View style={{ paddingVertical: 16 }}>
                    <PrimaryButton
                        title="Post"
                        isActive={this.state.title ? true : false}
                        onPress={this.onPress}
                        style={{ borderRadius: 100 }}
                        isLoading={this.state.isLoading}
                    />
                </View>
            </KeyboardAvoidingView>
        )
    }
}

export default QuestionForm
