import { StackNavigationProp } from '@react-navigation/stack'
import * as React from 'react'
import {
    View,
    StyleSheet,
    Text,
    KeyboardAvoidingView,
    Platform,
    TextInput,
    TouchableOpacity,
    Image,
} from 'react-native'
import { connect } from 'react-redux'
import { checkCameraGetPermissionAsync } from '../../../../utils/permissions'
import * as ImagePicker from 'expo-image-picker'
import { AxiosServiceExecutorPost } from '../../../../services/ServiceExecutor'
import Toast from '../../../../lib/Toast/Toast'
import { toast } from '../../lang/en'
import ScreenLevelAlert from '../../../../common/ScreenLevelAlert'
import Icon from '../../../../common/Icon'
import PrimaryButton from '../../../../common/PrimaryButton'

export interface Props {
    navigation: StackNavigationProp<any>
    token: string
    reRender?: any
}
export interface State {
    title: string
    body: string
    image: string
    channel_id: number
    error?: string
    isLoading?: boolean
}

export class OfferForm extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {
            title: '',
            body: '',
            image: null,
            channel_id: 3,
            error: null,
            isLoading: false,
        }
    }

    _pickImage = async () => {
        checkCameraGetPermissionAsync()
        try {
            let result = await ImagePicker.launchImageLibraryAsync({
                mediaTypes: ImagePicker.MediaTypeOptions.All,
                allowsEditing: true,
                aspect: [4, 3],
                quality: 1,
            })
            if (!result.cancelled) {
                //@ts-ignore
                this.setState({ image: result.uri })
            }

            // console.log(result)
        } catch (E) {
            console.warn(E)
        }
    }

    changeTitleHandle = (text) => {
        this.setState({ title: text, error: null })
    }
    changeBodyHandle = (text) => {
        this.setState({ body: text, error: null })
    }

    onPress = () => {
        this.setState({ isLoading: true })
        const { title, body, image, channel_id } = this.state
        var data = new FormData()
        data.append('title', title)
        data.append('body', body)
        data.append('channel_id', channel_id)
        if (image) {
            let imageUrl = image
            let filename = imageUrl.split('/').pop()
            let match = /\.(\w+)$/.exec(filename)
            let type = match ? `image/${match[1]}` : `image`
            data.append('image', { uri: imageUrl, name: filename, type })
        }

        AxiosServiceExecutorPost('forums/threads/store', data, this.onPressResponseCallback, true)
    }
    onPressResponseCallback = (response) => {
        if (response.status === 201) {
            this.setState({ title: '', body: '', image: null })
            this.props.reRender()
            Toast.show(toast.THREAD_POST_SUCCESS)
            this.props.navigation.goBack()
        } else {
            this.setState({ isLoading: false })
            this.setState({ error: response.message })
            console.warn('something went wrong')
        }
    }

    render() {
        return (
            <KeyboardAvoidingView
                behavior={Platform.OS === 'ios' ? 'padding' : null}
                style={{ paddingHorizontal: 20, position: 'absolute', width: '100%' }}
            >
                {this.state.error && <ScreenLevelAlert errorMessage={this.state.error} />}
                <View style={{ borderBottomColor: '#EBECF0', borderBottomWidth: 1, paddingVertical: 16 }}>
                    <TextInput
                        style={[{ fontSize: 20, fontWeight: '700' }]}
                        placeholder="Add a Title"
                        placeholderTextColor="#858E9D"
                        value={this.state.title}
                        onChangeText={this.changeTitleHandle}
                        multiline={true}
                        returnKeyType="done"
                    />
                </View>
                <View style={{ borderBottomColor: '#EBECF0', borderBottomWidth: 1, paddingVertical: 16 }}>
                    <TextInput
                        style={[{ fontSize: 16, fontWeight: '400' }]}
                        placeholder="Write description"
                        placeholderTextColor="#858E9D"
                        value={this.state.body}
                        onChangeText={this.changeBodyHandle}
                        multiline={true}
                        returnKeyType="done"
                    />
                </View>
                <View style={{ borderBottomColor: '#EBECF0', borderBottomWidth: 1, paddingVertical: 16 }}>
                    <View
                        style={{
                            backgroundColor: '#fafafa',
                            width: 65,
                            height: 65,
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderWidth: 1,
                            borderColor: '#ccc',
                            borderRadius: 100,
                        }}
                    >
                        <TouchableOpacity onPress={() => this._pickImage()} style={{ padding: 20 }}>
                            {this.state.image ? (
                                <Image
                                    source={{ uri: this.state.image }}
                                    style={{ width: 64, height: 64, borderRadius: 100 }}
                                />
                            ) : (
                                <Icon name="photo" />
                            )}
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ paddingVertical: 16 }}>
                    <PrimaryButton
                        title="Post"
                        isActive={this.state.title && this.state.body ? true : false}
                        onPress={this.onPress}
                        style={{ borderRadius: 100 }}
                        isLoading={this.state.isLoading}
                    />
                </View>
            </KeyboardAvoidingView>
        )
    }
}

const mapStateToProps = (state) => {
    return {}
}

export default connect(mapStateToProps)(OfferForm)
