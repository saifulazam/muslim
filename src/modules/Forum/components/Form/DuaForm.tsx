import { StackNavigationProp } from '@react-navigation/stack'
import * as React from 'react'
import { View, StyleSheet, Text, KeyboardAvoidingView, Platform, TextInput, Switch } from 'react-native'
import { connect } from 'react-redux'
import PrimaryButton from '../../../../common/PrimaryButton'
import ScreenLevelAlert from '../../../../common/ScreenLevelAlert'
import Toast from '../../../../lib/Toast/Toast'
import { AxiosServiceExecutorPost } from '../../../../services/ServiceExecutor'
import { toast } from '../../lang/en'

export interface Props {
    navigation: StackNavigationProp<any>
    token: string
    reRender?: any
}
export interface State {
    body: string
    is_anonymous: boolean
    channel_id: number
    error?: string
    isLoading?: boolean
}

export class DuaForm extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {
            body: '',
            is_anonymous: false,
            channel_id: 4,
            error: null,
            isLoading: false,
        }
    }

    changeBodyHandle = (text) => {
        this.setState({ body: text, error: null })
    }

    onSwitchIsAnonymousToggle = () => {
        this.setState({ is_anonymous: !this.state.is_anonymous })
    }

    onPress = () => {
        this.setState({ isLoading: true })
        const { body, is_anonymous, channel_id } = this.state
        var data = new FormData()
        data.append('body', body)
        data.append('channel_id', channel_id)
        if (is_anonymous) {
            data.append('is_anonymous', 1)
        }
        AxiosServiceExecutorPost('forums/threads/store', data, this.onPressResponseCallback, true)
    }
    onPressResponseCallback = (response) => {
        console.log(response)
        if (response.status === 201) {
            this.setState({ body: '', is_anonymous: false })
            this.props.reRender()
            Toast.show(toast.THREAD_POST_SUCCESS)
            this.props.navigation.goBack()
        } else {
            this.setState({ isLoading: false })
            this.setState({ error: response.message })
            console.warn('something went wrong')
        }
    }

    render() {
        return (
            <KeyboardAvoidingView
                behavior={Platform.OS === 'ios' ? 'padding' : null}
                style={{ paddingHorizontal: 20, position: 'absolute', width: '100%' }}
            >
                {this.state.error && <ScreenLevelAlert errorMessage={this.state.error} />}
                <View style={{ borderBottomColor: '#EBECF0', borderBottomWidth: 1, paddingVertical: 16 }}>
                    <TextInput
                        style={[{ fontSize: 16, fontWeight: '400' }]}
                        placeholder="Write description..."
                        placeholderTextColor="#858E9D"
                        value={this.state.body}
                        onChangeText={this.changeBodyHandle}
                        multiline={true}
                        returnKeyType="done"
                    />
                </View>
                <View
                    style={{
                        borderBottomColor: '#EBECF0',
                        borderBottomWidth: 1,
                        paddingVertical: 22,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                    }}
                >
                    <View>
                        <Text style={{ color: '#1C232D', fontSize: 16, fontWeight: '400' }}>Post anonymously</Text>
                        {!this.state.is_anonymous ? (
                            <Text style={{ fontSize: 10, color: '#1C232D' }}>
                                Enable on to hide your name and avatar from this post.
                            </Text>
                        ) : null}
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Switch
                            trackColor={{ false: '#858E9D', true: '#6500D3' }}
                            thumbColor="white"
                            ios_backgroundColor="#858E9D"
                            onValueChange={() => this.onSwitchIsAnonymousToggle()}
                            value={this.state.is_anonymous}
                        />
                        <Text style={{ color: '#1C232D', fontSize: 16, fontWeight: '400', paddingStart: 10 }}>
                            {this.state.is_anonymous ? 'On' : 'Off'}
                        </Text>
                    </View>
                </View>
                <View style={{ paddingVertical: 16 }}>
                    <PrimaryButton
                        title="Post"
                        isActive={this.state.body ? true : false}
                        onPress={this.onPress}
                        style={{ borderRadius: 100 }}
                        isLoading={this.state.isLoading}
                    />
                </View>
            </KeyboardAvoidingView>
        )
    }
}

const mapStateToProps = (state) => {
    return {}
}

export default connect(mapStateToProps)(DuaForm)
