import { StackNavigationProp } from '@react-navigation/stack'
import React from 'react'
import { Text, TouchableOpacity, View } from 'react-native'
import ThreadCard from './CardItem/ThreadCard'
import Swiper from '../../../lib/Swiper'
interface Props {
    navigation: StackNavigationProp<any>
    isAuthenticated?: boolean
    token?: string
    threads: any
}

const VIABILITY_CONFIG = {
    minimumViewTime: 3000,
    viewAreaCoveragePercentThreshold: 95,
    waitForInteraction: true,
}

export const ForumExploreItems = (props: Props) => {
    const renderItem = () => {
        return props.threads.slice(0, 5).map((thread) => {
            return (
                <View key={Math.random() + thread.id}>
                    <ThreadCard thread={thread} navigation={props.navigation} height={350} />
                </View>
            )
        })
    }
    return (
        <View>
            <View
                style={{
                    marginHorizontal: 10,
                    marginTop: 20,
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignContent: 'space-between',
                    paddingBottom: 10,
                }}
            >
                <Text
                    style={{
                        color: 'black',
                        fontWeight: 'bold',
                        fontSize: 20,
                        fontFamily: 'AvantGardeLT-Demi',
                    }}
                >
                    Community Highlights
                </Text>
                <TouchableOpacity onPress={() => props.navigation.navigate('ForumStack')}>
                    <Text style={{ fontSize: 13, fontWeight: '500', color: '#6500D3' }}>SEE ALL</Text>
                </TouchableOpacity>
            </View>
            <View
                style={{
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    width: 'auto',
                    height: 410,
                }}
            >
                <Swiper showsPagination={true} activeDotColor="#1C232D">
                    {renderItem()}
                </Swiper>
            </View>
        </View>
    )
}
