import React, { Component } from 'react'
import { KeyboardAvoidingView, Platform, TextInput, View, Keyboard, Button, Text } from 'react-native'
import { AxiosServiceExecutorPost } from '../../../services/ServiceExecutor'
import KeyboardListener from 'react-native-keyboard-listener'
import PrimaryButton from '../../../common/PrimaryButton'
import Toast from '../../../lib/Toast/Toast'
import { toast } from '../lang/en'

interface Props {
    thread: any
    onRender?: any
    isAuthenticated?: boolean
    navigation?: any
}
interface State {
    body: string
    textBoxHeight: number
    showSubmitButton: boolean
}

class ThreadCommentBox extends Component<Props, State> {
    state = {
        body: '',
        textBoxHeight: 40,
        showSubmitButton: false,
    }

    _onPressCommentSubmit = () => {
        const { thread } = this.props
        const { body } = this.state
        AxiosServiceExecutorPost(`forums/threads/${thread.id}/replies`, { body }, this.responseCallback, true)
    }

    responseCallback = (response) => {
        if (response.status === 201) {
            Keyboard.dismiss()
            this.setState({ body: '' })
            Toast.show(toast.REPLY_SUCCESS)
            this.props.onRender()
        }
    }

    submitButton = () => {
        if (this.state.showSubmitButton) {
            if (this.props.isAuthenticated) {
                return (
                    <View style={{ paddingVertical: 15 }}>
                        <PrimaryButton
                            title="Post"
                            onPress={this._onPressCommentSubmit}
                            isActive={this.state.body ? true : false}
                            style={{ borderRadius: 25 }}
                        />
                    </View>
                )
            } else {
                this.props.navigation.navigate('LoginScreen')
            }
        }
    }

    render() {
        return (
            <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : null}>
                <KeyboardListener
                    onWillShow={() => {
                        this.setState({ textBoxHeight: 100, showSubmitButton: true })
                    }}
                    onWillHide={() => {
                        this.setState({ textBoxHeight: 40, showSubmitButton: false })
                    }}
                />

                <View
                    style={{
                        padding: 10,
                        backgroundColor: 'white',
                        borderTopColor: '#f2f2f2',
                        borderTopWidth: 1,
                    }}
                >
                    <TextInput
                        placeholder="Write a comment..."
                        style={{
                            backgroundColor: '#f2f2f2',
                            borderRadius: 5,
                            height: this.state.textBoxHeight,
                            paddingVertical: 0,
                            paddingHorizontal: 10,
                        }}
                        numberOfLines={20}
                        multiline={true}
                        value={this.state.body}
                        onChangeText={(text) => this.setState({ body: text })}
                    />
                    {this.submitButton()}
                </View>
            </KeyboardAvoidingView>
        )
    }
}

export default ThreadCommentBox
