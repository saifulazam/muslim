import React from 'react'
import { View, Text } from 'react-native'
import { Avatar } from 'react-native-elements'
import moment from 'moment'

interface Props {
    thread: any
}

const ThreadCardHeader = (props: Props) => {
    const getChannel = () => {
        if (props.thread?.channel.name === 'Questions') {
            return (
                <View style={{ backgroundColor: '#ECF7ED', padding: 5 }}>
                    <Text style={{ color: '#37833B', fontSize: 10, fontWeight: '800' }}>
                        {props.thread?.channel.name}
                    </Text>
                </View>
            )
        } else if (props.thread?.channel.name === 'Local Updates') {
            return (
                <View style={{ backgroundColor: '#EBF2FF', padding: 5 }}>
                    <Text style={{ color: '#2264D1', fontSize: 10, fontWeight: '800' }}>
                        {props.thread?.channel.name}
                    </Text>
                </View>
            )
        } else if (props.thread?.channel.name === 'Offers') {
            return (
                <View style={{ backgroundColor: '#FFEFDE', padding: 5 }}>
                    <Text style={{ color: '#CE6B00', fontSize: 10, fontWeight: '800' }}>
                        {props.thread?.channel.name}
                    </Text>
                </View>
            )
        } else if (props.thread?.channel.name === 'Duas') {
            return (
                <View style={{ backgroundColor: '#F5F5F5', padding: 5 }}>
                    <Text style={{ color: '#ACACAC', fontSize: 10, fontWeight: '800' }}>
                        {props.thread?.channel.name}
                    </Text>
                </View>
            )
        }
    }
    return (
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                {!props.thread?.isAnonymous ? (
                    <>
                        <Avatar source={{ uri: props.thread?.creator.avatar }} rounded />
                        <Text style={{ paddingHorizontal: 10, fontSize: 13, fontWeight: '600', lineHeight: 18 }}>
                            {props.thread?.creator.firstName}
                        </Text>
                    </>
                ) : (
                    <>
                        <Avatar source={require('../../../../../assets/images/anonymous.png')} rounded />
                        <Text style={{ paddingHorizontal: 10, fontSize: 13, fontWeight: '600', lineHeight: 18 }}>
                            Anonymous
                        </Text>
                    </>
                )}

                <Text style={{ color: 'gray' }}>{moment(props.thread?.createdAt).fromNow()}</Text>
            </View>
            {getChannel()}
        </View>
    )
}

export default ThreadCardHeader
