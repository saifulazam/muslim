import React, { useState } from 'react'
import { View, Text, Share, Alert, TouchableOpacity, Platform } from 'react-native'
import Icon from '../../../../common/Icon'
import LikeButton from '../../../../common/LikeButton'
import { connect } from 'react-redux'
import { StackNavigationProp } from '@react-navigation/stack'
import { AxiosServiceExecutorPost, AxiosServiceExecutorPrivateDelete } from '../../../../services/ServiceExecutor'
import ReportModal from '../../../../common/ReportModal'
import Toast from '../../../../lib/Toast/Toast'
import { toast } from '../../lang/en'

interface Props {
    thread: any
    isAuthenticated?: boolean
    navigation?: StackNavigationProp<any>
    reRender?: any
    showLike?: boolean
    showReply?: boolean
    showFavorite?: boolean
    showShare?: boolean
    showReport?: boolean
}

const ThreadCardFooter = ({
    navigation,
    isAuthenticated,
    reRender,
    thread,
    showLike = true,
    showReply = true,
    showFavorite,
    showShare = true,
    showReport,
}: Props) => {
    const [reportModalVisitable, setReportModalVisitable] = useState(false)
    const [reportValue, setReportValue] = useState('')

    const onPressLikeButton = () => {
        if (isAuthenticated) {
            if (thread?.isLiked) {
                AxiosServiceExecutorPrivateDelete(`forums/threads/${thread?.id}/likes`, responseCallback)
            } else {
                AxiosServiceExecutorPost(`forums/threads/${thread?.id}/likes`, {}, responseCallback, true)
            }
        } else {
            return navigation.navigate('InitialRoute')
        }
    }

    const onPressFavoriteButton = () => {
        if (isAuthenticated) {
            if (thread?.isFavorited) {
                AxiosServiceExecutorPrivateDelete(`forums/threads/${thread?.id}/favorites`, responseCallback)
            } else {
                AxiosServiceExecutorPost(`forums/threads/${thread?.id}/favorites`, {}, responseCallback, true)
            }
        } else {
            return navigation.navigate('InitialRoute')
        }
    }

    const responseCallback = (response) => {
        if (response.status === 200) {
            reRender()
        }
    }

    const onPressReportExecution = (item) => {
        const bodyParams = {
            type: item,
        }
        AxiosServiceExecutorPost(`forums/threads/${thread.id}/report`, bodyParams, responseReportCallback, true)
    }

    const responseReportCallback = (response) => {
        if (response.status === 200) {
            setReportModalVisitable(false)
            setReportValue('')
            Toast.show(toast.REPORT_SUCCESS)
        } else {
            console.warn('something went wrong')
        }
    }

    const threadShare = async () => {
        try {
            let result
            if (Platform.OS == 'ios') {
                result = await Share.share({
                    message: `${thread?.channel.name}: ${thread?.body}. @Alnur`,
                    title: `${thread?.channel.name}`,
                    url: 'https://testflight.apple.com/join/fVnB7a3c',
                })
            } else if (Platform.OS == 'android') {
                result = await Share.share({
                    message: `${thread?.body}`,
                    title: `${thread?.channel.name}: `,
                    url: 'https://testflight.apple.com/join/fVnB7a3c',
                })
            }

            if (result.action === Share.sharedAction) {
                Alert.alert('Thank you for sharing')
            } else if (result.action === Share.dismissedAction) {
                Alert.alert('Share cancelled')
            }
        } catch (error) {
            Alert.alert(error.massage)
        }
    }

    return (
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            {showLike ? (
                <LikeButton onPress={onPressLikeButton} isLiked={thread?.isLiked} likeCount={thread?.likesCount} />
            ) : null}
            <View style={{ flexDirection: 'row' }}>
                {showReply ? (
                    <View style={{ flexDirection: 'row', alignItems: 'center', paddingRight: 10 }}>
                        <Icon name="chat" width="20" />
                        <Text style={{ color: '#858E9D', paddingHorizontal: 5 }}>{thread?.repliesCount}</Text>
                    </View>
                ) : null}
                {showFavorite ? (
                    <TouchableOpacity
                        onPress={() => onPressFavoriteButton()}
                        style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10 }}
                    >
                        <Icon name={thread?.isFavorited ? 'heart_active' : 'heart_inactive'} width="20" />
                    </TouchableOpacity>
                ) : null}

                <TouchableOpacity
                    onPress={() => threadShare()}
                    style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10 }}
                >
                    <Icon name="sm_share" width="20" />
                    {showShare ? <Text style={{ color: '#858E9D', paddingHorizontal: 5 }}>Share</Text> : null}
                </TouchableOpacity>
                {showReport ? (
                    <TouchableOpacity
                        onPress={() => setReportModalVisitable(true)}
                        style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10 }}
                    >
                        <Icon name="report" width="20" />
                    </TouchableOpacity>
                ) : null}
            </View>
            <ReportModal
                visible={reportModalVisitable}
                onCancel={() => setReportModalVisitable(false)}
                onPress={onPressReportExecution}
            />
        </View>
    )
}

const mapStateToProps = (state) => {
    return {
        isAuthenticated: state.auth.isAuthenticated,
    }
}

export default connect(mapStateToProps)(ThreadCardFooter)
