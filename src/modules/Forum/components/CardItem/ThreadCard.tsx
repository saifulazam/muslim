import React from 'react'
import { View, Text, Platform } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import ThreadCardBody from './ThreadCardBody'
import ThreadCardFooter from './ThreadCardFooter'
import ThreadCardHeader from './ThreadCardHeader'
import { StackNavigationProp } from '@react-navigation/stack'

interface Props {
    thread: any
    navigation?: StackNavigationProp<any>
    reRender?: any
    height?: number
}

const ThreadCard = (props: Props) => {
    return (
        <View style={{ paddingHorizontal: 10, paddingVertical: 5 }}>
            <View
                style={{
                    backgroundColor: '#fff',
                    padding: 10,
                    shadowColor: '#000000',
                    borderRadius: 5,
                    shadowOffset: {
                        width: 0,
                        height: 1,
                    },
                    shadowRadius: 1,
                    shadowOpacity: 0.1,
                    borderColor: 'rgba(0,0,0, 0.1)',
                    borderWidth: 0.3,
                    height: props.height ?? 'auto',
                }}
            >
                <ThreadCardHeader thread={props.thread} />
                <TouchableOpacity
                    onPress={() => props.navigation.navigate('ThreadDetails', { threadId: props.thread.id })}
                    activeOpacity={0.9}
                >
                    <ThreadCardBody thread={props.thread} />
                </TouchableOpacity>
                <ThreadCardFooter thread={props.thread} navigation={props.navigation} reRender={props.reRender} />
            </View>
        </View>
    )
}

export default ThreadCard
