import React from 'react'
import { View, Text, Image } from 'react-native'
import LazyImage from '../../../../common/LazyImage/LazyImage'

interface Props {
    thread: any
}

const ThreadCardBody = (props: Props) => {
    return (
        <View style={{ paddingVertical: 10 }}>
            {props.thread.image ? (
                <View style={{ paddingBottom: 10 }}>
                    <Image
                        resizeMode="cover"
                        source={{ uri: props.thread.image }}
                        style={{ width: '100%', height: 187 }}
                    />
                </View>
            ) : null}
            {props.thread.title ? (
                <Text style={{ fontWeight: 'bold', fontSize: 20, color: '#333333', paddingBottom: 8 }}>
                    {props.thread.title}
                </Text>
            ) : null}
            <Text numberOfLines={3}>{props.thread.body}</Text>
        </View>
    )
}

export default ThreadCardBody
