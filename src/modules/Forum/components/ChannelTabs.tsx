import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import _COLORS from '../../../utils/Color'

interface Props {
    channels: any
    tabSelected: string
    onPress: any
}

const ChannelTabs = (props: Props) => {
    const getChannels = () => {
        return props.channels.map((channel) => {
            return (
                <View key={Math.random() + channel.id} style={{ paddingHorizontal: 5 }}>
                    <TouchableOpacity
                        onPress={() => props.onPress(channel)}
                        style={{
                            backgroundColor: props.tabSelected == channel.slug ? '#EBECF0' : '#fff',
                            borderWidth: 1,
                            borderColor: '#EBECF0',
                            borderRadius: 20,
                            alignItems: 'center',
                            paddingHorizontal: 10,
                        }}
                    >
                        <Text
                            style={{
                                fontSize: 12,
                                fontWeight: '600',
                                fontFamily: 'AvantGardeLT-Demi',
                                color: props.tabSelected == channel.slug ? '#1C232D' : '#707989',
                                lineHeight: 30,
                            }}
                        >
                            {channel.name}
                        </Text>
                    </TouchableOpacity>
                </View>
            )
        })
    }

    return (
        <View
            style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: '#FFF', padding: 10, width: '100%' }}
        >
            <View style={{ paddingRight: 5 }}>
                <TouchableOpacity
                    onPress={() => props.onPress('all')}
                    style={{
                        backgroundColor: props.tabSelected == 'all' ? '#EBECF0' : '#fff',
                        padding: 5,
                        borderWidth: 1,
                        borderColor: '#EBECF0',
                        borderRadius: 20,
                        width: 40,
                        height: 28,
                        alignItems: 'center',
                    }}
                >
                    <Text
                        style={{
                            fontSize: 12,
                            fontWeight: '600',
                            fontFamily: 'AvantGardeLT-Demi',
                            color: props.tabSelected == 'all' ? '#1C232D' : '#707989',
                        }}
                    >
                        All
                    </Text>
                </TouchableOpacity>
            </View>
            {getChannels()}
        </View>
    )
}

export default ChannelTabs
