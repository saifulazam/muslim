import { ForumActionType } from './ForumActionType'

export function getChannels(payload) {
    return {
        type: ForumActionType.GET_CHANNELS,
        payload,
    }
}

export function getThreads(payload) {
    return {
        type: ForumActionType.GET_NEWEST_THREADS,
        payload,
    }
}

export function setPageNumber(payload) {
    return {
        type: ForumActionType.THREADS_PAGINATION_PAGE_NUMBER,
        payload,
    }
}

export function setLocalUpdatesChannel(payload) {
    return {
        type: ForumActionType.GET_CHANNEL_LOCAL_UPDATE_THREADS,
        payload,
    }
}

export function setOffersChannel(payload) {
    return {
        type: ForumActionType.GET_CHANNEL_OFFERS_THREADS,
        payload,
    }
}

export function setQuestionChannel(payload) {
    return {
        type: ForumActionType.GET_CHANNEL_QUESTIONS_THREADS,
        payload,
    }
}

export function setPopular(payload) {
    return {
        type: ForumActionType.GET_POPULAR_THREADS,
        payload,
    }
}
