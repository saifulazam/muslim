import State from 'pusher-js/types/src/core/http/state'
import { ForumActionType, ForumState, ForumAction } from './ForumActionType'

const INITIAL_STATE: ForumState = {
    page: null,
    channels: [],
    threads: [],
    localUpdates: [],
    offers: [],
    questions: [],
    popular: [],
}
const reducer = (state = INITIAL_STATE, actionType: ForumAction) => {
    switch (actionType.type) {
        case ForumActionType.GET_CHANNELS:
            return getChannels(state, actionType.payload)
        case ForumActionType.GET_NEWEST_THREADS:
            return getThreads(state, actionType.payload)
        case ForumActionType.THREADS_PAGINATION_PAGE_NUMBER:
            return paginationPage(state, actionType.payload)
        case ForumActionType.GET_CHANNEL_LOCAL_UPDATE_THREADS:
            return getLocalUpdate(state, actionType.payload)
        case ForumActionType.GET_CHANNEL_OFFERS_THREADS:
            return getOffers(state, actionType.payload)
        case ForumActionType.GET_CHANNEL_QUESTIONS_THREADS:
            return getQuestions(state, actionType.payload)
        case ForumActionType.GET_POPULAR_THREADS:
            return getPopular(state, actionType.payload)
        default:
            return state
    }
}

const getChannels = (state, payload) => {
    return { ...state, channels: payload }
}

const getThreads = (state, payload) => {
    return { ...state, threads: state.page === 1 ? Array.from(payload) : [...state.threads, ...payload] }
}

const paginationPage = (state, payload) => {
    return { ...state, page: payload }
}

const getLocalUpdate = (state, payload) => {
    return { ...state, localUpdates: payload }
}

const getOffers = (state, payload) => {
    return { ...state, offers: payload }
}

const getQuestions = (state, payload) => {
    return { ...state, questions: payload }
}

const getPopular = (state, payload) => {
    return { ...state, popular: payload }
}

export default reducer
