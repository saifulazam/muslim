export enum ForumActionType {
    GET_NEWEST_THREADS = '@GET_NEWEST_THREADS',
    GET_CHANNELS = '@GET_CHANNELS',
    THREADS_PAGINATION_PAGE_NUMBER = '@THREADS_PAGINATION_PAGE_NUMBER',
    GET_CHANNEL_LOCAL_UPDATE_THREADS = '@GET_CHANNEL_LOCAL_UPDATE_THREADS',
    GET_CHANNEL_OFFERS_THREADS = '@GET_CHANNEL_OFFERS_THREADS',
    GET_CHANNEL_QUESTIONS_THREADS = '@GET_CHANNEL_QUESTIONS_THREADS',
    GET_POPULAR_THREADS = '@GET_POPULAR_THREADS',
}

export type ThreadModel = {
    id: number
    title: string
    user_id: number
    channel_id: number
    replies_count: number
    visits: number
    favoritesCount: number
    isFavorited: boolean
    is_anonymous: boolean
    body: string
    image: string
    best_reply_id: number
    locked: boolean
    created_at: string
    updated_at: string
    creator: CreatorModel
    channel: ChannelModel
}

export type CreatorModel = {
    id: number
    name: string
    first_name: string
    last_name: string
    avatar: string
}

export type ChannelModel = {
    id: number
    name: string
    slug: string
    created_at: string
    updated_at: string
}

export type ForumState = {
    page?: number
    channels: ChannelModel[]
    threads: ThreadModel[]
    localUpdates?: ThreadModel[]
    offers?: ThreadModel[]
    questions?: ThreadModel[]
    popular?: ThreadModel[]
}

export type ForumAction = {
    type: string
    payload: any
}
