import React, { Component } from 'react'
import { Text, View, StatusBar, Alert } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import _COLORS from '../../../utils/Color'
import Icon from '../../../common/Icon'
import { StackNavigationProp } from '@react-navigation/stack'
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler'
import DonationInput from '../components/DonationInput'
import { Tooltip } from 'react-native-elements'
import CardWallet from '../components/CardWallet'
import Toast from 'react-native-tiny-toast'
import { ServicePrivateExecutorPost } from '../../../services/ServiceExecutor'
import { API } from 'react-native-dotenv'
import { CustomStatusBar } from '../../../utils/StatusBar'

interface Props {
    navigation?: StackNavigationProp<any>
    route?: any
}
interface State {
    sadaqah: string
    zakat: string
    maintenace: string
    imam: string
    subTotal: string
    isWalletModalVisible: boolean
    card: any
}
export default class MosqueDonationScreen extends Component<Props, State> {
    constructor(props) {
        super(props)
        this.state = {
            sadaqah: '',
            zakat: '',
            maintenace: '',
            imam: '',
            subTotal: '',
            isWalletModalVisible: false,
            card: {},
        }
    }
    componentDidMount() {
        CustomStatusBar(this.props.navigation, 'light-content')
    }

    renderForm = () => {
        return (
            <View style={{ marginTop: 30 }}>
                <DonationInput
                    title="Sadaqah"
                    value={this.state.sadaqah}
                    onChangeText={this.onSadaqahTextInputChange}
                    itemRemove={() => this.setState({ sadaqah: '' })}
                />
                <View style={{ borderBottomColor: _COLORS.primary_01, borderBottomWidth: 1 }} />
                <DonationInput
                    title="Zakat"
                    value={this.state.zakat}
                    onChangeText={this.onZakatTextInputChange}
                    itemRemove={() => this.setState({ zakat: '' })}
                />
                <View style={{ borderBottomColor: _COLORS.primary_01, borderBottomWidth: 1 }} />
                <DonationInput
                    title="Maintenace"
                    value={this.state.maintenace}
                    onChangeText={this.onMaintenaceTextInputChange}
                    itemRemove={() => this.setState({ maintenace: '' })}
                />
                <View style={{ borderBottomColor: _COLORS.primary_01, borderBottomWidth: 1 }} />
                <DonationInput
                    title="Imam"
                    value={this.state.imam}
                    onChangeText={this.onImamTextInputChange}
                    itemRemove={() => this.setState({ imam: '' })}
                />
            </View>
        )
    }

    renderTotalAmount = () => {
        const { sadaqah, zakat, maintenace, imam } = this.state
        if (sadaqah || zakat || maintenace || imam) {
            return (
                <View style={{ marginTop: 20 }}>
                    <View
                        style={{
                            marginHorizontal: 20,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                        }}
                    >
                        <Text style={{ fontSize: 15, color: 'white' }}>Donation</Text>
                        <Text style={{ fontSize: 15, color: 'white' }}>${this.getSubTotal().toFixed(2)}</Text>
                    </View>
                    <View
                        style={{
                            marginHorizontal: 20,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            paddingTop: 10,
                        }}
                    >
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={{ fontSize: 15, color: 'white' }}>Service fee</Text>
                            <Tooltip
                                backgroundColor="#fff"
                                withOverlay={false}
                                width={300}
                                height={54}
                                // containerStyle={{ width: 300, height: 54 }}
                                popover={
                                    <Text style={{ color: '#000', fontSize: 12, fontWeight: '500' }}>
                                        10% fee helps us operate Alnur app (including transaction fees for the mosque)
                                    </Text>
                                }
                            >
                                <Icon name="more_info" width="24" />
                            </Tooltip>
                        </View>
                        <Text style={{ fontSize: 15, color: 'white' }}>${this.getServiceFee().toFixed(2)}</Text>
                    </View>
                    <View style={{ borderBottomColor: '#6500d3', borderBottomWidth: 1, paddingVertical: 10 }} />
                    <View
                        style={{
                            marginHorizontal: 20,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            paddingTop: 10,
                        }}
                    >
                        <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold' }}>Total Amount</Text>
                        <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold' }}>
                            ${this.getTotal().toFixed(2)}
                        </Text>
                    </View>
                    <View style={{ borderBottomColor: '#6500d3', borderBottomWidth: 1, paddingVertical: 10 }} />
                    <TouchableOpacity
                        onPress={() => this.setState({ isWalletModalVisible: true })}
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            marginHorizontal: 20,
                            marginTop: 20,
                        }}
                    >
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Icon name="credit_card" />
                            <Text style={{ paddingLeft: 10, color: '#fff', fontSize: 15, fontWeight: '800' }}>
                                {this.state.card.valid
                                    ? this.convertCardNumber(this.state.card.values.number).replace(/\d(?=\d{4})/g, '*')
                                    : 'Add Card'}
                            </Text>
                        </View>
                        <Icon name="arrow_right_light" />
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() =>
                            this.state.card.valid ? this.onPressPay() : this.setState({ isWalletModalVisible: true })
                        }
                        style={{
                            backgroundColor: '#308788',
                            marginHorizontal: 20,
                            flexDirection: 'row',
                            justifyContent: 'center',
                            alignItems: 'center',
                            height: 62,
                            marginTop: 30,
                            borderRadius: 31,
                        }}
                    >
                        <Text style={{ fontSize: 15, fontWeight: 'bold', color: 'white' }}>Pay</Text>
                    </TouchableOpacity>
                    <View style={{ paddingBottom: 30 }} />
                </View>
            )
        }
    }
    render() {
        const { sadaqah, zakat, maintenace, imam } = this.state
        const { mosque } = this.props.route.params
        return (
            <ScrollView
                showsVerticalScrollIndicator={false}
                style={{
                    flex: 1,
                    backgroundColor: sadaqah || zakat || maintenace || imam ? _COLORS.primary_01 : _COLORS.primary,
                }}
            >
                <SafeAreaView style={{ backgroundColor: _COLORS.primary }}>
                    <View style={{ marginHorizontal: 20 }}>
                        <View
                            style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                marginTop: 10,
                                justifyContent: 'space-between',
                            }}
                        >
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                                <Icon name="go_back_light" width="24" />
                            </TouchableOpacity>
                            <Text style={{ color: 'white', fontSize: 24, fontWeight: 'bold', textAlign: 'center' }}>
                                Make a donation
                            </Text>
                            <View />
                        </View>
                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'center',
                                marginHorizontal: 30,
                                marginTop: 20,
                            }}
                        >
                            <Text style={{ textAlign: 'center', fontSize: 15, fontWeight: 'bold', color: 'white' }}>
                                100% of your donation will go to {mosque.name}{' '}
                            </Text>
                        </View>
                        {this.renderForm()}
                    </View>
                </SafeAreaView>
                {this.renderTotalAmount()}
                <CardWallet
                    isWalletModalVisible={this.state.isWalletModalVisible}
                    onPressCancel={this.onPressAddCardCancel}
                    onChange={this.onChangeCard}
                />
            </ScrollView>
        )
    }

    onPressPay = () => {
        const { sadaqah, zakat, maintenace, imam, card } = this.state
        const { mosque } = this.props.route.params
        const url = `${API}/mosques/${mosque.id}/donation/post`
        const bodyParams = {
            imam,
            sadaqah,
            zakat,
            maintenance: maintenace,
            card_no: this.convertCardNumber(card.values.number),
            ccExpiryMonth: card.values.expiry.slice(0, 2),
            ccExpiryYear: card.values.expiry.slice(3),
            cvvNumber: card.values.cvc,
        }
        ServicePrivateExecutorPost(url, bodyParams, this.responseCallback)
    }

    responseCallback = (response) => {
        const { mosque } = this.props.route.params
        if (response.status === 201) {
            Toast.showSuccess('success')
            return this.props.navigation.navigate('MosqueDetails', { mosque: mosque })
        } else {
            console.warn(response)
        }
    }

    onChangeCard = (formData) => {
        if (formData.valid) {
            this.setState({ card: formData })
        }
    }

    onSadaqahTextInputChange = (amount: string) => {
        if (/^\d+$/.test(amount) || amount === '') {
            this.setState({ sadaqah: amount })
        }
    }
    onZakatTextInputChange = (amount: string) => {
        if (/^\d+$/.test(amount) || amount === '') {
            this.setState({ zakat: amount })
        }
    }
    onMaintenaceTextInputChange = (amount: string) => {
        if (/^\d+$/.test(amount) || amount === '') {
            this.setState({ maintenace: amount })
        }
    }
    onImamTextInputChange = (amount: string) => {
        if (/^\d+$/.test(amount) || amount === '') {
            this.setState({ imam: amount })
        }
    }

    getTotal = () => {
        return this.getSubTotal() + this.getServiceFee()
    }

    getServiceFee = () => {
        return (this.getSubTotal() * 10) / 100
    }

    onPressAddCardCancel = () => {
        this.setState({ isWalletModalVisible: false })
    }

    getSubTotal = () => {
        const sadaqah = parseFloat(this.state.sadaqah)
        const zakat = parseFloat(this.state.zakat)
        const maintenace = parseFloat(this.state.maintenace)
        const imam = parseFloat(this.state.imam)

        var totalAmount = 0

        if (sadaqah) totalAmount += sadaqah
        if (zakat) totalAmount += zakat
        if (maintenace) totalAmount += maintenace
        if (imam) totalAmount += imam

        return totalAmount
    }

    convertCardNumber = (number) => {
        return number.replace(/\s/g, '')
    }
}
