import React, { Component } from 'react'
import { Text, View, TextInput, Alert, Modal } from 'react-native'
import Icon from '../../../common/Icon'
import { TouchableOpacity } from 'react-native-gesture-handler'

interface Props {
    title: string
    value: string
    onChangeText: any
    itemRemove: any
}
interface State {
    isModalVisible: boolean
}

export default class DonationInput extends Component<Props, State> {
    state = {
        isModalVisible: false,
    }
    _modalRender = () => {
        return (
            <Modal animationType="slide" transparent={false} visible={this.state.isModalVisible}>
                <View style={{ flex: 1, alignItems: 'center', backgroundColor: '#fff', justifyContent: 'center' }}>
                    <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Enter Amount</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20 }}>
                        <Text style={{ fontSize: 14, paddingRight: 10, fontWeight: 'bold' }}>$</Text>
                        <TextInput
                            style={{ height: 40, fontSize: 40 }}
                            onChangeText={this.props.onChangeText}
                            value={this.props.value}
                            placeholder="0.00"
                            placeholderTextColor="grey"
                            keyboardType={'numeric'}
                            maxLength={4}
                            autoFocus
                        />
                    </View>
                    <TouchableOpacity
                        onPress={() => this.setState({ isModalVisible: false })}
                        style={{ backgroundColor: '#308788', padding: 20, width: 300, borderRadius: 50, marginTop: 30 }}
                    >
                        <Text style={{ alignSelf: 'center', fontSize: 15, fontWeight: 'bold', color: 'white' }}>
                            Add
                        </Text>
                    </TouchableOpacity>
                </View>
            </Modal>
        )
    }
    render() {
        return (
            <View>
                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingVertical: 20,
                    }}
                >
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'white' }}>{this.props.title}</Text>
                    <View>
                        {!this.props.value ? (
                            <TouchableOpacity onPress={() => this.setState({ isModalVisible: true })}>
                                <Icon name="add" />
                            </TouchableOpacity>
                        ) : (
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <TouchableOpacity onPress={this.props.itemRemove}>
                                    <Icon name="remove_red" />
                                </TouchableOpacity>
                                <View
                                    style={{
                                        backgroundColor: 'white',
                                        marginLeft: 20,
                                        width: 80,
                                        height: 38,
                                        borderRadius: 40,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}
                                >
                                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#6500d3' }}>
                                        $ {this.props.value}
                                    </Text>
                                </View>
                            </View>
                        )}
                    </View>
                </View>
                {this._modalRender()}
            </View>
        )
    }
}
