import React, { Component } from 'react'
import { Text, View, Modal, SafeAreaView, Dimensions } from 'react-native'
import Icon from '../../../common/Icon'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { CreditCardInput } from 'react-native-credit-card-input'

const { width, height } = Dimensions.get('window')

interface Props {
    isWalletModalVisible: boolean
    onPressCancel: any
    onChange: any
}
interface State {}
export default class CardWallet extends Component<Props, State> {
    render() {
        return (
            <Modal animationType="slide" transparent={false} visible={this.props.isWalletModalVisible}>
                <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
                    <View
                        style={{
                            marginHorizontal: 20,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                        }}
                    >
                        <TouchableOpacity onPress={() => this.props.onPressCancel()}>
                            <Icon name="back_dark" width="24" />
                        </TouchableOpacity>
                        <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Wallet</Text>
                        <View />
                    </View>
                    <View style={{ marginHorizontal: 20, marginTop: 40 }}>
                        <CreditCardInput onChange={this.props.onChange} />
                    </View>
                    <TouchableOpacity
                        onPress={() => this.props.onPressCancel()}
                        style={{
                            marginHorizontal: 20,
                            marginTop: 40,
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: '#308788',
                            height: 60,
                            borderRadius: 30,
                        }}
                    >
                        <Text style={{ color: 'white', fontSize: 15, fontWeight: 'bold' }}>Add</Text>
                    </TouchableOpacity>
                </SafeAreaView>
            </Modal>
        )
    }
}
