import { LocationActionType } from './LocationActionType'

export function insertLocationCoords(payload) {
    return {
        type: LocationActionType.GET_COORDS,
        payload,
    }
}

export function insertLocationAddress(payload) {
    return {
        type: LocationActionType.GET_ADDRESS,
        payload,
    }
}
