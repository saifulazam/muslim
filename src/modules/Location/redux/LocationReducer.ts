import { LocationAction, LocationActionType, LocationState } from './LocationActionType'

const INITIAL_STATE: LocationState = {
    coords: null,
    address: null,
}

const reducer = (state = INITIAL_STATE, actionType: LocationAction) => {
    switch (actionType.type) {
        case LocationActionType.GET_COORDS:
            return setLocationCoords(state, actionType.payload)
        case LocationActionType.GET_ADDRESS:
            return setLocationAddress(state, actionType.payload)
        default:
            return state
    }
}

const setLocationCoords = (state, payload) => {
    return { ...state, coords: payload }
}

const setLocationAddress = (state, payload) => {
    return { ...state, address: payload }
}

export default reducer
