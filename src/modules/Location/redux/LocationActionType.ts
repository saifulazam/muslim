export enum LocationActionType {
    GET_COORDS = '@GET_COORDS',
    GET_ADDRESS = '@GET_ADDRESS',
}

export type CoordsModel = {
    latitude: number
    longitude: number
}

export type AddressModel = {
    name: string
    street: string
    city: string
    region: string
    postalCode: string
    country: string
}

export type LocationState = {
    coords: CoordsModel
    address: AddressModel
}

export type LocationAction = {
    type: string
    payload: any
}
