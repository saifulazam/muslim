import * as LocationAction from '../redux/LocationAction'
import { GOOGLE_API, MAPBOX_TOKEN, MAPBOX_API } from 'react-native-dotenv'
import Geocoder from 'react-native-geocoding'
import * as Location from 'expo-location'
import { getStorage } from '../../../utils/Storage'
//@ts-ignore
Geocoder.init(GOOGLE_API)

export function locationCoordsStore(latitude, longitude, address) {
    return (dispatch) => {
        new Promise((resolve, reject) => {
            if (latitude && longitude && address) {
                const coords = { latitude, longitude }
                dispatch(LocationAction.insertLocationCoords(coords))
                dispatch(LocationAction.insertLocationAddress(address))
                return resolve()
            } else {
                console.warn('Something went wrong.')
                return reject()
            }
        })
    }
}

const getLocation = async () => {
    let location = await Location.getCurrentPositionAsync({ accuracy: Location.Accuracy.Highest })
    const { latitude, longitude } = location.coords
    const address = await Location.reverseGeocodeAsync({ latitude, longitude })
    const addressItem = address[0]
    const formatAddress = `${addressItem.city}, ${addressItem.region} ${addressItem.postalCode}`
    return {
        completeAddress: addressItem,
        address: formatAddress,
        addressItem: [formatAddress],
        coords: { lat: latitude, lng: longitude },
    }
}

export function googleSearchAddress(input) {
    return new Promise((resolve, reject) => {
        try {
            fetch(
                `https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${input}&key=${GOOGLE_API}&sessiontoken=${generateToken(
                    10
                )}`
            )
                .then((res) => res.json())
                .then((result) => {
                    return resolve(result)
                })
        } catch (err) {
            reject(err)
        }
    })
}

export const geocodeCoordsByAddress = (address) => {
    return new Promise((resulve, reject) => {
        Geocoder.from(address)
            .then((json) => {
                const { location } = json.results[0].geometry
                resulve(location)
            })
            .catch((error) => reject(error))
    })
}

export const geocodeLocationByName = (locationName) => {
    return new Promise((resolve, reject) => {
        Geocoder.from(locationName)
            .then((json) => {
                const addressComponent = json.results[0].address_components[0]
                resolve(addressComponent)
            })
            .catch((error) => reject(error))
    })
}
