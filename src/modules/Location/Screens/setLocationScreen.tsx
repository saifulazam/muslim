import React from 'react'
import { SafeAreaView } from 'react-native-safe-area-context'
import { View, Text, Alert, FlatList, StatusBar } from 'react-native'
import FormField from '../../../common/FormField'
import { StackNavigationProp } from '@react-navigation/stack'
import Icon from '../../../common/Icon'
import { Divider } from 'react-native-elements'
import { TouchableOpacity } from 'react-native-gesture-handler'
import * as Permissions from 'expo-permissions'
import * as Location from 'expo-location'
import testData from '../../../data/verified'
import Spinner from '../../../common/Spinner'
import { googleSearchAddress, mapBoxSearch } from '../../../services/location.service'
import { connect } from 'react-redux'
import { CustomStatusBar } from '../../../utils/StatusBar'

import { locationCoordsStore, geocodeCoordsByAddress } from '../Services/LocationService'
import { setAllPrayerTimes } from '../../Prayer/services/PrayerService'
import { getNearByMosques } from '../../Mosque/Services/MosqueService'
import * as Localization from 'expo-localization'
interface Props {
    navigation: StackNavigationProp<any>
    onPermissionGrantedAction: any
    fetchPrayerTimes: any
    getNearbyVirefiedMossque: any
    homeMosques: any
    prayerTimes: any
    getNearByAllMosques: any
    onPermissionDeniedAction: any
    unauthorizedLocationData: any

    locationCoordsStore?: any
    setAllPrayerTimes?: any
    getNearByMosques?: any
    coords?: any
    address?: any
}

interface State {
    searchInput: string
    coords: any
    searchItems: any[]
    isSpinnerActive: boolean
}

export class setLocationScreen extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {
            searchInput: null,
            coords: null,
            searchItems: [],
            isSpinnerActive: false,
        }
    }

    public componentDidMount() {
        CustomStatusBar(this.props.navigation, 'dark-content')
    }
    public render() {
        return (
            <SafeAreaView style={{ flex: 1, marginHorizontal: 20 }}>
                <View style={{ flexDirection: 'row', marginTop: 20 }}>
                    <Text style={{ marginStart: 0, fontSize: 24, fontWeight: 'bold' }}>Enter a location</Text>
                    <Text
                        style={{
                            marginTop: 10,
                            fontSize: 13,
                            color: '#3c00b5',
                            position: 'absolute',
                            right: 10,
                            fontWeight: 'bold',
                        }}
                        onPress={() => this.props.navigation.goBack()}
                    >
                        Cancel
                    </Text>
                </View>
                <View style={{ marginTop: 30 }}>
                    <FormField
                        placeholder={'Try: Parkchester'}
                        value={this.state.searchInput}
                        onChangeText={this.onSearchInputChange}
                    />
                </View>
                <Divider style={{ backgroundColor: '#3c00b5', marginTop: 20 }} />
                <TouchableOpacity style={{ marginTop: 20, flexDirection: 'row' }} onPress={this.onPressLocation}>
                    <Icon name={'check_green'} />
                    <Text style={{ color: '#308788', fontSize: 18, marginStart: 20, marginTop: 5, fontWeight: '900' }}>
                        Use current location
                    </Text>
                </TouchableOpacity>
                <Divider style={{ backgroundColor: '#3c00b5', marginTop: 20 }} />
                {this.state.isSpinnerActive && <Spinner />}
                <this.SearchedItemsContainer input={this.state.searchInput} />
            </SafeAreaView>
        )
    }

    public onSearchInputChange = (searchInput) => {
        this.setState({ searchInput })
        if (searchInput.length > 3) {
            // mapBoxSearch(searchInput).then((result) => {
            //     this.setState({ searchItems: result })
            // })

            googleSearchAddress(searchInput).then((result) => {
                if (result.status == 'OK') {
                    this.setState({ searchItems: result.predictions })
                }
            })
        }
    }

    public onPressLocation = async () => {
        let { status } = await Permissions.getAsync(Permissions.LOCATION)
        if (status !== 'granted') {
            Alert.alert(
                'Permission to access location was denied, Please enable location services from your phone settings or enter an address manually'
            )
        } else if (status === 'granted') {
            this.setState({ isSpinnerActive: true })
            await this.onGranted()
        }
    }

    public onGranted = async () => {
        let location = await Location.getCurrentPositionAsync({ accuracy: Location.Accuracy.Highest })
        const { latitude, longitude } = location.coords
        const address = await Location.reverseGeocodeAsync({ latitude, longitude })
        const addressItem = address[0]
        const formatAddress = `${addressItem.name}, ${addressItem.city}, ${addressItem.region} ${addressItem.postalCode}`
        const addressfull = {
            name: addressItem.name,
            city: addressItem.city,
            region: addressItem.region,
            postalCode: addressItem.postalCode,
        }
        this.props.locationCoordsStore(latitude, longitude, addressfull)
        await this._reduxDespatchItems()

        this.setState(
            {
                searchInput: formatAddress,
                searchItems: [formatAddress],
                coords: { latitude, longitude },
                isSpinnerActive: false,
            },
            () => {
                this.props.navigation.navigate('Discover', {
                    locationDetails: {
                        coords: { latitude, longitude },
                        addressItem,
                    },
                })
            }
        )
    }

    public SearchedItemsContainer = ({ input }) => {
        const data = testData.filter((item) => (input ? item.location.formatted_address.includes(input) : null))
        return (
            <FlatList
                data={this.state.searchItems}
                renderItem={this._renderSearchResults}
                ItemSeparatorComponent={() => <Divider />}
                keyExtractor={(item) => Math.random() + item.id}
            />
        )
    }

    public _renderSearchResults = ({ item, index }) => {
        return (
            <>
                <TouchableOpacity
                    key={item.id}
                    onPress={() => this.setSearchedItem(item)}
                    style={{ height: 40, flexDirection: 'row', marginTop: 30 }}
                >
                    <Text style={{ fontSize: 15, fontWeight: '500', color: 'black' }} key={index}>
                        {item.description}
                    </Text>
                </TouchableOpacity>
            </>
        )
    }

    public setSearchedItem = async (item) => {
        const address = {
            name: item.description,
            street: item.terms[1].value,
            city: item.terms[2].value,
            region: item.terms[3].value,
            country: item.terms[4].value,
        }
        this.setState({ isSpinnerActive: true, searchInput: item.description })
        geocodeCoordsByAddress(item.description).then((result) => {
            //@ts-ignore
            this.props.locationCoordsStore(result.lat, result.lng, address)
        })
        await this._reduxDespatchItems()
        this.props.navigation.navigate('Discover')
    }

    _reduxDespatchItems = async () => {
        return new Promise(() =>
            setTimeout(() => {
                if (this.props.coords && this.props.address) {
                    const { latitude, longitude } = this.props.coords
                    this.props.setAllPrayerTimes(latitude, longitude, Localization.timezone)
                    this.props.getNearByMosques(latitude, longitude)
                    this.props.navigation.navigate('Discover')
                }
                this.setState({ isSpinnerActive: false })
            }, 2000)
        )
    }
}

const mapStateToProps = function (state) {
    return {
        prayerTimes: state.prayerTimes,
        mosques: state.mosques.mosques,
        coords: state.location.coords,
        address: state.location.address,
    }
}

export default connect(mapStateToProps, { locationCoordsStore, setAllPrayerTimes, getNearByMosques })(setLocationScreen)
