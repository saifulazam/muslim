const toast = {
    CREATED_SUCCESS: {
        title: 'Successfully created',
        text: '',
        color: '#2ecc71',
        timing: 2000,
    },
    DELETED_SUCCESS: {
        title: 'Successfully deleted',
        text: '',
        color: '#2ecc71',
        timing: 2000,
    },
    REPORT_SUCCESS: {
        title: 'Successful',
        text: '',
        color: '#2ecc71',
        timing: 2000,
    },
    COMMENT_REPORT_SUCCESS: {
        title: 'Successful',
        text: '',
        color: '#2ecc71',
        timing: 2000,
    },
    COMMENT_SUCCESS: {
        title: 'Successfully created',
        text: '',
        color: '#2ecc71',
        timing: 2000,
    },
    COMMENT_DELETE_SUCCESS: {
        title: 'Successfully deleted',
        text: '',
        color: '#2ecc71',
        timing: 2000,
    },
}

export { toast }
