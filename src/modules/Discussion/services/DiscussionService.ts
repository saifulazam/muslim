import Toast from '../../../lib/Toast/Toast'
import Http from '../../../utils/Http'
import Transformer from '../../../utils/Transformer'
import * as DiscussAction from '../redux/DiscussionAction'
import { toast } from '../lang/en'

export function getCategories() {
    return (dispatch) => {
        new Promise((resolve, reject) => {
            Http.get('discussions/categories')
                .then((response) => {
                    const data = Transformer.fetch(response.data)
                    dispatch(DiscussAction.categoriesFetch(data))
                    return resolve()
                })
                .catch((error) => {
                    console.warn(error)
                    return reject(error)
                })
        })
    }
}

export function getDiscussions(category, page) {
    return (dispatch) => {
        new Promise((resolve, reject) => {
            Http.get(`discussions/all?name=${category}&page=${page}`)
                .then((response) => {
                    const data = Transformer.fetch(response.data)
                    dispatch(DiscussAction.discussionsPageFetch(page))
                    dispatch(DiscussAction.discussionsFetch(data))
                    return resolve()
                })
                .catch((error) => {
                    console.warn(error)
                    return reject(error)
                })
        })
    }
}

export function getDiscussionById(id) {
    return (dispatch) => {
        new Promise((resolve, reject) => {
            Http.get(`discussions/${id}`)
                .then((response) => {
                    const data = Transformer.fetch(response.data)
                    dispatch(DiscussAction.discussionFetch(data))
                    return resolve()
                })
                .catch((error) => {
                    console.warn(error)
                    return reject(error)
                })
        })
    }
}

export function deleteDiscussion(id, token) {
    return (dispatch) => {
        new Promise((resolve, reject) => {
            Http.defaults.headers.common['Authorization'] = `Bearer ${token}`
            Http.delete(`/discussions/${id}/delete`)
                .then((response) => {
                    if (response.status === 204) {
                        dispatch(DiscussAction.discussionDelete())
                        Toast.show(toast.DELETED_SUCCESS)
                        return resolve()
                    }
                })
                .catch((error) => {
                    console.warn(error)
                    return reject(error)
                })
        })
    }
}
