import { createStackNavigator, StackNavigationOptions, TransitionPresets } from '@react-navigation/stack'
import React from 'react'
import DiscussionDetailScreen from '../Screens/DiscussionDetailScreen'
import DiscussionLandingScreen from '../Screens/DiscussionLandingScreen'

export type DiscussionStackParamList = {
    Discussion: undefined
    DiscussionDetail: undefined
}

const Stack = createStackNavigator<DiscussionStackParamList>()

const DiscussionStack = () => {
    const navigationOptions: StackNavigationOptions = {
        headerShown: false,
        gestureEnabled: false,
    }

    return (
        <Stack.Navigator screenOptions={navigationOptions}>
            <Stack.Screen
                name="Discussion"
                component={DiscussionLandingScreen}
                options={{
                    ...TransitionPresets.DefaultTransition,
                }}
            />
            <Stack.Screen
                name="DiscussionDetail"
                component={DiscussionDetailScreen}
                options={{
                    ...TransitionPresets.DefaultTransition,
                }}
            />
        </Stack.Navigator>
    )
}

export default DiscussionStack
