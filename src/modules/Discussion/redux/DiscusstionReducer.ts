import { DiscussionState, DiscussionActionType, DiscussionAction } from './DiscussionActionType'

const INITIAL_STATE: DiscussionState = {
    discussions: [],
    categories: [],
    discussion: null,
    page: null,
}

const reducer = (state = INITIAL_STATE, actionType: DiscussionAction) => {
    switch (actionType.type) {
        case DiscussionActionType.DISCUSSION_CATEGORIES_FETCH:
            return categories(state, actionType.payload)
        case DiscussionActionType.DISCUSSION_ALL__FETCH:
            return discussions(state, actionType.payload)
        case DiscussionActionType.DISCUSSION_FETCH:
            return discussion(state, actionType.payload)
        case DiscussionActionType.DISCUSSION_ALL__FETCH_PAGE:
            return discussionsPage(state, actionType.payload)
        case DiscussionActionType.DISCUSSION_DELETE:
            return discussionDelete(state)
        default:
            return state
    }
}

const discussionDelete = (state) => {
    return { ...state }
}
const categories = (state, payload) => {
    return { ...state, categories: payload.data }
}

const discussions = (state, payload) => {
    return {
        ...state,
        discussions: state.page === 1 ? Array.from(payload.data) : [...state.discussions, ...payload.data],
    }
}

const discussionsPage = (state, payload) => {
    return { ...state, page: payload }
}

const discussion = (state, payload) => {
    return {
        ...state,
        discussion: payload.data,
    }
}

export default reducer
