export enum DiscussionActionType {
    DISCUSSION_ALL__FETCH = '@DISCUSSION_ALL__FETCH',
    DISCUSSION_ALL__FETCH_PAGE = '@DISCUSSION_ALL__FETCH_PAGE',
    DISCUSSION_CATEGORIES_FETCH = '@DISCUSSION_CATEGORIES_FETCH',
    DISCUSSION_FETCH = '@DISCUSSION_FETCH',
    DISCUSSION_DELETE = '@DISCUSSION_DELETE',
    DISCUSSION_REPORT = '@DISCUSSION_REPORT',
}

export type CreatorModel = {
    id: number
    name: string
    avatar: string
}

export type DiscussionCategoryModel = {
    id: number
    name: string
    slug: string
    image?: string
    description?: string
    created_at: any
    updated_at: any
}

export type DiscussionCommentModel = {
    id: number
    added: string
    discuss_id: number
    message: string
    user: CreatorModel
}

export type DiscussionModel = {
    id: number
    added: any
    content: any
    images?: any
    title?: string
    popularity?: number
    isAnswered?: boolean
    comments: DiscussionCommentModel[]
    category: DiscussionCategoryModel[]
    creator: CreatorModel
}

export type DiscussionState = {
    discussions: DiscussionModel[]
    categories: DiscussionCategoryModel[]
    discussion: DiscussionModel
    page?: number
}

export type DiscussionAction = {
    type: string
    payload: any
}
