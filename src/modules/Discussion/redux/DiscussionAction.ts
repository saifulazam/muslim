import { DiscussionActionType } from './DiscussionActionType'

export function categoriesFetch(payload) {
    return {
        type: DiscussionActionType.DISCUSSION_CATEGORIES_FETCH,
        payload,
    }
}

export function discussionsFetch(payload) {
    return {
        type: DiscussionActionType.DISCUSSION_ALL__FETCH,
        payload,
    }
}

export function discussionsPageFetch(payload) {
    return {
        type: DiscussionActionType.DISCUSSION_ALL__FETCH_PAGE,
        payload,
    }
}

export function discussionFetch(payload) {
    return {
        type: DiscussionActionType.DISCUSSION_FETCH,
        payload,
    }
}

export function discussionDelete() {
    return {
        type: DiscussionActionType.DISCUSSION_DELETE,
    }
}
