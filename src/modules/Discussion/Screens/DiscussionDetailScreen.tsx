import React, { Component } from 'react'
import { View, SafeAreaView, TouchableOpacity, ScrollView, RefreshControl } from 'react-native'
import { StackNavigationProp } from '@react-navigation/stack'
import _COLORS from '../../../utils/Color'
import { wait } from '../../../utils/datetime'
import DiscussionDetailScreenBody from '../components/DiscussionDetailScreenBody'
import DiscussionDetailScreenHeader from '../components/DiscussionDetailScreenHeader'
import DiscussionComments from '../components/DiscussionComments'
import { connect } from 'react-redux'
import CommentBox from '../components/CommentBox'
import { getDiscussionById } from '../services/DiscussionService'
import Spinner from '../../../common/Spinner'
import { CustomStatusBar } from '../../../utils/StatusBar'

interface Props {
    navigation: StackNavigationProp<any>
    route: any
    isAuthenticated?: boolean
    getDiscussionById?: any
    discussion: any
}
interface State {
    readMore: boolean
    isRefresh: boolean
    isCommentFormModalVisible: boolean
}

export class DiscussionDetailScreen extends Component<Props, State> {
    constructor(props) {
        super(props)
        this.state = {
            readMore: false,
            isRefresh: false,
            isCommentFormModalVisible: false,
        }
    }

    async componentDidMount() {
        CustomStatusBar(this.props.navigation, 'dark-content')
        this._fetchDiscussion()
    }

    _fetchDiscussion = () => {
        this.props.getDiscussionById(this.props.route.params.discussion_id)
    }

    render() {
        if (!this.props.discussion) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Spinner />
                </View>
            )
        }
        return (
            <SafeAreaView style={{ backgroundColor: 'white', flex: 1 }}>
                <View style={{ borderBottomWidth: 1, borderBottomColor: '#eee', paddingBottom: 15 }}>
                    <DiscussionDetailScreenHeader
                        discussion={this.props.discussion}
                        navigation={this.props.navigation}
                        auth={this.props.isAuthenticated}
                    />
                </View>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    // centerContent={true}
                    contentContainerStyle={{ paddingBottom: 100 }}
                    overScrollMode="auto"
                    refreshControl={<RefreshControl refreshing={this.state.isRefresh} onRefresh={this._onRefresh} />}
                >
                    <View style={{ borderBottomWidth: 1, borderBottomColor: '#eee', paddingBottom: 15 }}>
                        <DiscussionDetailScreenBody
                            discussion={this.props.discussion}
                            navigation={this.props.navigation}
                        />
                    </View>
                    <DiscussionComments
                        comments={this.props.discussion.comments}
                        navigation={this.props.navigation}
                        reRender={this._fetchDiscussion}
                        auth={this.props.isAuthenticated}
                    />
                </ScrollView>
                <CommentBox
                    discussion={this.props.discussion}
                    onRender={this._fetchDiscussion}
                    isAuthenticated={this.props.isAuthenticated}
                    navigation={this.props.navigation}
                />
            </SafeAreaView>
        )
    }

    _onRefresh = () => {
        this.setState({ isRefresh: true })
        this._fetchDiscussion()
        wait(1000).then(() => this.setState({ isRefresh: !this.state.isRefresh }))
    }
}

const mapStateToProps = function (state) {
    return {
        isAuthenticated: state.auth.isAuthenticated,
        discussion: state.discuss.discussion,
    }
}

export default connect(mapStateToProps, { getDiscussionById })(DiscussionDetailScreen)
