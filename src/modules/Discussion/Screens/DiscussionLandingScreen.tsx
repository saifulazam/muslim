import React, { Component } from 'react'
import { View, TouchableOpacity, SafeAreaView, StatusBar, StyleSheet } from 'react-native'
import Spinner from '../../../common/Spinner'
import { StackNavigationProp } from '@react-navigation/stack'
import _COLORS from '../../../utils/Color'
import Icon from '../../../common/Icon'
import DiscussionAddButtonActionModal from '../components/Actions/DiscussionAddButtonActionModal'
import DiscussionFormModal from '../components/Forms/DiscussionFormModal'
import DiscussionScreenCategoriesTabs from '../components/DiscussionScreenCategoriesTabs'
import DiscussionsList from '../components/DiscussionsList'
import { connect } from 'react-redux'
import { getCategories, getDiscussions } from '../services/DiscussionService'
import { CustomStatusBar } from '../../../utils/StatusBar'
interface Props {
    navigation: StackNavigationProp<any>
    isAuthenticated?: boolean
    getCategories?: any
    categories?: any
    discussions?: any
    getDiscussions?: any
}

interface State {
    categories: any
    isLoading: boolean
    actionModalVisitable: boolean
    tabSelected: string
    category: any
    formModalVisitable: boolean
    discussions: any
    isRefresh: boolean
    page: number
    isLoadMore: boolean
}

class DiscussionLandingScreen extends Component<Props, State> {
    constructor(props) {
        super(props)
        this.state = {
            categories: [],
            isLoading: true,
            actionModalVisitable: false,
            tabSelected: 'all',
            category: {},
            formModalVisitable: false,
            discussions: [],
            isRefresh: false,
            page: 1,
            isLoadMore: false,
        }
    }
    componentDidMount() {
        CustomStatusBar(this.props.navigation, 'dark-content')

        this.props.getCategories()
        this._fetchAllDiscussions()
    }

    _fetchAllDiscussions = () => {
        const { page, tabSelected } = this.state
        this.props.getDiscussions(tabSelected, page)
    }

    _handleLoadMore = () => {
        this.setState(
            (prevState, nextProps) => ({
                page: prevState.page + 1,
                isLoadMore: true,
            }),
            () => {
                this._fetchAllDiscussions()
            }
        )
    }

    _onRefresh = () => {
        this.setState({ isRefresh: true })
        this._fetchAllDiscussions()
        setTimeout(() => {
            this.setState({ isRefresh: false })
        }, 2000)
    }

    onCancelActionModal = () => {
        this.setState({ actionModalVisitable: !this.state.actionModalVisitable })
    }

    onPressFormModal = (category) => {
        this.setState({
            actionModalVisitable: !this.state.actionModalVisitable,
            category: category,
            formModalVisitable: true,
        })
    }

    __onPressAddPost = async () => {
        if (this.props.isAuthenticated) {
            this.setState({ formModalVisitable: true })
        } else {
            this.props.navigation.navigate('LoginScreen')
        }
    }

    __onPressCategoryHandler = (item) => {
        requestAnimationFrame(() => {
            this.setState(
                {
                    tabSelected: item.name ? item.name : item,
                    isRefresh: false,
                    page: 1,
                    isLoadMore: false,
                    discussions: [],
                },
                () => this._fetchAllDiscussions()
            )
        })
    }

    render() {
        if (!Array.isArray(this.props.categories)) {
            return <Spinner size="large" animating />
        }
        return (
            <>
                <View style={{ flex: 1 }}>
                    <SafeAreaView
                        style={{
                            backgroundColor: '#fafafa',
                            marginBottom: 10,
                            elevation: 0,
                            shadowOpacity: 0.3,
                            borderBottomWidth: 0.1,
                            shadowColor: 'rgba(250 250 250, 0.9)',
                        }}
                    >
                        <View style={{ paddingVertical: 20 }}>
                            <DiscussionScreenCategoriesTabs
                                onPress={this.__onPressCategoryHandler}
                                categories={this.props.categories}
                                tabSelected={this.state.tabSelected}
                            />
                        </View>
                    </SafeAreaView>
                    {Array.isArray(this.props.discussions) ? (
                        <DiscussionsList
                            discussions={this.props.discussions}
                            category={this.state.tabSelected}
                            navigation={this.props.navigation}
                            isRefresh={this.state.isRefresh}
                            isLoadMore={this.state.isLoadMore}
                            onRefresh={this._onRefresh}
                            onLoadMore={this._handleLoadMore}
                        />
                    ) : (
                        <Spinner size="large" animating />
                    )}

                    <TouchableOpacity onPress={() => this.__onPressAddPost()} style={styles.addBtn}>
                        <Icon name="add" width="50" height="50" />
                    </TouchableOpacity>
                    <DiscussionAddButtonActionModal
                        visitable={this.state.actionModalVisitable}
                        categories={this.props.categories}
                        onCancel={this.onCancelActionModal}
                        actionOnPress={this.onPressFormModal}
                    />
                    <DiscussionFormModal
                        visitable={this.state.formModalVisitable}
                        onCancel={() => this.setState({ formModalVisitable: !this.state.formModalVisitable })}
                        category={this.state.category}
                        onReRender={this._fetchAllDiscussions}
                    />
                </View>
            </>
        )
    }
}

const styles = StyleSheet.create({
    addBtn: {
        position: 'absolute',
        alignSelf: 'flex-end',
        padding: 10,
        shadowColor: '#000000',
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowRadius: 5,
        shadowOpacity: 0.5,
        bottom: 0,
    },
})

const mapStateToProps = function (state) {
    return {
        isAuthenticated: state.auth.isAuthenticated,
        categories: state.discuss.categories,
        discussions: state.discuss.discussions,
    }
}

export default connect(mapStateToProps, { getCategories, getDiscussions })(DiscussionLandingScreen)
