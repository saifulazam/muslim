import React, { Component } from 'react'
import { Text, View } from 'react-native'
import _COLORS from '../../../utils/Color'
import LazyImage from '../../../common/LazyImage/LazyImage'
import moment from 'moment'
import { StackNavigationProp } from '@react-navigation/stack'
import ReadMore from '../../../common/ReadMore'

interface Props {
    discussion: any
    navigation: StackNavigationProp<any>
}
interface State {}
export default class DiscussionDetailScreenBody extends Component<Props, State> {
    __renderCreator = (creator) => {
        if (creator) {
            return <LazyImage source={{ url: creator.avatar }} style={{ width: 40, height: 40, borderRadius: 20 }} />
        } else {
            return (
                <LazyImage
                    source={require('../../../../assets/images/appIconSmall.png')}
                    style={{ width: 40, height: 40, borderRadius: 20 }}
                />
            )
        }
    }
    _renderBody = () => {
        return (
            <View style={{ paddingTop: 15, marginHorizontal: 20 }}>
                <View style={{ flexDirection: 'row' }}>
                    {this.__renderCreator(this.props.discussion.creator)}
                    <View style={{ paddingLeft: 10 }}>
                        <Text style={{ fontSize: 20 }}>
                            {this.props.discussion.creator ? this.props.discussion.creator.name : 'Alnur'}
                        </Text>
                        <Text style={{ color: 'gray' }}>{moment(this.props.discussion.added).fromNow()}</Text>
                    </View>
                </View>
                {this.props.discussion.images.length > 0 ? (
                    <View style={{ paddingTop: 10 }}>
                        <LazyImage
                            source={{ uri: this.props.discussion.images[0].path }}
                            style={{ width: '100%', height: 187 }}
                        />
                    </View>
                ) : null}
                <View style={{ paddingTop: 10 }}>
                    <ReadMore>{this.props.discussion.content}</ReadMore>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={{ color: 'gray', paddingTop: 10 }}>
                        {this.props.discussion.comments.length} replies
                    </Text>
                    <Text style={{ color: 'gray', paddingTop: 10 }}>{this.props.discussion.popularity} views</Text>
                </View>
            </View>
        )
    }
    render() {
        return <View>{this._renderBody()}</View>
    }
}
