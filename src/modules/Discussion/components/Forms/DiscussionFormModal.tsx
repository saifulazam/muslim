import * as React from 'react'
import {
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
    TextInput,
    Dimensions,
    Alert,
    Image,
    SafeAreaView,
    KeyboardAvoidingView,
    Platform,
} from 'react-native'
import Icon from '../../../../common/Icon'
import _COLORS from '../../../../utils/Color'
import Modal from '../../../../common/Modal'
import { API } from 'react-native-dotenv'
import * as ImagePicker from 'expo-image-picker'
import Constants from 'expo-constants'
import * as Permissions from 'expo-permissions'
import Toast from '../../../../lib/Toast/Toast'
import { toast } from '../../lang/en'
import { getStorage } from '../../../../utils/Storage'

export interface Props {
    visitable: boolean
    onCancel: any
    category: any
    onReRender?: any
}

export interface State {
    text: string
    image: string
    error: string
}

export default class DiscussFormModal extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {
            text: '',
            image: null,
            error: '',
        }
    }

    componentDidMount() {
        // this.getPermissionAsync()
    }

    getPermissionAsync = async () => {
        if (Constants.platform.ios) {
            const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
            if (status !== 'granted') {
                Alert.alert('Sorry, we need camera roll permissions to make this work!')
            }
        }
    }

    _pickImage = async () => {
        this.getPermissionAsync()
        try {
            let result = await ImagePicker.launchImageLibraryAsync({
                mediaTypes: ImagePicker.MediaTypeOptions.All,
                allowsEditing: true,
                aspect: [4, 3],
                quality: 1,
            })
            if (!result.cancelled) {
                //@ts-ignore
                this.setState({ image: result.uri })
            }
        } catch (E) {
            console.warn(E)
        }
    }

    onPressSubmit = async (category) => {
        const token = await getStorage('access_token')
        const url = `${API}/discussions/store`

        let formData = new FormData()
        formData.append('category_id', 1)
        formData.append('content', this.state.text)
        if (this.state.image) {
            let imageUrl = this.state.image
            let filename = imageUrl.split('/').pop()
            let match = /\.(\w+)$/.exec(filename)
            let type = match ? `image/${match[1]}` : `image`
            formData.append('images', { uri: imageUrl, name: filename, type })
        }

        let headers = {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'multipart/form-data',
            Accept: 'application/json',
        }

        fetch(url, {
            method: 'POST',
            headers: headers,
            body: formData,
        })
            .then((response) => response.json())
            .then((json) => {
                if (json.status === 201) {
                    this.setState({ text: '', image: null })
                    Toast.show(toast.CREATED_SUCCESS)
                    this.props.onCancel()
                    this.props.onReRender()
                    // return this.props.navigation.navigate('Discussion')
                } else {
                    this.setState({ error: json.message })
                    console.warn('something went wrong')
                }
            })
    }

    public FormTitleRender = () => {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    // paddingBottom: 15,
                    padding: 20,
                }}
            >
                <TouchableOpacity onPress={() => this.props.onCancel()}>
                    <Icon name="close_dark" width="17" />
                </TouchableOpacity>

                <Text style={{ fontSize: 18, fontWeight: 'bold', fontFamily: 'AvantGardeLT-Demi' }}>Question</Text>
                <TouchableOpacity
                    onPress={() =>
                        this.state.text
                            ? this.onPressSubmit(this.props.category)
                            : this.setState({ error: 'Please enter something.' })
                    }
                >
                    <Text
                        style={{
                            fontSize: 18,
                            fontWeight: 'bold',
                            fontFamily: 'AvantGardeLT-Demi',
                            color: this.state.text ? _COLORS.primary : _COLORS.gray,
                        }}
                    >
                        POST
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }

    public render() {
        return (
            <Modal isOpen={this.props.visitable} keyboardTopOffset={1}>
                <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : null}>
                    <View style={{ padding: 20, flexDirection: 'column' }}>
                        {this.FormTitleRender()}
                        {this.state.error ? <Text style={{ color: 'red' }}>{this.state.error}</Text> : null}
                        <TextInput
                            style={{ height: 300, justifyContent: 'flex-start' }}
                            underlineColorAndroid="transparent"
                            placeholder="Enter your message..."
                            placeholderTextColor="grey"
                            numberOfLines={20}
                            multiline={true}
                            value={this.state.text}
                            onChangeText={(text) => this.setState({ text: text, error: '' })}
                            autoFocus
                        />
                        <TouchableOpacity onPress={() => this._pickImage()} style={{ width: 112, height: 112 }}>
                            {this.state.image ? (
                                <Image source={{ uri: this.state.image }} style={{ width: 112, height: 112 }} />
                            ) : (
                                <View
                                    style={{
                                        backgroundColor: '#fafafa',
                                        width: 112,
                                        height: 112,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        borderWidth: 1,
                                        borderColor: '#ccc',
                                        borderStyle: 'dashed',
                                        borderRadius: 8,
                                    }}
                                >
                                    <Icon name="photo" />
                                    <Text style={{ paddingTop: 5 }}>Add Photo</Text>
                                </View>
                            )}
                        </TouchableOpacity>
                    </View>
                </KeyboardAvoidingView>
            </Modal>
        )
    }
}
