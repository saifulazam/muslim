import React, { useState } from 'react'
import { View, Text, Dimensions, TouchableOpacity, TextInput, Modal } from 'react-native'
import Toast from 'react-native-tiny-toast'
import Icon from '../../../../common/Icon'
import _COLORS from '../../../../utils/Color'
import { ServicePrivateExecutorPost } from '../../../../services/ServiceExecutor'
import { API } from 'react-native-dotenv'

interface Props {
    discussion: any
    onCancel: any
    visible: boolean
    onRender?: any
    onSwipingUp?: any
}

const DiscussionCommentFormModal = (props: Props) => {
    const [text, setText] = useState('')

    const _onPressCommentSubmit = () => {
        const url = `${API}/discussions/${props.discussion.id}/comment/store`
        const bodyParams = {
            message: text,
        }
        ServicePrivateExecutorPost(url, bodyParams, responseCallback)
    }

    const responseCallback = (response) => {
        if (response.status === 201) {
            //This will reRender the comment api
            setText('')
            Toast.showSuccess('success')
            props.onRender()
            props.onCancel()
        } else {
            console.warn(response)
        }
    }

    const formHeaderRender = () => {
        return (
            <View style={{ borderBottomWidth: 1, borderBottomColor: '#eee' }}>
                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        padding: 20,
                    }}
                >
                    <TouchableOpacity onPress={() => props.onCancel()}>
                        <Icon name="close_dark" width="17" />
                    </TouchableOpacity>

                    <Text style={{ fontSize: 18, fontWeight: 'bold', fontFamily: 'AvantGardeLT-Demi' }}>Reply</Text>
                    <TouchableOpacity onPress={() => _onPressCommentSubmit()}>
                        <Text
                            style={{
                                fontSize: 18,
                                fontWeight: 'bold',
                                fontFamily: 'AvantGardeLT-Demi',
                                color: _COLORS.primary,
                            }}
                        >
                            POST
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    return (
        <Modal visible={props.visible} animationType="slide" presentationStyle="pageSheet">
            <View>
                {formHeaderRender()}
                <View style={{ padding: 20 }}>
                    <TextInput
                        style={{ height: 200, justifyContent: 'flex-start' }}
                        underlineColorAndroid="transparent"
                        placeholder="Enter your message..."
                        placeholderTextColor="grey"
                        numberOfLines={20}
                        multiline={true}
                        value={text}
                        onChangeText={(text) => setText(text)}
                        autoFocus
                    />
                </View>
            </View>
        </Modal>
    )
}

export default DiscussionCommentFormModal
