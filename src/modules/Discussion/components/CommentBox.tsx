import React, { Component } from 'react'
import { KeyboardAvoidingView, Platform, TextInput, View, Keyboard, Button, Text } from 'react-native'
import { ServicePrivateExecutorPost } from '../../../services/ServiceExecutor'
import { API } from 'react-native-dotenv'
import KeyboardListener from 'react-native-keyboard-listener'
import PrimaryButton from '../../../common/PrimaryButton'
import Toast from '../../../lib/Toast/Toast'
import { toast } from '../lang/en'

interface Props {
    discussion: any
    onRender?: any
    isAuthenticated?: boolean
    navigation?: any
}
interface State {
    text: string
    textBoxHeight: number
    showSubmitButton: boolean
}

class CommentBox extends Component<Props, State> {
    state = {
        text: '',
        textBoxHeight: 40,
        showSubmitButton: false,
    }

    onChangeTextHandler = (e) => {}

    _onPressCommentSubmit = () => {
        const url = `${API}/discussions/${this.props.discussion.id}/comment/store`
        const bodyParams = {
            message: this.state.text,
        }
        ServicePrivateExecutorPost(url, bodyParams, this.responseCallback)
    }

    responseCallback = (response) => {
        if (response.status === 201) {
            //This will reRender the comment api
            Keyboard.dismiss()
            this.setState({ textBoxHeight: 40, showSubmitButton: false, text: '' })
            Toast.show(toast.COMMENT_SUCCESS)
            this.props.onRender()
        } else {
            console.warn(response)
        }
    }
    submitButton = () => {
        if (this.state.showSubmitButton) {
            if (this.props.isAuthenticated) {
                return (
                    <View style={{ paddingVertical: 15 }}>
                        <PrimaryButton
                            title="Post"
                            onPress={this._onPressCommentSubmit}
                            isActive={this.state.text ? true : false}
                            style={{ borderRadius: 25 }}
                        />
                    </View>
                )
            } else {
                this.props.navigation.navigate('LoginScreen')
            }
        }
    }

    render() {
        return (
            <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : null}>
                <KeyboardListener
                    onWillShow={() => {
                        this.setState({ textBoxHeight: 100, showSubmitButton: true })
                    }}
                    onWillHide={() => {
                        this.setState({ textBoxHeight: 40, showSubmitButton: false })
                    }}
                />

                <View
                    style={{
                        padding: 10,
                        backgroundColor: 'white',
                        borderTopColor: '#f2f2f2',
                        borderTopWidth: 1,
                    }}
                >
                    <TextInput
                        placeholder="Write a comment..."
                        style={{
                            backgroundColor: '#f2f2f2',
                            borderRadius: 5,
                            height: this.state.textBoxHeight,
                            paddingVertical: 0,
                            paddingHorizontal: 10,
                        }}
                        numberOfLines={20}
                        multiline={true}
                        value={this.state.text}
                        onChangeText={(text) => this.setState({ text: text })}
                    />
                    {this.submitButton()}
                </View>
            </KeyboardAvoidingView>
        )
    }
}

export default CommentBox
