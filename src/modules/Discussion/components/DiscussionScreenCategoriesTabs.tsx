import React from 'react'
import { View, Text, TouchableOpacity, ScrollView } from 'react-native'
import _COLORS from '../../../utils/Color'

interface Props {
    categories: any
    tabSelected: string
    onPress: any
}

const DiscussionScreenCategoriesTabs = (props: Props) => {
    const getCategories = () => {
        return props.categories.map((category) => {
            return (
                <TouchableOpacity
                    key={Math.random() + category.id}
                    onPress={() => props.onPress(category)}
                    style={{
                        borderBottomColor: props.tabSelected == category.name ? _COLORS.primary : null,
                        borderBottomWidth: props.tabSelected == category.name ? 1 : 0,
                        marginRight: 10,
                    }}
                >
                    <Text
                        style={{
                            paddingRight: 10,
                            fontSize: 18,
                            fontWeight: 'bold',
                            fontFamily: 'AvantGardeLT-Demi',
                            color: props.tabSelected == category.name ? _COLORS.primary : '#8e8e93',
                            lineHeight: 30,
                        }}
                    >
                        {category.name.toUpperCase()}
                    </Text>
                </TouchableOpacity>
            )
        })
    }
    return (
        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
            <TouchableOpacity
                onPress={() => props.onPress('all')}
                style={{
                    borderBottomColor: props.tabSelected == 'all' ? _COLORS.primary : null,
                    borderBottomWidth: props.tabSelected == 'all' ? 1 : 0,
                    paddingHorizontal: 25,
                }}
            >
                <Text
                    style={{
                        paddingRight: 10,
                        fontSize: 18,
                        fontWeight: 'bold',
                        fontFamily: 'AvantGardeLT-Demi',
                        color: props.tabSelected == 'all' ? _COLORS.primary : '#8e8e93',
                        lineHeight: 30,
                    }}
                >
                    Newest
                </Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={() => props.onPress('popularity')}
                style={{
                    borderBottomColor: props.tabSelected == 'popularity' ? _COLORS.primary : null,
                    borderBottomWidth: props.tabSelected == 'popularity' ? 1 : 0,
                    paddingHorizontal: 25,
                }}
            >
                <Text
                    style={{
                        paddingRight: 10,
                        fontSize: 18,
                        fontWeight: 'bold',
                        fontFamily: 'AvantGardeLT-Demi',
                        color: props.tabSelected == 'popularity' ? _COLORS.primary : '#8e8e93',
                        lineHeight: 30,
                    }}
                >
                    Popular
                </Text>
            </TouchableOpacity>
        </View>
    )
}

export default DiscussionScreenCategoriesTabs
