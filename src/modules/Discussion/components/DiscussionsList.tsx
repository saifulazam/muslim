import React from 'react'
import { View, Text, FlatList, RefreshControl, Animated } from 'react-native'
import { StackNavigationProp } from '@react-navigation/stack'
import DiscussionCard from './DiscussionCard'
import Spinner from '../../../common/Spinner'

interface Props {
    category: string
    discussions: any
    onLoadMore: any
    onRefresh: any
    isRefresh: boolean
    isLoadMore: boolean
    navigation?: StackNavigationProp<any>
}

const DiscussionsList = (props: Props) => {
    return (
        <FlatList
            data={props.discussions}
            renderItem={({ item }: { item: any }) => <DiscussionCard discussion={item} navigation={props.navigation} />}
            keyExtractor={(item) => Math.random() + item.id.toString()}
            showsVerticalScrollIndicator={false}
            refreshControl={<RefreshControl refreshing={props.isRefresh} onRefresh={props.onRefresh.bind(this)} />}
            onEndReached={props.onLoadMore}
            onEndReachedThreshold={0}
            initialNumToRender={5}
            maxToRenderPerBatch={10}
            getItemLayout={(data, index) => ({ length: 40, offset: 40 * index, index })}
            // windowSize={10}
            ListFooterComponent={() => {
                if (!props.isLoadMore) return null
                return <Spinner />
            }}
        />
    )
}

export default DiscussionsList
