import ModalTitle from '../../../../common/Modal/ModalTitle'
import React, { useCallback, useMemo, useRef, useState } from 'react'
import { StyleSheet, Text, View, Dimensions } from 'react-native'
import { Overlay } from 'react-native-elements'
import ActionButton from '../../../../common/ActionButton'
import Modal from '../../../../common/Modal'
// import { SwipeablePanel } from 'rn-swipeable-panel'
// import BottomSheet from '../../../../lib/bottom-sheet'

var screen = Dimensions.get('window')

interface Props {
    visitable: boolean
    onCancel: any
    categories: any
    actionOnPress?: any
}

const DiscussionAddButtonActionModal = (props: Props) => {
    return (
        <Modal isOpen={props.visitable} position={'bottom'} style={{ height: 300 }} backdropPressToClose={false}>
            <View style={{ width: screen.width, paddingLeft: 10 }}>
                <ModalTitle title="Choose" />
                {props.categories.map((category) => {
                    return (
                        <View key={category.id}>
                            <ActionButton onPress={() => props.actionOnPress(category)} name={category.name} />
                        </View>
                    )
                })}
                <ActionButton onPress={() => props.onCancel()} name="Cancel" color="red" />
            </View>
        </Modal>
    )
}

// const DiscussionAddButtonActionModal = (props: Props) => {
//     const panelProps = {
//         onlySmall: true,
//         fullWidth: true,
//         showCloseButton: true,
//         onClose: props.onCancel,
//         onPressCloseButton: props.onCancel,
//         // closeOnTouchOutside: true,
//         // allowTouchOutside: true,
//         // style: { backgroundColor: '#1f1f1f' },
//         // barStyle: { backgroundColor: 'rgba(255,255,255,0.2)' },
//         // closeRootStyle: { backgroundColor: 'rgba(255,255,255,0.2)' },
//     }

//     const renderActions = () => {
//         return props.categories.map((category) => {
//             return (
//                 <View key={category.id}>
//                     <ActionButton onPress={() => props.actionOnPress(category)} name={category.name} />
//                 </View>
//             )
//         })
//     }

//     return (
//         <SwipeablePanel {...panelProps} isActive={props.visitable}>
//             <View style={{ paddingBottom: 30 }}>
//                 {renderActions()}
//                 <ActionButton onPress={() => props.onCancel()} name="Cancel" color="red" />
//             </View>
//         </SwipeablePanel>
//     )
// }

export default DiscussionAddButtonActionModal
