import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Linking, Share, Dimensions, Modal } from 'react-native'
import Toast from 'react-native-tiny-toast'
import { API } from 'react-native-dotenv'
import { StackNavigationProp } from '@react-navigation/stack'
import {
    AxiosServiceExecutorPost,
    ServiceExecutorPrivateDelete,
    ServicePrivateExecutorPost,
} from '../../../../services/ServiceExecutor'
import Icon from '../../../../common/Icon'
import ActionButton from '../../../../common/ActionButton'
import { connect } from 'react-redux'
import DeleteConfirmModal from '../../../../common/DeleteConfirmModal'
import ReportModal from '../../../../common/ReportModal'
import ActionModal from '../../../../components/ActionModal/ActionModal'
import { deleteDiscussion, getDiscussions } from '../../services/DiscussionService'

var screen = Dimensions.get('window')
interface Props {
    discussion: any
    navigation?: StackNavigationProp<any>
    auth?: any
    isAuthenticated?: boolean
    deleteDiscussion?: any
    token?: string
    getDiscussions?: any
}
interface State {
    actionModalVisitble: boolean
    reportModalVisitble: boolean
    reportValue: string
    deleteModalVisitable: boolean
}
export class DiscussionActionModal extends Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {
            actionModalVisitble: false,
            reportModalVisitble: false,
            reportValue: '',
            deleteModalVisitable: false,
        }
    }

    onPressActionModel = () => {
        this.setState({ actionModalVisitble: true })
    }
    onCancelActionModal = () => {
        this.setState({ actionModalVisitble: !this.state.actionModalVisitble })
    }

    onPressReport = async () => {
        if (this.props.isAuthenticated) {
            this.setState({ reportModalVisitble: true, actionModalVisitble: false })
        } else {
            this.setState({ reportModalVisitble: false, actionModalVisitble: false })
            this.props.navigation.navigate('LoginScreen')
        }
    }

    onPressReportExecution = (item) => {
        const bodyParams = {
            type: item,
        }
        AxiosServiceExecutorPost(
            `/reports/discussion/${this.props?.discussion?.id}`,
            bodyParams,
            this.responseReportCallback,
            true
        )
    }

    responseReportCallback = (response) => {
        if (response.status === 201) {
            this.setState({ reportModalVisitble: false })
            Toast.showSuccess('report success')
        } else {
            console.warn('something went wrong')
        }
    }

    onPressShare = async () => {
        try {
            const result = await Share.share({
                message: `${this.props.discussion.content}`,
            })
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            console.warn(error.message)
        }
    }

    onPressDeleteDiscuss = () => {
        const { token, discussion } = this.props
        this.props.deleteDiscussion(discussion.id, token)
        this.props.getDiscussions('all', 1)
        this.props.navigation.goBack()
    }

    _onPressCloseDeleteModal = () => {
        this.setState({ deleteModalVisitable: !this.state.deleteModalVisitable })
    }

    renderAuthButtons = () => {
        if (this.props.isAuthenticated) {
            if (this.props.discussion.creator.id === this.props.auth.id) {
                return (
                    <ActionButton
                        onPress={() => {
                            this.setState({ deleteModalVisitable: true, actionModalVisitble: false })
                        }}
                        name="Delete"
                        color="red"
                    />
                )
            }
        }
    }

    renderActionSheet = () => {
        return (
            <View>
                <ActionButton
                    onPress={() => {
                        this.onPressShare()
                    }}
                    name="Share"
                />
                <ActionButton onPress={() => this.onPressReport()} name="Report" />
                {this.renderAuthButtons()}
            </View>
        )
    }

    onCancelReportModal = () => {
        this.setState((prevState) => ({ reportModalVisitble: !prevState.reportModalVisitble }))
    }

    render() {
        return (
            <>
                <TouchableOpacity onPress={() => this.onPressActionModel()}>
                    <Icon name="more_vertical_dark" width="24" />
                </TouchableOpacity>

                <ActionModal modalVisitble={this.state.actionModalVisitble} onCancel={this.onCancelActionModal}>
                    {this.renderActionSheet()}
                </ActionModal>

                <ReportModal
                    visible={this.state.reportModalVisitble}
                    onCancel={this.onCancelReportModal}
                    onPress={this.onPressReportExecution}
                />
                <DeleteConfirmModal
                    visibility={this.state.deleteModalVisitable}
                    onCancel={this._onPressCloseDeleteModal}
                    onPress={this.onPressDeleteDiscuss}
                />
            </>
        )
    }
}

const mapStateToProps = function (state) {
    return {
        isAuthenticated: state.auth.isAuthenticated,
        auth: state.auth.user,
        token: state.auth.token,
    }
}

export default connect(mapStateToProps, { deleteDiscussion, getDiscussions })(DiscussionActionModal)
