import * as React from 'react'
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native'
import Toast from '../../../../lib/Toast/Toast'
import ActionButton from '../../../../common/ActionButton'
import DeleteConfirmModal from '../../../../common/DeleteConfirmModal'
import Icon from '../../../../common/Icon'
import ReportModal from '../../../../common/ReportModal'
import { AxiosServiceExecutorPost, ServiceExecutorPrivateDelete } from '../../../../services/ServiceExecutor'
import { connect } from 'react-redux'
import ActionModal from '../../../../components/ActionModal/ActionModal'
import { toast } from '../../lang/en'

export interface Props {
    comment: any
    reRender?: any
    auth?: any
    isAuthenticated?: boolean
    navigation?: any
}
export interface State {
    actionModalVisible: boolean
    reportModalVisible: boolean
    deleteModalVisible: boolean
}

export class DiscussionCommentActionModal extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {
            actionModalVisible: false,
            reportModalVisible: false,
            deleteModalVisible: false,
        }
    }

    _onPressActionSheetModalClose = () => {
        this.setState({ actionModalVisible: !this.state.actionModalVisible })
    }
    _onPressReportModalClose = () => {
        this.setState({ reportModalVisible: !this.state.reportModalVisible })
    }
    _onPressReportSubmit = (item) => {
        const bodyParams = {
            type: item,
        }
        AxiosServiceExecutorPost(
            `/reports/comment/${this.props.comment.id}`,
            bodyParams,
            this.responseReportCallback,
            true
        )
    }
    responseReportCallback = (response) => {
        if (response.status === 201) {
            this.setState({ reportModalVisible: false })
            Toast.show(toast.COMMENT_REPORT_SUCCESS)
        } else {
            console.warn('something went wrong')
        }
    }
    _onPressDeleteModalClose = () => {
        this.setState({ deleteModalVisible: !this.state.deleteModalVisible })
    }
    _onPressDeleteSubmit = () => {
        const url = `/discussions/${this.props.comment.discussId}/comment/${this.props.comment.id}`
        ServiceExecutorPrivateDelete(url, this.responseCommentDeleteCallback)
    }

    responseCommentDeleteCallback = (response) => {
        if (response.status === 204) {
            Toast.show(toast.COMMENT_DELETE_SUCCESS)
            this.props.reRender()
        }
    }

    renderAuthButtons = () => {
        if (this.props.isAuthenticated) {
            if (this.props.comment.user.id === this.props.auth.id) {
                return (
                    <ActionButton
                        onPress={() => {
                            this.setState({ deleteModalVisible: true, actionModalVisible: false })
                        }}
                        name="Delete"
                        color="red"
                    />
                )
            }
        }
    }

    onPressReportModalOpen = () => {
        if (this.props.isAuthenticated) {
            this.setState({ reportModalVisible: true, actionModalVisible: false })
        } else {
            this.setState({ reportModalVisible: false, actionModalVisible: false })
            this.props.navigation.navigate('LoginScreen')
        }
    }

    actionSheetModal = () => {
        return (
            <ActionModal
                modalVisitble={this.state.actionModalVisible}
                onCancel={() => this._onPressActionSheetModalClose()}
            >
                <ActionButton onPress={() => this.onPressReportModalOpen()} name="Report" />
                {this.renderAuthButtons()}
            </ActionModal>
        )
    }

    public render() {
        return (
            <View>
                <TouchableOpacity onPress={() => this.setState({ actionModalVisible: true })}>
                    <Icon name="more_vertical_gray" width="17" />
                    {this.actionSheetModal()}
                </TouchableOpacity>
                <ReportModal
                    visible={this.state.reportModalVisible}
                    onPress={this._onPressReportSubmit}
                    onCancel={this._onPressReportModalClose}
                />
                <DeleteConfirmModal
                    visibility={this.state.deleteModalVisible}
                    onPress={this._onPressDeleteSubmit}
                    onCancel={this._onPressDeleteModalClose}
                />
            </View>
        )
    }
}

const mapStateToProps = function (state) {
    return {
        isAuthenticated: state.auth.isAuthenticated,
        auth: state.auth.user,
    }
}

export default connect(mapStateToProps)(DiscussionCommentActionModal)
