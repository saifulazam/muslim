import React from 'react'
import { Text, View, TouchableOpacity, StyleSheet, Animated } from 'react-native'
import Icon from '../../../common/Icon'
import moment from 'moment'
import LazyImage from '../../../common/LazyImage/LazyImage'

interface Props {
    navigation: any
    discussion: any
}

const DiscussionCard = (props: Props) => {
    const __renderCreator = (creator) => {
        if (creator) {
            return (
                <LazyImage
                    source={{ url: props.discussion.creator.avatar }}
                    style={{ width: 40, height: 40, borderRadius: 20 }}
                />
            )
        } else {
            return (
                <LazyImage
                    source={require('../../../../assets/images/appIconSmall.png')}
                    style={{ width: 40, height: 40, borderRadius: 20 }}
                />
            )
        }
    }

    return (
        <View style={styles.card}>
            <View style={{ padding: 15 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <View style={{ flexDirection: 'row' }}>
                        {__renderCreator(props.discussion.creator)}
                        <View style={{ paddingLeft: 10 }}>
                            <Text style={{ fontSize: 20 }}>
                                {props.discussion.creator ? props.discussion.creator.name : 'Alnur'}
                            </Text>
                            <Text style={{ color: 'gray' }}>{moment(props.discussion.added).fromNow()}</Text>
                        </View>
                    </View>
                    {/* <View
                        style={{
                            backgroundColor: '#f3f1f1',
                            padding: 10,
                            borderRadius: 20,
                            borderWidth: 0.2,
                            borderColor: '#d8d8d8',
                        }}
                    >
                        <Text style={{ color: 'gray' }}>{props.discussion.category.name}</Text>
                    </View> */}
                </View>
                <TouchableOpacity
                    onPress={() =>
                        props.navigation.navigate('DiscussionDetail', { discussion_id: props.discussion.id })
                    }
                >
                    {props.discussion.images[0]?.path ? (
                        <View style={{ paddingTop: 10 }}>
                            <LazyImage
                                source={{ uri: props.discussion.images[0]?.path }}
                                style={{ width: '100%', height: 187 }}
                            />
                        </View>
                    ) : null}
                    <Text numberOfLines={3} style={{ marginVertical: 10 }}>
                        {props.discussion.content}
                    </Text>
                </TouchableOpacity>

                <Text style={{ color: 'gray' }}>{props.discussion.comments.length} replies</Text>

                <View style={{ borderBottomColor: 'rgba(0,0,0, 0.1)', borderBottomWidth: 0.5, paddingVertical: 10 }} />

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingTop: 10 }}>
                    <TouchableOpacity
                        onPress={() =>
                            props.navigation.navigate('DiscussionDetail', { discussion_id: props.discussion.id })
                        }
                        style={{ flexDirection: 'row', alignItems: 'center' }}
                    >
                        <Icon name="reply" width="24" height="24" />
                        <Text style={{ paddingLeft: 10, fontWeight: 'bold', color: 'gray' }}>Reply</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Icon name="sm_share" width="24" height="24" />
                        <Text style={{ paddingLeft: 10, fontWeight: 'bold', color: 'gray' }}>Share</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

export default DiscussionCard

const styles = StyleSheet.create({
    card: {
        backgroundColor: '#fafafa',
        marginVertical: 10,
        shadowColor: '#000000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowRadius: 1,
        shadowOpacity: 0.1,
        borderColor: 'rgba(0,0,0, 0.1)',
        borderWidth: 0.3,
    },
})
