import { StackNavigationProp } from '@react-navigation/stack'
import React, { useRef } from 'react'
import { View, Text, TouchableOpacity, Button } from 'react-native'
import Icon from '../../../common/Icon'
import DiscussionActionModal from '../components/Actions/DiscussionActionModal'

interface Props {
    discussion: any
    navigation?: StackNavigationProp<any>
    auth?: any
}

const DiscussionDetailScreenHeader = (props: Props) => {
    return (
        <View
            style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                marginHorizontal: 20,
            }}
        >
            <TouchableOpacity onPress={() => props.navigation.goBack()}>
                <Icon name="back_dark" width="24" />
            </TouchableOpacity>

            <Text style={{ fontSize: 20, fontWeight: 'bold', fontFamily: 'AvantGardeLT-Demi' }}>
                {/* {props.discussion.category.name} */} Question
            </Text>
            <DiscussionActionModal discussion={props.discussion} navigation={props.navigation} />
        </View>
    )
}

export default DiscussionDetailScreenHeader
