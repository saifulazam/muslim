import React, { Component } from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import { StackNavigationProp } from '@react-navigation/stack'
import LazyImage from '../../../common/LazyImage/LazyImage'
import moment from 'moment'
import _COLORS from '../../../utils/Color'
import DiscussionCommentActionModal from './Actions/DiscussionCommentActionModal'
import ReadMore from '../../../common/ReadMore'

interface Props {
    navigation: StackNavigationProp<any>
    comments: any
    reRender?: any
    auth?: any
}
interface State {
    commentReadMore: number
}
export default class DiscussionComments extends Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {
            commentReadMore: null,
        }
    }
    _renderComments = () => {
        return this.props.comments.map((comment) => {
            return (
                <View
                    key={comment.id}
                    style={{
                        borderBottomWidth: 1,
                        borderBottomColor: '#ebebeb',
                        paddingVertical: 20,
                        marginHorizontal: 20,
                    }}
                >
                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            marginBottom: 10,
                        }}
                    >
                        <View style={{ flexDirection: 'row' }}>
                            <LazyImage
                                source={{ uri: comment.user.avatar }}
                                style={{ width: 40, height: 40, borderRadius: 20 }}
                            />
                            <View style={{ paddingLeft: 10 }}>
                                <Text style={{ fontSize: 14 }}>{comment.user.name}</Text>
                                <Text style={{ color: 'gray' }}>{moment(comment.added).fromNow()}</Text>
                            </View>
                        </View>
                        <DiscussionCommentActionModal
                            comment={comment}
                            auth={this.props.auth}
                            reRender={this.props.reRender}
                            navigation={this.props.navigation}
                        />
                    </View>
                    <ReadMore>{comment.message}</ReadMore>
                </View>
            )
        })
    }
    render() {
        return <View style={{ backgroundColor: '#fafafa' }}>{this._renderComments()}</View>
    }
}
