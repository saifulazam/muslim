import React, { Component, Dispatch } from 'react'
import { Text, View, SafeAreaView, StatusBar } from 'react-native'
import MoreScreenHeader from '../components/MoreScreenHeader'
import { NavigationProp } from '@react-navigation/native'
import { connect } from 'react-redux'
import { logout } from '../../Auth/services/AuthService'
import { ServiceExecutorPrivateDelete } from '../../../services/ServiceExecutor'
import UserMainCard from '../../User/components/UserMainCard'

interface Props {
    navigation?: NavigationProp<any>
    isAuthenticated?: boolean
    // dispatch?: Dispatch<any>
    logout?: any
    user?: any
}

interface State {}

export class MoreScreen extends Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {}
        this._onPressLogout = this._onPressLogout.bind(this)
    }
    componentDidMount() {
        StatusBar.setBarStyle('dark-content')
    }

    _onPressLogout = async () => {
        await this.props.logout()

        if (!this.props.isAuthenticated) {
            this.props.navigation.navigate('InitialAuth')
        }
    }
    render() {
        return (
            <View>
                <SafeAreaView style={{ backgroundColor: '#fff' }}>
                    <MoreScreenHeader
                        isAuthenticated={this.props.isAuthenticated}
                        navigation={this.props.navigation}
                        onPressLogout={this._onPressLogout}
                    />
                </SafeAreaView>
                {this.props.isAuthenticated ? (
                    <UserMainCard navigation={this.props.navigation} user={this.props.user} />
                ) : null}
            </View>
        )
    }
}

const mapStateToProps = function (state) {
    return {
        isAuthenticated: state.auth.isAuthenticated,
        user: state.auth.user,
    }
}

export default connect(mapStateToProps, { logout })(MoreScreen)
