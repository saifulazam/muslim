import React, { useState, useEffect } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import _COLORS from '../../../utils/Color'

interface Props {
    navigation?: any
    onPressLogout?: any
    isAuthenticated?: any
}

const MoreScreenHeader = (props: Props) => {
    if (props.isAuthenticated) {
        return (
            <View style={{ padding: 10, flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text></Text>
                <TouchableOpacity
                    onPress={() => props.onPressLogout()}
                    style={{ backgroundColor: _COLORS.primary, padding: 10, borderRadius: 20 }}
                >
                    <Text style={{ color: '#fff', fontWeight: '500' }}>Logout</Text>
                </TouchableOpacity>
            </View>
        )
    } else {
        return (
            <View style={{ padding: 10, flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text></Text>
                <TouchableOpacity
                    onPress={() => props.navigation.navigate('InitialAuth')}
                    style={{ backgroundColor: _COLORS.primary, padding: 10, borderRadius: 20 }}
                >
                    <Text style={{ color: '#fff', fontWeight: '500' }}>Login</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

export default MoreScreenHeader
