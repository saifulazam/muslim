import { PrayerActionType } from './PrayerActionType'

export function allPrayerTimes(payload) {
    return {
        type: PrayerActionType.ALL_PRAYER,
        payload,
    }
}

export function nextPrayerTimes(payload) {
    return {
        type: PrayerActionType.NEXT_PRAYER,
        payload,
    }
}
