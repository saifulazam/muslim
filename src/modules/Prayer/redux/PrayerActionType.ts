export enum PrayerActionType {
    ALL_PRAYER = '@ALL_PRAYER',
    NEXT_PRAYER = '@NEXT_PRAYER'
}

export type PrayerModel = {}

export type PrayerState = {
    prayers: any
}

export type PrayerAction = {
    type: string
    payload: any
}
