import moment from 'moment'
import { PrayerActionType, PrayerState, PrayerAction } from './PrayerActionType'

const INITIAL_STATE: PrayerState = {
    prayers: [],
}

const reducer = (state = INITIAL_STATE, actionType: PrayerAction) => {
    switch (actionType.type) {
        case PrayerActionType.ALL_PRAYER:
            return allPrayerTimes(state, actionType.payload)
            case PrayerActionType.NEXT_PRAYER:
                return nextPrayerTime(state, actionType.payload)
        default:
            return state
    }   
}

const nextPrayerTime = (state, times: any) => {
    const objToArr = Object.entries(times)
    let prayers = []
    let nextprayerTime = []
    var currentTime = moment()
    objToArr?.map((item: any, index) => {
        const serv = item && item[1].time
        prayers.push(item[1].time)
        if (moment(serv, 'hh:mm A') > moment(currentTime, 'hh:mm A')) {
            nextprayerTime.push(item)
            return nextprayerTime
        }
        return nextprayerTime
    })
    return {...state, nextPrayer: nextprayerTime}
}

const allPrayerTimes = (state, payload) => {
    return { ...state, prayers: Object.entries(payload) }
}

export default reducer
