import * as PrayerAction from '../redux/PrayerAction'
import Http from '../../../utils/Http'
import Transformer from '../../../utils/Transformer'

export function setAllPrayerTimes(latitude, longitude, timezone) {
    return (dispatch) => {
        new Promise((resolve, reject) => {
            Http.get(`prayer/time?latitude=${latitude}&longitude=${longitude}&timezone=${timezone}`)
                .then((response) => {
                    const data = Transformer.fetch(response.data)
                    dispatch(PrayerAction.allPrayerTimes(data))
                    dispatch(PrayerAction.nextPrayerTimes(data))
                    return resolve()
                })
                .catch((error) => {
                    console.warn(error)
                    return reject(error)
                })
        })
    }
}
