import React, { Component } from 'react'
import { Text, View, SafeAreaView, TouchableOpacity, ActivityIndicator, Image, StatusBar } from 'react-native'
import Icon from '../../../common/Icon'
import { StackNavigationProp } from '@react-navigation/stack'
import Compass from '../components/compass'
import { connect } from 'react-redux'
import { CustomStatusBar } from '../../../utils/StatusBar'

interface Props {
    logoutCurrent: (params?: any) => void
    navigation: StackNavigationProp<any>
    coords?: any
    address?: any
}

interface State {
    compassImage: any
}

class QiblaDirectionScreen extends Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {
            compassImage: require('../../../../assets/images/compass-2.png'),
        }
    }

    componentDidMount() {
        CustomStatusBar(this.props.navigation, 'light-content')
    }

    render() {
        const { coords, address, navigation } = this.props
        const addressConvertString = `${address.city},${address.region}`
        if (!coords) {
            return (
                <View style={{ backgroundColor: 'black', flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator />
                </View>
            )
        }
        return (
            <SafeAreaView
                style={{
                    backgroundColor: 'black',
                    flex: 1,
                }}
            >
                <View style={{ marginTop: 30 }}>
                    <Text
                        style={{ textAlign: 'left', color: 'white', fontSize: 26, fontWeight: 'bold', marginStart: 20 }}
                    >
                        {address ? `${address.street}` : ''}
                    </Text>
                    <Text style={{ color: 'white', marginStart: 20 }}>{addressConvertString}</Text>
                </View>
                <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 40 }}>
                    <Icon name="qibla_active" width="70" height="80" />
                </View>
                <View style={{ alignItems: 'center', justifyContent: 'center', right: 200, top: -220, flex: 1 }}>
                    <Compass image={this.state.compassImage} />
                </View>
                <View
                    style={{
                        height: 50,
                        width: 50,
                        flexDirection: 'row',
                        left: 30,
                        bottom: 20,
                        justifyContent: 'space-between',
                    }}
                >
                    <TouchableOpacity
                        onPress={() => this.switchImage(require('../../../../assets/images/compass-2.png'))}
                    >
                        <Image
                            source={require('../../../../assets/images/compass-2.png')}
                            style={{ width: 50, height: 50, marginHorizontal: 20 }}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.switchImage(require('../../../../assets/images/compass.png'))}
                    >
                        <Image
                            source={require('../../../../assets/images/compass.png')}
                            style={{ width: 50, height: 50, marginHorizontal: 20 }}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{}}
                        onPress={() => this.switchImage(require('../../../../assets/images/compass-3.png'))}
                    >
                        <Image
                            source={require('../../../../assets/images/compass-3.png')}
                            style={{ width: 50, height: 50, marginHorizontal: 20 }}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity style={{ marginTop: 3, left: 10 }} onPress={() => navigation.goBack()}>
                        <Icon name="remove_dark" width="47" height="49" />
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        )
    }

    public switchImage = (compassImage) => {
        this.setState({ compassImage })
    }
}

const mapStateToProps = (state) => {
    return {
        coords: state.location.coords,
        address: state.location.address,
    }
}

export default connect(mapStateToProps)(QiblaDirectionScreen)
