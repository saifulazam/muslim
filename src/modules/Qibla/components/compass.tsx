import React from 'react'
import { View, StyleSheet, Animated, Easing, Dimensions } from 'react-native'
import * as Permissions from 'expo-permissions'
import * as Location from 'expo-location'

interface Props {
    image: any
}
interface State {
    location: any
    errorMessage: any
    heading: any
    truenoth: any
}

export default class Compass extends React.Component<Props, State> {
    spinValue: Animated.Value
    constructor(props) {
        super(props)
        this.spinValue = new Animated.Value(0)
        this.state = {
            location: null,
            errorMessage: null,
            heading: null,
            truenoth: null,
        }
    }

    public componentDidMount() {
        this._getLocationAsync()
    }

    public componentDidUpdate() {
        this.spin()
    }

    public _getLocationAsync = async () => {
        // Checking device location permissions
        let { status } = await Permissions.askAsync(Permissions.LOCATION)
        if (status !== 'granted') {
            this.setState({
                errorMessage: 'Permission to access location was denied',
            })
        } else {
            Location.watchHeadingAsync((obj) => {
                let heading = obj.magHeading
                this.setState({ heading })
            })
        }
    }

    public spin() {
        let start = JSON.stringify(this.spinValue)
        let heading = Math.round(this.state.heading)

        let rot = +start
        let rotM = rot % 360

        if (rotM < 180 && heading > rotM + 180) rot -= 360
        if (rotM >= 180 && heading <= rotM - 180) rot += 360

        rot += heading - rotM

        //@ts-ignore
        Animated.timing(this.spinValue, {
            toValue: rot,
            duration: 300,
            easing: Easing.inOut((ease) => ease),
        }).start()
    }

    componentWillUnmount() {
        this.spinValue.stopAnimation()
    }

    public render() {
        let LoadingText = 'Loading...'
        let display: string | number = LoadingText

        if (this.state.errorMessage) display = this.state.errorMessage

        const spin = this.spinValue.interpolate({
            inputRange: [0, 360],
            outputRange: ['-0deg', '-360deg'],
        })

        // @ts-ignore
        display = Math.round(JSON.stringify(this.spinValue))

        if (display < 0) display += 360
        if (display > 360) display -= 360

        return (
            <>
                <View style={styles.container}>
                    <View style={styles.imageContainer}>
                        <Animated.Image
                            resizeMode="contain"
                            source={this.props.image}
                            style={{
                                width: deviceWidth - 10,
                                height: deviceHeight / 2 - 10,
                                left: deviceWidth / 2 - (deviceWidth - 10) / 2,
                                top: deviceHeight / 2 - (deviceHeight / 2 - 10) / 2,
                                transform: [{ rotate: spin }],
                            }}
                        />
                    </View>
                </View>
            </>
        )
    }
}

// Device dimensions so we can properly center the images set to 'position: absolute'
const deviceWidth = Dimensions.get('window').width
const deviceHeight = Dimensions.get('window').height

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    // text: {
    //     color: '#263544',
    //     fontSize: 80,
    //     transform: [{ translateY: -(deviceHeight / 2 - (deviceHeight / 2 - 10) / 2) - 50 }],
    // },
    imageContainer: {
        ...StyleSheet.absoluteFillObject,
        marginTop: 10,
    },
    arrowContainer: {
        ...StyleSheet.absoluteFillObject,
    },
    // arrow: {
    //     width: deviceWidth / 7,
    //     height: deviceWidth / 7,
    //     left: deviceWidth / 2 - deviceWidth / 7 / 2,
    //     top: deviceHeight / 2 - deviceWidth / 7 / 2,
    //     opacity: 0.9,
    // },
})
