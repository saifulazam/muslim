import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import LazyImage from '../../../common/LazyImage/LazyImage'

const PodCastItemCard = (props) => {
    const { data, width, onPress, imageHeight, limitLine } = props
    return (
        <TouchableOpacity activeOpacity={1} onPress={onPress} style={{ marginHorizontal: 10, width: width ?? 300 }}>
            <LazyImage source={{ uri: data.photo }} style={{ width: '100%', height: imageHeight ?? 170 }} />
            <View style={{ flexDirection: 'row' }}>
                <Text
                    numberOfLines={limitLine ?? 0}
                    style={{ color: 'black', fontSize: 15, fontWeight: 'bold', paddingTop: 10, paddingBottom: 5 }}
                >
                    {data.title}
                </Text>
            </View>
        </TouchableOpacity>
    )
}

export default PodCastItemCard
