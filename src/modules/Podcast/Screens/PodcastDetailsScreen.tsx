import React, { useRef, useState, useEffect } from 'react'
import YoutubePlayer from 'react-native-youtube-iframe'
import { View, Text, StatusBar } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'

const PodcastDetailsScreen = (props) => {
    const playerRef = useRef(null)
    const [playing, setPlaying] = useState(true)

    const { data } = props.route.params

    useEffect(() => {
        StatusBar.setBarStyle('dark-content')
    }, [])

    return (
        <>
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ marginHorizontal: 10, paddingBottom: 20, paddingTop: 10, flexDirection: 'row' }}>
                    <Text style={{ fontSize: 16, fontWeight: 'bold' }}>{data.category}:</Text>
                    <Text style={{ alignSelf: 'center', marginLeft: 5 }}>{data.title}</Text>
                </View>
                <YoutubePlayer
                    ref={playerRef}
                    height={300}
                    width={'100%'}
                    videoId={data.video}
                    play={playing}
                    volume={50}
                    playbackRate={1}
                    initialPlayerParams={{
                        cc_lang_pref: 'us',
                        showClosedCaptions: true,
                    }}
                />
            </SafeAreaView>
        </>
    )
}

export default PodcastDetailsScreen
