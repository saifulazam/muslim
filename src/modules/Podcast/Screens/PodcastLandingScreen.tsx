import React, { Component } from 'react'
import { Text, View, SafeAreaView, StatusBar, ScrollView, Image, TouchableOpacity } from 'react-native'
import { StackNavigationProp } from '@react-navigation/stack'
import { ServiceExecutorGet } from '../../../services/ServiceExecutor'
import { API } from 'react-native-dotenv'
import Spinner from '../../../common/Spinner'
import PodCastItemCard from '../components/PodCastItemCard'

interface Props {
    navigation: StackNavigationProp<any>
}

interface State {
    categories: any
    isLoading: boolean
}

export default class PodcastLandingScreen extends Component<Props, State> {
    constructor(props) {
        super(props)
        this.state = {
            categories: [],
            isLoading: true,
        }
    }
    public componentDidMount() {
        StatusBar.setBarStyle('dark-content')
        ServiceExecutorGet(`${API}/podcasts/categories`, this.responseCallback)
    }

    public responseCallback = (response) => {
        if (response.success) {
            this.setState({ categories: response.data, isLoading: false })
        } else {
            console.warn(response)
        }
    }

    public renderItemCard = (items) => {
        return items.map((item) => {
            if (item.category == 'Live') {
                return (
                    <PodCastItemCard
                        key={item.id}
                        data={item}
                        onPress={() => {
                            this.props.navigation.navigate('PodcastDetails', { data: item })
                        }}
                        width={157}
                        imageHeight={240}
                    />
                )
            } else if (item.category == 'Learn') {
                return (
                    <PodCastItemCard
                        key={item.id}
                        data={item}
                        onPress={() => {
                            this.props.navigation.navigate('PodcastDetails', { data: item })
                        }}
                        width={158}
                        imageHeight={158}
                        limitLine={2}
                    />
                )
            } else {
                return (
                    <PodCastItemCard
                        key={item.id}
                        data={item}
                        onPress={() => {
                            this.props.navigation.navigate('PodcastDetails', { data: item })
                        }}
                    />
                )
            }
        })
    }

    public renderCategory = () => {
        return this.state.categories.map((category) => {
            if (category.podcasts.length > 0) {
                return (
                    <View key={category.id} style={{ marginTop: 20 }}>
                        <Text
                            style={{
                                marginHorizontal: 10,
                                fontSize: 24,
                                fontWeight: 'bold',
                                fontFamily: 'AvantGardeLT-Demi',
                                paddingBottom: 10,
                            }}
                        >
                            {category.name}
                        </Text>
                        <ScrollView
                            horizontal={true}
                            decelerationRate={1}
                            snapToInterval={200}
                            snapToAlignment={'center'}
                            showsHorizontalScrollIndicator={false}
                            scrollEventThrottle={16}
                        >
                            {this.renderItemCard(category.podcasts)}
                        </ScrollView>
                    </View>
                )
            }
        })
    }

    render() {
        if (this.state.isLoading) {
            return <Spinner />
        }
        return (
            <SafeAreaView style={{ backgroundColor: 'white', flex: 1 }}>
                <View>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <this.renderCategory />
                    </ScrollView>
                </View>
            </SafeAreaView>
        )
    }
}
